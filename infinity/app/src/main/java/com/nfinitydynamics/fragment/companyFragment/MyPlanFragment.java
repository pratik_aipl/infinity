package com.nfinitydynamics.fragment.companyFragment;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import com.nfinitydynamics.R;
import com.nfinitydynamics.adapter.MatchJobAdapter;
import com.nfinitydynamics.fragment.JobDetailFragment;
import com.nfinitydynamics.model.MatchJobModel;
import com.nfinitydynamics.model.PlanDetailModel;
import com.nfinitydynamics.model.PlanModel;
import com.nfinitydynamics.utils.App;
import com.nfinitydynamics.utils.AsynchTaskListner;
import com.nfinitydynamics.utils.CallRequest;
import com.nfinitydynamics.utils.Constant;
import com.nfinitydynamics.utils.JsonParserUniversal;
import com.nfinitydynamics.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.nfinitydynamics.activity.CompanyActivity.CompanyDashboard.toolbar;
import static com.nfinitydynamics.activity.MainActivity.changeFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyPlanFragment extends Fragment implements AsynchTaskListner {


    public MyPlanFragment() {
        // Required empty public constructor
    }

    public JsonParserUniversal jParser;
    public View view;
    public TextView txtExpiry, txtPlaneName, txtDownlodeResume, txtRemaiingDownlode, txtSubscribeDate;
    public MyPlanFragment instance;
    public SharedPreferences shared;
    public String candidateId;
    public ArrayList<PlanDetailModel>list=new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_my_plan, container, false);
        instance = this;
        toolbar.setTitle("My Plan");
        jParser = new JsonParserUniversal();
        shared = getActivity().getSharedPreferences(getContext().getPackageName(), 0);
        candidateId = shared.getString("userId", "");
        setHasOptionsMenu(true);
        txtPlaneName = view.findViewById(R.id.txt_plan_name);
        txtDownlodeResume = view.findViewById(R.id.txt_down_resume);
        txtRemaiingDownlode = view.findViewById(R.id.txt_remain_down);
        txtSubscribeDate = view.findViewById(R.id.txt_subscribe_date);
        txtExpiry = view.findViewById(R.id.txt_expiry);
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        new CallRequest(instance).getPlanDetail(candidateId);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case getConpanyPlanDetail:

                    Utils.hideProgressDialog();

                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {

                            JSONObject dataObj = jObj.getJSONObject("data");
                            //   JSONArray ob1 = jObj.getJSONArray("data");


                            String planeName=dataObj.getString("PlanName");
                            String subscribeDate=dataObj.getString("subscribedate");
                            String remainingDate=dataObj.getString("RemainDownload");
                            String downlodeResume=dataObj.getString("TotalDownload");
                            String expiry=dataObj.getString("expirydate");

                           txtPlaneName.setText(": "+planeName);
                            txtSubscribeDate.setText(": "+subscribeDate);
                            txtRemaiingDownlode.setText(": "+remainingDate);
                            txtDownlodeResume.setText(": "+downlodeResume);
                            txtExpiry.setText(": "+expiry);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d("new exception:", String.valueOf(e));
                    }
                    break;
            }
        }
    }
}
