package com.nfinitydynamics.fragment.companyFragment;


import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.hbb20.CountryCodePicker;
import com.nfinitydynamics.R;
import com.nfinitydynamics.activity.CompanyActivity.CompanyDashboard;
import com.nfinitydynamics.model.company.CompanyDataModel;
import com.nfinitydynamics.utils.App;
import com.nfinitydynamics.utils.AsynchTaskListner;
import com.nfinitydynamics.utils.CallRequest;
import com.nfinitydynamics.utils.Constant;
import com.nfinitydynamics.utils.JsonParserUniversal;
import com.nfinitydynamics.utils.Utils;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import static android.app.Activity.RESULT_OK;
import static com.nfinitydynamics.activity.CompanyActivity.CompanyDashboard.activityCompany;
import static com.nfinitydynamics.activity.CompanyActivity.CompanyDashboard.toolbar;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyAccountFragment extends Fragment implements AsynchTaskListner {
    public View view;
    public ImageView ivProfileComp;
    public EditText edtCompanyName, edtEmail, edtWebSite, edtTelNo, edtMoNo, edtAddress, edtTelNoCode,
            edtMoNoCode,edtRpslno,edtValidUntil,edt_city,edt_zipcode,edt_country;
    public String companyNmae, email, website, telNo, moNo, address, companyId, profileImage,
            selectedType, compTeleNoCode, compContryCode, compMContryCode,rsplNo,validUntil;
    public SharedPreferences shared;
    public MyAccountFragment instance;
    public JsonParserUniversal jParser;
    public Button btnUpdate;
    public ArrayList companyList = new ArrayList();
    public Uri selectedUri;
    public static String profImagePath = "";
    public CropImage.ActivityResult result;
    public CountryCodePicker ccpContryCode, ccpmContryCode;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_my_account, container, false);
        instance = this;
        toolbar.setTitle("My Account");
        jParser = new JsonParserUniversal();
        shared = getActivity().getSharedPreferences(getContext().getPackageName(), 0);
        companyId = shared.getString("userId", "");
        ivProfileComp = view.findViewById(R.id.iv_company);
        edtCompanyName = view.findViewById(R.id.edt_company_name);
        edtEmail = view.findViewById(R.id.edt_email);
        edtWebSite = view.findViewById(R.id.edt_website);
        edtTelNo = view.findViewById(R.id.edt_tel_no);
        edtTelNoCode = view.findViewById(R.id.edt_tel_no_code);
        edt_city = view.findViewById(R.id.edt_city);
        edt_zipcode = view.findViewById(R.id.edt_zipcode);
        edt_country = view.findViewById(R.id.edt_country);
        edtMoNo = view.findViewById(R.id.edt_mobile_no);
      //  edtMoNoCode = view.findViewById(R.id.edt_mobile_no_code);
        edtAddress = view.findViewById(R.id.edt_address);
        btnUpdate = view.findViewById(R.id.btnUpdate);
        ccpContryCode = view.findViewById(R.id.ccpContryCode);
        ccpmContryCode = view.findViewById(R.id.ccpMContryCode);
      //  edtTelNoCode = view.findViewById(R.id.edt_tel_no_code);
        edtRpslno=view.findViewById(R.id.edt_rpslno);
        edtValidUntil=view.findViewById(R.id.edt_valid_until);
        TextView txtedit = view.findViewById(R.id.txt_edit_pic);
        getCompanyDetail();

        ccpContryCode.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected() {
                compContryCode = ccpContryCode.getSelectedCountryCode();
            }
        });
        ccpmContryCode.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected() {
                compMContryCode = ccpmContryCode.getSelectedCountryCode();
                //edtMoNoCode.setText(compMContryCode);
            }
        });
//        ivProfileComp.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (CropImage.isExplicitCameraPermissionRequired(getContext())) {
//                    requestPermissions(
//                            new String[]{Manifest.permission.CAMERA},
//                            CropImage.CAMERA_CAPTURE_PERMISSIONS_REQUEST_CODE);
//                } else {
//                    CropImage.startPickImageActivity(getActivity());
//                }
//            }
//        });
        edtValidUntil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager)getActivity(). getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                Utils.generateDatePicker(getContext(), edtValidUntil);
                //  App.availableFromTemp = edtValidUntil.getText().toString();

            }
        });
        txtedit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (CropImage.isExplicitCameraPermissionRequired(getContext())) {
                    requestPermissions(
                            new String[]{Manifest.permission.CAMERA},
                            CropImage.CAMERA_CAPTURE_PERMISSIONS_REQUEST_CODE);
                } else {
                    CropImage.startPickImageActivity(getActivity());
                }
            }
        });
        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doUpdate();
            }
        });
        return view;
    }

    private void doUpdate() {
        Calendar cal = Calendar.getInstance();
        Date sysDate = cal.getTime();
        Date intValidity = null;
        companyNmae = edtCompanyName.getText().toString().trim();
        website = edtWebSite.getText().toString().trim();
        email = edtEmail.getText().toString().trim();
        telNo = edtTelNo.getText().toString().trim();
        moNo = edtMoNo.getText().toString().trim();
        address = edtAddress.getText().toString().trim();
        compTeleNoCode = edtTelNoCode.getText().toString();
        rsplNo=edtRpslno.getText().toString();
        validUntil=edtValidUntil.getText().toString();

        if (TextUtils.isEmpty(companyNmae)) {
            edtCompanyName.findFocus();
            Utils.showAlert("Please Enter Company Name", getActivity());
        } else if (TextUtils.isEmpty(rsplNo)) {
            edtRpslno.findFocus();
            Utils.showAlert("Please Enter RSPL No", getActivity());
        } else if (TextUtils.isEmpty(validUntil)) {
            edtValidUntil.findFocus();
            Utils.showAlert("Please Enter Utility Date", getActivity());
        } else if (!TextUtils.isEmpty(validUntil)) {
            try {
                intValidity = new SimpleDateFormat("dd-MMM-yyyy").parse(validUntil);
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                validUntil = formatter.format(intValidity);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            if (sysDate.compareTo(intValidity) > 0) {
                Utils.showAlert("Enter Valid Date in Valid Utility", getActivity());
            } else if (TextUtils.isEmpty(email)) {
                edtEmail.findFocus();
                Utils.showAlert("Please Enter Email", getActivity());
            } else if (Utils.emailValidation(email).equals("false")) {
                Utils.showAlert("Please enter valid email", getActivity());
            } else if (TextUtils.isEmpty(moNo)) {
                edtMoNo.findFocus();
                Utils.showAlert("Please Enter Mobile No", getActivity());
            } else if (TextUtils.isEmpty(website)) {
                edtWebSite.findFocus();
                Utils.showAlert("Please Enter Website", getActivity());
            } else if (TextUtils.isEmpty(address)) {
                edtAddress.findFocus();
                Utils.showAlert("Please Enter Address", getActivity());
            } else if (TextUtils.isEmpty(compTeleNoCode)) {
                edtTelNoCode.findFocus();
                Utils.showAlert("Please Enter STD Code", getActivity());
            } else {
                new CallRequest(instance).updateCompanyDetail(companyId, companyNmae,rsplNo,validUntil, email, moNo, telNo, website, profImagePath, address, compMContryCode, compContryCode, compTeleNoCode);
            }
        }
    }

    private void getCompanyDetail() {

        new CallRequest(instance).getComapnyDetail(companyId);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case getCompanyDetail:
                    Utils.hideProgressDialog();
                    companyList.clear();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            JSONObject ob1 = jObj.getJSONObject("data");
                            CompanyDataModel model = new CompanyDataModel();
                            model = (CompanyDataModel) jParser.parseJson(ob1, new CompanyDataModel());
                            companyList.add(model);
                            companyNmae = model.getCompanyName();
                            email = model.getEmailID();
                            website = model.getWebsite();
                            profileImage = model.getLogoURL();
                            telNo = model.getTelephone();
                            moNo = model.getMobile();
                            address = model.getAddress();
                            edtAddress.setText(address);
                            edtMoNo.setText(""+model.getCCode()+" "+moNo);
                            edtTelNo.setText(""+model.getTelCountryCode()+" "+model.getSTDCode()+" "+model.getTelephone()+" ");
                            edtWebSite.setText(website);
                            edtEmail.setText(email);
                            edtCompanyName.setText(companyNmae);
                            edtTelNoCode.setText(model.getSTDCode());
                            edtRpslno.setText(model.getRPSLNo());
                            edt_city.setText(model.getCityName());
                            edt_country.setText(model.getCountryName());
                            edt_zipcode.setText(model.getZipcode());
                            edtValidUntil.setText(model.getValidUntil());
                          //  Utils.converDateToDDMMYY(edtValidUntil,model.getValidUntil());
                            if (profileImage != "") {
                                Picasso.with(getContext()).load(profileImage).placeholder(R.drawable.profilepic)
                                        .error(R.drawable.profilepic).into(ivProfileComp);
                               // profImagePath=profileImage;

                            }

                            compContryCode = model.getTelCountryCode();
                            compMContryCode = model.getCCode();

                            try {
                                ccpContryCode.setCountryForPhoneCode(Integer.parseInt(compContryCode));
                                ccpmContryCode.setCountryForPhoneCode(Integer.parseInt(compMContryCode));

                            }catch (NumberFormatException e){
                                e.printStackTrace();
                            }
                        } else {

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                case updateCompany:

                    Utils.hideProgressDialog();

                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            JSONObject ob1 = jObj.getJSONObject("data");
                            CompanyDataModel model = new CompanyDataModel();
                            model = (CompanyDataModel) jParser.parseJson(ob1, new CompanyDataModel());
                            profileImage=model.getLogoURL();
                            companyNmae = edtCompanyName.getText().toString().trim();
                            String address = edtAddress.getText().toString().trim();
                            SharedPreferences.Editor et = shared.edit();
                            et.putString("logoImage", profileImage);
                            et.putString("username", companyNmae);
                            et.putString("address", address);
                            et.commit();
                            App.profilePic=profileImage;
                            App.companyName=companyNmae;
                            App.Address=address;

                            Utils.showToast("Profile Update Successfully", getContext());
                            activityCompany.onBackPressed();

                        } else {
                            Utils.showToast("Try Again!!", getContext());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();

                        Log.d("new exception:", String.valueOf(e));
                    }
                    break;
            }
        }
    }

    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setMinCropResultSize(500,500)
                .setAspectRatio(5, 2)
                .setFixAspectRatio(true)
                .setMultiTouchEnabled(false)
                .start(getActivity());
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(getContext(), data);

            if (CropImage.isReadExternalStoragePermissionsRequired(
                    getContext(), imageUri)) {
                selectedUri = imageUri;
                profImagePath = selectedUri.getPath().toString();
                Log.d("selected image1----", profImagePath);
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
            } else {
                startCropImageActivity(imageUri);
            }
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                ivProfileComp.setImageURI(result.getUri());
                selectedUri = result.getUri();
                profImagePath = selectedUri.getPath().toString();
                Log.d("selected image----", profImagePath);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
            }
        }

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        //Checking the request code of our request
        if (requestCode == 222) {

            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (selectedType.equalsIgnoreCase("Take Photo")) {
                    Log.i("TAG", " REQUET Take Photo");
                    File f = new File(Environment.getExternalStorageDirectory() + "/InfinityDynamics/images");
                    if (!f.exists()) {
                        f.mkdirs();
                    }
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    File file = new File(Environment.getExternalStorageDirectory(), "InfinityDynamics/images/img_" + System.currentTimeMillis() + ".jpg");
                    selectedUri = Uri.fromFile(file);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, selectedUri);
                    startActivityForResult(intent, 222);
                }

            } else {
                Utils.showToast("Permission not granted",
                        getContext());
            }
        }
    }
}