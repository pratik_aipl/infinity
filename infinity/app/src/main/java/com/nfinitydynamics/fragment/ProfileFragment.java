package com.nfinitydynamics.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nfinitydynamics.R;
import com.nfinitydynamics.activity.MainActivity;
import com.nfinitydynamics.fragment.companyFragment.CandidateFragment;
import com.nfinitydynamics.model.UserDetailModel;
import com.nfinitydynamics.utils.AsynchTaskListner;
import com.nfinitydynamics.utils.CallRequest;
import com.nfinitydynamics.utils.Constant;
import com.nfinitydynamics.utils.FileDownloader;
import com.nfinitydynamics.utils.JsonParserUniversal;
import com.nfinitydynamics.utils.Utils;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.net.wifi.WifiConfiguration.Status.strings;
import static com.nfinitydynamics.activity.MainActivity.changeFragment;
import static com.nfinitydynamics.activity.MainActivity.toolbar;
import static com.nfinitydynamics.utils.Utils.hasPermissions;

public class ProfileFragment extends Fragment implements AsynchTaskListner {

    public View view;
    public RelativeLayout relativeEditProfile, relativeEditExperience, relativeEducation, relativeSeaman, relativePassport,
            relativeCCC, relativeDCE, relativeJobApplied, relativeMatchJob, relativeBuildResume, relativeVisaDetails,
            relativeSTCW;
    public SharedPreferences shared;
    public ProfileFragment instance;
    public CircleImageView ivProfile;
    public TextView txtFullname, txtMobile, txtEmail, txtIndos, txtRank;
    public String candidateId;
    public JsonParserUniversal jParser;
    public static final int progress_bar_type = 0;
    public ProgressDialog pDialog;
    public String resumeUrl,firstName,lastName;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_profile, container, false);
        toolbar.setTitle("My Profile");
        instance = this;
        jParser = new JsonParserUniversal();
        shared = getActivity().getSharedPreferences(getContext().getPackageName(), 0);
        candidateId = shared.getString("userId", "");
        relativeEditProfile = view.findViewById(R.id.relative_edit_profile);
        relativeEditExperience = view.findViewById(R.id.relative_edit_eprofile);
        relativeEducation = view.findViewById(R.id.relative_education);
        relativeSeaman = view.findViewById(R.id.relative_seaman);
        relativePassport = view.findViewById(R.id.relative_passport_details);
        relativeCCC = view.findViewById(R.id.relative_ccc_details);
        relativeDCE = view.findViewById(R.id.relative_dce_details);
        relativeJobApplied = view.findViewById(R.id.relative_jobs_applied);
        relativeMatchJob = view.findViewById(R.id.relative_match_jobs);
        relativeBuildResume = view.findViewById(R.id.relative_build_resume);
        relativeVisaDetails = view.findViewById(R.id.relative_visa_details);
        relativeSTCW = view.findViewById(R.id.relative_stcw_details);
        relativeBuildResume = view.findViewById(R.id.relative_build_resume);
        ivProfile = view.findViewById(R.id.iv_candidate);
        txtFullname = view.findViewById(R.id.txt_full_name);
        txtEmail = view.findViewById(R.id.txt_email);
        txtIndos = view.findViewById(R.id.txt_indisno);
        txtMobile = view.findViewById(R.id.txt_mobileNo);
        txtRank = view.findViewById(R.id.txt_rank);
         firstName = shared.getString("firstName", "");
         lastName = shared.getString("lastName", "");
        String middleName = shared.getString("middleName", "");
        String mobileNo = shared.getString("mobileNo", "");
        String rank = shared.getString("rank", "");
        String indusno = shared.getString("indusno", "");
        String email = shared.getString("email", "");
        String image = shared.getString("image", "");
        txtFullname.setText(firstName + " " + middleName + " " + lastName);
        txtEmail.setText("Email: " + email);
        txtMobile.setText("Mobile: " + mobileNo);
        txtRank.setText("Current Rank: " + rank);
        txtIndos.setText("INDOS# " + indusno);
        Picasso.with(getContext()).load(image).placeholder(R.drawable.profilepic)
                .error(R.drawable.profilepic).into(ivProfile);
        relativeEditProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeFragment(new EditProfileFragment(), true);
            }
        });
        relativeEditExperience.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeFragment(new ExperienceFragment(), true);
            }
        });
        relativeSeaman.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeFragment(new SeamanBookFragment(), true);
            }
        });
        relativeMatchJob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeFragment(new MatchJobDrawerFragment(), true);
            }
        });
        relativeJobApplied.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeFragment(new HistoryFragment(), true);
            }
        });
        relativePassport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeFragment(new PassportFragment(), true);
            }
        });
        relativeCCC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeFragment(new COCFragment(), true);
            }
        });
        relativeDCE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeFragment(new DCEFragment(), true);
            }
        });
        relativeEducation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeFragment(new EducationDetailFragment(), true);
            }
        });
        relativeVisaDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeFragment(new VisaDetailFragment(), true);
            }
        });
        relativeSTCW.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeFragment(new STCWDetailFragment(), true);
            }
        });
        relativeBuildResume.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeFragment(new BulidResumeFragment(), true);
              //  downlodResume();
            }
        });
        return view;
    }

    private void downlodResume() {

        new CallRequest(instance).bulidResume(candidateId);
    }


    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case bulidResume:

                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            UserDetailModel userDetailModel;
                            userDetailModel = new UserDetailModel();
                            userDetailModel = (UserDetailModel) jParser.parseJson(jObj, new UserDetailModel());
                            resumeUrl = userDetailModel.getResume_url();
                            if (Build.VERSION.SDK_INT >= 23) {
                                String[] PERMISSIONS = {android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE};
                                if (!hasPermissions(getContext(), PERMISSIONS)) {
                                    ActivityCompat.requestPermissions((Activity) getContext(), PERMISSIONS, 112);

                                } else {
                                    createPdFFile();
                                }
                            } else {
                                createPdFFile();
                            }
                            Utils.showToast("download Successfully", getContext());
                            MainActivity.changeFragment(new HomeFragment(), false);

                        } else {
                            Utils.showToast("Try Again!!", getContext());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d("new exception:", String.valueOf(e));
                    }
                    break;
            }
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        //Checking the request code of our request
        if (requestCode == 112) {

            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                new DownloadFile().execute(resumeUrl);

            } else {
                Utils.showToast("Permission not granted", getContext());
            }
        }
    }
//    class DownloadFileFromURL extends AsyncTask<String, String, String> {
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            //showDialog(progress_bar_type);
//        }
//
//        @Override
//        protected String doInBackground(String... f_url) {
//            int count;
//            try {
//                URL url = new URL(f_url[0]);
//                URLConnection conection = url.openConnection();
//                conection.connect();
//                int lenghtOfFile = conection.getContentLength();
//                InputStream input = new BufferedInputStream(url.openStream(),
//                        8192);
//                OutputStream output = new FileOutputStream(Environment
//                        .getExternalStorageDirectory().toString()
//                        + "/infinityDynamics/" + System.currentTimeMillis() + ".pdf");
//
//                byte data[] = new byte[1024];
//
//                long total = 0;
//
//                while ((count = input.read(data)) != -1) {
//                    total += count;
//                    // publishing the progress....
//                    // After this onProgressUpdate will be called
//                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));
//
//                    // writing data to file
//                    output.write(data, 0, count);
//                }
//
//                // flushing output
//                output.flush();
//
//                // closing streams
//                output.close();
//                input.close();
//
//            } catch (Exception e) {
//                Log.e("Error: ", e.getMessage());
//            }
//
//            return null;
//        }
//    }


    private class DownloadFile extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... strings) {
            if (Build.VERSION.SDK_INT >= 23) {
                String[] PERMISSIONS = {android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE};
                if (!hasPermissions(getContext(), PERMISSIONS)) {
                    ActivityCompat.requestPermissions((Activity) getContext(), PERMISSIONS, 112);

                } else {

                    System.out.println("Going to Create PDF file");;
                    createPdFFile();
                }
            }

            return null;
        }
    }

    public File f;

    private void createPdFFile() {

        String fileUrl = strings[0];
        String fileName = strings[1];// -> http://maven.apache.org/maven-1.x/maven.pdf
        File  dir = new File(Environment.getExternalStorageDirectory().toString()+"/Infinity Dynamics/");
        if(!dir.exists()){
            System.out.println("Directory not exist");;
            dir.mkdirs();
        }
         f = new File(dir.getAbsolutePath(), "Infinity Dynamics "+firstName+"_"+lastName+"_Resume "+System.currentTimeMillis()+".pdf");
        try {
            System.out.println("Creating file " +  f.getAbsolutePath());;
            f.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("File URL" +  resumeUrl);;
        Thread thread = new Thread(new Runnable() {

            @Override
            public void run() {
                try  {
                    FileDownloader.downloadFile(resumeUrl,  f);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
    }


}
