package com.nfinitydynamics.fragment;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.nfinitydynamics.R;
import com.nfinitydynamics.activity.MainActivity;
import com.nfinitydynamics.utils.AsynchTaskListner;
import com.nfinitydynamics.utils.CallRequest;
import com.nfinitydynamics.utils.Constant;
import com.nfinitydynamics.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import static com.nfinitydynamics.activity.MainActivity.toolbar;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChangePasswordCandidateFragment extends Fragment implements AsynchTaskListner {
    public EditText edtOldPass, edtNewPass, edtConfirm;
    public Button btnSend;
    public String oldPass, newPass, confirmPass;
    public ChangePasswordCandidateFragment instance;
    public SharedPreferences shared;
    public String companyId;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view;
        view = inflater.inflate(R.layout.fragment_change_password, container, false);
        instance = this;
        shared = getActivity().getSharedPreferences(getContext().getPackageName(), 0);
        companyId = shared.getString("userId", "");
        btnSend = view.findViewById(R.id.btn_send);
        edtOldPass = view.findViewById(R.id.edt_old_password);
        edtNewPass = view.findViewById(R.id.edt_new_password);
        edtConfirm = view.findViewById(R.id.edt_cpassword);
        toolbar.setTitle("Change Password");

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                oldPass = edtOldPass.getText().toString().trim();
                newPass = edtNewPass.getText().toString().trim();
                confirmPass = edtConfirm.getText().toString().trim();

                if (TextUtils.isEmpty(oldPass)) {
                    Utils.showAlert("Please Enter Old Password", getContext());
                } else if (TextUtils.isEmpty(newPass)) {
                    Utils.showAlert("Please Enter New Password", getContext());
                } else if (TextUtils.isEmpty(confirmPass)) {
                    Utils.showAlert("Please Enter Confirm  Password", getContext());
                } else if (!confirmPass.equals(newPass)) {
                    Utils.showAlert("Please Enter Correct Confirm Password", getContext());
                } else if (edtNewPass.getText().toString().length() < 6) {
                    Utils.showAlert("Please enter the password of atlest 6 characters", getContext());
                } else {
                    //for company
                    new CallRequest(instance).change_password(companyId, oldPass, newPass, "2");


                }
            }
        });
        return view;
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case change_password:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            Utils.showToast(jObj.getString("message"), getContext());
                            SharedPreferences.Editor et = shared.edit();
                            et.putString("password", newPass);
                            et.commit();
                            MainActivity.changeFragment(new HomeFragment(), false);
                        } else {
                            Utils.showToast(jObj.getString("message"), getContext());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d("new exception:", String.valueOf(e));
                    }
                    break;
//                case change_password_candidate:
//                    Utils.hideProgressDialog();
//                    try {
//                        JSONObject jObj = new JSONObject(result);
//                        if (jObj.getBoolean("status")) {
//                            Utils.showToast("Password Change Successfully", getContext());
//
//                        } else {
//                            Utils.showToast("Try Again!!", getContext());
//                        }
//
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                        Log.d("new exception:", String.valueOf(e));
//                    }
//                    break;
            }
        }
    }

}
