package com.nfinitydynamics.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.nfinitydynamics.R;

import static com.nfinitydynamics.activity.MainActivity.toolbar;

/**
 * A simple {@link Fragment} subclass.
 */
public class TermsAndConditionFragment extends Fragment {

    View view;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view= inflater.inflate(R.layout.fragment_terms_and_condition, container, false);

        toolbar.setTitle("Terms And Conditions");
        WebView webView = (WebView)view.findViewById(R.id.webview);
        WebSettings settings = webView.getSettings();
//       settings.setMinimumFontSize(20);
        settings.setDefaultFontSize(30);
        settings.setAllowFileAccess(true);
        settings.setAllowContentAccess(true);
        //settings.setAllowFileAccessFromFileURLs(true);
        settings.setLoadWithOverviewMode(true);
        settings.setUseWideViewPort(true);
        settings.setLoadsImagesAutomatically(true);
        webView.setInitialScale(1);
        webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        webView.setScrollbarFadingEnabled(false);
        webView.loadUrl("file:///android_asset/infinitytnc.html");
        return  view;
    }

}
