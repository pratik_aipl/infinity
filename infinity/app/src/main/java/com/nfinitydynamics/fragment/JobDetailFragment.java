package com.nfinitydynamics.fragment;


import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nfinitydynamics.R;
import com.nfinitydynamics.adapter.SeanspanOtherJobsAdapter;
import com.nfinitydynamics.model.FeaturedJobModel;
import com.nfinitydynamics.utils.AsynchTaskListner;
import com.nfinitydynamics.utils.CallRequest;
import com.nfinitydynamics.utils.Constant;
import com.nfinitydynamics.utils.JsonParserUniversal;
import com.nfinitydynamics.utils.RecyclerTouchListener;
import com.nfinitydynamics.utils.Utils;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static com.nfinitydynamics.activity.MainActivity.changeFragment;
import static com.nfinitydynamics.activity.MainActivity.toolbar;


public class JobDetailFragment extends Fragment implements AsynchTaskListner {
    public ImageView ivCompany;
    public TextView txtShipType, txtCompany, txtJob, txtPosition, txtDescription, txtJobTitle, txtApply;
    public RelativeLayout relativeApplyNow;
    public View view;
    public String image, jobTitle, company, shipType, description, companyId, jobId, rank, isApplied, JobEndDate, experienceRange;
    public JobDetailFragment instance;
    public RecyclerView recyclerOtherJob;
    public ArrayList<FeaturedJobModel> otherJobList = new ArrayList<>();
    public SeanspanOtherJobsAdapter otherJobsAdapter;
    public JsonParserUniversal jParser;
    public SharedPreferences shared;
    public FeaturedJobModel fJobModel;
    public Button btnApplyNow;
    public TextView txtOther, txtOtherJobs, txtExpireOn, txtExperienceRange;

    @SuppressLint("NewApi")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_job_detail, container, false);
        instance = this;
        jParser = new JsonParserUniversal();
        toolbar.setTitle("Job Details");
        relativeApplyNow = view.findViewById(R.id.relative_apply);
        ivCompany = view.findViewById(R.id.iv_image_company);
        txtShipType = view.findViewById(R.id.txt_ship_type_value);
        txtCompany = view.findViewById(R.id.txt_company_value);
        txtApply = view.findViewById(R.id.txt_apply);
        recyclerOtherJob = view.findViewById(R.id.recycler_view_other);
        txtExpireOn = view.findViewById(R.id.txt_expireon);
        txtExperienceRange = view.findViewById(R.id.txtExperienceRange);
        // txtJob=view.findViewById(R.id.txt_job_value);
        txtJobTitle = view.findViewById(R.id.txt_job_title);
        txtOtherJobs = view.findViewById(R.id.txt_other_job);
        // txtPosition=view.findViewById(R.id.txt_position_value);
        txtDescription = view.findViewById(R.id.txt_description);
        txtOther = view.findViewById(R.id.txt_other_job);
        relativeApplyNow = view.findViewById(R.id.relative_apply);
        image = getArguments().getString("image");
        jobTitle = getArguments().getString("jobTitle");
        shipType = getArguments().getString("shipType");
        company = getArguments().getString("company");
        description = getArguments().getString("description");
        companyId = getArguments().getString("companyId");
        jobId = getArguments().getString("jobId");
        rank = getArguments().getString("rank");
        isApplied = getArguments().getString("isApplied");
        JobEndDate = getArguments().getString("JobEndDate");
        experienceRange=getArguments().getString("fromExperience")+" upto "+getArguments().getString("toExperience");
        System.out.println("fromExperience====>" + experienceRange);
        Picasso.with(getContext()).load(image).placeholder(R.drawable.no_image)
                .error(R.drawable.no_image).into(ivCompany);
        txtCompany.setText(": " + company);
        txtJobTitle.setText(jobTitle);
        txtShipType.setText(": " + shipType);
        txtDescription.setText(Html.fromHtml(description));
        txtOtherJobs.setText("OTHER JOBS OF " + company);
        txtExperienceRange.setText(": " +experienceRange);
        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat outputFormat = new SimpleDateFormat("dd-MMM-yyyy");
        Date date = null;
        try {
            date = inputFormat.parse(JobEndDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String ddMMYYDate = outputFormat.format(date);
        System.out.println("converted date====" + ddMMYYDate);
        txtExpireOn.setText(": " + ddMMYYDate);
        if (isApplied.equalsIgnoreCase("1")) {
            txtApply.setText("APPLIED");
            txtApply.setPadding(0, 5, 0, 5);
            relativeApplyNow.setBackground(getResources().getDrawable(R.drawable.applied_bg));
        } else {
            relativeApplyNow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    relativeApplyNow.setBackgroundColor(R.drawable.red_round);
                    txtApply.setText("APPLY NOW");
                    ApplyNowFragment applyNowFragment = new ApplyNowFragment();
                    Bundle b = new Bundle();
                    b.putString("jobTitle", jobTitle);
                    b.putString("jobId", jobId);
                    b.putString("shipType", shipType);
                    b.putString("company", company);
                    b.putString("companyId", companyId);
                    b.putString("rank", rank);
                    b.putString("isApplied", isApplied);
                    applyNowFragment.setArguments(b);
                    changeFragment(applyNowFragment, true);
                }
            });
        }

        getOtherJobs();
        return view;
    }

    private void getOtherJobs() {

        new CallRequest(instance).getOtherJobsOfCopamny(companyId, jobId);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case otherJob:

                    Utils.hideProgressDialog();
                    otherJobList.clear();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {

                            //Utils.showToast(jObj.getString("message"), getContext());
                            JSONArray ob1 = jObj.getJSONArray("data");

                            for (int i = 0; i < ob1.length(); i++) {
                                fJobModel = new FeaturedJobModel();
                                Log.d("cheak status:---", "shdgcusduh");
                                JSONObject jObject = ob1.getJSONObject(i);
                                fJobModel = (FeaturedJobModel) jParser.parseJson(jObject, new FeaturedJobModel());
                                otherJobList.add(fJobModel);
                            }
                            if (otherJobList.size() > 0) {
                                otherJobsAdapter = new SeanspanOtherJobsAdapter(otherJobList);
                                recyclerOtherJob.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
                                recyclerOtherJob.setItemAnimator(new DefaultItemAnimator());
                                recyclerOtherJob.setAdapter(otherJobsAdapter);
                                otherJobsAdapter.notifyDataSetChanged();
                            }
                            if (otherJobList.size() == 0) {
                                txtOther.setVisibility(View.GONE);
                            }

                            recyclerOtherJob.addOnItemTouchListener(new RecyclerTouchListener(getContext(), recyclerOtherJob, new RecyclerTouchListener.ClickListener() {
                                @Override
                                public void onClick(View view, int position) {
                                    // Utils.showToast("click-------", getContext());
                                    JobDetailFragment jobDetailFragment = new JobDetailFragment();
                                    Bundle b = new Bundle();
                                    FeaturedJobModel model = otherJobList.get(position);
                                    image = model.getLogoImgPath();
                                    jobTitle = otherJobList.get(position).getJobTitle();
                                    shipType = model.getShipType();
                                    company = model.getCompanyName();
                                    description = model.getDescription();
                                    companyId = otherJobList.get(position).getCompanyID();
                                    jobId = otherJobList.get(position).getJobID();
                                    rank=otherJobList.get(position).getRank();
                                    Log.d("jobTitle =====", jobTitle);
                                    isApplied = String.valueOf(otherJobList.get(position).getIs_Applied_Job());
                                    Log.d("copanyId", companyId);
                                    Log.d("value", image + jobTitle + shipType + company + description);
                                    b.putString("image", image);
                                    b.putString("jobTitle", jobTitle);
                                    b.putString("shipType", shipType);
                                    b.putString("company", company);
                                    b.putString("description", description);
                                    b.putString("companyId", companyId);
                                    b.putString("isApplied", isApplied);
                                    b.putString("jobId", jobId);
                                    b.putString("JobEndDate", model.getJobEndDate());
                                    b.putString("fromExperience",model.getFromExperience());
                                    b.putString("toExperience",model.getToExperience());
                                    b.putString("rank", rank);
                                    jobDetailFragment.setArguments(b);
                                    changeFragment(jobDetailFragment, true);
                                }

                                @Override
                                public void onLongClick(View view, int position) {

                                }
                            }));

                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }
}
