package com.nfinitydynamics.fragment;


import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.nfinitydynamics.R;
import com.nfinitydynamics.activity.MainActivity;
import com.nfinitydynamics.adapter.NothingSelectedSpinnerAdapter;
import com.nfinitydynamics.utils.AsynchTaskListner;
import com.nfinitydynamics.utils.CallRequest;
import com.nfinitydynamics.utils.Constant;
import com.nfinitydynamics.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

import static com.nfinitydynamics.activity.MainActivity.toolbar;
import static com.nfinitydynamics.utils.App.joiningTypeValue;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddEducationFragment extends Fragment implements AsynchTaskListner {

    public EditText edtInstituteName, edtDegree, edtPlace;
    public Spinner spEducationType;
    public TextView edtPassingYear;
    public String candidateId, id, instituteName, degree, place, passingYear, educationTypeValue = "";
    public AddEducationFragment instance;
    public SharedPreferences shared;
    public Button btnSave;
    public int s, s1;
    public int mYear;
    public ArrayAdapter aa;
    public String[] educationType = {"Academic Education", "Pre-Sea Training"};

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view;
        view = inflater.inflate(R.layout.fragment_add_education, container, false);
        instance = this;
        toolbar.setTitle("Add Education Details");
        edtInstituteName = view.findViewById(R.id.edt_inst_name);
        edtDegree = view.findViewById(R.id.edt_degree);
        edtPlace = view.findViewById(R.id.edt_place);
        edtPassingYear = view.findViewById(R.id.edt_passing_year);
        btnSave = view.findViewById(R.id.btn_save);
        spEducationType = view.findViewById(R.id.spinner_education_type);
        shared = getContext().getSharedPreferences(getContext().getPackageName(), 0);
        candidateId = shared.getString("userId", "");

        getEducationType();
        Bundle b = getArguments();
        s = b.getInt("value");
        if (s == 1) {
            toolbar.setTitle("Edit Education Details");
            id = b.getString("id");
            instituteName = b.getString("institueName");
            place = b.getString("place");
            degree = b.getString("degree");
            passingYear = b.getString("passingYear");
            educationTypeValue = b.getString("educationType");
            edtInstituteName.setText(instituteName);
            edtPlace.setText(place);
            edtDegree.setText(degree);
            edtPassingYear.setText(passingYear);

            if (educationTypeValue != null) {
                int spinnerPosition = aa.getPosition(educationTypeValue);
                Log.d("spinnerPosition", String.valueOf(aa.getPosition(educationTypeValue)));
                spEducationType.setSelection(spinnerPosition + 1);
            }

        } else {
            educationTypeValue = "";
        }

        edtPassingYear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR);
                int mMonth = c.get(Calendar.MONTH);
                int mDay = c.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), AlertDialog.THEME_HOLO_DARK,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                String stringOfDate = String.valueOf(year);
                                edtPassingYear.setText(stringOfDate);


                            }
                        }, mYear, mMonth, mDay);
                try {
                    ((ViewGroup) datePickerDialog.getDatePicker()).findViewById(Resources.getSystem().getIdentifier("month", "id", "android")).setVisibility(View.GONE);
                    ((ViewGroup) datePickerDialog.getDatePicker()).findViewById(Resources.getSystem().getIdentifier("day", "id", "android")).setVisibility(View.GONE);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                datePickerDialog.show();
            }

        });

        spEducationType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    educationTypeValue = educationType[position - 1];
                    //  educationTypeValue = String.valueOf(spEducationType.getSelectedItem());
                } catch (IndexOutOfBoundsException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                instituteName = edtInstituteName.getText().toString().trim();
                place = edtPlace.getText().toString().trim();
                degree = edtDegree.getText().toString().trim();
                passingYear = edtPassingYear.getText().toString().trim();

                if (educationTypeValue == null) {
                    educationTypeValue = "";
                }
                if (TextUtils.isEmpty(educationTypeValue)) {
                    Utils.showAlert("Please Select Education Type", getContext());
                } else if (instituteName.equals("")) {
                    edtInstituteName.findFocus();
                    Utils.showAlert("Please Enter Institute Name", getContext());
                } else if (degree.equals("")) {
                    edtDegree.findFocus();
                    Utils.showAlert("Enter Certificate Type", getContext());
                } else if (place.equals("")) {
                    edtPlace.findFocus();
                    Utils.showAlert("Please Enter Place ", getContext());
                } else if (passingYear.equals("")) {
                    edtPassingYear.findFocus();
                    Utils.showAlert("Please Enter Passing Year", getContext());
                } else {
                    if (s == 0) {
                        addEducationDetail();
                    }
                    if (s == 1) {
                        updateEducationDetail();
                    }
                }

            }
        });
        return view;
    }

    private void getEducationType() {

        aa = new ArrayAdapter(getContext(), R.layout.spinner_text, educationType);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spEducationType.setAdapter(new NothingSelectedSpinnerAdapter(
                aa, R.layout.education_spinner_row_nothing_selected,
                getContext()));
    }

    private void updateEducationDetail() {

        new CallRequest(instance).updateEducationDetail(candidateId, educationTypeValue, instituteName, degree, place, passingYear, "1", id);
    }

    private void addEducationDetail() {
        new CallRequest(instance).addEducationDetail(candidateId, educationTypeValue, instituteName, degree, place, passingYear, "0");
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case addEducationDetail:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            Utils.showToast("Added Successfully", getContext());
                            MainActivity.activity.onBackPressed();
                        } else {
                            Utils.showToast("Try Again!!", getContext());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();

                        Log.d("new exception:", String.valueOf(e));
                    }
                    break;


                case updateEducationDetail:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            Utils.showToast("Updated Successfully", getContext());
                            MainActivity.activity.onBackPressed();
                        } else {
                            Utils.showToast("Try Again!!", getContext());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();

                        Log.d("new exception:", String.valueOf(e));
                    }
                    break;


            }
        }
    }


}

