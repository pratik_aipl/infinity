package com.nfinitydynamics.fragment.companyFragment;

import android.app.Dialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.nfinitydynamics.R;
import com.nfinitydynamics.activity.CompanyActivity.CompanyDashboard;
import com.nfinitydynamics.adapter.CompanyAdapter.CandidateAdapter;
import com.nfinitydynamics.adapter.NothingSelectedSpinnerAdapter;
import com.nfinitydynamics.model.CountryModel;
import com.nfinitydynamics.model.RankModel;
import com.nfinitydynamics.model.ShipModel;
import com.nfinitydynamics.model.company.CandidateModel;
import com.nfinitydynamics.utils.App;
import com.nfinitydynamics.utils.AsynchTaskListner;
import com.nfinitydynamics.utils.CallRequest;
import com.nfinitydynamics.utils.Constant;
import com.nfinitydynamics.utils.JsonParserUniversal;
import com.nfinitydynamics.utils.MultiselectionSpinnerCountry;
import com.nfinitydynamics.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static com.nfinitydynamics.activity.CompanyActivity.CompanyDashboard.toolbar;
import static com.nfinitydynamics.utils.Constant.FILTER_TYPE.COUNTRY;

/**
 * Created by empiere-vaibhav on 10/27/2018.
 */

public class ShoreJobFragment extends Fragment implements AsynchTaskListner, MultiselectionSpinnerCountry.OnMultipleItemsSelectedListener {
    public Spinner spinnerShip, spExperienceFrom;
    public ListView lvCandidate;
    public ShoreJobFragment instance;
    public String candidateId, jobId = "", rankValue = "", shipType = "", experienceFromValue = "", experienceToValue = "", criteareaTo = "",
            criteareaFrom = "", availableFromValue = "", latestAvailble = "";
    public ArrayList<CandidateModel> candidateList = new ArrayList<>();
    public JsonParserUniversal jParser;
    public TextView txtNoData;
    public Spinner spinnerRank, spAvailableFrom;
    public ArrayList<RankModel> rankList = new ArrayList<>();
    public ArrayList<String> rankStringList = new ArrayList<>();
    public int value;
    public LinearLayout linerFilter;
    public CandidateModel model;
    public CandidateAdapter candidateAdapter;
    public EditText edtCriteAreaFrom, edtCiteAreaTo;
    public ArrayList<ShipModel> shipList = new ArrayList<>();
    public ArrayList<String> shipStringList = new ArrayList<>();
    public String[] experienceFrom = {"Select Minimum Experience ", "On Promotion", "6 Months", "12 Months", "18 Months", "24 Months", "36 Months"};
    public String[] experienceTo = {"Select Experience To", "6 Months", "12 Months", "18 Months", "24 Months", "24+ Months"};
    public String[] availableFrom = {"Select Available Immediately", "15 - 30 Days", "30 - 45 Days", "45 - 60 Days", "All Available"};
    public SharedPreferences shared;
    public Dialog dialogView;
    public int rankPos = 0, shipPos = 0, contractorPos = 0, servicePos = 0;
    public ArrayAdapter aa;
    public ArrayAdapter aaRank;
    public ArrayList<CountryModel> countryModelArrayList = new ArrayList<CountryModel>();
    public ArrayList<String> countyStringList = new ArrayList<>();
    public int selectedPos = 0;
    public String countryId;
    // public ArrayAdapter aaCountry;
    public TextView select_country;
    public MultiselectionSpinnerCountry spIssingCountry;
    public static String type;
    public String multiSelectedCountryId = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view;
        view = inflater.inflate(R.layout.fragment_candidate, container, false);
        instance = this;
        toolbar.setTitle("Shore Job");
        lvCandidate = view.findViewById(R.id.lv_cnadidate);
        txtNoData = view.findViewById(R.id.txt_nodata);
        linerFilter = view.findViewById(R.id.linearFilter);
        shared = getActivity().getSharedPreferences(getContext().getPackageName(), 0);
        candidateId = shared.getString("userId", "");
        final EditText edtSearch = view.findViewById(R.id.edt_search_candidate);

        jParser = new JsonParserUniversal();
        toolbar.setTitle("Shore Job Resume");
        getAllCandidateDetail();

        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String text = edtSearch.getText().toString().toLowerCase(Locale.getDefault());
                candidateAdapter.filters(text);

            }
        });
        linerFilter.setVisibility(View.GONE);
        linerFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFilterDialog();
            }
        });

        return view;
    }


    private void getAllCandidateDetail() {
        new CallRequest(instance).getShoreJob();
    }

    private void getFilter() {
        new CallRequest(instance).getFilterShoreJob(rankValue, shipType, jobId, candidateId, experienceFromValue, experienceToValue, criteareaFrom, criteareaTo, availableFromValue, multiSelectedCountryId, latestAvailble);
    }

    public void showFilterDialog() {
//        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
//        LayoutInflater inflater = this.getLayoutInflater();
//        final View dialogView = inflater.inflate(R.layout.custom_filter_dialog, null);
//        dialogBuilder.setView(dialogView);

        dialogView = new Dialog(getActivity());
        dialogView.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogView.setCancelable(true);
        dialogView.setContentView(R.layout.custom_filter_dialog);
        dialogView.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        spinnerRank = dialogView.findViewById(R.id.spinner_rank);
        spinnerShip = dialogView.findViewById(R.id.spinner_ship_type);
        spExperienceFrom = dialogView.findViewById(R.id.sp_experience_from);
        spIssingCountry = dialogView.findViewById(R.id.sp_issuing_country);
        select_country = dialogView.findViewById(R.id.select_country); // spExperienceTo = dialogView.findViewById(R.id.sp_experience_to);
        edtCriteAreaFrom = dialogView.findViewById(R.id.edt_age_criteria_from);
        // edtCiteAreaTo = dialogView.findViewById(R.id.edt_age_criteria_to);
        spAvailableFrom = dialogView.findViewById(R.id.spinner_available_from);
        Button btnClear = dialogView.findViewById(R.id.btnClear);
        Button btnOk = dialogView.findViewById(R.id.btnOk);
        TextView txtAvailableFrom = dialogView.findViewById(R.id.txt_available_from);
        //edtCiteAreaTo.setText(criteareaTo);
        edtCriteAreaFrom.setText(criteareaFrom);
        getCountry();
        if (value == 2) {
            spAvailableFrom.setVisibility(View.GONE);
            txtAvailableFrom.setVisibility(View.GONE);
            latestAvailble = "yes";
        }
        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rankValue = "";
                shipType = "";
                spAvailableFrom.setSelection(0);
                spExperienceFrom.setSelection(0);
                spIssingCountry.clearSelection();
                spExperienceFrom.setSelection(0);
                criteareaTo = "";
                criteareaFrom = "";
                candidateId = "";
                dialogView.dismiss();
                spinnerRank.setSelection(0);
                spinnerShip.setSelection(0);
                rankPos = 0;
                shipPos = 0;
                selectedPos = 0;
                spinnerRank.setAdapter(aaRank);
                spinnerShip.setAdapter(aa);
                experienceFromValue = "0";
                experienceToValue = "0";
                availableFromValue = "0";
                getAllCandidateDetail();
            }
        });

        spIssingCountry.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                type = "country";
                return false;
            }
        });

        if (countyStringList.size() > 0) {
            select_country.setVisibility(View.GONE);
        } else {
            select_country.setVisibility(View.VISIBLE);
        }

        if (!TextUtils.isEmpty(multiSelectedCountryId)) {
            int[] array = new int[App.indexListCountry.size()];
            int counter = 0;
            for (Integer myInt : App.indexListCountry) {
                array[counter++] = myInt;
            }
            spIssingCountry.setSelection(array);

        }

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                criteareaFrom = edtCriteAreaFrom.getText().toString().trim();
                int criteAge = 0;
                //  criteareaTo = edtCiteAreaTo.getText().toString().trim();
                try {
                    criteAge = Integer.parseInt(criteareaFrom);
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }

                if (criteAge > 70) {
                    Utils.showToast("Maximum Age Limit is 70", getActivity());
                }
// else  if(!TextUtils.isEmpty(experienceFromValue)||!TextUtils.isEmpty(experienceToValue)||!TextUtils.isEmpty(rankValue)){
//                    if(TextUtils.isEmpty(rankValue)){
//                        Utils.showToast("Please Select Rank",getActivity());
//                    }else  if(TextUtils.isEmpty(experienceToValue)){
//                        Utils.showToast("Please Select Experience To",getActivity());
//                    }else  if(TextUtils.isEmpty(experienceFromValue)){
//                        Utils.showToast("Please Select Experience From",getActivity());
//                    }else {
//                        dialogView.dismiss();
//                        getAllCandidateDetail();
//                    }
//                }
                else {
                    dialogView.dismiss();
                    getFilter();
                }


            }
        });

//        edtCiteAreaTo.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//            @Override
//            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
//
//
//                    return true;
//
//            }
//        });


        aa = new ArrayAdapter<>(getContext(), R.layout.spinner_text, shipStringList);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        spinnerShip.setAdapter(aa);
        spinnerShip.setAdapter(new NothingSelectedSpinnerAdapter(
                aa, R.layout.contact_spinner_row_nothing_selected_ship,
                getContext()));
        System.out.println("ship pos====" + shipPos);
        spinnerShip.setSelection(shipPos);

        aaRank = new ArrayAdapter<>(getContext(), R.layout.spinner_text, rankStringList);
        aaRank.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerRank.setAdapter(new NothingSelectedSpinnerAdapter(
                aaRank, R.layout.contact_spinner_row_nothing_selected,
                getContext()));
//        spinnerRank.setAdapter(aaRank);
        spinnerRank.setSelection(rankPos);

        ArrayAdapter aaCountry = new ArrayAdapter<>(getContext(), R.layout.spinner_text, countyStringList);
        aaCountry.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
      /*  spIssingCountry.setAdapter(new NothingSelectedSpinnerAdapter(
                aaCountry, R.layout.country_recidency,
                getContext()));
     */   //spIssingCountry.setSelection(selectedPos);
        spinnerRank.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                try {
                    rankValue = rankList.get(i - 1).getRankId();
                    System.out.println("selected rank===" + rankValue);
                } catch (NullPointerException e) {
                    e.printStackTrace();
                } catch (ArrayIndexOutOfBoundsException ex) {
                    ex.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
       /* spIssingCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    countryId = countryModelArrayList.get(position - 1).getCountryID();
                    System.out.println("country id===" + countryId);
                } catch (NullPointerException e) {
                    e.printStackTrace();
                } catch (ArrayIndexOutOfBoundsException ex) {
                    ex.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });*/

        spinnerShip.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                try {
                    shipType = shipList.get(i - 1).getShipID();

                } catch (NullPointerException e) {
                    e.printStackTrace();
                } catch (ArrayIndexOutOfBoundsException ex) {
                    ex.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        ArrayAdapter aa1 = new ArrayAdapter(getContext(), android.R.layout.simple_spinner_item, experienceFrom);
        aa1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spExperienceFrom.setAdapter(aa1);

        if (experienceFromValue.equalsIgnoreCase("1")) {
            spExperienceFrom.setSelection(1);
        } else if (experienceFromValue.equalsIgnoreCase("6")) {
            spExperienceFrom.setSelection(2);
        } else if (experienceFromValue.equalsIgnoreCase("12")) {
            spExperienceFrom.setSelection(3);
        } else if (experienceFromValue.equalsIgnoreCase("18")) {
            spExperienceFrom.setSelection(4);
        } else if (experienceFromValue.equalsIgnoreCase("24")) {
            spExperienceFrom.setSelection(5);
        } else if (experienceFromValue.equalsIgnoreCase("36")) {
            spExperienceFrom.setSelection(6);
        } else {
            spExperienceFrom.setSelection(0);
        }


        spExperienceFrom.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                if (i == 1) {
                    experienceFromValue = "1";
                } else if (i == 2) {
                    experienceFromValue = "6";
                } else if (i == 3) {
                    experienceFromValue = "12";
                } else if (i == 4) {
                    experienceFromValue = "18";
                } else if (i == 5) {
                    experienceFromValue = "24";
                } else if (i == 6) {
                    experienceFromValue = "36";
                }
                //  getAllCandidateDetail();
                // dialogView.dismiss();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
//        ArrayAdapter aa = new ArrayAdapter(getContext(), android.R.layout.simple_spinner_item, experienceTo);
//        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        spExperienceTo.setAdapter(aa);
//
//        if (experienceToValue.equalsIgnoreCase("6")) {
//            spExperienceTo.setSelection(1);
//        } else if (experienceToValue.equalsIgnoreCase("12")) {
//            spExperienceTo.setSelection(2);
//        } else if (experienceToValue.equalsIgnoreCase("18")) {
//            spExperienceTo.setSelection(3);
//        } else if (experienceToValue.equalsIgnoreCase("24")) {
//            spExperienceTo.setSelection(4);
//        } else if (experienceToValue.equalsIgnoreCase("24+")) {
//            spExperienceTo.setSelection(5);
//        } else {
//            spExperienceTo.setSelection(0);
//        }
//
//        spExperienceTo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                Log.d("iiiiiiiiiiTooo======", String.valueOf(i));
//                if (i == 1) {
//                    experienceToValue = "6";
//
//                } else if (i == 2) {
//                    experienceToValue = "12";
//
//                } else if (i == 3) {
//                    experienceToValue = "18";
//
//                } else if (i == 4) {
//                    experienceToValue = "24";
//
//                } else if (i == 5) {
//                    experienceToValue = "24+";
//
//                }
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> adapterView) {
//
//            }
//        });
        ArrayAdapter aAvailable = new ArrayAdapter(getContext(), android.R.layout.simple_spinner_item, availableFrom);
        aAvailable.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spAvailableFrom.setAdapter(aAvailable);
        spAvailableFrom.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                if (i == 1) {
                    availableFromValue = "15";
                } else if (i == 2) {
                    availableFromValue = "30";
                } else if (i == 3) {
                    availableFromValue = "45";
                } else if (i == 4) {
                    availableFromValue = "0";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        if (availableFromValue.equalsIgnoreCase("15")) {
            spAvailableFrom.setSelection(1);
        } else if (availableFromValue.equalsIgnoreCase("30")) {
            spAvailableFrom.setSelection(2);
        } else if (availableFromValue.equalsIgnoreCase("45")) {
            spAvailableFrom.setSelection(3);
        } else if (availableFromValue.equalsIgnoreCase("0")) {
            spAvailableFrom.setSelection(4);
        }
        dialogView.show();
    }

    private void getShipType() {
        new CallRequest(instance).getShipFragment();
    }

    private void getCountry() {
        new CallRequest(instance).getCountry();
    }

    private void getRank() {
        new CallRequest(instance).getRankFragment();
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case getShoreJob:
                    Utils.hideProgressDialog();
                    candidateList.clear();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            JSONArray ob1 = jObj.getJSONArray("data");

                            for (int i = 0; i < ob1.length(); i++) {
                                JSONObject jObject = ob1.getJSONObject(i);
                                model = new CandidateModel();
                                model = (CandidateModel) jParser.parseJson(jObject, new CandidateModel());
                                candidateList.add(model);
                            }

                            candidateAdapter = new CandidateAdapter(getContext(), R.layout.layout_candidate, candidateList);
                            lvCandidate.setAdapter(candidateAdapter);
                            txtNoData.setVisibility(View.GONE);
                            lvCandidate.setVisibility(View.VISIBLE);

                            lvCandidate.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                                    model = candidateList.get(i);
                                    String name = model.getCanidateName();
                                    String rankName = model.getRankName();
                                    String email = model.getEmailID();
                                    String maritalStatus = model.getMaritalStatus();
                                    String image = model.getImageURL();
                                    String indous = model.getIndos();
                                    String mobileNo = model.getMobileNo();
                                    String lastLogin = model.getLastVisited();
                                    String modified = model.getModified();
                                    String nationality = model.getNationalName();
                                    String idCandidate = model.getCandidateID();
                                    String appliedShipType = model.getAppliedShipName();
                                    String appliedRank = model.getAppliedRankName();
                                    String seaTime = model.getSeatimeInLastRank();


                                    ShoreJobDetailFragment candidateDetailFragment = new ShoreJobDetailFragment();
                                    Bundle b = new Bundle();
                                    b.putInt("value", 1);
                                    b.putString("code", model.getCandidateCode());
                                    b.putString("name", model.getCanidateName());
                                    b.putString("nameCandidate", name);
                                    b.putString("rankName", rankName);
                                    b.putString("email", email);
                                    b.putString("maritalStatus", maritalStatus);
                                    b.putString("image", image);
                                    b.putString("indous", indous);
                                    b.putString("mobileNo", mobileNo);
                                    b.putString("LastVisited", lastLogin);
                                    b.putString("modified", modified);
                                    b.putString("nationality", nationality);
                                    b.putString("idCandidate", idCandidate);
                                    b.putString("appliedShipType", appliedShipType);
                                    b.putString("appliedRank", appliedRank);
                                    b.putString("lastLogin", lastLogin);
                                    b.putString("seaTime", seaTime);
                                    candidateDetailFragment.setArguments(b);
                                    CompanyDashboard.changeFragment(candidateDetailFragment, true);
                                }
                            });
                            getShipType();

                        } else {
                            txtNoData.setVisibility(View.VISIBLE);
                            lvCandidate.setVisibility(View.GONE);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                case getCandidateListRank:
                    Utils.hideProgressDialog();
                    candidateList.clear();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            JSONArray ob1 = jObj.getJSONArray("data");
                            CandidateModel model;
                            for (int i = 0; i < ob1.length(); i++) {
                                JSONObject jObject = ob1.getJSONObject(i);
                                model = new CandidateModel();
                                model = (CandidateModel) jParser.parseJson(jObject, new CandidateModel());
                                candidateList.add(model);

                            }

                            candidateAdapter = new CandidateAdapter(getContext(), R.layout.layout_candidate, candidateList);
                            lvCandidate.setAdapter(candidateAdapter);
                            txtNoData.setVisibility(View.GONE);
                            lvCandidate.setVisibility(View.VISIBLE);

                        } else {
                            txtNoData.setVisibility(View.VISIBLE);
                            lvCandidate.setVisibility(View.GONE);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    break;

                case getRankFragment:

                    rankList.clear();
                    rankStringList.clear();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            JSONArray ob1 = jObj.getJSONArray("data");
                            for (int i = 0; i < ob1.length(); i++) {
                                JSONObject ob = ob1.getJSONObject(i);
                                String rankId = ob.getString("RankID");
                                String rankName = ob.getString("Name");
                                rankList.add(new RankModel(rankId, rankName));
                                rankStringList.add(rankName);
                                if (rankValue != null) {
                                    if (rankValue.equalsIgnoreCase(rankList.get(i).getRankId())) {
                                        rankPos = i + 1;
                                    }
                                }
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d("new exception:", String.valueOf(e));
                    }
                    //  getCountry();
                    Utils.hideProgressDialog();
                    break;

                case getShipType:
                    shipList.clear();
                    shipStringList.clear();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            JSONArray ob1 = jObj.getJSONArray("data");
                            for (int i = 0; i < ob1.length(); i++) {
                                JSONObject ob = ob1.getJSONObject(i);
                                String shipId = ob.getString("ShipID");
                                String shipName = ob.getString("ShipType");

                                shipList.add(new ShipModel(shipId, shipName));

                                shipStringList.add(shipName);

                                if (shipType != null) {
                                    if (shipType.equalsIgnoreCase(shipList.get(i).getShipID())) {
                                        shipPos = i + 1;
                                    }
                                }
                            }
                        } else {
                            Utils.showToast(jObj.getString("message"), getContext());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();

                        Log.d("new exception:", String.valueOf(e));
                    }
                    //  Utils.hideProgressDialog();
                    getRank();
                    break;
                case getCountryList:
                    countyStringList.clear();
                    countryModelArrayList.clear();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            JSONArray ob1 = jObj.getJSONArray("data");
                            CountryModel countryModel = new CountryModel();
                            for (int i = 0; i < ob1.length(); i++) {
                                countryModel = (CountryModel) jParser.parseJson(ob1.getJSONObject(i), new CountryModel());
                                countryModelArrayList.add(countryModel);
                                /*if (countryId != null) {
                                    if (countryId.equalsIgnoreCase(countryModel.getCountryName())) {
                                        selectedPos = i + 1;
                                    }
                                }*/
                                countyStringList.add(countryModel.getCountryName());

                            }
                            spIssingCountry.setItems(countyStringList, Constant.FILTER_TYPE.COUNTRY);
                            spIssingCountry.setSelection(new int[]{0});
                            spIssingCountry.setListener(this);
                            spIssingCountry.clearSelection();

                        } else {
                            Utils.showToast("Try Again!!", getContext());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d("new exception:", String.valueOf(e));
                    }
                    Utils.hideProgressDialog();
            }
        }
    }

    @Override
    public void selectedIndices(List<Integer> indices, Constant.FILTER_TYPE filter_type) {
        if (filter_type == COUNTRY) {
            App.indexListCountry = indices;
            multiSelectedCountryId = "";
            ArrayList<String> rankIDArray = new ArrayList<>();
            for (Integer i : indices) {
                rankIDArray.add(countryModelArrayList.get(i).getCountryID());

            }
            if (rankIDArray.size() > 0) {
                select_country.setVisibility(View.GONE);
            } else {
                select_country.setVisibility(View.VISIBLE);
            }
            multiSelectedCountryId = android.text.TextUtils.join(",", rankIDArray);
        }
    }

    @Override
    public void selectedStrings(List<String> strings, Constant.FILTER_TYPE filter_type) {

    }
}
