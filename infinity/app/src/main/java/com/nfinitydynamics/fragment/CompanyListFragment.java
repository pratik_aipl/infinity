package com.nfinitydynamics.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.nfinitydynamics.R;
import com.nfinitydynamics.adapter.CompanyListAdapter;
import com.nfinitydynamics.model.CompanyListModel;
import com.nfinitydynamics.utils.AsynchTaskListner;
import com.nfinitydynamics.utils.CallRequest;
import com.nfinitydynamics.utils.Constant;
import com.nfinitydynamics.utils.JsonParserUniversal;
import com.nfinitydynamics.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.nfinitydynamics.activity.MainActivity.changeFragment;
import static com.nfinitydynamics.activity.MainActivity.toolbar;

/**
 * A simple {@link Fragment} subclass.
 */
public class CompanyListFragment extends Fragment implements AsynchTaskListner {

    public ArrayList<CompanyListModel> companyList = new ArrayList<>();
    public GridView gvCompanyList;
    public CompanyListFragment instance;
    public JsonParserUniversal jParser;
    public String companyID;
    public CompanyListModel companyModel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view;
        view = inflater.inflate(R.layout.fragment_company_list, container, false);
        gvCompanyList = view.findViewById(R.id.gv_company);
        toolbar.setTitle("Companies");
        jParser = new JsonParserUniversal();
        instance = this;
        getCopanyList();
        return view;
    }

    private void getCopanyList() {
        new CallRequest(instance).getCompanyListFragment();
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case getCompanyListFragment:

                    Utils.hideProgressDialog();
                    companyList.clear();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {

                            //  Utils.showToast(jObj.getString("message"), getContext());
                            JSONArray ob1 = jObj.getJSONArray("data");

                            for (int i = 0; i < ob1.length(); i++) {
                                JSONObject jObject = ob1.getJSONObject(i);
                                companyModel = new CompanyListModel();
                                companyModel = (CompanyListModel) jParser.parseJson(jObject, new CompanyListModel());
                                companyList.add(companyModel);

                            }
                            CompanyListAdapter latestJobAdapter = new CompanyListAdapter(getContext(), R.layout.layout_home_listview, companyList);
                            gvCompanyList.setAdapter(latestJobAdapter);

                            gvCompanyList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                    companyID = companyList.get(i).getCompanyID();
                                    CompanyJobListFragment companyJobListFragment = new CompanyJobListFragment();
                                    Bundle b = new Bundle();
                                    b.putString("companyID", companyID);
                                    b.putString("url", companyList.get(i).getWebsite());
                                    companyJobListFragment.setArguments(b);
                                    changeFragment(companyJobListFragment, true);
                                }
                            });


//                            Intent intent = new Intent(getContext(), MainActivity.class);
//                            startActivity(intent);
//                            finish();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;

            }
        }
    }

}
