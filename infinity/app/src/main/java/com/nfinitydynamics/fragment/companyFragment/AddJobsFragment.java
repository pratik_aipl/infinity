package com.nfinitydynamics.fragment.companyFragment;


import android.app.DatePickerDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;

import com.nfinitydynamics.R;
import com.nfinitydynamics.activity.CompanyActivity.CompanyDashboard;
import com.nfinitydynamics.adapter.NothingSelectedSpinnerAdapter;
import com.nfinitydynamics.model.RankModel;
import com.nfinitydynamics.model.ShipModel;
import com.nfinitydynamics.utils.AsynchTaskListner;
import com.nfinitydynamics.utils.CallRequest;
import com.nfinitydynamics.utils.Constant;
import com.nfinitydynamics.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import static com.nfinitydynamics.activity.CompanyActivity.CompanyDashboard.toolbar;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddJobsFragment extends Fragment implements AsynchTaskListner {

    public EditText edtExpiryDate, edtExperienceRange, edtDescription;
    public Spinner spiinerShipType, spinnerRank;
    public String companyName, jobTitle, expiryDate, experienceRange, rankValue, shipId, candidateId,
            featured, shore, front, description, jobId, experienceFromValue = "", experienceToValue = "";
    public AddJobsFragment instance;
    public ArrayList<RankModel> rankList = new ArrayList<>();
    public ArrayList<String> rankStringList = new ArrayList<>();
    public ArrayList<ShipModel> shipList = new ArrayList<>();
    public ArrayList<String> shipStringList = new ArrayList<>();
    public Button btnAddJob;
    public SharedPreferences shared;
    public CheckBox checkBoxFeatured, checkBoxShore, checkBoxFront;
    public int value;
    public Spinner spExperienceFrom, spExperienceTo;
    public String[] experienceFrom = {"Select Experience From", "On Promotion", "6 Months", "12 Months", "18 Months", "24 Months", "24+ Months"};
    public String[] experienceTo = {"Select  Experience upto", "6 Months", "12 Months", "18 Months", "24 Months"};

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view;
        view = inflater.inflate(R.layout.fragment_add_jobs, container, false);
        instance = this;
        toolbar.setTitle("Add Vacancy");
        shared = getActivity().getSharedPreferences(getContext().getPackageName(), 0);
        candidateId = shared.getString("userId", "");
        edtExpiryDate = view.findViewById(R.id.edt_expiry_date);
        // edtExperienceRange = view.findViewById(R.id.edt_experience_range);
        spiinerShipType = view.findViewById(R.id.spinner_ship_type);
        spinnerRank = view.findViewById(R.id.spinner_rank);
        btnAddJob = view.findViewById(R.id.btn_add_job);
        checkBoxFeatured = view.findViewById(R.id.check_feature_job);
        checkBoxShore = view.findViewById(R.id.check_shore_job);
        checkBoxFront = view.findViewById(R.id.check_display_front);
        edtDescription = view.findViewById(R.id.edt_description);
        spExperienceFrom = view.findViewById(R.id.sp_experience_from);
        spExperienceTo = view.findViewById(R.id.sp_experience_to);
        edtExpiryDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                final Calendar c = Calendar.getInstance();
               int mYear = c.get(Calendar.YEAR);
                int mMonth = c.get(Calendar.MONTH);
                int mDay = c.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog dpd;
                dpd = new DatePickerDialog(getContext(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                edtExpiryDate.setText( Utils.getDateFormat("d", "dd", String.valueOf(dayOfMonth)) + "-" + Utils.getDateFormat("M", "MMM", String.valueOf((monthOfYear + 1)))+ "-" + year);
                            }
                        }, mYear, mMonth, mDay);
                dpd.show();
                dpd.getDatePicker().setMinDate(System.currentTimeMillis());
            }
        });
        getRank();
        getShipType();
        ArrayAdapter aa1 = new ArrayAdapter(getContext(), android.R.layout.simple_spinner_item, experienceFrom);
        aa1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spExperienceFrom.setAdapter(aa1);

        ArrayAdapter aa = new ArrayAdapter(getContext(), android.R.layout.simple_spinner_item, experienceTo);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spExperienceTo.setAdapter(aa);


        value = getArguments().getInt("value");
        if (value == 1) {
            jobId = getArguments().getString("id");
            companyName = getArguments().getString("cName");
            rankValue = getArguments().getString("rank");
            shipId = getArguments().getString("shipType");
            expiryDate = getArguments().getString("leavingDate");
            description = getArguments().getString("description");
            experienceRange = getArguments().getString("experienceRange");
            // jobTitle = getArguments().getString("jobTitle");
            featured = getArguments().getString("isFeatured");
            shore = getArguments().getString("isShor");
            front = getArguments().getString("isFront");
            experienceFromValue = getArguments().getString("expFrom");
            experienceToValue = getArguments().getString("expTo");

            //   edtExpiryDate.setText(expiryDate);
            edtDescription.setText(description);
            //  edtExperienceRange.setText(experienceRange);
            Utils.converDateToDDMMYY(edtExpiryDate, expiryDate);

            // edtJobTitle.setText(jobTitle);
            if (featured.equalsIgnoreCase("Yes")) {
                checkBoxFeatured.setChecked(true);
            } else {
                checkBoxFeatured.setChecked(false);
            }
            if (shore.equalsIgnoreCase("Yes")) {
                checkBoxShore.setChecked(true);
            } else {
                checkBoxShore.setChecked(false);
            }
            if (front.equalsIgnoreCase("Yes")) {
                checkBoxFront.setChecked(true);
            } else {
                checkBoxFront.setChecked(false);
            }
        }

        try {
            if (experienceFromValue.equalsIgnoreCase("On Promotion")) {
                spExperienceFrom.setSelection(1);
            } else if (experienceFromValue.equalsIgnoreCase("6 Months")) {
                spExperienceFrom.setSelection(2);
            } else if (experienceFromValue.equalsIgnoreCase("12 Months")) {
                spExperienceFrom.setSelection(3);
            } else if (experienceFromValue.equalsIgnoreCase("18 Months")) {
                spExperienceFrom.setSelection(4);
            } else if (experienceFromValue.equalsIgnoreCase("24 Months")) {
                spExperienceFrom.setSelection(5);
            } else if (experienceFromValue.equalsIgnoreCase("24+ Months")) {
                spExperienceFrom.setSelection(6);
            } else {
                spExperienceFrom.setSelection(0);
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        try {
            if (experienceToValue.equalsIgnoreCase("6 Months")) {
                spExperienceTo.setSelection(1);
            } else if (experienceToValue.equalsIgnoreCase("12 Months")) {
                spExperienceTo.setSelection(2);
            } else if (experienceToValue.equalsIgnoreCase("18 Months")) {
                spExperienceTo.setSelection(3);
            } else if (experienceToValue.equalsIgnoreCase("24 Months")) {
                spExperienceTo.setSelection(4);
            } else {
                spExperienceTo.setSelection(0);
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }


        spExperienceFrom.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(i!=0) {
                    experienceFromValue = experienceFrom[i];
                }
//                if (i == 1) {
//                    experienceFromValue = "On Promotion";
//                } else if (i == 2) {
//                    experienceFromValue = "6 Months";
//                } else if (i == 3) {
//                    experienceFromValue = "12 Months";
//                } else if (i == 4) {
//                    experienceFromValue = "18 Months";
//                } else if (i == 5) {
//                    experienceFromValue = "24 Months";
//                } else if (i == 6) {
//                    experienceFromValue = "24 Months +";
//                }
                //  getAllCandidateDetail();
                // dialogView.dismiss();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        spExperienceTo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(i!=0) {
                    experienceToValue = experienceTo[i];
                }
//                Log.d("iiiiiiiiiiTooo======", String.valueOf(i));
//                if (i == 1) {
//                    experienceToValue = "6";
//
//                } else if (i == 2) {
//                    experienceToValue = "12";
//
//                } else if (i == 3) {
//                    experienceToValue = "18";
//
//                } else if (i == 4) {
//                    experienceToValue = "24";
//
//                }

                // getAllCandidateDetail();

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        btnAddJob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // jobTitle = edtJobTitle.getText().toString().trim();
                expiryDate = edtExpiryDate.getText().toString().trim();
                // experienceRange = edtExperienceRange.getText().toString().trim();
                description = edtDescription.getText().toString().trim();
//                try {
//                    StringBuilder dobBilder = new StringBuilder();
//                    dobBilder = dobBilder.append(expiryDate.substring(6)).append("-").append(expiryDate.substring(0, 2)).append("-").append(expiryDate.substring(3, 5));
//                    expiryDate = String.valueOf(dobBilder);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
                if (!TextUtils.isEmpty(expiryDate)) {
                    converDateToYYMMddTextView(expiryDate);
                }
                if (TextUtils.isEmpty(shipId)) {
                    spiinerShipType.findFocus();
                    Utils.showAlert("Please Select Ship Type", getContext());
                } else if (TextUtils.isEmpty(rankValue)) {
                    spinnerRank.findFocus();
                    Utils.showAlert("Please Select Rank", getContext());
                } else if (expiryDate.equals("")) {
                    edtExpiryDate.findFocus();
                    Utils.showAlert("Please Enter Expiry Date", getContext());
                } else if (experienceFromValue.equals("")) {
                    spExperienceFrom.findFocus();
                    Utils.showAlert("Please Enter Experience From", getContext());
                } else if (experienceToValue.equals("")) {
                    spExperienceTo.findFocus();
                    Utils.showAlert("Please Enter Experience Upto", getContext());
                } else if (description.equals("")) {
                    edtDescription.findFocus();
                    Utils.showAlert("Please Enter Remarks", getContext());
                } else {
                    if (checkBoxFeatured.isChecked()) {
                        featured = "Yes";
                    } else {
                        featured = "No";
                    }
                    if (checkBoxShore.isChecked()) {
                        shore = "Yes";
                    } else {
                        shore = "No";
                    }
                    if (checkBoxFront.isChecked()) {
                        front = "Yes";
                    } else {
                        front = "No";
                    }

                    if (value == 1) {
                        String dateCurrent = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
                        if(featured.equalsIgnoreCase("No") && shore.equalsIgnoreCase("No") && front.equalsIgnoreCase("No")){
                            Utils.showAlert("Please select any one job section", getContext());
                        }else{
                            new CallRequest(instance).updateJob(jobId, candidateId, rankValue, shipId, jobTitle, experienceFromValue, experienceToValue, dateCurrent,
                                    expiryDate, description, featured, shore, front, "1");

                        }

                    } else {
                        String dateCurrent = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
                        if(featured.equalsIgnoreCase("No") && shore.equalsIgnoreCase("No") && front.equalsIgnoreCase("No")){
                            Utils.showAlert("Please select any one job section", getContext());
                        }else {
                            new CallRequest(instance).addJob(candidateId, rankValue, shipId, jobTitle, dateCurrent, expiryDate, description, featured, shore, front, experienceFromValue, experienceToValue, "0");
                        }
                    }
                }
            }
        });

        spinnerRank.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                try {
                    rankValue = rankList.get(i - 1).getRankId();
                } catch (IndexOutOfBoundsException e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spiinerShipType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                try {
                    shipId = shipList.get(i - 1).getShipID();
                    Log.d("selected ship type----", shipId);
                } catch (IndexOutOfBoundsException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        return view;
    }

    public void converDateToYYMMddTextView(String dateExpiry) {
        DateFormat inputFormat = new SimpleDateFormat("dd-MMM-yyyy");
        DateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = inputFormat.parse(dateExpiry);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String ddMMYYDate = outputFormat.format(date);
        System.out.println("converted date====" + ddMMYYDate);
        expiryDate = ddMMYYDate;
    }

    private void getShipType() {

        new CallRequest(instance).getShipFragment();
    }

    private void getRank() {

        new CallRequest(instance).getRankFragment();
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case getRankFragment:

                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            JSONArray ob1 = jObj.getJSONArray("data");
                            for (int i = 0; i < ob1.length(); i++) {
                                JSONObject ob = ob1.getJSONObject(i);
                                String rankId = ob.getString("RankID");
                                String rankName = ob.getString("Name");
                                rankList.add(new RankModel(rankId, rankName));
                                rankStringList.add(rankName);
                            }
                            ArrayAdapter aa = new ArrayAdapter(getContext(), R.layout.spinner_text, rankStringList);
                            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            //  spinnerRank.setAdapter(aa);
                            spinnerRank.setAdapter(new NothingSelectedSpinnerAdapter(
                                    aa, R.layout.contact_spinner_row_nothing_selected,
                                    getContext()));
                            if (rankValue != null) {
                                int spinnerPosition = aa.getPosition(rankValue);
                                Log.d("spinnerPosition", String.valueOf(aa.getPosition(rankValue)));
                                spinnerRank.setSelection(spinnerPosition + 1);
                            }

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d("new exception:", String.valueOf(e));
                    }
                    break;

                case getShipType:


                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            JSONArray ob1 = jObj.getJSONArray("data");
                            for (int i = 0; i < ob1.length(); i++) {
                                JSONObject ob = ob1.getJSONObject(i);
                                String shipId = ob.getString("ShipID");
                                String shipName = ob.getString("ShipType");

                                shipList.add(new ShipModel(shipId, shipName));

                                shipStringList.add(shipName);
                            }
                            ArrayAdapter aa = new ArrayAdapter(getContext(), R.layout.spinner_text, shipStringList);
                            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            // spiinerShipType.setAdapter(aa);
                            spiinerShipType.setAdapter(new NothingSelectedSpinnerAdapter(
                                    aa, R.layout.contact_spinner_row_nothing_selected_ship,
                                    getContext()));

                            if (shipId != null) {
                                int spinnerPosition = aa.getPosition(shipId);
                                Log.d("spinnerPosition", String.valueOf(aa.getPosition(shipId)));
                                spiinerShipType.setSelection(spinnerPosition + 1);
                            }

                        } else {
                            Utils.showToast(jObj.getString("message"), getContext());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();

                        Log.d("new exception:", String.valueOf(e));
                    }
                    break;

                case addJob:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            Utils.showToast(jObj.getString("message"), getContext());
                            CompanyDashboard.activityCompany.onBackPressed();
                        } else {
                            Utils.showToast("Try Again!!", getContext());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();

                        Log.d("new exception:", String.valueOf(e));
                    }
                    break;
                case updateJob:

                    Utils.hideProgressDialog();

                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            Utils.showToast(jObj.getString("message"), getContext());
                            CompanyDashboard.activityCompany.onBackPressed();
                        } else {
                            Utils.showToast("Try Again!!", getContext());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();

                        Log.d("new exception:", String.valueOf(e));
                    }
                    break;
            }
        }
    }

}
