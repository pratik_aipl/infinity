package com.nfinitydynamics.fragment;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.nfinitydynamics.R;
import com.nfinitydynamics.activity.MainActivity;
import com.nfinitydynamics.adapter.NothingSelectedSpinnerAdapter;
import com.nfinitydynamics.model.CountryModel;
import com.nfinitydynamics.model.PassportModel;
import com.nfinitydynamics.utils.AsynchTaskListner;
import com.nfinitydynamics.utils.CallRequest;
import com.nfinitydynamics.utils.Constant;
import com.nfinitydynamics.utils.JsonParserUniversal;
import com.nfinitydynamics.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import static com.nfinitydynamics.activity.MainActivity.toolbar;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddPassportFragment extends Fragment implements AsynchTaskListner {
    public EditText edtPNo, edtValidity;
    public SharedPreferences shared;
    public String candidateId;

    public static String passportId, passportNo, validity, placeOfIssue="", productid;
    public Button btnSave;
    public AddPassportFragment instance;
    public View view;
    public int s;
    public ArrayList<CountryModel> countryModelArrayList=new ArrayList<CountryModel>();
    public ArrayList<String> countyStringList=new ArrayList<>();
    public int selectedPos;
    public Spinner spPlaceIssue;
    public JsonParserUniversal jParser;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_add_passport, container, false);
        instance = this;
        toolbar.setTitle("Add Passport Details");
        shared = getContext().getSharedPreferences(getContext().getPackageName(), 0);
        jParser = new JsonParserUniversal();
        candidateId = shared.getString("userId", "");
        edtPNo = view.findViewById(R.id.edt_pnumber);
        edtValidity = view.findViewById(R.id.edt_validity);
        spPlaceIssue =  view.findViewById(R.id.sp_place_issue);
        btnSave = view.findViewById(R.id.btn_save);
        getCountry();
        Bundle b = getArguments();
        s = b.getInt("value");
        if (s == 1) {
            toolbar.setTitle("Edit Passport Details");
            passportId = b.getString("id");
            passportNo = b.getString("number");
            validity = b.getString("validity");
            placeOfIssue = b.getString("place");
            edtPNo.setText(passportNo);
            Utils.converDateToDDMMYY(edtValidity,validity);


        }else{
            placeOfIssue="";
        }
        edtValidity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager)getContext(). getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                Utils.generateDatePicker(getContext(), edtValidity);
            }
        });
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                passportNo = edtPNo.getText().toString().trim();
                validity = edtValidity.getText().toString().trim();
            //    placeOfIssue = edtPlace.getText().toString().trim();
                Calendar cal = Calendar.getInstance();
                Date sysDate = cal.getTime();
                Date intValidity = null;
                try {
                    intValidity = new SimpleDateFormat("dd-MMM-yyyy").parse(validity);
                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                    validity = formatter.format(intValidity);


                } catch (ParseException e) {
                    e.printStackTrace();
                }
                if (passportNo.equals("")) {
                    edtPNo.findFocus();
                    Utils.showAlert("Please Enter Passport No.", getContext());
                } else if (TextUtils.isEmpty(placeOfIssue)) {
                    spPlaceIssue.findFocus();
                    Utils.showAlert("Please Select Country", getContext());
                } else if (validity.equals("")) {
                    edtValidity.findFocus();
                    Utils.showAlert("Please Enter Validity Date", getContext());
                } else if(sysDate.compareTo(intValidity)>0){
                    Utils.showAlert("Please Enter Valid Validity", getContext());
                }else {
                    if (s ==1) {
                        new CallRequest(instance).updatePassportDetail(candidateId, passportNo, placeOfIssue, validity, "1", passportId);
                    } else if(s==0) {
                        new CallRequest(instance).addPassportDetail(candidateId, passportNo, placeOfIssue, validity, "0");
                    }
                }

            }
        });

        spPlaceIssue.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                try {
                    placeOfIssue = countryModelArrayList.get(i - 1).getCountryID();
                } catch (IndexOutOfBoundsException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        return view;
    }
    private void getCountry() {
        new CallRequest(instance).getCountry();
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {

        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result Job : " + result);
            switch (request) {

                case addPassportDetail:

                    Utils.hideProgressDialog();

                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {

                            Utils.showToast("Added Successfully", getContext());
                            MainActivity.activity.onBackPressed();

                        } else {
                            Utils.showToast(jObj.getString("Try Again"), getContext());
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                break;
                case updatePassportDetail:

                    Utils.hideProgressDialog();

                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {

                            Utils.showToast("Updated Successfully", getContext());
                            MainActivity.activity.onBackPressed();

                        } else {
                            Utils.showToast(jObj.getString("Try Again"), getContext());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                case  getCountryList:

                    Utils.hideProgressDialog();

                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            JSONArray ob1 = jObj.getJSONArray("data");
                            CountryModel countryModel ;
                            for (int i = 0; i < ob1.length(); i++) {
                                countryModel = new CountryModel();
                                countryModel = (CountryModel) jParser.parseJson(ob1.getJSONObject(i), new CountryModel());
                                countryModelArrayList.add(countryModel);
                                if (placeOfIssue != null) {
                                    if (placeOfIssue.equalsIgnoreCase(countryModel.getCountryName())) {
                                        selectedPos = i + 1;
                                    }
                                }
                                countyStringList.add(countryModel.getCountryName());
                            }
                            ArrayAdapter aa = new ArrayAdapter(getActivity(), R.layout.spinner_text, countyStringList);
                            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spPlaceIssue.setAdapter(new NothingSelectedSpinnerAdapter(
                                    aa, R.layout.country_place_issue,
                                    getActivity()));
                            spPlaceIssue.setSelection(selectedPos);
                        }
                        else {
                            Utils.showToast("Try Again!!",getContext());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d("new exception:", String.valueOf(e));
                    }
                    break;
            }
        }
    }
}