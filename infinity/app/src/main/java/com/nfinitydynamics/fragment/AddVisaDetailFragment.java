package com.nfinitydynamics.fragment;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.nfinitydynamics.R;
import com.nfinitydynamics.activity.MainActivity;
import com.nfinitydynamics.adapter.NothingSelectedSpinnerAdapter;
import com.nfinitydynamics.model.CountryModel;
import com.nfinitydynamics.utils.AsynchTaskListner;
import com.nfinitydynamics.utils.CallRequest;
import com.nfinitydynamics.utils.Constant;
import com.nfinitydynamics.utils.JsonParserUniversal;
import com.nfinitydynamics.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import static com.nfinitydynamics.activity.MainActivity.toolbar;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddVisaDetailFragment extends Fragment implements AsynchTaskListner {
    public EditText edtVissaNo, edtValidity;
    public String visaNo, placeOfIssue, validity, candidateId, id, VisaType;
    public Button btnSave;
    public int s;
    public SharedPreferences shared;
    public AddVisaDetailFragment instance;
    public Spinner spJobType;
    public String[] visaType = {"Tourist Visa", "Student Visa", "Business Visa", "Seaman Visa", "USA C1/D", "USA B1/B2",
    "Schengen Category A","Schengen Category C"};
    public ArrayAdapter aa1;
    public ArrayList<CountryModel> countryModelArrayList = new ArrayList<CountryModel>();
    public ArrayList<String> countyStringList = new ArrayList<>();
    public int selectedPos;
    public Spinner spPlaceIssue;
    public JsonParserUniversal jParser;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view;
        view = inflater.inflate(R.layout.fragment_add_visa_detail, container, false);
        jParser = new JsonParserUniversal();
        toolbar.setTitle("Add Visa Details");
        instance = this;
        shared = getActivity().getSharedPreferences(getContext().getPackageName(), 0);
        candidateId = shared.getString("userId", "");
        // edtVissaNo = view.findViewById(R.id.edt_visa_number);
        spPlaceIssue = view.findViewById(R.id.sp_place_issue);
        edtValidity = view.findViewById(R.id.edt_validity);
        btnSave = view.findViewById(R.id.btn_save);
        spJobType = view.findViewById(R.id.spinner_job_type);
        getCountry();
        aa1 = new ArrayAdapter(getContext(), android.R.layout.simple_spinner_item, visaType);
        aa1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spJobType.setAdapter(new NothingSelectedSpinnerAdapter(
                aa1, R.layout.visa_type,
                getActivity()));
        //spJobType.setAdapter(aa1);
        s = getArguments().getInt("value");
        if (s == 1) {
            toolbar.setTitle("Edit Visa Details");
            id = getArguments().getString("id");
            Log.d("dod detail id---", id);
            // visaNo = getArguments().getString("number");
            validity = getArguments().getString("validity");
            placeOfIssue = getArguments().getString("place");
            // edtVissaNo.setText(visaNo);
            Utils.converDateToDDMMYY(edtValidity, validity);

            VisaType = getArguments().getString("visaType");

            if (VisaType != null) {
                int spinnerPosition = aa1.getPosition(VisaType);
                spJobType.setSelection(spinnerPosition + 1);
            }
        }


        spJobType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                try {
                    VisaType = visaType[i - 1];
                } catch (IndexOutOfBoundsException e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //   visaNo = edtVissaNo.getText().toString().trim();
                //  placeOfIssue = edtPlaceOfIssue.getText().toString().trim();
                validity = edtValidity.getText().toString().trim();
                Calendar cal = Calendar.getInstance();
                Date sysDate = cal.getTime();
                Date intValidity = null;
                try {
                    intValidity = new SimpleDateFormat("dd-MMM-yyyy").parse(validity);
                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                    validity = formatter.format(intValidity);


                } catch (ParseException e) {
                    e.printStackTrace();
                }

                if (TextUtils.isEmpty(placeOfIssue)) {
                    Utils.showAlert("Please Select Country", getContext());
                } else if (validity.equals("")) {
                    Utils.showAlert("Please Enter Validity", getContext());
                } else if (TextUtils.isEmpty(VisaType)) {
                    Utils.showAlert("Please Select Visa Type", getContext());
                } else if (sysDate.compareTo(intValidity) > 0) {
                    Utils.showAlert("Please Enter Valid Validity", getContext());
                } else {
                    if (s == 0) {
                        new CallRequest(instance).addVisa(candidateId, visaNo, placeOfIssue, validity, VisaType, "0");
                    } else if (s == 1) {
                        new CallRequest(instance).updateVisa(candidateId, visaNo, placeOfIssue, validity, id, VisaType, "1");
                    }
                }

            }
        });
        edtValidity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                Utils.generateDatePicker(getContext(), edtValidity);
            }
        });
        spPlaceIssue.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                try {
                    placeOfIssue = countryModelArrayList.get(i - 1).getCountryID();
                } catch (IndexOutOfBoundsException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        return view;
    }

    private void getCountry() {
        new CallRequest(instance).getCountry();
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case addVisa:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            Utils.showToast("Added Successfully", getContext());
                            MainActivity.activity.onBackPressed();
                        } else {
                            Utils.showToast("Try Again!!", getContext());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();

                        Log.d("new exception:", String.valueOf(e));
                    }
                    break;


                case updateVisa:

                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            Utils.showToast("Updated Successfully", getContext());
                            MainActivity.activity.onBackPressed();
                        } else {
                            Utils.showToast("Try Again!!", getContext());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();

                        Log.d("new exception:", String.valueOf(e));
                    }
                    break;
                case getCountryList:

                    Utils.hideProgressDialog();

                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            JSONArray ob1 = jObj.getJSONArray("data");
                            CountryModel countryModel;
                            for (int i = 0; i < ob1.length(); i++) {
                                countryModel = new CountryModel();
                                countryModel = (CountryModel) jParser.parseJson(ob1.getJSONObject(i), new CountryModel());
                                countryModelArrayList.add(countryModel);
                                if (placeOfIssue != null) {
                                    if (placeOfIssue.equalsIgnoreCase(countryModel.getCountryName())) {
                                        selectedPos = i + 1;
                                    }
                                }
                                countyStringList.add(countryModel.getCountryName());
                            }
                            ArrayAdapter aa = new ArrayAdapter(getActivity(), R.layout.spinner_text, countyStringList);
                            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spPlaceIssue.setAdapter(new NothingSelectedSpinnerAdapter(
                                    aa, R.layout.country_place_issue,
                                    getActivity()));
                            spPlaceIssue.setSelection(selectedPos);
                        } else {
                            Utils.showToast("Try Again!!", getContext());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d("new exception:", String.valueOf(e));
                    }

            }
        }
    }
}
