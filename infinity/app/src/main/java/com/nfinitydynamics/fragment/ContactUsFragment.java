package com.nfinitydynamics.fragment;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nfinitydynamics.R;
import com.nfinitydynamics.model.ContactUSModel;
import com.nfinitydynamics.utils.AsynchTaskListner;
import com.nfinitydynamics.utils.CallRequest;
import com.nfinitydynamics.utils.Constant;
import com.nfinitydynamics.utils.JsonParserUniversal;
import com.nfinitydynamics.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.nfinitydynamics.activity.MainActivity.toolbar;


public class ContactUsFragment extends Fragment implements AsynchTaskListner {
    public ContactUsFragment instance;
    public JsonParserUniversal jParser;
    public SharedPreferences shared;
    public ArrayList<ContactUSModel> list = new ArrayList<>();
    public String address, Help_Line, Enquires, Fax, Email_1, Email_2, Email_3;
    public TextView txtAddress, txtHelpLine, txtEnquires, txtFlaxNo, txtEmail, txtEmail2, txtEmail3;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view;
        view = inflater.inflate(R.layout.fragment_contact_us, container, false);
        instance = this;
        shared = getActivity().getSharedPreferences(getContext().getPackageName(), 0);
        jParser = new JsonParserUniversal();
        toolbar.setTitle("Contact Us");
        txtAddress = (TextView) view.findViewById(R.id.txtAddress);
        txtHelpLine = (TextView) view.findViewById(R.id.txtHelpLine);
        txtEnquires = view.findViewById(R.id.txtEnquires);
        txtFlaxNo = view.findViewById(R.id.txtFlaxNo);
        txtEmail = view.findViewById(R.id.txtEmail);
        txtEmail2 = view.findViewById(R.id.txtEmail2);
        txtEmail3 = view.findViewById(R.id.txtEmail3);
        new CallRequest(instance).contactUs();
        return view;
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case contactUs:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {

                            //  Utils.showToast(jObj.getString("message"), getContext());
                            JSONObject ob1 = jObj.getJSONObject("data");

                            ContactUSModel contactUSModel;
                            contactUSModel = new ContactUSModel();
                            contactUSModel = (ContactUSModel) jParser.parseJson(ob1, new ContactUSModel());
                            txtAddress.setText(contactUSModel.getAddress());
                            txtEnquires.setText(contactUSModel.getEnquires());
                            txtHelpLine.setText(contactUSModel.getHelp_Line());
                            txtEmail.setText(contactUSModel.getEmail_1());
                            txtEmail2.setText(contactUSModel.getEmail_2());
                            txtEmail3.setText(contactUSModel.getEmail_3());
                            txtFlaxNo.setText(contactUSModel.getFax());
                        } else {
                            Utils.showToast("Try Again!!", getContext());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();

                        Log.d("new exception:", String.valueOf(e));
                    }
                    break;
            }
        }
    }
}
