package com.nfinitydynamics.fragment.companyFragment;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.nfinitydynamics.R;

import static com.nfinitydynamics.activity.CompanyActivity.CompanyDashboard.toolbar;

/**
 * A simple {@link Fragment} subclass.
 */
public class DownloadedResumeFragment extends Fragment {

    public DownloadedResumeFragment instance;
    WebView myWebView;
    public SharedPreferences shared;
    public String url;
    View view;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view= inflater.inflate(R.layout.fragment_downloaded_resume, container, false);
        instance=this;
        toolbar.setTitle("Build Resume");
        myWebView = view.findViewById(R.id.webview);
        shared = getActivity().getSharedPreferences(getContext().getPackageName(), 0);
        url = getArguments().getString("url");
        myWebView.getSettings().setJavaScriptEnabled(true);
        String pdf = "https://drive.google.com/viewerng/viewer?embedded=true&url=" + url;
        myWebView.loadUrl(pdf);
        return view;
    }

}
