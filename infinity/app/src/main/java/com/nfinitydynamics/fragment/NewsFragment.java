package com.nfinitydynamics.fragment;


import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.nfinitydynamics.R;
import com.nfinitydynamics.adapter.NewsAdapter;
import com.nfinitydynamics.model.NewsModel;
import com.nfinitydynamics.utils.AsynchTaskListner;
import com.nfinitydynamics.utils.CallRequest;
import com.nfinitydynamics.utils.Constant;
import com.nfinitydynamics.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.nfinitydynamics.activity.MainActivity.toolbar;


public class NewsFragment extends Fragment implements AsynchTaskListner{


    public ListView lvNews;
    public NewsFragment newsInstance;
    public ArrayList<NewsModel> newsList = new ArrayList<>();
    public View view;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_news, container, false);
        newsInstance = this;
        toolbar.setTitle("News");
        lvNews = view. findViewById(R.id.lv_news);
        getNews();
        return view;
    }


    private void getNews() {
        newsList.clear();
        new CallRequest(newsInstance).getNews();
    }


    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case get_news:

                     Utils.hideProgressDialog();

                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {

                          //  Utils.showToast(jObj.getString("message"), getContext());

                            JSONArray ob1=jObj.getJSONArray("data");
                            for (int i=0;i<ob1.length();i++) {
                                JSONObject ob = ob1.getJSONObject(i);
                                String newsId = ob.getString("NewsID");
                                String newsTitle = ob.getString("NewsTitle");
                                String newImageURL = ob.getString("NewsImageURL");
                                String newsDesc=ob.getString("Description");
                                String newsDate = ob.getString("NewsDate");
                              newsList.add(new NewsModel(newsId,newImageURL,newsTitle,newsDesc,newsDate));

                            }
                            NewsAdapter adapter = new NewsAdapter(getContext(), R.layout.layout_news, newsList);
                            lvNews.setAdapter(adapter);


                        }

                    } catch (JSONException e) {
                        e.printStackTrace();

                        Log.d("new exception:", String.valueOf(e));
                    }
                    break;
            }
        }
    }




}
