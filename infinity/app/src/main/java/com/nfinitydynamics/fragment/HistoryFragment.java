package com.nfinitydynamics.fragment;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.nfinitydynamics.R;
import com.nfinitydynamics.adapter.JobAppliedAdapter;
import com.nfinitydynamics.model.JobAppliedModel;
import com.nfinitydynamics.utils.AsynchTaskListner;
import com.nfinitydynamics.utils.CallRequest;
import com.nfinitydynamics.utils.Constant;
import com.nfinitydynamics.utils.JsonParserUniversal;
import com.nfinitydynamics.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.nfinitydynamics.activity.MainActivity.toolbar;

/**
 * A simple {@link Fragment} subclass.
 */
public class HistoryFragment extends Fragment implements AsynchTaskListner {
    public ArrayList<JobAppliedModel> appliedJobList = new ArrayList<>();
    public ListView lvJobApplied;
    public View view;
    public HistoryFragment instance;
    public SharedPreferences shared;
    public String candidateId;
    public JsonParserUniversal jParser;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_history, container, false);
        shared = getContext().getSharedPreferences(getContext().getPackageName(), 0);
        jParser=new JsonParserUniversal();
        candidateId = shared.getString("userId", "");
        lvJobApplied =  view.findViewById(R.id.lv_job_applied);
        instance = this;
        toolbar.setTitle("History");
        getAppliedJob();
        return view;
    }

    private void getAppliedJob() {
        new CallRequest(instance).getAppliedJob(candidateId);
    }
    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result Job : " + result);
            switch (request) {
                case getAppliedJob:
                    Utils.hideProgressDialog();
                    appliedJobList.clear();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            JSONArray ob1 = jObj.getJSONArray("data");
                            JobAppliedModel appliedJobModel;
                            for (int i = 0; i < ob1.length(); i++) {
                                JSONObject jObject = ob1.getJSONObject(i);
                                appliedJobModel = new JobAppliedModel();
                                appliedJobModel = (JobAppliedModel) jParser.parseJson(jObject, new JobAppliedModel());
                                appliedJobList.add(appliedJobModel);
                            }
                            JobAppliedAdapter appliedJobAdapter = new JobAppliedAdapter(getContext(), R.layout.layout_job_applied, appliedJobList);
                            lvJobApplied.setAdapter(appliedJobAdapter);

//                            Intent intent = new Intent(getContext(), MainActivity.class);
//                            startActivity(intent);
//                            finish();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }

}
