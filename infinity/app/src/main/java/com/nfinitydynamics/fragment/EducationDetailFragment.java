package com.nfinitydynamics.fragment;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.nfinitydynamics.R;
import com.nfinitydynamics.activity.MainActivity;
import com.nfinitydynamics.adapter.EducationAdapter;
import com.nfinitydynamics.model.EducationModel;
import com.nfinitydynamics.utils.AsynchTaskListner;
import com.nfinitydynamics.utils.CallRequest;
import com.nfinitydynamics.utils.Constant;
import com.nfinitydynamics.utils.JsonParserUniversal;
import com.nfinitydynamics.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.nfinitydynamics.activity.MainActivity.toolbar;

/**
 * A simple {@link Fragment} subclass.
 */
public class EducationDetailFragment extends Fragment implements AsynchTaskListner {
    public FloatingActionButton fabAdd ;
    public SharedPreferences shared;
    public ListView lvEducation;
    public String candidateId;
    public ArrayList<EducationModel> eduList = new ArrayList<>();
    public JsonParserUniversal jParser;
    public static EducationDetailFragment instance;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view;
        view= inflater.inflate(R.layout.fragment_education_detail, container, false);
        instance=this;
        shared = getActivity().getSharedPreferences(getContext().getPackageName(), 0);
        candidateId = shared.getString("userId","");
        jParser=new JsonParserUniversal();
        fabAdd = view. findViewById(R.id.fab_add);
        lvEducation=view.findViewById(R.id.lv_education);
        toolbar.setTitle("Education Details");
        eduList.clear();
        fabAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AddEducationFragment fragment=new AddEducationFragment();
                Bundle args = new Bundle();
                args.putInt("value",0);
                fragment.setArguments(args);
                MainActivity.changeFragment(fragment,true);
            }
        });

        getEducationDetail();

        return view;
    }

    private void getEducationDetail() {
        new CallRequest(instance).getEducationDetail(candidateId);
    }
    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case getEducationDetail:
                    Utils.hideProgressDialog();
                    eduList.clear();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {

                            //  Utils.showToast(jObj.getString("message"), getContext());
                            JSONArray ob1 = jObj.getJSONArray("data");
                            EducationModel educationModel;
                            for (int i = 0; i < ob1.length(); i++) {
                                JSONObject jObject = ob1.getJSONObject(i);
                                educationModel = new EducationModel();
                                educationModel = (EducationModel) jParser.parseJson(jObject, new EducationModel());
                                eduList.add(educationModel);
                            }
                            EducationAdapter latestJobAdapter = new EducationAdapter(getContext(), R.layout.layout_education, eduList);
                            lvEducation.setAdapter(latestJobAdapter);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;

                case deleteEducation:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            Utils.showToast("Deleted Successfully",getContext());
                            getEducationDetail();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }
}
