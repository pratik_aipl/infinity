package com.nfinitydynamics.fragment;

import android.content.SharedPreferences;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.nfinitydynamics.R;
import com.nfinitydynamics.adapter.FeaturedJobAdapter;
import com.nfinitydynamics.adapter.LatestJobAdapter;
import com.nfinitydynamics.model.FeaturedJobModel;
import com.nfinitydynamics.model.LatestJobModel;
import com.nfinitydynamics.utils.AsynchTaskListner;
import com.nfinitydynamics.utils.CallRequest;
import com.nfinitydynamics.utils.Constant;
import com.nfinitydynamics.utils.JsonParserUniversal;

import com.nfinitydynamics.utils.RecyclerTouchListener;
import com.nfinitydynamics.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.nfinitydynamics.activity.MainActivity.changeFragment;
import static com.nfinitydynamics.activity.MainActivity.toolbar;

public class HomeFragment extends Fragment implements AsynchTaskListner {

    public View view;
    public ListView lvLatestJobs;
    public RecyclerView recyclerFeaturedJob;
    public HomeFragment homeInstance;
    public ArrayList<LatestJobModel> latestJobList = new ArrayList<>();
    public ArrayList<FeaturedJobModel> featuredJobList = new ArrayList<>();
    public FeaturedJobAdapter featuredAdapter;
    public JsonParserUniversal jParser;
    public SharedPreferences shared;
    public FeaturedJobModel fJobModel;
    public String image, jobTitle, company, shipType, description, companyId, jobId,rank,isApplied,experienceRange;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_home, container, false);
        lvLatestJobs =  view.findViewById(R.id.list_view_latest_job);
        homeInstance = this;
        toolbar.setTitle("Dash Board");
        jParser = new JsonParserUniversal();
        shared = getActivity().getSharedPreferences(getContext().getPackageName(), 0);
        recyclerFeaturedJob = (RecyclerView) view.findViewById(R.id.recycler_view_featured);
        getFeaturedJob();
        setHasOptionsMenu(true);
        return view;
    }

    private void getLatestJob() {
        String candidateId = shared.getString("userId", "");
        new CallRequest(homeInstance).getLatest(candidateId);
    }

    private void getFeaturedJob() {

        String candidateId = shared.getString("userId", "");
        new CallRequest(homeInstance).getFeatured(candidateId);

    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {

        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case featureJob:
                    featuredJobList.clear();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {

                            //Utils.showToast(jObj.getString("message"), getContext());
                            JSONArray ob1 = jObj.getJSONArray("data");

                            for (int i = 0; i < ob1.length(); i++) {
                                fJobModel = new FeaturedJobModel();
                                Log.d("cheak status:---", "shdgcusduh");
                                JSONObject jObject = ob1.getJSONObject(i);
                                fJobModel = (FeaturedJobModel) jParser.parseJson(jObject, new FeaturedJobModel());
                                featuredJobList.add(fJobModel);
                            }

                            if (featuredJobList.size() > 0) {
                                featuredAdapter = new FeaturedJobAdapter(featuredJobList);
                                recyclerFeaturedJob.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
                                recyclerFeaturedJob.setItemAnimator(new DefaultItemAnimator());
                                recyclerFeaturedJob.setAdapter(featuredAdapter);
                                featuredAdapter.notifyDataSetChanged();
                            }
//                            Intent intent = new Intent(getContext(), MainActivity.class);
//                            startActivity(intent);
//                            finish();


                            recyclerFeaturedJob.addOnItemTouchListener(new RecyclerTouchListener(getContext(), recyclerFeaturedJob, new RecyclerTouchListener.ClickListener() {
                                @Override
                                public void onClick(View view, int position) {
                                    JobDetailFragment jobDetailFragment = new JobDetailFragment();
                                    Bundle b = new Bundle();
                                    FeaturedJobModel model = featuredJobList.get(position);
                                    image = model.getLogoImgPath();
                                    jobTitle = model.getJobTitle();
                                    shipType = model.getShipType();
                                    company = model.getCompanyName();
                                    description = model.getDescription();
                                    companyId = model.getCompanyID();
                                    jobId = model.getJobID();
                                    isApplied= String.valueOf(model.getIs_Applied_Job());
                                    Log.d("jobTitle =====", jobTitle);
                                    rank=model.getRank();
                                    b.putString("image", image);
                                    b.putString("jobTitle", jobTitle);
                                    b.putString("shipType", shipType);
                                    b.putString("company", company);
                                    b.putString("description", description);
                                    b.putString("companyId", companyId);
                                    b.putString("jobId", jobId);
                                    b.putString("rank", rank);
                                    b.putString("isApplied", isApplied);
                                    b.putString("JobEndDate",model.getJobEndDate());
                                    b.putString("fromExperience",model.getFromExperience());
                                    b.putString("toExperience",model.getToExperience());
                                    jobDetailFragment.setArguments(b);
                                    changeFragment(jobDetailFragment, true);
                                }

                                @Override
                                public void onLongClick(View view, int position) {

                                }
                            }));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    getLatestJob();
                    break;

                case latestJob:

                    latestJobList.clear();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {

                            JSONArray ob1 = jObj.getJSONArray("data");

                            LatestJobModel lJobModel;
                            for (int i = 0; i < ob1.length(); i++) {
                                JSONObject jObject = ob1.getJSONObject(i);
                                lJobModel = new LatestJobModel();
                                lJobModel = (LatestJobModel) jParser.parseJson(jObject, new LatestJobModel());
                                latestJobList.add(lJobModel);
                            }
                            try {
                                LatestJobAdapter latestJobAdapter = new LatestJobAdapter(getContext(), R.layout.layout_home_listview, latestJobList);
                                lvLatestJobs.setAdapter(latestJobAdapter);
                            }catch (Exception e){
                                e.printStackTrace();
                            }

                            lvLatestJobs.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                    JobDetailFragment jobDetailFragment = new JobDetailFragment();
                                    Bundle b = new Bundle();
                                    LatestJobModel model = latestJobList.get(i);
                                    image = model.getLogoImgPath();
                                    jobTitle = model.getJobTitle();
                                    shipType = model.getShipType();
                                    company = model.getCompanyName();
                                    description = model.getDescription();
                                    companyId = model.getCompanyID();
                                    jobId = model.getJobID();
                                    rank=model.getRank();
                                    Log.d("jobTitle =====", jobTitle);
                                    isApplied= String.valueOf(model.getIs_Applied_Job());
                                    Log.d("compppp--", companyId);
                                    Log.d("value", image + jobTitle + shipType + company + description + companyId);
                                    b.putString("image", image);
                                    b.putString("jobTitle", jobTitle);
                                    b.putString("shipType", shipType);
                                    b.putString("company", company);
                                    b.putString("description", description);
                                    b.putString("companyId", companyId);
                                    b.putString("jobId", jobId);
                                    b.putString("rank", rank);
                                    b.putString("isApplied", isApplied);
                                    b.putString("JobEndDate",model.getJobEndDate());
                                    b.putString("fromExperience",model.getFromExperience());
                                    b.putString("toExperience",model.getToExperience());
                                    jobDetailFragment.setArguments(b);
                                    changeFragment(jobDetailFragment, true);
                                }
                            });
//                            Intent intent = new Intent(getContext(), MainActivity.class);
//                            startActivity(intent);
//                            finish();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Utils.hideProgressDialog();
                    break;
            }
        }
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.candidate, menu);

        //  MenuItem item = menu.findItem(R.id.login).setVisible(true);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.login) {
            changeFragment(new ProfileFragment(), true);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
