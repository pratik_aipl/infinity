package com.nfinitydynamics.fragment;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.nfinitydynamics.R;
import com.nfinitydynamics.activity.MainActivity;
import com.nfinitydynamics.adapter.SeamanBookAdapter;
import com.nfinitydynamics.model.SeamanBookModel;
import com.nfinitydynamics.utils.AsynchTaskListner;
import com.nfinitydynamics.utils.CallRequest;
import com.nfinitydynamics.utils.Constant;
import com.nfinitydynamics.utils.JsonParserUniversal;
import com.nfinitydynamics.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.nfinitydynamics.activity.MainActivity.toolbar;

/**
 * A simple {@link Fragment} subclass.
 */
public class SeamanBookFragment extends Fragment implements AsynchTaskListner {
    public FloatingActionButton fabAdd ;
    public SharedPreferences shared;
    public ListView lvSeaman;
    public String candidateId;
    public ArrayList<SeamanBookModel> bookList = new ArrayList<>();
    public JsonParserUniversal jParser;
    public static SeamanBookFragment instance;
    public  SeamanBookAdapter latestJobAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view;
        view= inflater.inflate(R.layout.fragment_seaman_book, container, false);
        instance=this;
        shared = getActivity().getSharedPreferences(getContext().getPackageName(), 0);
        candidateId = shared.getString("userId","");
        jParser=new JsonParserUniversal();
        fabAdd = view. findViewById(R.id.fab_add);
        lvSeaman=view.findViewById(R.id.lv_seaman_book);
        toolbar.setTitle("Seaman Book Details");
        lvSeaman=view.findViewById(R.id.lv_seaman_book);


        fabAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AddSemeanBookFragment fragment=new AddSemeanBookFragment();
                Bundle args = new Bundle();
                args.putInt("value",0);
                fragment.setArguments(args);
                MainActivity.changeFragment(fragment,true);
            }
        });

        getSeamaBook();

        return  view;
    }

    private void getSeamaBook() {

        new CallRequest(instance).getSeamenBook(candidateId);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case getSemeanBook:
                    Utils.hideProgressDialog();
                    bookList.clear();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {

                            //  Utils.showToast(jObj.getString("message"), getContext());
                            JSONArray ob1 = jObj.getJSONArray("data");

                            SeamanBookModel sBookModel;
                            for (int i = 0; i < ob1.length(); i++) {
                                JSONObject jObject = ob1.getJSONObject(i);
                                sBookModel = new SeamanBookModel();
                                sBookModel = (SeamanBookModel) jParser.parseJson(jObject, new SeamanBookModel());
                                bookList.add(sBookModel);
                            }
                             latestJobAdapter = new SeamanBookAdapter(getContext(), R.layout.layout_seaman_book_list, bookList);
                            lvSeaman.setAdapter(latestJobAdapter);


//                            Intent intent = new Intent(getContext(), MainActivity.class);
//                            startActivity(intent);
//                            finish();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;

                case deleteSemean:
                    Utils.hideProgressDialog();
                    bookList.clear();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            // Utils.showToast(jObj.getString("message"), getContext());
                            Utils.showToast("Deleted Successfully",getContext());

                            getSeamaBook();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }
}
