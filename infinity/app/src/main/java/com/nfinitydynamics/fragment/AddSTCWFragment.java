package com.nfinitydynamics.fragment;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.nfinitydynamics.R;
import com.nfinitydynamics.activity.MainActivity;
import com.nfinitydynamics.adapter.NothingSelectedSpinnerAdapter;
import com.nfinitydynamics.model.CountryModel;
import com.nfinitydynamics.model.STCWCertificateModel;
import com.nfinitydynamics.model.STCWModel;
import com.nfinitydynamics.utils.AsynchTaskListner;
import com.nfinitydynamics.utils.CallRequest;
import com.nfinitydynamics.utils.Constant;
import com.nfinitydynamics.utils.JsonParserUniversal;
import com.nfinitydynamics.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import static com.nfinitydynamics.activity.MainActivity.toolbar;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddSTCWFragment extends Fragment implements AsynchTaskListner {
    public EditText edtNumber, edtValidity;
    public Spinner spinnerProficiency;
    public String number, placeIssue, validity, candidateId, id, uNumber, uPlace, uValidity, ucertificateType;
    public AddSTCWFragment instance;
    public SharedPreferences shared;
    public STCWModel stcwModel;
    public Button btnSave;
    public int s;
    public ArrayList<String> certificateList = new ArrayList<>();
    public ArrayAdapter aaCertificate = null;
    public ArrayList<STCWCertificateModel> cerList = new ArrayList<>();
    public ArrayList<CountryModel> countryModelArrayList = new ArrayList<CountryModel>();
    public ArrayList<String> countyStringList = new ArrayList<>();
    public int selectedPos;
    public Spinner spPlaceIssue;
    public JsonParserUniversal jParser;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view;
        view = inflater.inflate(R.layout.fragment_add_stwc, container, false);
        instance = this;
        jParser = new JsonParserUniversal();
        toolbar.setTitle("Add STCW Details");
        stcwModel = (STCWModel) getArguments().getSerializable("STCWModel");
        edtNumber = view.findViewById(R.id.edt_number);
        spPlaceIssue = view.findViewById(R.id.sp_place_issue);
        edtValidity = view.findViewById(R.id.edt_validity);
        btnSave = view.findViewById(R.id.btn_save_experience);
        spinnerProficiency = view.findViewById(R.id.spinner_proficiency);
        getSTWCCertificateType();
        Bundle b = getArguments();
        s = b.getInt("value");
        if (s == 1) {
            toolbar.setTitle("Edit STCW Details");
            edtNumber.setText(stcwModel.CertificateNo);
            try {
                Utils.converDateToDDMMYY(edtValidity, stcwModel.getValidity());
            } catch (Exception e) {
                e.printStackTrace();
            }
            placeIssue = stcwModel.getAuthority();

            //ucertificateType = stcwModel.getCertificateType();
            ucertificateType = b.getString("certificateType");
            System.out.println("certificate type==" + ucertificateType);
            id = stcwModel.getSTWCCertiDetailsID();
            if (ucertificateType != null) {
                try {
                    int spinnerPosition = aaCertificate.getPosition(ucertificateType);
                    spinnerProficiency.setSelection(spinnerPosition + 1);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }


        spinnerProficiency.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                try {
                    ucertificateType = cerList.get(i - 1).getSTCWCertificateID();

                } catch (IndexOutOfBoundsException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }

        });
        shared = getActivity().getSharedPreferences(getContext().getPackageName(), 0);
        candidateId = shared.getString("userId", "");
        edtValidity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                Utils.generateDatePicker(getContext(), edtValidity);
            }
        });
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                number = edtNumber.getText().toString().trim();
                //   countryId = edtPlaceIssue.getText().toString().trim();
                validity = edtValidity.getText().toString().trim();
                Calendar cal = Calendar.getInstance();
                Date sysDate = cal.getTime();
                Date intValidity = null;
                try {
                    intValidity = new SimpleDateFormat("dd-MMM-yyyy").parse(validity);
                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                    validity = formatter.format(intValidity);


                } catch (ParseException e) {
                    e.printStackTrace();
                }
                if (TextUtils.isEmpty(ucertificateType)) {
                    Utils.showAlert("Please Select Certificate Type", getContext());
                } else if (number.equals("")) {
                    edtNumber.findFocus();
                    Utils.showAlert("Please Enter Certificate number", getContext());
                } else if (TextUtils.isEmpty(placeIssue)) {
                    spPlaceIssue.findFocus();
                    Utils.showAlert("Please Select Country", getContext());
                }/* else if (validity.equals("")) {
                    edtValidity.findFocus();
                    Utils.showAlert("Please Enter Validity", getContext());
                } */ else if (intValidity != null && sysDate.compareTo(intValidity) > 0) {
                    Utils.showAlert("Please Enter Valid Validity", getContext());
                } else {
                    if (s == 0) {
                        addCOCDetail();
                    } else if (s == 1) {

                        updateCOCDetail();
                    }
                }
            }
        });
        spPlaceIssue.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                try {
                    placeIssue = countryModelArrayList.get(i - 1).getCountryID();
                } catch (IndexOutOfBoundsException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        return view;
    }

    private void getSTWCCertificateType() {

        new CallRequest(instance).getSTWCCertificateType();
    }

    private void updateCOCDetail() {
        new CallRequest(instance).updateSTWC(candidateId, number, ucertificateType, placeIssue, validity, "1", id);
    }

    private void addCOCDetail() {
        new CallRequest(instance).addSTWC(candidateId, number, ucertificateType, placeIssue, validity, "0");
    }

    private void getCountry() {
        new CallRequest(instance).getCountry();
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case addSWTC:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            Utils.showToast("Added Successfully", getContext());
                            MainActivity.activity.onBackPressed();
                        } else {
                            Utils.showToast("Try Again!!", getContext());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();

                        Log.d("new exception:", String.valueOf(e));
                    }
                    break;
                case updateSTWC:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            Utils.showToast("Updated Successfully", getContext());
                            MainActivity.activity.onBackPressed();
                        } else {
                            Utils.showToast("Try Again!!", getContext());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();

                        Log.d("new exception:", String.valueOf(e));
                    }
                    break;
                case getSTWCCertificate:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            JSONArray ob1 = jObj.getJSONArray("data");
                            for (int i = 0; i < ob1.length(); i++) {
                                JSONObject ob = ob1.getJSONObject(i);
                                String certificateId = ob.getString("STCWCertificateID");
                                String certificateName = ob.getString("STCWCertificateType");
                                cerList.add(new STCWCertificateModel(certificateId, certificateName));
                                certificateList.add(certificateName);
                            }

//                            aaCertificate = new ArrayAdapter(getContext(), R.layout.spinner_text, certificateList);
//                            aaCertificate.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//                            spinnerProficiency.setAdapter(aaCertificate);

                            aaCertificate = new ArrayAdapter(getActivity(), R.layout.spinner_text, certificateList);
                            aaCertificate.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spinnerProficiency.setAdapter(new NothingSelectedSpinnerAdapter(
                                    aaCertificate, R.layout.certificate_noting_selected,
                                    getActivity()));
                            if (ucertificateType != null) {
                                int spinnerPosition = aaCertificate.getPosition(ucertificateType);
                                spinnerProficiency.setSelection(spinnerPosition + 1);
                            }

                        } else {
                            Utils.showToast(jObj.getString("message"), getContext());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();

                        Log.d("new exception:", String.valueOf(e));
                    }
                    getCountry();
                    break;
                case getCountryList:

                    Utils.hideProgressDialog();

                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            JSONArray ob1 = jObj.getJSONArray("data");
                            CountryModel countryModel;
                            for (int i = 0; i < ob1.length(); i++) {
                                countryModel = new CountryModel();
                                countryModel = (CountryModel) jParser.parseJson(ob1.getJSONObject(i), new CountryModel());
                                countryModelArrayList.add(countryModel);
                                if (placeIssue != null) {
                                    if (placeIssue.equalsIgnoreCase(countryModel.getCountryName())) {
                                        selectedPos = i + 1;
                                    }
                                }
                                countyStringList.add(countryModel.getCountryName());
                            }
                            ArrayAdapter aa = new ArrayAdapter(getActivity(), R.layout.spinner_text, countyStringList);
                            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spPlaceIssue.setAdapter(new NothingSelectedSpinnerAdapter(
                                    aa, R.layout.country_place_issue,
                                    getActivity()));
                            spPlaceIssue.setSelection(selectedPos);
                        } else {
                            Utils.showToast("Try Again!!", getContext());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d("new exception:", String.valueOf(e));
                    }
                    break;
            }
        }
    }

}
