package com.nfinitydynamics.fragment;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.nfinitydynamics.R;
import com.nfinitydynamics.activity.MainActivity;
import com.nfinitydynamics.adapter.PassportAdapter;
import com.nfinitydynamics.model.PassportModel;
import com.nfinitydynamics.utils.AsynchTaskListner;
import com.nfinitydynamics.utils.CallRequest;
import com.nfinitydynamics.utils.Constant;
import com.nfinitydynamics.utils.JsonParserUniversal;
import com.nfinitydynamics.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.nfinitydynamics.activity.MainActivity.toolbar;

public class PassportFragment extends Fragment implements AsynchTaskListner {
    public FloatingActionButton fabAdd ;
    public ListView lPassport;
    public EditText edtPNo, edtValidity, edtPlace;
    public static PassportFragment instance;
    public SharedPreferences shared;
    public String candidateId;
    public JsonParserUniversal jParser;
    public static String passportId, passportNo, validity, placeOfIssue,productid;
    public Button btnSave;
    public ArrayList<PassportModel> passportList = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view;
        instance = this;
        view = inflater.inflate(R.layout.fragment_passport, container, false);
        toolbar.setTitle("Passport Details");
        shared = getContext().getSharedPreferences(getContext().getPackageName(), 0);
        jParser = new JsonParserUniversal();
        shared = getActivity().getSharedPreferences(getContext().getPackageName(), 0);
        candidateId = shared.getString("userId","");
        jParser=new JsonParserUniversal();
        fabAdd = view. findViewById(R.id.fab_add);
        lPassport=view.findViewById(R.id.lv_passport);
        passportList.clear();

        fabAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AddPassportFragment fragment=new AddPassportFragment();
                Bundle args = new Bundle();
                args.putInt("value",0);
                fragment.setArguments(args);
                MainActivity.changeFragment(fragment,true);
            }
        });
        getPassportDetail();
        return view;
    }
//
//    private void updatePassportDetail() {
//
//        new CallRequest(instance).updatePassportDetail(candidateId, passportNo, placeOfIssue, validity, "1", passportId);
//    }
    private void getPassportDetail() {
        new CallRequest(instance).getPassportDetail(candidateId);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {

        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result Job : " + result);
            switch (request) {
                case getPassportDetail:
                    Utils.hideProgressDialog();
                    passportList.clear();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            JSONArray ob1 = jObj.getJSONArray("data");

                            PassportModel passportModel;
                            for (int i = 0; i < ob1.length(); i++) {
                                JSONObject jObject = ob1.getJSONObject(i);
                                passportModel = (PassportModel) jParser.parseJson(jObject, new PassportModel());
                                passportList.add(passportModel);
                            }
                            PassportAdapter passportAdapter = new PassportAdapter(getContext(), R.layout.layout_passport, passportList);
                            lPassport.setAdapter(passportAdapter);

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                case deletePassport:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {

                            Utils.showToast("Deleted Successfully",getContext());

                            getPassportDetail();

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }
}
