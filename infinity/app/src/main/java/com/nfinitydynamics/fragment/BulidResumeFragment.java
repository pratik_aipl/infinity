package com.nfinitydynamics.fragment;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.nfinitydynamics.R;
import com.nfinitydynamics.activity.MainActivity;
import com.nfinitydynamics.model.UserDetailModel;
import com.nfinitydynamics.utils.AsynchTaskListner;
import com.nfinitydynamics.utils.CallRequest;
import com.nfinitydynamics.utils.Constant;
import com.nfinitydynamics.utils.JsonParserUniversal;
import com.nfinitydynamics.utils.Utils;
import org.json.JSONException;
import org.json.JSONObject;

public class BulidResumeFragment extends Fragment implements AsynchTaskListner{
    public View view;
    public JsonParserUniversal jParser;
    public BulidResumeFragment instance;
    public String candidateId;
    public SharedPreferences shared;
    public WebView myWebView;
    public int value;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view= inflater.inflate(R.layout.fragment_bulid_resume, container, false);
        jParser=new JsonParserUniversal();
        instance=this;
        MainActivity.toolbar.setTitle("Build Resume");
        myWebView = view.findViewById(R.id.webview);
        shared = getActivity().getSharedPreferences(getContext().getPackageName(), 0);
        candidateId = shared.getString("userId", "");
        
        new CallRequest(instance).bulidResume(candidateId);
        return view;
    }
    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case bulidResume:

                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            UserDetailModel userDetailModel;
                            userDetailModel = new UserDetailModel();
                            userDetailModel = (UserDetailModel) jParser.parseJson(jObj, new UserDetailModel());
                            String resumeUrl=userDetailModel.getResume_url();
                            Log.d("resume::",resumeUrl);
                          //  myWebView.loadUrl("http://docs.google.com/gview?embedded=true&url="+resumeUrl);

                            myWebView.getSettings().setJavaScriptEnabled(true);
                            String pdf = "https://drive.google.com/viewerng/viewer?embedded=true&url=" + resumeUrl;
                            myWebView.loadUrl(pdf);
                            // new DownloadFile().execute(resumeUrl, "maven.pdf");
                          //  Utils.showToast("Downlod Successfully", getContext());
                           // MainActivity.changeFragment(new HistoryFragment(), false);
                        } else {
                            Utils.showToast(jObj.getString("message"), getContext());
                           MainActivity.activity.onBackPressed();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d("new exception:", String.valueOf(e));
                    }
                    break;
            }
        }
    }
}
