package com.nfinitydynamics.fragment;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.nfinitydynamics.R;
import com.nfinitydynamics.activity.MainActivity;
import com.nfinitydynamics.adapter.ExperienceAdapter;
import com.nfinitydynamics.model.ExperienceModel;
import com.nfinitydynamics.utils.AsynchTaskListner;
import com.nfinitydynamics.utils.CallRequest;
import com.nfinitydynamics.utils.Constant;
import com.nfinitydynamics.utils.JsonParserUniversal;
import com.nfinitydynamics.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.nfinitydynamics.activity.MainActivity.toolbar;

/**
 * A simple {@link Fragment} subclass.
 */
public class ExperienceFragment extends Fragment implements AsynchTaskListner {
    public FloatingActionButton fabAdd ;
    public ListView lvExperience;
    public View view;
    public String candidateId;
    public ArrayList<ExperienceModel> experienceList = new ArrayList<>();
    public JsonParserUniversal jParser;
    public static ExperienceFragment instance;
    public SharedPreferences shared;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        view= inflater.inflate(R.layout.fragment_experience, container, false);

        fabAdd = view. findViewById(R.id.fab_add);
        lvExperience=(ListView)view.findViewById(R.id.lv_experience);
        toolbar.setTitle("Experience Details");
        instance=this;
        shared = getContext().getSharedPreferences(getContext().getPackageName(), 0);
        candidateId = shared.getString("userId", "");
        jParser=new JsonParserUniversal();
        fabAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AddExperienceFragment fragment=new AddExperienceFragment();
                Bundle args = new Bundle();
                args.putInt("value",0);
                fragment.setArguments(args);
                MainActivity.changeFragment(fragment,true);
            }
        });

        getExperienceDetail();
        return view;
    }

    private void getExperienceDetail() {

        new CallRequest(instance).getExperienceDetail(candidateId);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result Job : " + result);
            switch (request) {
                case getExperience:
                    Utils.hideProgressDialog();
                    experienceList .clear();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {

                            //  Utils.showToast(jObj.getString("message"), getContext());
                            JSONArray ob1 = jObj.getJSONArray("data");

                            ExperienceModel appliedJobModel;
                            for (int i = 0; i < ob1.length(); i++) {
                                JSONObject jObject = ob1.getJSONObject(i);
                                appliedJobModel = (ExperienceModel) jParser.parseJson(jObject, new ExperienceModel());
                                appliedJobModel.setTonnageValue(jObject.getString("TonnageValue"));
                                experienceList.add(appliedJobModel);
                            }
                            ExperienceAdapter appliedJobAdapter = new ExperienceAdapter(getContext(), R.layout.layout_experience, experienceList);
                            lvExperience.setAdapter(appliedJobAdapter);

//                            Intent intent = new Intent(getContext(), MainActivity.class);
//                            startActivity(intent);
//                            finish();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;

                case deleteExperience:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {

                            // Utils.showToast(jObj.getString("message"), getContext());
                            Utils.showToast("Deleted Successfully",getContext());

                            getExperienceDetail();

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }
}
