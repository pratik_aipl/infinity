package com.nfinitydynamics.fragment;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.nfinitydynamics.R;
import com.nfinitydynamics.activity.MainActivity;
import com.nfinitydynamics.utils.App;
import com.nfinitydynamics.utils.AsynchTaskListner;
import com.nfinitydynamics.utils.CallRequest;
import com.nfinitydynamics.utils.Constant;
import com.nfinitydynamics.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static com.nfinitydynamics.activity.MainActivity.toolbar;

/**
 * A simple {@link Fragment} subclass.
 */
public class ApplyNowFragment extends Fragment implements AsynchTaskListner {

    public EditText edtCompName, edtJobTitle, edtShipType, edtRank,edtAvailable;
    public Button btnConfirm;
    public String cName, jobTitle, shipType, rank, companyId, jobId, candidateId,isApplied,availableDate,availableFromValue;
    public SharedPreferences shared;
    public ApplyNowFragment instance;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view;
        view = inflater.inflate(R.layout.fragment_apply_now, container, false);
        instance = this;
        toolbar.setTitle("Apply for this Vacancy");
        shared = getActivity().getSharedPreferences(getContext().getPackageName(), 0);
        candidateId = shared.getString("userId", "");
        edtCompName = view.findViewById(R.id.edt_company_name);
        edtJobTitle = view.findViewById(R.id.edt_job_title);
        edtShipType = view.findViewById(R.id.edt_ship_type);
        edtRank = view.findViewById(R.id.edt_rank);
        btnConfirm = view.findViewById(R.id.btn_submit);

//        spinnerJoingType=view.findViewById(R.id.spinner_joing_type);
//        spAvailableFrom=view.findViewById(R.id.spinner_available_from);
        jobTitle = getArguments().getString("jobTitle");
        shipType = getArguments().getString("shipType");
        cName = getArguments().getString("company");
        companyId = getArguments().getString("companyId");
        jobId = getArguments().getString("jobId");
        rank = getArguments().getString("rank");
        isApplied = getArguments().getString("isApplied");
        System.out.println(" rank::====>"+rank);
        System.out.println(" jobTitle::====>"+jobTitle);
        edtRank.setText(rank);
        edtShipType.setText(shipType);
        edtJobTitle.setText(jobTitle);
        edtCompName.setText(cName);
//        getJoingType();
//        getAvailableFrom();


        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String dateCurrent = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
                new CallRequest(instance).applyNow(candidateId, jobId, dateCurrent);

            }
        });
//        spAvailableFrom.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                availableFromValue= String.valueOf(spAvailableFrom.getSelectedItem());
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });
//        spinnerJoingType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                joiningTypeValue = String.valueOf(spinnerJoingType.getSelectedItem());
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });
        return view;
    }

//    private void getAvailableFrom() {
//        aaAvailable = new ArrayAdapter(getContext(), android.R.layout.simple_spinner_item, availableArray);
//        aaAvailable.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        spAvailableFrom.setAdapter(aaAvailable);
//    }
//
//    private void getJoingType() {
//
//        aa = new ArrayAdapter(getContext(), android.R.layout.simple_spinner_item, joiningType);
//        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        spinnerJoingType.setAdapter(aa);
//    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case applyNow:

                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            Utils.showToast("Applied successfully", getContext());
                            App.isApply=true;
                           MainActivity.changeFragment(new HistoryFragment(), false);
                        } else {
                            Utils.showToast("Try Again!!", getContext());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d("new exception:", String.valueOf(e));
                    }
                    break;
            }
        }
    }
}
