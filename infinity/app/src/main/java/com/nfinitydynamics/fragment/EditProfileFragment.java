package com.nfinitydynamics.fragment;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import com.nfinitydynamics.R;
import com.nfinitydynamics.adapter.NothingSelectedSpinnerAdapter;
import com.nfinitydynamics.model.AppliedShip;
import com.nfinitydynamics.model.CountryResidenceModel;
import com.nfinitydynamics.model.Nationality;
import com.nfinitydynamics.model.RankModel;
import com.nfinitydynamics.model.UpdateProfileModel;
import com.nfinitydynamics.utils.App;
import com.nfinitydynamics.utils.AsynchTaskListner;
import com.nfinitydynamics.utils.CallRequest;
import com.nfinitydynamics.utils.Constant;
import com.nfinitydynamics.utils.CountryCodePicker;
import com.nfinitydynamics.utils.JsonParserUniversal;
import com.nfinitydynamics.utils.LatoTextView;
import com.nfinitydynamics.utils.MultiSelectionSpinnerForEdit;
import com.nfinitydynamics.utils.Utils;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import static android.app.Activity.RESULT_OK;
import static com.nfinitydynamics.activity.MainActivity.changeFragment;
import static com.nfinitydynamics.activity.MainActivity.toolbar;
import static com.nfinitydynamics.utils.App.countryResidence;
import static com.nfinitydynamics.utils.Constant.FILTER_TYPE.APPLIEDRANK;
import static com.nfinitydynamics.utils.Constant.FILTER_TYPE.SHIPTYPE;

/**
 * A simple {@link Fragment} subclass.
 */
public class EditProfileFragment extends Fragment implements AsynchTaskListner, MultiSelectionSpinnerForEdit.OnMultipleItemsSelectedListener, AdapterView.OnItemSelectedListener {
    public EditText edtFirstName, edtMiddleName, edtLastName, edtEmail, edtEmail2, edtIndusNo,
            edtTelephoneNo, edtMobileNo, edtMobileNo2, edtMobileNoCode, edtMobileNoCode2, edt_bmi, edt_weight, edt_height,
            edtChildren, edtDob, edtAddress, edtZipCode, edtCity, edtAvailableFrom, edtTelephoneNoCode;
    public ArrayList<String> nationalityStringList = new ArrayList<>();
    public ArrayList<Nationality> natinalityList = new ArrayList<>();
    public String rankvalue, maritalValue, children, address, city, telCode, zipCode, nationality, availableDate, multiSelecteRankId = "", multiSelecteShipTypeId = "";
    public ArrayList<RankModel> rankList = new ArrayList<>();
    public ArrayList<RankModel> appliedRankList = new ArrayList<>();
    public ArrayList<RankModel> selectedRankList = new ArrayList<>();
    public ArrayList<AppliedShip> selectedShipList = new ArrayList<>();
    public ArrayList<String> rankStringList = new ArrayList<>();
    public String[] maritalStatus = {"Single", "Married", "Divorced", "Widow", "Widower"};
    public Spinner spinnerRank, spinnerMarital, spinnerNationality, spinnerJoingType, spGender, spinnerCountryResidency;
    public Uri selectedUri;
    public static String profImagePath = "";
    public LatoTextView txtEditImage;
    public ImageView ivProfile;
    public String selectedType;
    public EditProfileFragment instance;
    public Bitmap bm;
    public CropImage.ActivityResult result;
    public Button btnUpdate;
    public String firstName, middleName, lastName, email, email2, indusNo, telephoneNo, mobileNo1,
            mobileNo2, dob, joiningTypeValue, contryCode = "91", mCode, mCode2;
    public JsonParserUniversal jParser;
    public SharedPreferences shared;
    String candidateId, contryMCode, contryM2Code;
    public int index;
    public ArrayAdapter aa1, aaJoinType, aaGender, aaCountryResi;
    public String mariStatus;
    public ArrayAdapter aNationality;
    public ArrayAdapter aRank;
    public String cimage, cfirstName, cmiddleName, clastName, cemail, cindous, cmobileno, crankeName, cnationality, gender, cCountryResidence, cCountryId = "", country_name = "";
    public MultiSelectionSpinnerForEdit multiSelectionSpinner, multiSelectionSpinnerShipType;
    public List<String> shipMultiArray = new ArrayList<String>();
    public ArrayList<AppliedShip> appliedShipList = new ArrayList<>();
    public List<String> rankMultiArray = new ArrayList<String>();
    public List<Integer> indexList = new ArrayList<>();
    public String[] joiningType = {"Available After", "Urgent Joining Before"};
    public int[] intRankArray;
    public CountryCodePicker ccpContryCode, ccpmContryCode, ccpm2ContryCode;
    public String rankSelectedID;
    public boolean isSelectedRank = false;
    public boolean isSelectedShip = false;
    public String[] genderArray = {"Male", "Female", "Decline to declare", "Others"};
    public static String type;
    public static int mYear;
    public static int mMonth;
    public static int mDay;
    public ArrayList<CountryResidenceModel> list = new ArrayList<>();
    public ArrayList<String> listString = new ArrayList<>();
    public UpdateProfileModel detail;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view;
        view = inflater.inflate(R.layout.fragment_edit_profile, container, false);
        toolbar.setTitle("Edit Profile");
        instance = this;
        contryMCode = "";
        contryCode = "";
        contryM2Code = "";
        multiSelecteRankId = "";
        rankvalue = "";
        multiSelecteShipTypeId = "";
        shared = getActivity().getSharedPreferences(getContext().getPackageName(), 0);
        txtEditImage = view.findViewById(R.id.txt_edit_pic);
        ivProfile = view.findViewById(R.id.iv_candidate);
        edtFirstName = view.findViewById(R.id.edt_firstname);
        edtMiddleName = view.findViewById(R.id.edt_middlename);
        edtLastName = view.findViewById(R.id.edt_lastname);
        edtEmail = view.findViewById(R.id.edt_email);
        edtEmail2 = view.findViewById(R.id.edt_email2);
        edtIndusNo = view.findViewById(R.id.edt_indusno);
        edtTelephoneNo = view.findViewById(R.id.edt_telephone_no);
        edtMobileNo = view.findViewById(R.id.edt_mobile_no);
        edtMobileNo2 = view.findViewById(R.id.edt_mobile_no2);

        edt_bmi = view.findViewById(R.id.edt_bmi);
        edt_weight = view.findViewById(R.id.edt_weight);
        edt_height = view.findViewById(R.id.edt_height);

        spinnerRank = view.findViewById(R.id.spinner_job_rank);
        spinnerMarital = view.findViewById(R.id.spinner_marital_status);
        btnUpdate = view.findViewById(R.id.btn_register);
        edtAddress = view.findViewById(R.id.edt_address);
        edtCity = view.findViewById(R.id.edt_city);
        edtZipCode = view.findViewById(R.id.edt_zipcode);
        spinnerNationality = view.findViewById(R.id.spinner_country);
        edtChildren = view.findViewById(R.id.edt_children);
        edtDob = view.findViewById(R.id.edt_dob);
        edtAvailableFrom = view.findViewById(R.id.edt_available_from);
        multiSelectionSpinner = view.findViewById(R.id.multiselectSpinner);
        multiSelectionSpinnerShipType = view.findViewById(R.id.multiselectSpinnerShipType);
        spinnerJoingType = view.findViewById(R.id.spinner_joing_type);
        ccpmContryCode = view.findViewById(R.id.ccpmContryCode);
        ccpm2ContryCode = view.findViewById(R.id.ccpm2ContryCode);
        ccpContryCode = view.findViewById(R.id.ccpContryCode);
        edtTelephoneNoCode = view.findViewById(R.id.edt_telephone_no_code);
        spGender = view.findViewById(R.id.spinner_gender);
        spinnerCountryResidency = view.findViewById(R.id.spinner_country_residency);
//        edtMobileNoCode = view.findViewById(R.id.edt_mobile_no_code);
//        edtMobileNoCode2 = view.findViewById(R.id.edt_mobile_no_code2);

        ccpContryCode.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected() {
                contryCode = ccpContryCode.getSelectedCountryCode();
            }
        });


        jParser = new JsonParserUniversal();
        candidateId = shared.getString("userId", "");
        getCandidateDetail();
        getRank();
        getMaritalStatus();
        getJoingType();
        getAppliedShipType();
        getNationality();

        multiSelectionSpinnerShipType.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                type = "shipType";
                return false;
            }
        });
        multiSelectionSpinner.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                type = "appliedRank";
                return false;
            }
        });
        aaGender = new ArrayAdapter(getContext(), R.layout.spinner_text, genderArray);
        aaGender.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spGender.setAdapter(new NothingSelectedSpinnerAdapter(
                aaGender, R.layout.gender_spinner_row_nothing_selected,
                getContext()));


        spGender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    gender = genderArray[position - 1];
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        edtAvailableFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                Utils.generateDatePicker(getContext(), edtAvailableFrom);
            }
        });

        spinnerJoingType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    joiningTypeValue = joiningType[position - 1];
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        txtEditImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (CropImage.isExplicitCameraPermissionRequired(getContext())) {
                    requestPermissions(
                            new String[]{Manifest.permission.CAMERA},
                            CropImage.CAMERA_CAPTURE_PERMISSIONS_REQUEST_CODE);
                } else {
                    CropImage.startPickImageActivity(getActivity());
                }
            }

        });
        edt_weight.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    float height = Float.parseFloat(edt_height.getText().toString());
                    float weight = Float.parseFloat(edt_weight.getText().toString());
                    float bmi = ((weight * 10000) / (height * height));
                      int x = Math.round(bmi);
                        edt_bmi.setText(String.valueOf((x)));
                   // edt_bmi.setText(String.valueOf((bmi)));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {

            }
        });

        edt_height.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    float height = Float.parseFloat(edt_height.getText().toString());
                    float weight = Float.parseFloat(edt_weight.getText().toString());
                    float bmi = ((weight * 10000) / (height * height));
                    int x = Math.round(bmi);
                    edt_bmi.setText(String.valueOf((x)));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {


            }
        });
        edtDob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                //  Utils.generateDatePicker(instance, edtDob);
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog dpd;
                dpd = new DatePickerDialog(getContext(),
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                Calendar userAge = new GregorianCalendar(year, monthOfYear, dayOfMonth);
                                Calendar minAdultAge = new GregorianCalendar();
                                minAdultAge.add(Calendar.YEAR, -18);
                                if (minAdultAge.before(userAge)) {
                                    Utils.showToast("Select date above 18 year", getContext());
                                } else {
                                    edtDob.setText(Utils.getDateFormat("d", "dd", String.valueOf(dayOfMonth)) + "-" + Utils.getDateFormat("M", "MMM", String.valueOf((monthOfYear + 1))) + "-" + year);
                                }
                            }
                        }, mYear, mMonth, mDay);
                dpd.show();
            }
        });

        ccpContryCode.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected() {
                //  Toast.makeText(getContext(), "Contry Code   " + ccpContryCode.getSelectedCountryCode(), Toast.LENGTH_LONG).show();
                contryCode = ccpContryCode.getSelectedCountryCode();
            }
        });
        ccpmContryCode.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected() {
                // Toast.makeText(getContext(), "Contry Code   " + ccpmContryCode.getSelectedCountryCode(), Toast.LENGTH_LONG).show();
                contryMCode = ccpmContryCode.getSelectedCountryCode();
//
            }
        });
        ccpm2ContryCode.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected() {
                contryM2Code = ccpm2ContryCode.getSelectedCountryCode();
            }
        });
        spinnerMarital.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                try {
                    maritalValue = maritalStatus[i - 1];
                    App.maritualStatus = maritalValue;
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        spinnerNationality.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                try {
                    if (natinalityList != null) {
                        cnationality = natinalityList.get(i - 1).getNationalityId();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }

        });
        spinnerCountryResidency.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                try {
                    countryResidence = list.get(i - 1).getCountryID();
                    country_name = list.get(i - 1).getCountryID();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }

        });
        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doUpdate();

            }
        });

        if (App.isFromCoc) {
            edtIndusNo.findFocus();
            App.isFromCoc = false;
        }

        return view;
    }

    private void getCountryOfResidence() {
        new CallRequest(instance).getCountryListFragment();

    }

    private void getJoingType() {

//        aaJoinType = new ArrayAdapter(getContext(), android.R.layout.simple_spinner_item, joiningType);
//        aaJoinType.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        spinnerJoingType.setAdapter(aaJoinType);

        aaJoinType = new ArrayAdapter(getContext(), R.layout.spinner_text, joiningType);
        aaJoinType.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerJoingType.setAdapter(new NothingSelectedSpinnerAdapter(
                aaJoinType, R.layout.joiningtype_spinner_row_nothing_selected,
                getContext()));
    }

    private void getMaritalStatus() {
//        aa1 = new ArrayAdapter(getContext(), android.R.layout.simple_spinner_item, maritalStatus);
//        aa1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        spinnerMarital.setAdapter(aa1);

        aa1 = new ArrayAdapter(getContext(), R.layout.spinner_text, maritalStatus);
        aa1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerMarital.setAdapter(new NothingSelectedSpinnerAdapter(
                aa1, R.layout.marital_spinner_row_nothing_selected,
                getContext()));
    }

    private void getCountryResidense() {
//        aa1 = new ArrayAdapter(getContext(), android.R.layout.simple_spinner_item, maritalStatus);
//        aa1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        spinnerMarital.setAdapter(aa1);


    }

    private void getCandidateDetail() {
        new CallRequest(instance).getCandidateDetail(candidateId);
    }

    private void getRank() {

        new CallRequest(instance).getRankFragment();
    }

    private void getAppliedRank() {
        try {
            new CallRequest(instance).getAppliedRankFragment(rankvalue);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    private void getAppliedShipType() {
        new CallRequest(instance).getAppliedShipTypeFragment();
    }


    public void converDateToYYMMddTextView(String dateDOB) {
        DateFormat inputFormat = new SimpleDateFormat("dd-MMM-yyyy");
        DateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = inputFormat.parse(dateDOB);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String ddMMYYDate = outputFormat.format(date);
        // System.out.println("converted date====" + ddMMYYDate);
        dob = ddMMYYDate;
    }

    public void doUpdate() {
        firstName = edtFirstName.getText().toString().trim();
        lastName = edtLastName.getText().toString().trim();
        middleName = edtMiddleName.getText().toString().trim();
        email = edtEmail.getText().toString().trim();
        email2 = edtEmail2.getText().toString().trim();
        indusNo = edtIndusNo.getText().toString().trim();
        telephoneNo = edtTelephoneNo.getText().toString().trim();
        mobileNo1 = edtMobileNo.getText().toString().trim();
        mobileNo2 = edtMobileNo2.getText().toString().trim();
        children = edtChildren.getText().toString().trim();
        address = edtAddress.getText().toString().trim();
        city = edtCity.getText().toString().trim();
        zipCode = edtZipCode.getText().toString().trim();
        dob = edtDob.getText().toString().trim();
        availableDate = edtAvailableFrom.getText().toString().trim();
        telCode = edtTelephoneNoCode.getText().toString().trim();
        converDateToYYMMddTextView(dob);
        Calendar cal = Calendar.getInstance();
        Date sysDate = cal.getTime();
        Date intValidity = null;
        try {
            intValidity = new SimpleDateFormat("dd-MMM-yyyy").parse(availableDate);
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            availableDate = formatter.format(intValidity);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (firstName.equals("")) {
            edtFirstName.findFocus();
            Utils.showAlert("Please Enter First Name", getActivity());
        } else if (lastName.equals("")) {
            edtLastName.findFocus();
            Utils.showAlert("Please Enter Last Name", getActivity());
        } else if (TextUtils.isEmpty(email)) {
            edtEmail.findFocus();
            Utils.showAlert("Please Enter Email", getActivity());
        } else if (Utils.emailValidation(email).equals("false")) {
            Utils.showAlert("Please Enter Valid Email", getActivity());
        } else if (email2.equals(email)) {
            Utils.showAlert("Both Email ids are same", getActivity());
        } else if (TextUtils.isEmpty(dob)) {
            edtDob.findFocus();
            Utils.showAlert("Please Enter Date Of Birth", getActivity());
        } else if (mobileNo1.equalsIgnoreCase("") && !telephoneNo.equalsIgnoreCase("")) {
            if (telCode.equalsIgnoreCase("")) {
                Utils.showAlert("Please Select Telephone STD Code", getActivity());
            } else if (contryCode.equalsIgnoreCase("")) {
                Utils.showAlert("Please Select Telephone ISD Code", getActivity());
            } else if (mobileNo1.equalsIgnoreCase("") && telephoneNo.equalsIgnoreCase("") && mobileNo2.equalsIgnoreCase("")
                    && !contryCode.equalsIgnoreCase("") && !telCode.equalsIgnoreCase("")) {
                Utils.showAlert("Please Select Telephone No", getActivity());
            } else if (!mobileNo1.equalsIgnoreCase("") && !telephoneNo.equalsIgnoreCase("") && contryMCode.equalsIgnoreCase("")) {
                Utils.showToast("Please Select Country Code", getActivity());
            } else if (!mobileNo1.equalsIgnoreCase("") && contryMCode.equalsIgnoreCase("")) {
                Utils.showToast("Please Select Country Code", getActivity());
            } else if (contryMCode.equalsIgnoreCase("91") && !mobileNo1.equalsIgnoreCase("") && mobileNo1.length() != 10) {
                Utils.showAlert("Mobile No should be 10 digits for India.", getActivity());
            } else if (!mobileNo2.equalsIgnoreCase("") && contryM2Code.equalsIgnoreCase("")) {
                Utils.showToast("Please Select Country Code 2", getActivity());
            } else if (contryM2Code.equalsIgnoreCase("91") && !mobileNo2.equalsIgnoreCase("") && mobileNo2.length() != 10) {
                Utils.showAlert("Mobile No2 should be 10 digits for India.", getActivity());
            } else if (!telephoneNo.equalsIgnoreCase("") && telCode.equalsIgnoreCase("")) {
                Utils.showAlert("Please Select Telephone STD Code", getActivity());
            } else if (!telephoneNo.equalsIgnoreCase("") && contryCode.equalsIgnoreCase("")) {
                Utils.showAlert("Please Select Country Code", getActivity());
            }/* else if (!contryMCode.equalsIgnoreCase("") && TextUtils.isEmpty(mobileNo1)) {
                Utils.showAlert("Please enter mobile number", getActivity());
            }*/ else if (contryMCode.equalsIgnoreCase("") && !TextUtils.isEmpty(mobileNo1)) {
                Utils.showToast("Please Select Country Code", getActivity());
            } else if (contryM2Code.equalsIgnoreCase("") && !TextUtils.isEmpty(mobileNo2)) {
                Utils.showToast("Please Select Country Code", getActivity());
            } else if (TextUtils.isEmpty(rankvalue)) {
                spinnerRank.findFocus();
                Utils.showAlert("Please Select Current Rank", getActivity());
            } else if (TextUtils.isEmpty(gender)) {
                spGender.findFocus();
                Utils.showAlert("Please Select Gender", getActivity());
            } else if (edt_height.getText().toString().isEmpty()) {
                edt_height.findFocus();
                Utils.showAlert("Please Enter Height in CMS.", getActivity());
            } else if (edt_weight.getText().toString().isEmpty()) {
                edt_weight.findFocus();
                Utils.showAlert("Please Enter Weight in KGS.", getActivity());
            } else if (edt_bmi.getText().toString().isEmpty()) {
                edt_bmi.findFocus();
                Utils.showAlert("Please Enter BMI.", getActivity());
            } else if (TextUtils.isEmpty(maritalValue)) {
                spinnerMarital.findFocus();
                Utils.showAlert("Please Select Marital Status", getActivity());
            } else if (TextUtils.isEmpty(joiningTypeValue)) {
                Utils.showAlert("Please Select Joining Type", getActivity());
            } else if (TextUtils.isEmpty(availableDate)) {
                Utils.showAlert("Please Select Available From Date", getActivity());
            } else if (TextUtils.isEmpty(multiSelecteRankId)) {
                Utils.showAlert("Please Select Applied Rank", getActivity());
            } else if (TextUtils.isEmpty(multiSelecteShipTypeId)) {
                Utils.showAlert("Please Select Applied Ship Type ", getActivity());
            } else if (sysDate.compareTo(intValidity) > 0) {
                Utils.showAlert("Enter Valid Date in Available After", getActivity());
            } else if (address.equals("")) {
                edtAddress.findFocus();
                Utils.showAlert("Please Enter Address", getContext());
            } else if (city.equals("")) {
                edtCity.findFocus();
                Utils.showAlert("Please Enter City", getContext());
            } else if (zipCode.equals("")) {
                edtZipCode.findFocus();
                Utils.showAlert("Please Enter ZipCode", getContext());
            } else if (TextUtils.isEmpty(cCountryResidence)) {
                spinnerCountryResidency.findFocus();
                Utils.showAlert("Please Select Country of Residence", getContext());
            } else {
                new CallRequest(instance).UpdateCandidateProfile(candidateId, firstName, middleName, lastName,
                        email, email2, dob, maritalValue,
                        children, address, city, zipCode, cnationality, contryCode, telCode, telephoneNo, contryMCode
                        , mobileNo1, contryM2Code, mobileNo2, indusNo, rankvalue, multiSelecteRankId, availableDate,
                        profImagePath, joiningTypeValue, gender, multiSelecteShipTypeId, country_name, edt_weight.getText().toString(), edt_height.getText().toString(), edt_bmi.getText().toString());

            }/*if (telCode.equalsIgnoreCase("")) {
                Utils.showAlert("Please Select Telephone STD Code", getActivity());
            } else if (contryCode.equalsIgnoreCase("")) {
                Utils.showAlert("Please Select Telephone ISD Code", getActivity());
            } else if (mobileNo1.equalsIgnoreCase("") && telephoneNo.equalsIgnoreCase("") && mobileNo2.equalsIgnoreCase("")) {
                Utils.showAlert("Please Select Telephone No or Mobile No", getActivity());
            } else if (!mobileNo1.equalsIgnoreCase("") && contryMCode.equalsIgnoreCase("")) {
                Utils.showAlert("Please Select Country Code", getActivity());

            } else if (!mobileNo2.equalsIgnoreCase("") && contryM2Code.equalsIgnoreCase("")) {
                Utils.showAlert("Please Select Country Code 2", getActivity());
            } else {
                new CallRequest(instance).UpdateCandidateProfile(candidateId, firstName, middleName, lastName,
                        email, email2, dob, maritalValue,
                        children, address, city, zipCode, cnationality, contryCode, telCode, telephoneNo, contryMCode
                        , mobileNo1, contryM2Code, mobileNo2, indusNo, rankvalue, multiSelecteRankId, availableDate,
                        profImagePath, joiningTypeValue, gender, multiSelecteShipTypeId, country_name, edt_weight.getText().toString(), edt_height.getText().toString(), edt_bmi.getText().toString());
            }*/
        } else if (mobileNo1.equalsIgnoreCase("") && telephoneNo.equalsIgnoreCase("") && mobileNo2.equalsIgnoreCase("")) {
            Utils.showAlert("Please Select Telephone No or Mobile No", getActivity());
        } else if (!telephoneNo.equalsIgnoreCase("") && !contryCode.equalsIgnoreCase("") && telCode.equalsIgnoreCase("")) {
            Utils.showAlert("Please Select Telephone STD Code", getActivity());
        } else if (!contryMCode.equalsIgnoreCase("") && TextUtils.isEmpty(mobileNo1)) {
            Utils.showAlert("Please enter mobile number", getActivity());
        } else if (!mobileNo1.equalsIgnoreCase("") && contryMCode.equalsIgnoreCase("")) {
            Utils.showAlert("Please Select Country Code", getActivity());
        } else if (contryMCode.equalsIgnoreCase("91") && !mobileNo1.equalsIgnoreCase("") && mobileNo1.length() != 10) {
            Utils.showAlert("Mobile No should be 10 digits for India.", getActivity());
        } else if (!mobileNo2.equalsIgnoreCase("") && contryM2Code.equalsIgnoreCase("")) {
            Utils.showAlert("Please Select Country Code 2", getActivity());
        } else if (contryM2Code.equalsIgnoreCase("91") && !mobileNo2.equalsIgnoreCase("") && mobileNo2.length() != 10) {
            Utils.showAlert("Mobile No2 should be 10 digits for India.", getActivity());
        } else if (mobileNo2.equalsIgnoreCase(mobileNo1)) {
            Utils.showAlert("Both Mobile Numbers are same", getActivity());
        } else if (TextUtils.isEmpty(rankvalue)) {
            spinnerRank.findFocus();
            Utils.showAlert("Please Select Current Rank", getActivity());
        } else if (TextUtils.isEmpty(gender)) {
            spGender.findFocus();
            Utils.showAlert("Please Select Gender", getActivity());
        } else if (edt_height.getText().toString().isEmpty()) {
            edt_height.findFocus();
            Utils.showAlert("Please Enter Height in CMS.", getActivity());
        } else if (edt_weight.getText().toString().isEmpty()) {
            edt_weight.findFocus();
            Utils.showAlert("Please Enter Weight in KGS.", getActivity());
        } else if (edt_bmi.getText().toString().isEmpty()) {
            edt_bmi.findFocus();
            Utils.showAlert("Please Enter BMI.", getActivity());
        } else if (TextUtils.isEmpty(maritalValue)) {
            spinnerMarital.findFocus();
            Utils.showAlert("Please Select Marital Status", getActivity());
        } else if (TextUtils.isEmpty(joiningTypeValue)) {
            Utils.showAlert("Please Select Joining Type", getActivity());
        } else if (TextUtils.isEmpty(availableDate)) {
            Utils.showAlert("Please Select Available From Date", getActivity());
        } else if (TextUtils.isEmpty(multiSelecteRankId)) {
            Utils.showAlert("Please Select Applied Rank", getActivity());
        } else if (TextUtils.isEmpty(multiSelecteShipTypeId)) {
            Utils.showAlert("Please Select Applied Ship Type ", getActivity());
        } else if (sysDate.compareTo(intValidity) > 0) {
            Utils.showAlert("Enter Valid Date in Available After", getActivity());
        } else if (address.equals("")) {
            edtAddress.findFocus();
            Utils.showAlert("Please Enter Address", getContext());
        } else if (city.equals("")) {
            edtCity.findFocus();
            Utils.showAlert("Please Enter City", getContext());
        } else if (zipCode.equals("")) {
            edtZipCode.findFocus();
            Utils.showAlert("Please Enter ZipCode", getContext());
        } else if (TextUtils.isEmpty(cCountryResidence)) {
            spinnerCountryResidency.findFocus();
            Utils.showAlert("Please Select Country of Residence", getContext());
        } else if (mobileNo1.equalsIgnoreCase("") && !telephoneNo.equalsIgnoreCase("")) {
            if (telCode.equalsIgnoreCase("")) {
                Utils.showAlert("Please Select Telephone Code", getContext());
            } else {
                new CallRequest(instance).UpdateCandidateProfile(candidateId, firstName, middleName, lastName,
                        email, email2, dob, maritalValue,
                        children, address, city, zipCode, cnationality, contryCode, telCode, telephoneNo, contryMCode
                        , mobileNo1, contryM2Code, mobileNo2, indusNo, rankvalue, multiSelecteRankId, availableDate, profImagePath,
                        joiningTypeValue, gender, multiSelecteShipTypeId, country_name, edt_weight.getText().toString(),
                        edt_height.getText().toString(), edt_bmi.getText().toString());
            }
        } else {
            new CallRequest(instance).UpdateCandidateProfile(candidateId, firstName, middleName, lastName,
                    email, email2, dob, maritalValue,
                    children, address, city, zipCode, cnationality, contryCode, telCode, telephoneNo, contryMCode
                    , mobileNo1, contryM2Code, mobileNo2, indusNo, rankvalue, multiSelecteRankId, availableDate, profImagePath,
                    joiningTypeValue, gender, multiSelecteShipTypeId, country_name, edt_weight.getText().toString(),
                    edt_height.getText().toString(), edt_bmi.getText().toString());
        }
    }

    @SuppressLint("NewApi")
    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case getRankFragment:
                    try {
                        JSONObject jObj = new JSONObject(result);
                        int selectedPos = 0;
                        if (jObj.getBoolean("status")) {
                            JSONArray ob1 = jObj.getJSONArray("data");
                            for (int i = 0; i < ob1.length(); i++) {
                                JSONObject ob = ob1.getJSONObject(i);
                                String rankId = ob.getString("RankID");
                                String rankName = ob.getString("Name");
                                rankList.add(new RankModel(rankId, rankName));
                                rankStringList.add(rankName);
                                if (rankId.equalsIgnoreCase(detail.getRankID())) {
                                    selectedPos = i;
                                }
                            }
                            try {
                                if (getActivity() != null) {

                                    aRank = new ArrayAdapter(getContext(), R.layout.spinner_text, rankStringList);
                                    aRank.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                    spinnerRank.setAdapter(aRank);
                                    spinnerRank.setSelection(selectedPos);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d("new exception:", String.valueOf(e));
                    }
                    break;
                case getAppliedRankFragment:
                    rankMultiArray.clear();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            JSONArray ob1 = jObj.getJSONArray("data");
                            for (int i = 0; i < ob1.length(); i++) {
                                JSONObject ob = ob1.getJSONObject(i);
                                String rankId = ob.getString("RankID");
                                String rankName = ob.getString("Name");
                                appliedRankList.add(new RankModel(rankId, rankName));
                                rankMultiArray.add(rankName);
                            }
                            multiSelectionSpinner.setItems(rankMultiArray, Constant.FILTER_TYPE.APPLIEDRANK);
                            multiSelectionSpinner.setSelection(new int[]{0});
                            multiSelectionSpinner.setListener(this);
                            multiSelectionSpinner.clearSelection();
                            if (selectedRankList != null && isSelectedRank) {
                                int[] selectionArray = new int[selectedRankList.size()];
                                String[] rankSelectedIDsArray = new String[selectedRankList.size()];
                                int counter = 0;
                                for (int j = 0; j < appliedRankList.size(); j++) {
                                    for (RankModel r : selectedRankList) {
                                        try {
                                            if (r.getRankId().equalsIgnoreCase(appliedRankList.get(j).getRankId())) {
                                                selectionArray[counter] = j;
                                                rankSelectedIDsArray[counter] = r.getRankId();
                                                counter++;
                                            }
                                            try {

                                                multiSelecteRankId = convertStringArrayToString(rankSelectedIDsArray, ",");
                                                //    System.out.println(multiSelecteRankId);

                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }


                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                                try {
                                    //  System.out.println("selectionArray string" + selectionArray.length);
                                    multiSelectionSpinner.setSelection(selectionArray);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            } else {
                                multiSelectionSpinner.clearSelection();
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d("new exception:", String.valueOf(e));
                    }
                    Utils.hideProgressDialog();
                    break;
                case getAppliedShip:
                    shipMultiArray.clear();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            JSONArray ob1 = jObj.getJSONArray("data");
                            for (int i = 0; i < ob1.length(); i++) {
                                JSONObject ob = ob1.getJSONObject(i);
                                String shipId = ob.getString("ShipID");
                                String shipName = ob.getString("ShipType");
                                appliedShipList.add(new AppliedShip(shipId, shipName));
                                shipMultiArray.add(shipName);
                            }
                            //  System.out.println("applied ship size====" + shipMultiArray.size());
                            multiSelectionSpinnerShipType.setItems(shipMultiArray, Constant.FILTER_TYPE.SHIPTYPE);
                            multiSelectionSpinnerShipType.setSelection(new int[]{0});
                            multiSelectionSpinnerShipType.setListener(this);
                            multiSelectionSpinnerShipType.clearSelection();

                            if (selectedShipList != null && isSelectedShip) {
                                int[] selectionArray = new int[selectedShipList.size()];
                                String[] shipTypeSelectedIDsArray = new String[selectedShipList.size()];
                                int counter = 0;
                                for (int j = 0; j < appliedShipList.size(); j++) {
                                    for (AppliedShip r : selectedShipList) {
                                        try {
                                            if (r.getShipTypeID().equalsIgnoreCase(appliedShipList.get(j).getShipID())) {
                                                selectionArray[counter] = j;
                                                shipTypeSelectedIDsArray[counter] = r.getShipTypeID();
                                                counter++;
                                            }
                                            try {

                                                multiSelecteShipTypeId = convertStringArrayToString(shipTypeSelectedIDsArray, ",");
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }


                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                                //  System.out.println("selectionArray string of ship" + selectionArray.length);
                                multiSelectionSpinnerShipType.setSelection(selectionArray);
                                //multiSelectionSpinner.setListener(instance);
                            } else {
                                multiSelectionSpinnerShipType.clearSelection();
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d("new exception:", String.valueOf(e));
                    }
                    Utils.hideProgressDialog();
                    break;
                case getNationalityFragment:
                    try {
                        JSONObject jObj = new JSONObject(result);
                        int selectedPos = 0;
                        if (jObj.getBoolean("status")) {
                            JSONArray ob1 = jObj.getJSONArray("data");
                            for (int i = 0; i < ob1.length(); i++) {
                                JSONObject ob = ob1.getJSONObject(i);
                                String nationalityId = ob.getString("NationalID");
                                String nationalityName = ob.getString("NationalName");
                                natinalityList.add(new Nationality(nationalityId, nationalityName));
                                nationalityStringList.add(nationalityName);
                                if (cnationality.equalsIgnoreCase(nationalityId)) {
                                    selectedPos = i;
                                }

                            }
                            Log.d("natilist-----", String.valueOf(natinalityList));
//                            aNationality = new ArrayAdapter(getContext(), R.layout.spinner_text, nationalityStringList);
//                            aNationality.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//                            spinnerNationality.setAdapter(aNationality);
                            try {
                                if (getActivity() != null) {

                                    aNationality = new ArrayAdapter(getContext(), R.layout.spinner_text, nationalityStringList);
                                    aNationality.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                    spinnerNationality.setAdapter(new NothingSelectedSpinnerAdapter(
                                            aNationality, R.layout.nationality_spinner_row_nothing_selected,
                                            getContext()));
                                    spinnerNationality.setSelection(selectedPos + 1);
                                }

                            } catch (Exception e) {

                            }

                        } else {
                            Utils.showToast(jObj.getString("message"), getContext());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();

                        Log.d("new exception:", String.valueOf(e));
                    }
                    Utils.hideProgressDialog();
                    break;
                case getCandidateDetail:

                    Utils.hideProgressDialog();

                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            JSONObject jObject = jObj.getJSONObject("data");
                            detail = new UpdateProfileModel();
                            detail = (UpdateProfileModel) jParser.parseJson(jObject, new UpdateProfileModel());
                            edtFirstName.setText(detail.getFirstName());
                            edtLastName.setText(detail.getLastName());
                            edtMiddleName.setText(detail.getMiddleName());
                            edtEmail.setText(detail.getEmailID());
                            edtEmail2.setText(detail.getEmailID2());
                            edtIndusNo.setText(detail.getIndos());
                            edtTelephoneNo.setText(detail.getTelephoneNo());
                            edtMobileNo.setText(detail.getMobileNo());
                            edtMobileNo2.setText(detail.getOtherMobileNo());
                            edt_height.setText(detail.getHeight());
                            edt_weight.setText(detail.getWeight());
                            edt_bmi.setText(detail.getBMI());
                            // profImagePath=detail.getImageUrl();
                            Picasso.with(getContext()).load(detail.getImageUrl()).placeholder(R.drawable.profilepic)
                                    .error(R.drawable.profilepic).into(ivProfile);
                            rankSelectedID = detail.getRankID();
                            //     Log.d("selected rankSelected-", rankSelectedID + "");
                            isSelectedRank = true;
                            isSelectedShip = true;
                            spinnerNationality.setSelection(Integer.parseInt(detail.getNationality()));
                            mariStatus = detail.getMaritalStatus();
                            edtChildren.setText(detail.getNoofChildren());
                            edtAddress.setText(detail.getAddress());
                            edtCity.setText(detail.getCityName());
                            edtZipCode.setText(detail.getZipcode());
                            //  System.out.println("dob=====" + detail.getDOB());
                            Utils.converDateToDDMMYY(edtDob, detail.getDOB());
                            Utils.converDateToDDMMYY(edtAvailableFrom, detail.getAvailableFrom());

                            edtTelephoneNoCode.setText(detail.getSTDCode());
                            crankeName = detail.getRankName();
                            cnationality = detail.getNationality();
                            cCountryResidence = detail.getCountryofResidenceID();
                            joiningTypeValue = detail.getJoiningType();
                            gender = detail.getGender();

                            // System.out.println("joing type");
                            contryCode = detail.getTelCountryCode();
                            contryMCode = detail.getCCode();
                            contryM2Code = detail.getOtherCCode();
                            SharedPreferences.Editor et = shared.edit();
                            et.putString("image", detail.getImageUrl());
                            et.commit();
                            try {
                                ccpContryCode.setCountryForPhoneCode(Integer.parseInt(contryCode));

                            } catch (NumberFormatException e) {
                                e.printStackTrace();
                            }
                            try {
                                ccpmContryCode.setCountryForPhoneCode(Integer.parseInt(contryMCode));

                            } catch (NumberFormatException e) {
                                e.printStackTrace();
                            }
                            try {
                                ccpm2ContryCode.setCountryForPhoneCode(Integer.parseInt(contryM2Code));

                            } catch (NumberFormatException e) {
                                e.printStackTrace();
                            }
                            // System.out.println("selectedRankList size" + selectedRankList.size());

                            JSONArray shipJasonArray = jObject.getJSONArray("AppliedShipType");
                            for (int i = 0; i < shipJasonArray.length(); i++) {
                                selectedShipList.add((AppliedShip) (jParser.parseJson(shipJasonArray.getJSONObject(i), new AppliedShip())));
                            }
                            getCountryOfResidence();

                            getAppliedShipType();

                            //  System.out.println("selectedShipList size" + selectedShipList.size());

                            if (mariStatus != null) {
                                int spinnerPosition = aa1.getPosition(mariStatus);
                                spinnerMarital.setSelection(spinnerPosition + 1);
                            }
                            if (joiningTypeValue != null) {
                                int spinnerPosition = aaJoinType.getPosition(joiningTypeValue);
                                spinnerJoingType.setSelection(spinnerPosition + 1);
                            }
                            // System.out.println("gender===" + gender);
                            if (gender != null) {
                                int spinnerPosition = aaGender.getPosition(gender);
                                spGender.setSelection(spinnerPosition + 1);

                                //   System.out.println("position==" + spinnerPosition + 1);
                            }

                            try {
                                if (crankeName != null) {
                                    int spinnerPosition = aRank.getPosition(detail.getRankID());
                                    spinnerRank.setSelection(spinnerPosition);
                                }
                                if (cnationality != null) {
                                    int spinnerPosition = aNationality.getPosition(cnationality);
                                    spinnerNationality.setSelection(spinnerPosition + 1);
                                }
                                if (cCountryResidence != null) {
                                    int spinnerPosition = aa1.getPosition(cCountryResidence);
                                    spinnerCountryResidency.setSelection(spinnerPosition + 1);
                                }

                            } catch (NullPointerException e) {
                                e.printStackTrace();
                            }

                            spinnerRank.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                                    rankvalue = rankList.get(i).getRankId();
                                    if (rankList != null) {
                                        rankvalue = rankList.get(i).getRankId();
                                        Log.d("selected rank----", rankvalue);
                                    }
                                    //   Log.d("selected rankSelected-", rankSelectedID + "");
                                    //   Log.d("selected i----", i + "");
                                    if (rankSelectedID.equalsIgnoreCase(rankvalue)) {
                                        isSelectedRank = true;
                                    }
// else {
//                                        isSelectedRank = false;
//                                    }
                                    multiSelecteRankId = "";
                                    // getAppliedRank();
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> adapterView) {

                                }
                            });
                            JSONArray rankJasonArray = jObject.getJSONArray("AppliedRank");
                            for (int i = 0; i < rankJasonArray.length(); i++) {
                                selectedRankList.add((RankModel) (jParser.parseJson(rankJasonArray.getJSONObject(i), new RankModel())));
                            }
                            getAppliedRank();


                        } else {

                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d("new exception:", String.valueOf(e));
                    }
                    break;
                case get_country_list:
                    Utils.hideProgressDialog();

                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            listString.clear();
                            list.clear();
                            JSONArray ob1 = jObj.getJSONArray("data");
                            int selectedPos = 0;
                            for (int i = 0; i < ob1.length(); i++) {
                                JSONObject ob = ob1.getJSONObject(i);
                                CountryResidenceModel countryResidenceModel = new CountryResidenceModel();
                                countryResidenceModel = (CountryResidenceModel) jParser.parseJson(ob, new CountryResidenceModel());
                                list.add(countryResidenceModel);

                                listString.add(countryResidenceModel.getCountryName());
                                if (cCountryResidence.equalsIgnoreCase(countryResidenceModel.getCountryID())) {
                                    selectedPos = i;
                                }

                            }

                            try {
                                if (getActivity() != null) {

                                    ArrayAdapter aa = new ArrayAdapter(getContext(), R.layout.spinner_text, listString);
                                    aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                    spinnerCountryResidency.setAdapter(new NothingSelectedSpinnerAdapter(
                                            aa, R.layout.country_recidency,
                                            getContext()));
                                    spinnerCountryResidency.setSelection(selectedPos + 1);
                                }
                            } catch (Exception e) {

                            }


                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                case updateFragment:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            UpdateProfileModel detail;
                            JSONObject jObject = jObj.getJSONObject("data");
                            detail = new UpdateProfileModel();
                            detail = (UpdateProfileModel) jParser.parseJson(jObject, new UpdateProfileModel());
                            cimage = detail.getImageUrl();
                            cfirstName = detail.getFirstName();
                            cmiddleName = detail.getMiddleName();
                            clastName = detail.getLastName();
                            cemail = detail.getEmailID();
                            cindous = detail.getIndos();
                            cmobileno = detail.getMobileNo();
                            crankeName = detail.getRankName();
                            cnationality = detail.getNationalName();

                            SharedPreferences.Editor et = shared.edit();
                            et.putString("image", cimage);
                            et.putString("firstName", cfirstName);
                            et.putString("middleName", cmiddleName);
                            et.putString("lastName", clastName);
                            et.putString("email", cemail);
                            et.putString("indusno", cindous);
                            et.putString("mobileNo", cmobileno);
                            et.putString("rank", crankeName);
                            et.commit();
                            Utils.showToast("Updated Successfully", getContext());
                            changeFragment(new ProfileFragment(), false);
                        } else {
                            Utils.showToast(jObj.getString("message"), getContext());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d("new exception:", String.valueOf(e));
                    }
                    break;
            }
        }
    }

    public static String convertStringArrayToString(String[] strArr, String delimiter) {
        StringBuilder sb = new StringBuilder();
        for (String str : strArr)
            sb.append(str).append(delimiter);
        return sb.substring(0, sb.length() - 1);
    }

    private void getNationality() {

        new CallRequest(instance).getNationalityFragment();
    }

    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setMinCropResultSize(500, 500)
                .setAspectRatio(1, 1)
                .setFixAspectRatio(true)
                .setMultiTouchEnabled(true)
                .start(getActivity());
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(getContext(), data);

            if (CropImage.isReadExternalStoragePermissionsRequired(
                    getContext(), imageUri)) {
                selectedUri = imageUri;
                profImagePath = selectedUri.getPath().toString();
                // Log.d("selected image1----", profImagePath);
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
            } else {
                startCropImageActivity(imageUri);
            }
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                ivProfile.setImageURI(result.getUri());
                selectedUri = result.getUri();
                profImagePath = selectedUri.getPath().toString();

                Log.d("selected image----", profImagePath);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
            }
        }

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (requestCode == 222) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (selectedType.equalsIgnoreCase("Take Photo")) {
                    Log.i("TAG", " REQUET Take Photo");
                    File f = new File(Environment.getExternalStorageDirectory() + "/InfinityDynamics/images");
                    if (!f.exists()) {
                        f.mkdirs();
                    }
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    File file = new File(Environment.getExternalStorageDirectory(), "InfinityDynamics/images/img_" + System.currentTimeMillis() + ".jpg");
                    selectedUri = Uri.fromFile(file);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, selectedUri);
                    startActivityForResult(intent, 222);
                }

            } else {
                Utils.showToast("Permission not granted",
                        getContext());
            }
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void selectedIndices(List<Integer> indices, Constant.FILTER_TYPE filter_type) {

        //  System.out.println("size" + indexList.size());
        if (filter_type == APPLIEDRANK) {
            multiSelecteRankId = "";
            ArrayList<String> rankIDArray = new ArrayList<>();
            for (Integer i : indices) {
                rankIDArray.add(rankList.get(i).getRankId());
            }
            multiSelecteRankId = android.text.TextUtils.join(",", rankIDArray);
            //  System.out.println("multiSelecteRankId  : " + multiSelecteRankId);
        }
        if (filter_type == SHIPTYPE) {
            multiSelecteShipTypeId = "";
            ArrayList<String> shipIDArray = new ArrayList<>();
            for (Integer i : indices) {
                shipIDArray.add(appliedShipList.get(i).getShipID());
            }
            multiSelecteShipTypeId = android.text.TextUtils.join(",", shipIDArray);
        }
    }

    @Override
    public void selectedStrings(List<String> strings, Constant.FILTER_TYPE filter_type) {
        for (int j = 0; j < strings.size(); j++) {
            //    System.out.println("item:::" + strings.get(j));
        }
    }
}
