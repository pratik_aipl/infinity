package com.nfinitydynamics.fragment;


import android.app.DatePickerDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.nfinitydynamics.R;
import com.nfinitydynamics.activity.MainActivity;
import com.nfinitydynamics.adapter.NothingSelectedSpinnerAdapter;
import com.nfinitydynamics.model.EngineModel;
import com.nfinitydynamics.model.RankModel;
import com.nfinitydynamics.model.ShipModel;
import com.nfinitydynamics.model.Tonnage;
import com.nfinitydynamics.utils.AsynchTaskListner;
import com.nfinitydynamics.utils.CallRequest;
import com.nfinitydynamics.utils.Constant;
import com.nfinitydynamics.utils.Utils;

import org.joda.time.DateTime;
import org.joda.time.Period;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.concurrent.TimeUnit;

import static com.nfinitydynamics.activity.MainActivity.toolbar;
import static com.nfinitydynamics.utils.App.gender;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddExperienceFragment extends Fragment implements AsynchTaskListner {


    public EditText edtCompanyName, edtJoiningDate, edtLeavingDate, edtShipName, edt_tonnage, edtSeaDuration;
    public Spinner spinnerShip, spinnerRank,spinnerEngine,spinner_tonnage;
    public ArrayList<RankModel> rankList = new ArrayList<>();
    public ArrayList<String> rankStringList = new ArrayList<>();
    public ArrayList<ShipModel> shipList = new ArrayList<>();
    public ArrayList<EngineModel> engineList = new ArrayList<>();
    public ArrayList<String> shipStringList = new ArrayList<>();
    public ArrayList<String> engineStringList = new ArrayList<>();
    public ArrayList<Tonnage> tonnageList = new ArrayList<>();
    public ArrayList<String> tonnageStringList = new ArrayList<>();
    public AddExperienceFragment instance;
    public String rankvalue="", shipType="", engineType="",strTonnageType="";
    public Button btnSaveExperience;
    public String companyName, joinDate, leaveDate, candidateId, shipName, grt, seaDuration, experienceId,tonnageValue,tonnageType;
    public SharedPreferences shared;
    public int value;
    public long numberOfDays = 0;
    public static final String DATE_FORMAT = "yyyy/M/d";
    public Date date1, date2;
    Date dateJoin, dateLeaving;
    public int mYear;
    public int mMonth;
    public int mDay;
    public String time = null,tonnage="";
    String diff = "";
    public String[] tonnageArray = {"GRT", "DWT", "TEUs", "M3","CEUs","LIMs"};


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view;
        view = inflater.inflate(R.layout.fragment_add_experience, container, false);
        instance = this;
        toolbar.setTitle("Add Experience Details");
        shared = getActivity().getSharedPreferences(getContext().getPackageName(), 0);
        candidateId = shared.getString("userId", "");
        edtCompanyName = view.findViewById(R.id.edt_company_name);
        edtJoiningDate = view.findViewById(R.id.edt_joining_date);
        edtLeavingDate = view.findViewById(R.id.edt_leaving_date);
        edtCompanyName = view.findViewById(R.id.edt_company_name);
        edtShipName = view.findViewById(R.id.edt_ship_name);
        edt_tonnage = view.findViewById(R.id.edt_tonnage);
        edtSeaDuration = view.findViewById(R.id.edt_sea_duration);
        spinnerShip = view.findViewById(R.id.spinner_ship_type);
        spinnerRank = view.findViewById(R.id.spinner_rank);
        spinner_tonnage = view.findViewById(R.id.spinner_tonnage);
        spinnerEngine = view.findViewById(R.id.spinner_engine_type);
        btnSaveExperience = view.findViewById(R.id.btn_save_experience);
        value = getArguments().getInt("value");
         /* ArrayAdapter aaGender = new ArrayAdapter(getActivity(), R.layout.spinner_text, tonnageArray);
        aaGender.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);


        if (tonnage != null) {
            int spinnerPosition = aaGender.getPosition(tonnage);
            spinner_tonnage.setSelection(spinnerPosition + 1);
        }*/
     /*   spinner_tonnage.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    tonnage = tonnageArray[position - 1];
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });*/

        if (value == 1) {
            toolbar.setTitle("Edit Experience Details");
            experienceId = getArguments().getString("id");
            companyName = getArguments().getString("cName");
            rankvalue = getArguments().getString("rank");
            shipType = getArguments().getString("shipType");
            joinDate = getArguments().getString("joingDate");
            grt = getArguments().getString("grt");
            shipName = getArguments().getString("shipName");
            seaDuration = getArguments().getString("seaDuration");
            engineType = getArguments().getString("engineType");
            leaveDate = getArguments().getString("leavingDate");
            tonnageType = getArguments().getString("tonnageType");
            tonnageValue = getArguments().getString("tonnageValue");
            edt_tonnage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    joinDate = edtJoiningDate.getText().toString();
                    leaveDate = edtLeavingDate.getText().toString();

                    Date initDateJoin = null, initDateLeaving = null;
                    try {
                        initDateJoin = new SimpleDateFormat("dd-MMM-yyyy").parse(joinDate);
                        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MMM-dd");
                        joinDate = formatter.format(initDateJoin);
                        initDateLeaving = new SimpleDateFormat("dd-MMM-yyyy").parse(leaveDate);
                        leaveDate = formatter.format(initDateLeaving);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }


                }
            });
            edtCompanyName.setText(companyName);
            Utils.converDateToDDMMYY(edtJoiningDate, joinDate);
            Utils.converDateToDDMMYY(edtLeavingDate, leaveDate);
            edtShipName.setText(shipName);
            edt_tonnage.setText(tonnageValue);
            edtSeaDuration.setText(seaDuration);

           /* if(tonnageType.equalsIgnoreCase("GRT")){
                spinner_tonnage.setSelection(1);
            }else if(tonnageType.equalsIgnoreCase("GRT")){
                spinner_tonnage.setSelection(2);
            }else if(tonnageType.equalsIgnoreCase("TEUs")){
                spinner_tonnage.setSelection(3);
            }else if(tonnageType.equalsIgnoreCase("M3")){
                spinner_tonnage.setSelection(4);
            }else if(tonnageType.equalsIgnoreCase("CEUs")){
                spinner_tonnage.setSelection(5);
            }else if(tonnageType.equalsIgnoreCase("LIMs")){
                spinner_tonnage.setSelection(6);
            }*/

        }
        getRank();
        edtJoiningDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                //  Utils.generateDatePicker(getContext(), edtJoiningDate);
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dpd = new DatePickerDialog(getActivity(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                edtJoiningDate.setText(Utils.getDateFormat("d", "dd", String.valueOf(dayOfMonth)) + "-" + Utils.getDateFormat("M", "MMM", String.valueOf((monthOfYear + 1))) + "-" + year);
                                joinDate = Utils.getDateFormat("d", "dd", String.valueOf(dayOfMonth)) + "-" + Utils.getDateFormat("M", "MMM", String.valueOf((monthOfYear + 1))) + "-" + year;
                            }
                        }, mYear, mMonth, mDay);
                dpd.show();
                dpd.getDatePicker().setMaxDate(new Date().getTime());
                edtLeavingDate.setText("");


            }
        });
        edtLeavingDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dpd = new DatePickerDialog(getActivity(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                edtLeavingDate.setText(Utils.getDateFormat("d", "dd", String.valueOf(dayOfMonth)) + "-" + Utils.getDateFormat("M", "MMM", String.valueOf((monthOfYear + 1))) + "-" + year);
                                leaveDate = Utils.getDateFormat("d", "dd", String.valueOf(dayOfMonth)) + "-" + Utils.getDateFormat("M", "MMM", String.valueOf((monthOfYear + 1))) + "-" + year;
                                joinDate = edtJoiningDate.getText().toString();
                                System.out.println("joing date===" + joinDate + "leaving date===" + leaveDate);
                                countDifference(joinDate, leaveDate);

                            }
                        }, mYear, mMonth, mDay);
                dpd.getDatePicker().setMaxDate(new Date().getTime());
                dpd.show();


            }
        });


        spinnerRank.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                try {
                    rankvalue = rankList.get(i - 1).getRankId();
                } catch (IndexOutOfBoundsException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinnerShip.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                try {
                    shipType = shipList.get(i - 1).getShipID();
                } catch (IndexOutOfBoundsException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        spinnerEngine.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                try {
                    engineType = engineList.get(i-1).getEngineTypeID();
                } catch (IndexOutOfBoundsException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        spinner_tonnage.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                try {
                    strTonnageType = tonnageList.get(i-1).getTonnageType();
                } catch (IndexOutOfBoundsException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });



        btnSaveExperience.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Calendar cal = Calendar.getInstance();
                Date sysDate = cal.getTime();
                companyName = edtCompanyName.getText().toString();
                joinDate = edtJoiningDate.getText().toString();
                leaveDate = edtLeavingDate.getText().toString();
                shipName = edtShipName.getText().toString();
                grt = edt_tonnage.getText().toString();
                seaDuration = edtSeaDuration.getText().toString();
                if(spinner_tonnage.getSelectedItem()!=null){
                    strTonnageType = spinner_tonnage.getSelectedItem().toString();

                }
               Date initDateJoin = null, initDateLeaving = null;
                try {
                    initDateJoin = new SimpleDateFormat("dd-MMM-yyyy").parse(joinDate);
                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                    joinDate = formatter.format(initDateJoin);
                    initDateLeaving = new SimpleDateFormat("dd-MMM-yyyy").parse(leaveDate);
                    leaveDate = formatter.format(initDateLeaving);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                if (companyName.equals("")) {
                    edtCompanyName.findFocus();
                    Utils.showAlert("Please Enter Company Name", getContext());
                } else if (shipName.equals("")) {
                    edtShipName.findFocus();
                    Utils.showAlert("Please Enter Ship Name", getContext());
                }else if (TextUtils.isEmpty(shipType)) {
                    spinnerShip.findFocus();
                    Utils.showAlert("Please Select Ship Type", getContext());
                }else if (TextUtils.isEmpty(rankvalue)) {
                    spinnerRank.findFocus();
                    Utils.showAlert("Please Select Rank", getContext());
                }else if (TextUtils.isEmpty(strTonnageType)) {
                    spinner_tonnage.findFocus();
                    Utils.showAlert("Please Select Tonnage/Engine Power", getContext());
                }else if (edt_tonnage.getText().toString().equals("")) {
                    edt_tonnage.findFocus();
                    Utils.showAlert("Please Enter Tonnage/Engine Power Value", getContext());
                } else if (joinDate.equals("")) {
                    edtJoiningDate.findFocus();
                    Utils.showAlert("Please Enter Sign On  Date", getContext());
                } else if (leaveDate.equals("")) {
                    edtLeavingDate.findFocus();
                    Utils.showAlert("Please Enter Sign Off Date", getContext());
                }  else if (seaDuration.equals("")) {
                    edtSeaDuration.findFocus();
                    Utils.showAlert("Please Enter Sea Duration", getContext());
                } else if (joinDate.compareTo(leaveDate) > 0) {
                    Utils.showAlert("Please Enter Valid Sign Off Date", getContext());
                } else if (sysDate.compareTo(initDateJoin) < 0) {
                    Utils.showAlert("Please Enter Valid Sign On Date", getContext());
                } else if (joinDate.equalsIgnoreCase(leaveDate)) {
                    Utils.showAlert("Please Enter Valid Both Date", getContext());
                }
                else {
                    if (value == 0) {
                        addExperienceDetail();
                    }
                    if (value == 1) {
                        updateExperience();
                    }
                }
            }

        });

//        String startDate = "11-Apr-2018";
//        String endDate = "02-Jun-2018";

      //  getDaysBetweenDates(startDate, endDate);
        return view;
    }


    public void countDifference(String start, String end) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
        Date startDate = null, endDate = null;
        try {
            startDate = dateFormat.parse(start);
            endDate = dateFormat.parse(end);
            Calendar startCalendar = new GregorianCalendar();
            startCalendar.setTime(startDate);
            Calendar endCalendar = new GregorianCalendar();
            endCalendar.setTime(endDate);
            int startYear = startCalendar.get(Calendar.YEAR);
            int startDay = startCalendar.get(Calendar.DAY_OF_MONTH);
            int startMonth = startCalendar.get(Calendar.MONTH) + 1;

            int endMonth = endCalendar.get(Calendar.MONTH) + 1;
            int endDay = endCalendar.get(Calendar.DAY_OF_MONTH);
            int endYear = endCalendar.get(Calendar.YEAR);


            int day = 0;
            int month = 0;
            int year = 0;
            if (endYear == startYear) {
                year = 0;
                System.out.println("Calculation :  in Same year");

                if (startMonth == endMonth) {
                    System.out.println("Calculation :  in Same month");

                    // in same month
                    if (endDay == startDay) {
                        System.out.println("Calculation :  in Same day");
                        day = 1;
                        month = 0;

                    } else if (endDay > startDay) {
                        System.out.println("Calculation :  in endDay > startDay");
                        day = endDay - startDay;
                        month = 0;
                    } else {
                        System.out.println("Calculation :  in startDay > endDay  is not valid");
                        // is not valid days
                    }
                } else if (endMonth > startMonth) {
                    System.out.println("Calculation :  in endMonth > startMonth");
                    // in same month
                    if (endDay == startDay) {
                        System.out.println("Calculation :  in Same day");
                        day = 1;
                        month = endMonth - startMonth;
                    } else if (endDay > startDay) {
                        System.out.println("Calculation :  in endDay > startDay");
                        day = endDay - startDay;
                        month = (endMonth - startMonth);
                        day++;
                    } else if (startDay > endDay) {
                        month = (endMonth - startMonth);
                        month--;
                        int xStartMonthDays = (getDaysOfMonth(startMonth, startCalendar.get(Calendar.YEAR)) - startDay);
                        day = xStartMonthDays+ endDay;
                        day++;
                        System.out.println("Calculation :  in startDay > endDay");
                        // is not valid days
                    }
                    // valid month
                } else {
                    System.out.println("Calculation :  in endMonth > startMonth  is not valid");
                    // is not valid month in same year
                }
            } else if (endYear > startYear) {
                System.out.println("Calculation :  in endYear > startYear");

                if (startMonth == endMonth) {
                    System.out.println("Calculation :  in Same month");

                    // in same month
                    if (endDay == startDay) {
                        System.out.println("Calculation :  in Same day");
                        day = 1;
                        month = 0;
                        year = endYear - startYear;
                    } else if (endDay > startDay) {
                        System.out.println("Calculation :  in endDay > startDay");
                        day = endDay - startDay;

                        month = 0;
                        year = endYear - startYear;
                    } else if (startDay > endDay) {
                        System.out.println("Calculation :  in startDay > endDay ");
                        year = endYear - startYear;
                        year--;
                        month = 11;

                        int xStartMonthDays = (getDaysOfMonth(startMonth, startCalendar.get(Calendar.YEAR)) - startDay);
                        day = xStartMonthDays+ endDay;
                        day++;
                        // is not valid days
                    }
                } else if (endMonth > startMonth) {
                    System.out.println("Calculation :  in endMonth > startMonth");
                    // in same month
                    if (endDay == startDay) {
                        System.out.println("Calculation :  in Same day");
                        year = endYear - startYear;
                        day = 1;
                        month = endMonth - startMonth;
                    } else if (endDay > startDay) {
                        System.out.println("Calculation :  in endDay > startDay");
                        year = endYear - startYear;
                        day = endDay - startDay;
                        day++;
                        month = (endMonth - startMonth);

                    } else if (startDay > endDay) {
                        System.out.println("Calculation :  in startDay > endDay ");
                        year = endYear - startYear;
                        year--;
                        month = (endMonth - startMonth);
                        month--;
                        int xStartMonthDays = (getDaysOfMonth(startMonth, startCalendar.get(Calendar.YEAR)) - startDay);
                        int xEndMonthDays =endDay;
                        day = xStartMonthDays + xEndMonthDays;
                        // is not valid days
                    }
                } else if (startMonth > endMonth){
                    System.out.println("Calculation :  in startMonth > endMonth");
                    // in same month
                    if (endDay == startDay) {
                        System.out.println("Calculation :  in Same day");
                        year = endYear - startYear;
                        year--;
                        day = 1;
                        month = 12-(Math.abs(endMonth - startMonth));
                    } else if (endDay > startDay) {
                        System.out.println("Calculation :  in endDay > startDay");
                        year = endYear - startYear;
                        year--;
                        month = 12-(Math.abs(endMonth - startMonth));
                        day = endDay - startDay;
                        day++;


                    } else if (startDay > endDay) {
                        System.out.println("Calculation :  in startDay > endDay ");
                        year = endYear - startYear;
                        year--;

                        month = 12-(Math.abs(endMonth - startMonth));
                        month--;


                        int xStartMonthDays = (getDaysOfMonth(endMonth, endCalendar.get(Calendar.YEAR)) - startDay);
                        int xEndMonthDays =endDay;
                        day = xStartMonthDays + xEndMonthDays;
                        // is not valid days
                    }
                }
            } else {
                /// date is not valid
            }


            if(day>=30){
                int calculatedMonthsByYear = year * 12 + month+1;


                System.out.println("Calculation   --->  "+calculatedMonthsByYear + " Months " + "0" +" days" );
                edtSeaDuration.setText(calculatedMonthsByYear + " Months " + "0" +" Days");
                System.out.println("Calculation Day is Greather then 30");
            }else{
                int calculatedMonthsByYear = year * 12 + month;

                System.out.println("Calculation   --->  "+calculatedMonthsByYear + " Months " + day +" days" );
                edtSeaDuration.setText(calculatedMonthsByYear + " Months " + day +" Days");

            }

            //  edtSeaDuration.setText(calculatedMonthsByYear + " Months ");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public int getDaysOfMonth(int month, int year) {

        System.out.println(" final Date Month :" + month);
        System.out.println(" final Date year :" + year);
        int day = 0;
        switch (month) {
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                day = 31;
                break;
            case 4:
            case 6:
            case 9:
            case 11:
                day = 30;
                break;

            case 2:
                if (year % 4 == 0) {
                    day = 29;
                } else {
                    day = 28;
                }
//// remove below lin
                day = 30;

                break;
            default:
                day = 31;

                break;

        }

        System.out.println("Calculation final Date day return :" + day);
        return day;
    }

    public long getUnitBetweenDates(Date startDate, Date endDate, TimeUnit unit) {
        long timeDiff = endDate.getTime() - startDate.getTime();
        return unit.convert(timeDiff, TimeUnit.MILLISECONDS);
    }

    private void updateExperience() {

        System.out.println("candidate===="+candidateId);
        new CallRequest(instance).updateExperienceDetail(candidateId, companyName, shipName, joinDate, leaveDate, shipType, engineType, rankvalue, grt, seaDuration, "1", experienceId,strTonnageType,edt_tonnage.getText().toString());
    }

    private void getEngineType() {
        new CallRequest(instance).getEngineType();
    }

    private void getTonnageValue() {
        new CallRequest(instance).getTonnageType();
    }
    private void addExperienceDetail() {

        new CallRequest(instance).addExperienceDetail(candidateId, companyName, shipName, joinDate, leaveDate, shipType, engineType, rankvalue, grt, seaDuration, "0",strTonnageType,edt_tonnage.getText().toString());

    }

    private void getRank() {
        new CallRequest(instance).getRankFragment();
    }

    private void getShipType() {

        new CallRequest(instance).getShipFragment();
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case getRankFragment:
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            JSONArray ob1 = jObj.getJSONArray("data");
                            for (int i = 0; i < ob1.length(); i++) {
                                JSONObject ob = ob1.getJSONObject(i);
                                String rankId = ob.getString("RankID");
                                String rankName = ob.getString("Name");
                                rankList.add(new RankModel(rankId, rankName));
                                rankStringList.add(rankName);
                            }
                            ArrayAdapter aa = new ArrayAdapter(getContext(), R.layout.spinner_text, rankStringList);
                            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spinnerRank.setAdapter(new NothingSelectedSpinnerAdapter(
                                    aa, R.layout.contact_spinner_row_nothing_selected,
                                    getContext()));
                            //spinnerRank.setAdapter(aa);
                            if (rankvalue != null) {
                                int spinnerPosition = aa.getPosition(rankvalue);
                                Log.d("spinnerPosition", String.valueOf(aa.getPosition(rankvalue)));
                                spinnerRank.setSelection(spinnerPosition+1);
                            }

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();

                        Log.d("new exception:", String.valueOf(e));
                    }

                    getShipType();
                    break;
                case getShipType:

                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            JSONArray ob1 = jObj.getJSONArray("data");
                            for (int i = 0; i < ob1.length(); i++) {
                                JSONObject ob = ob1.getJSONObject(i);
                                String shipId = ob.getString("ShipID");
                                String shipName = ob.getString("ShipType");

                                shipList.add(new ShipModel(shipId, shipName));

                                shipStringList.add(shipName);
                            }

                            ArrayAdapter aa = new ArrayAdapter(getContext(), R.layout.spinner_text, shipStringList);
                            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spinnerShip.setAdapter(new NothingSelectedSpinnerAdapter(
                                    aa, R.layout.contact_spinner_row_nothing_selected_ship,
                                    getContext()));
                            //spinnerShip.setAdapter(aa);
                            if (shipType != null) {
                                int spinnerPosition = aa.getPosition(shipType);
                                Log.d("spinnerPosition", String.valueOf(aa.getPosition(shipType)));
                                spinnerShip.setSelection(spinnerPosition+1);
                            }

                        } else {
                            Utils.showToast(jObj.getString("message"), getContext());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();

                        Log.d("new exception:", String.valueOf(e));
                    }
                 getEngineType();
                    break;
                case addExperience:
                    Utils.hideProgressDialog();

                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            Utils.showToast("Added Successfully", getContext());
                            MainActivity.activity.onBackPressed();
                        } else {
                            Utils.showAlert(jObj.getString("message"), getContext());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();

                        Log.d("new exception:", String.valueOf(e));
                    }
                    break;
                case getEngineType:
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            JSONArray ob1 = jObj.getJSONArray("data");
                            for (int i = 0; i < ob1.length(); i++) {
                                JSONObject ob = ob1.getJSONObject(i);
                                String engineId = ob.getString("EngineTypeID");
                                String engineName = ob.getString("EngineType");

                                engineList.add(new EngineModel(engineId, engineName));

                                engineStringList.add(engineName);
                            }

                            ArrayAdapter aa = new ArrayAdapter(getContext(), R.layout.spinner_text, engineStringList);
                            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spinnerEngine.setAdapter(new NothingSelectedSpinnerAdapter(
                                    aa,R.layout.contact_spinner_row_nothing_selected_engine,
                                    getContext()));
                            //spinnerEngine.setAdapter(aa);
                            getTonnageValue();
                            if (engineType != null) {
                                int spinnerPosition = aa.getPosition(engineType);
                                spinnerEngine.setSelection(spinnerPosition+1);
                            }


                        } else {
                            Utils.showToast(jObj.getString("message"), getContext());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();

                    }
                    Utils.hideProgressDialog();

                    break;

                case getTonnageType:
                    int spinnerPosition=0;
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            JSONArray ob1 = jObj.getJSONArray("data");
                            for (int i = 0; i < ob1.length(); i++) {
                                JSONObject ob = ob1.getJSONObject(i);
                                String tonnageId = ob.getString("TonnageTypeID");
                                String tonnageName = ob.getString("TonnageType");

                                tonnageList.add(new Tonnage(tonnageId, tonnageName));

                                tonnageStringList.add(tonnageName);

                                if(tonnageName.equalsIgnoreCase(tonnageType)){
                                     spinnerPosition = i;

                                }
                            }

                            ArrayAdapter aa = new ArrayAdapter(getContext(), R.layout.spinner_text, tonnageStringList);
                            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spinner_tonnage.setAdapter(new NothingSelectedSpinnerAdapter(
                                    aa,R.layout.tonnage_spinner_row_nothing_selected,
                                    getContext()));
                            if (tonnageType != null) {
                                spinnerPosition = aa.getPosition(tonnageType);
                                spinner_tonnage.setSelection(spinnerPosition+1);
                            }


                            //spinnerEngine.setAdapter(aa);
                               //   spinner_tonnage.setSelection(spinnerPosition+1);



                        } else {
                            Utils.showToast(jObj.getString("message"), getContext());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();

                    }
                    Utils.hideProgressDialog();

                    break;
                case updateExperience:

                    Utils.hideProgressDialog();

                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            Utils.showToast("Updated Successfully", getContext());
                            MainActivity.activity.onBackPressed();
                        } else {
                            Utils.showAlert(jObj.getString("message"), getContext());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;

            }

        }
    }


}
