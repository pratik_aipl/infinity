package com.nfinitydynamics.fragment;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ListView;
import android.widget.TextView;

import com.nfinitydynamics.R;
import com.nfinitydynamics.adapter.LatestJobAdapter;
import com.nfinitydynamics.model.LatestJobModel;
import com.nfinitydynamics.utils.JsonParserUniversal;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class CompanyJobListFragment extends Fragment {

    public ArrayList<LatestJobModel> latestJobList = new ArrayList<>();
    public LatestJobAdapter companyJobAdapter;
    public JsonParserUniversal jParser;
    public SharedPreferences shared;
    public CompanyJobListFragment instance;
    public String image, jobTitle, company, shipType, description, companyId, jobId, rank, isApplied, userId;
    public ListView lvJob;
    public View view;
    public TextView txtNoData;
    WebView myWebView;
    public String url;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_company_job_list, container, false);
        instance = this;
        url = getArguments().getString("url");
        companyId = getArguments().getString("companyID");

        Log.i("URL", "==>" + url);
        ;
        myWebView = view.findViewById(R.id.webview);
        myWebView.getSettings().setJavaScriptEnabled(true);
        myWebView.loadUrl(url);
        //  getCompanyJob();
        return view;
    }


}
