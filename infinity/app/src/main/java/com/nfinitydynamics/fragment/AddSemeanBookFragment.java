package com.nfinitydynamics.fragment;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.nfinitydynamics.R;
import com.nfinitydynamics.activity.MainActivity;
import com.nfinitydynamics.adapter.NothingSelectedSpinnerAdapter;
import com.nfinitydynamics.model.CountryModel;
import com.nfinitydynamics.model.RankModel;
import com.nfinitydynamics.utils.AsynchTaskListner;
import com.nfinitydynamics.utils.CallRequest;
import com.nfinitydynamics.utils.Constant;
import com.nfinitydynamics.utils.JsonParserUniversal;
import com.nfinitydynamics.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import static com.nfinitydynamics.activity.MainActivity.toolbar;
import static com.nfinitydynamics.utils.App.companyName;
import static com.nfinitydynamics.utils.App.rankvalue;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddSemeanBookFragment extends Fragment implements AsynchTaskListner {

    public EditText edtNumber, edtValidity;
    public String number, placeIssue, validity,candidateId,id,uNumber,uPlace,uValidity;
    public AddSemeanBookFragment instance;
    public SharedPreferences shared;
    public Button btnSave;

    public  int s,s1;
    public JsonParserUniversal jParser;
    public ArrayList<CountryModel> countryModelArrayList=new ArrayList<CountryModel>();
    public ArrayList<String> countyStringList=new ArrayList<>();
    public int selectedPos;
    public Spinner spPlaceIssue;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view;
        view = inflater.inflate(R.layout.fragment_add_semean_book, container, false);
        instance=this;
        jParser=new JsonParserUniversal();
        toolbar.setTitle("Add Seaman Book Details");
        edtNumber =  view.findViewById(R.id.edt_number);
        spPlaceIssue =  view.findViewById(R.id.sp_place_issue);
        edtValidity =  view.findViewById(R.id.edt_validity);
        btnSave=view.findViewById(R.id.btn_save_experience);
        Bundle b = getArguments();
        s = b.getInt("value");

        getCountry();
        if(s==1){
            toolbar.setTitle("Edit Seaman Book Details");
            id=b.getString("id");
            uNumber=b.getString("number");
            uValidity=b.getString("validity");
            uPlace=b.getString("place");
            edtNumber.setText(uNumber);

            try {
                Utils.converDateToDDMMYY(edtValidity, uValidity);
            }catch (Exception e){
                e.printStackTrace();
            }
            //spPlaceIssue.setText(uPlace);
        }

        shared = getActivity().getSharedPreferences(getContext().getPackageName(), 0);
        candidateId = shared.getString("userId", "");
        edtValidity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager)getContext(). getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                Utils.generateDatePicker(getContext(), edtValidity);
            }
        });
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                number = edtNumber.getText().toString().trim();
             //   countryId = edtPlaceIssue.getText().toString().trim();
                validity = edtValidity.getText().toString().trim();

                Calendar cal = Calendar.getInstance();
                Date sysDate = cal.getTime();
                Date intValidity = null;
                try {
                    intValidity = new SimpleDateFormat("dd-MMM-yyyy").parse(validity);
                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                    validity = formatter.format(intValidity);
                    } catch (ParseException e) {
                    e.printStackTrace();
                }
                if (number.equals("")) {
                    edtNumber.findFocus();
                    Utils.showAlert("Please Enter Seaman Book No.", getContext());
                } else if (TextUtils.isEmpty(placeIssue)) {
                  //  edtPlaceIssue.findFocus();
                    Utils.showAlert("Please Select Country ", getContext());
                }else if (validity.equals("")) {
                    edtValidity.findFocus();
                    Utils.showAlert("Please Enter Validity Date", getContext());
                }else if(sysDate.compareTo(intValidity)>0){
                    Utils.showAlert("Please Enter Valid Validity", getContext());
                }
                else {

                    if(s==0){
                        new CallRequest(instance).addSeamenBook(candidateId,number,placeIssue,validity,"0");
                    }if(s==1) {

                        new CallRequest(instance).updateSeamenBook(candidateId,number,placeIssue,validity,"1",id);
                    }

                }

            }
        });
        spPlaceIssue.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                try {
                    placeIssue = countryModelArrayList.get(i - 1).getCountryID();
                } catch (IndexOutOfBoundsException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        return view;
    }

    private void getCountry() {
        new CallRequest(instance).getCountry();
    }


    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case addSemeanBook:

                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                                Utils.showToast("Added Successfully",getContext());
                            MainActivity.activity.onBackPressed();
                            }
                            else {
                            Utils.showToast("Try Again!!",getContext());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d("new exception:", String.valueOf(e));
                    }
                    break;


                case  updateSemeanBook:

                    Utils.hideProgressDialog();

                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            Utils.showToast("Updated Successfully",getContext());
                            MainActivity.activity.onBackPressed();
                        }
                        else {
                            Utils.showToast("Try Again!!",getContext());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d("new exception:", String.valueOf(e));
                    }
                    break;
                case  getCountryList:

                    Utils.hideProgressDialog();

                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            JSONArray ob1 = jObj.getJSONArray("data");
                            CountryModel countryModel ;
                            for (int i = 0; i < ob1.length(); i++) {
                                countryModel = new CountryModel();
                                countryModel = (CountryModel) jParser.parseJson(ob1.getJSONObject(i), new CountryModel());
                                countryModelArrayList.add(countryModel);
                                if (uPlace != null) {
                                    if (uPlace.equalsIgnoreCase(countryModel.getCountryName())) {
                                        selectedPos = i + 1;
                                    }
                                }
                                countyStringList.add(countryModel.getCountryName());
                            }
                            ArrayAdapter aa = new ArrayAdapter(getActivity(), R.layout.spinner_text, countyStringList);
                            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spPlaceIssue.setAdapter(new NothingSelectedSpinnerAdapter(
                                    aa, R.layout.country_place_issue,
                                    getActivity()));
                            spPlaceIssue.setSelection(selectedPos);
                        }
                        else {
                            Utils.showToast("Try Again!!",getContext());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d("new exception:", String.valueOf(e));
                    }
                    break;
            }
        }
    }
}
