package com.nfinitydynamics.fragment.companyFragment;


import android.app.Activity;
import android.app.Dialog;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;

import com.nfinitydynamics.R;
import com.nfinitydynamics.activity.CompanyActivity.CompanyDashboard;
import com.nfinitydynamics.fragment.EditProfileFragment;
import com.nfinitydynamics.model.UserDetailModel;
import com.nfinitydynamics.utils.App;
import com.nfinitydynamics.utils.AsynchTaskListner;
import com.nfinitydynamics.utils.CallRequest;
import com.nfinitydynamics.utils.Constant;
import com.nfinitydynamics.utils.JsonParserUniversal;
import com.nfinitydynamics.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import static com.nfinitydynamics.activity.CompanyActivity.CompanyDashboard.activityCompany;
import static com.nfinitydynamics.activity.CompanyActivity.CompanyDashboard.toolbar;
import static com.nfinitydynamics.activity.MainActivity.changeFragment;
import static com.nfinitydynamics.utils.Utils.hasPermissions;

/**
 * A simple {@link Fragment} subclass.
 */
public class BulidComanyResumeFragment extends Fragment implements AsynchTaskListner {
    public JsonParserUniversal jParser;
    public BulidComanyResumeFragment instance;
    public String candidateId, companyId;
    public SharedPreferences shared;
    public WebView myWebView;
    public int value;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view;
        view = inflater.inflate(R.layout.fragment_bulid_comany_resume, container, false);
        jParser = new JsonParserUniversal();
        instance = this;
        toolbar.setTitle("Build Resume");
        myWebView = view.findViewById(R.id.webview);
        shared = getActivity().getSharedPreferences(getContext().getPackageName(), 0);
        companyId = shared.getString("userId", "");

        candidateId = getArguments().getString("candidateId");
        new CallRequest(instance).bulidResumeCompamy(candidateId, companyId);
        return view;
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case bulidResumeCompany:

                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            UserDetailModel userDetailModel;
                            userDetailModel = new UserDetailModel();
                            userDetailModel = (UserDetailModel) jParser.parseJson(jObj, new UserDetailModel());
                            String resumeUrl = userDetailModel.getResume_url();
                            Log.d("resume::", resumeUrl);
                            myWebView.getSettings().setJavaScriptEnabled(true);
                            // String pdf = "http://docs.google.com/gview?embedded=true&url=" + resumeUrl;
                            String pdf = "https://drive.google.com/viewerng/viewer?embedded=true&url=" + resumeUrl;
                            myWebView.loadUrl(pdf);
                            // new DownloadFile().execute(resumeUrl, "maven.pdf");
                            //  Utils.showToast("Downlod Successfully", getContext());
                            // MainActivity.changeFragment(new HistoryFragment(), false);

                        } else {

                            final Dialog dialog = new Dialog(getActivity());
                            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            dialog.setCancelable(true);
                            dialog.setContentView(R.layout.dialog_download_again);
                            dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                            Button btnNo = dialog.findViewById(R.id.btnNo);
                            Button btnYes = dialog.findViewById(R.id.btnYes);

                            TextView txtMessage = dialog.findViewById(R.id.txtMessage);
                            txtMessage.setText(jObj.getString("message"));

                            btnYes.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog.dismiss();
                                    new CallRequest(instance).reDownlodeResumeCompamy(candidateId, companyId, "1");
                                }
                            });

                            btnNo.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog.dismiss();
                                    activityCompany.onBackPressed();
                                }
                            });

                            dialog.show();

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d("new exception:", String.valueOf(e));
                    }
                    break;
                case reDownlode:

                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            UserDetailModel userDetailModel;
                            userDetailModel = new UserDetailModel();
                            userDetailModel = (UserDetailModel) jParser.parseJson(jObj, new UserDetailModel());
                            String resumeUrl = userDetailModel.getResume_url();
                            Log.d("resume::", resumeUrl);
                            myWebView.getSettings().setJavaScriptEnabled(true);
                            myWebView.getSettings().setAllowFileAccess(true);
                            String pdf = "https://drive.google.com/viewerng/viewer?embedded=true&url=" + resumeUrl;
                            myWebView.loadUrl(pdf);
                        } else {

                            Utils.showAlert(jObj.getString("message"), getActivity());

                            activityCompany.onBackPressed();

//                            final Dialog dialog = new Dialog(getActivity());
//                            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//                            dialog.setCancelable(true);
//                            dialog.setContentView(R.layout.dialog_download_again);
//                            dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
//
//                            Button btnNo = dialog.findViewById(R.id.btnNo);
//                            Button btnYes = dialog.findViewById(R.id.btnYes);
//
//                            TextView txtMessage = dialog.findViewById(R.id.txtMessage);
//                            txtMessage.setText(jObj.getString("message"));
//
//                            btnYes.setOnClickListener(new View.OnClickListener() {
//                                @Override
//                                public void onClick(View v) {
//                                    dialog.dismiss();
//                                    new CallRequest(instance).reDownlodeResumeCompamy(candidateId, companyId, "1");
//                                }
//                            });
//                            btnNo.setOnClickListener(new View.OnClickListener() {
//                                @Override
//                                public void onClick(View v) {
//                                    dialog.dismiss();
//                                    activityCompany.onBackPressed();
//                                }
//                            });
//                            dialog.show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d("new exception:", String.valueOf(e));
                    }
                    break;
            }
        }
    }

    public void openDialog() {

    }
}
