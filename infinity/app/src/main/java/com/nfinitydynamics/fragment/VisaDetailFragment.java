package com.nfinitydynamics.fragment;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.nfinitydynamics.R;
import com.nfinitydynamics.activity.MainActivity;
import com.nfinitydynamics.adapter.VisaAdapter;
import com.nfinitydynamics.model.VisaModel;
import com.nfinitydynamics.utils.AsynchTaskListner;
import com.nfinitydynamics.utils.CallRequest;
import com.nfinitydynamics.utils.Constant;
import com.nfinitydynamics.utils.JsonParserUniversal;
import com.nfinitydynamics.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.nfinitydynamics.activity.MainActivity.toolbar;

public class VisaDetailFragment extends Fragment implements AsynchTaskListner{

    public FloatingActionButton fabAdd ;
    public SharedPreferences shared;
    public ListView lvVisa;
    public String candidateId;
    public ArrayList<VisaModel> visaList = new ArrayList<>();
    public JsonParserUniversal jParser;
    public static VisaDetailFragment instance;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view;
        view=inflater.inflate(R.layout.fragment_visa_detail, container, false);
        instance=this;
        shared = getActivity().getSharedPreferences(getContext().getPackageName(), 0);
        candidateId = shared.getString("userId","");
        jParser=new JsonParserUniversal();
        fabAdd = view. findViewById(R.id.fab_add);
        lvVisa=view.findViewById(R.id.lv_visa);
        toolbar.setTitle("Visa Details");

        fabAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AddVisaDetailFragment fragment=new AddVisaDetailFragment();
                Bundle args = new Bundle();
                args.putInt("value",0);
                fragment.setArguments(args);
                MainActivity.changeFragment(fragment,true);
            }
        });
        getVisaDetail();
        return view;
    }
    private void getVisaDetail() {


        new CallRequest(instance).getVisa(candidateId);
    }
    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case getVisa:
                    Utils.hideProgressDialog();
                    visaList.clear();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {

                            //  Utils.showToast(jObj.getString("message"), getContext());
                            JSONArray ob1 = jObj.getJSONArray("data");

                            VisaModel visaModel;
                            for (int i = 0; i < ob1.length(); i++) {
                                JSONObject jObject = ob1.getJSONObject(i);
                                visaModel = new VisaModel();
                                visaModel = (VisaModel) jParser.parseJson(jObject, new VisaModel());
                                visaList.add(visaModel);
                            }
                            VisaAdapter visaAdapter = new VisaAdapter(getContext(), R.layout.layout_visa, visaList);
                            lvVisa.setAdapter(visaAdapter);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;

                case deleteVisa:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {

                            Utils.showToast("Deleted Successfully",getContext());

                          getVisaDetail();

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }


}
