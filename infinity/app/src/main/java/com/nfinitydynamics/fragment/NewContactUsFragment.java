package com.nfinitydynamics.fragment;


import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.nfinitydynamics.R;
import com.nfinitydynamics.activity.CompanyActivity.CompanyDashboard;
import com.nfinitydynamics.activity.MainActivity;
import com.nfinitydynamics.fragment.companyFragment.CompanyHomeFragment;
import com.nfinitydynamics.model.UserDetailModel;
import com.nfinitydynamics.utils.AsynchTaskListner;
import com.nfinitydynamics.utils.CallRequest;
import com.nfinitydynamics.utils.Constant;
import com.nfinitydynamics.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import static com.nfinitydynamics.utils.Utils.hasPermissions;

/**
 * A simple {@link Fragment} subclass.
 */
public class NewContactUsFragment extends Fragment implements AsynchTaskListner {


    public NewContactUsFragment() {
        // Required empty public constructor
    }


    private View view;
    private EditText edtName, edtEmail, edtPhoneNo, edtSubject, edtMessage;
    private Button btnSend;
    private String name, email, phoneNo, subject, message;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_new_contact_us, container, false);
        edtName = view.findViewById(R.id.edtName);
        edtEmail = view.findViewById(R.id.edtEmail);
        edtPhoneNo = view.findViewById(R.id.edtPhoneNo);
        edtSubject = view.findViewById(R.id.edtSubject);
        edtMessage = view.findViewById(R.id.edtMessage);
        btnSend = view.findViewById(R.id.btnSend);

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMessage();
            }
        });
        return view;
    }

    private void sendMessage() {
        name = edtName.getText().toString();
        email = edtEmail.getText().toString();
        phoneNo = edtPhoneNo.getText().toString();
        subject = edtSubject.getText().toString();
        message = edtMessage.getText().toString();

        if (TextUtils.isEmpty(name)) {
            edtName.requestFocus();
            Utils.showToast("Please Enter Name", getActivity());
        } else if (TextUtils.isEmpty(email)) {
            edtEmail.requestFocus();
            Utils.showToast("Please Enter Email", getActivity());
        } else if (TextUtils.isEmpty(phoneNo)) {
            edtPhoneNo.requestFocus();
            Utils.showToast("Please Enter Phone No", getActivity());
        } else if (TextUtils.isEmpty(subject)) {
            edtSubject.requestFocus();
            Utils.showToast("Please Enter Subject", getActivity());
        } else if (TextUtils.isEmpty(message)) {
            edtMessage.requestFocus();
            Utils.showToast("Please Enter Message", getActivity());
        } else {
            new CallRequest(NewContactUsFragment.this).sendMessage(name, email, phoneNo, subject, message);
        }
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case contactUs:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            Utils.showToast("Message sent successfully\n" +
                                    "Thank you for contacting us.", getContext());
                            getActivity().onBackPressed();
                        } else {
                            Utils.showToast(jObj.getString("message"), getContext());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d("new exception:", String.valueOf(e));
                    }
                    break;
            }
        }
    }
}
