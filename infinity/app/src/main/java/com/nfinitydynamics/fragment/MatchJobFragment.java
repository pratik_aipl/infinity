package com.nfinitydynamics.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nfinitydynamics.R;
import com.nfinitydynamics.adapter.MatchJobAdapter;
import com.nfinitydynamics.model.MatchJobModel;
import com.nfinitydynamics.utils.AsynchTaskListner;
import com.nfinitydynamics.utils.CallRequest;
import com.nfinitydynamics.utils.Constant;
import com.nfinitydynamics.utils.JsonParserUniversal;
import com.nfinitydynamics.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.nfinitydynamics.activity.MainActivity.changeFragment;

public class MatchJobFragment extends Fragment implements AsynchTaskListner {

    View view;
    public MatchJobFragment matchInstance;
    public ListView lvMatcheJob;
    public ArrayList<MatchJobModel> mJobList = new ArrayList<>();
    public SharedPreferences shared;
    public MatchJobAdapter adapter;
    public TextView txtNoData;
    public RelativeLayout relativeList;
    public JsonParserUniversal jParser;
    public String image, jobTitle, company, shipType, description, companyId, jobId, rank, isApplied;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_match_job, container, false);
        matchInstance = this;
        jParser = new JsonParserUniversal();
        shared = getActivity().getSharedPreferences(getContext().getPackageName(), 0);
        lvMatcheJob = view.findViewById(R.id.lv_matchjob);
        txtNoData = view.findViewById(R.id.txt_nodata);
        relativeList = view.findViewById(R.id.relative_list);
        //   final EditText edtSearch=view.findViewById(R.id.edt_search);
        getMatchJob();
      /*  edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String text = edtSearch.getText().toString().toLowerCase(Locale.getDefault());
                adapter.filters(text);

            }
        });
*/

        if (mJobList.size() != 0) {
            relativeList.setVisibility(View.VISIBLE);
            txtNoData.setVisibility(View.GONE);
        }

        return view;
    }

    private void getMatchJob() {

        String candidateId = shared.getString("userId", "");

        Log.d("id-------", candidateId);
        new CallRequest(matchInstance).getMatchJob(candidateId);

    }
//    @Override
//    public void setUserVisibleHint(boolean isVisibleToUser) {
//        super.setUserVisibleHint(isVisibleToUser);
//        if(isVisibleToUser) {
//            Log.i("TAG", "In VISIBle getMatchJob");
//           // getMatchJob();
//        }
//    }
//
//
//    @Override
//    public void onResume() {
//        super.onResume();
//       // getMatchJob();
//    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case getMatchJob:

                    Utils.hideProgressDialog();
                    mJobList.clear();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            JSONArray ob1 = jObj.getJSONArray("data");

                            MatchJobModel mJobModel;
                            for (int i = 0; i < ob1.length(); i++) {
                                relativeList.setVisibility(View.VISIBLE);
                                txtNoData.setVisibility(View.GONE);
                                JSONObject jObject = ob1.getJSONObject(i);
                                mJobModel = new MatchJobModel();
                                mJobModel = (MatchJobModel) jParser.parseJson(jObject, new MatchJobModel());
                                mJobList.add(mJobModel);
                            }
                            if (getActivity() != null) {
                                adapter = new MatchJobAdapter(MatchJobFragment.this, R.layout.layout_home_listview, mJobList);
                                lvMatcheJob.setAdapter(adapter);
                            }
                            lvMatcheJob.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                    JobDetailFragment jobDetailFragment = new JobDetailFragment();
                                    Bundle b = new Bundle();
                                    MatchJobModel model = mJobList.get(i);
                                    image = model.getLogoURL();
                                    jobTitle = model.getJobTitle();
                                    shipType = model.getShipType();
                                    company = model.getCompanyName();
                                    description = model.getDescription();
                                    companyId = model.getCompanyID();
                                    jobId = model.getJobID();
                                    rank = model.getRank();
                                    isApplied = String.valueOf(model.getIs_Applied_Job());
                                    Log.d("compppp--", companyId);
                                    Log.d("value", image + jobTitle + shipType + company + description + companyId);
                                    b.putString("image", image);
                                    b.putString("jobTitle", jobTitle);
                                    b.putString("shipType", shipType);
                                    b.putString("company", company);
                                    b.putString("description", description);
                                    b.putString("companyId", companyId);
                                    b.putString("jobId", jobId);
                                    b.putString("rank", rank);
                                    b.putString("isApplied", isApplied);
                                    b.putString("JobEndDate", model.getJobEndDate());
                                    b.putString("fromExperience", model.getFromExperience());
                                    b.putString("toExperience", model.getToExperience());
                                    jobDetailFragment.setArguments(b);
                                    changeFragment(jobDetailFragment, true);
                                }
                            });
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d("new exception:", String.valueOf(e));
                    }
                    break;
            }
        }
    }
}
