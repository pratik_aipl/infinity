package com.nfinitydynamics.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.nfinitydynamics.R;
import com.nfinitydynamics.adapter.CompanyListAdapter;
import com.nfinitydynamics.adapter.InstituteAdapter;
import com.nfinitydynamics.model.CompanyListModel;
import com.nfinitydynamics.model.Institute;
import com.nfinitydynamics.utils.AsynchTaskListner;
import com.nfinitydynamics.utils.CallRequest;
import com.nfinitydynamics.utils.Constant;
import com.nfinitydynamics.utils.JsonParserUniversal;
import com.nfinitydynamics.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.nfinitydynamics.activity.MainActivity.changeFragment;
import static com.nfinitydynamics.activity.MainActivity.toolbar;

/**
 * Created by empiere-vaibhav on 10/30/2018.
 */

public class InstituteListFragment extends Fragment implements AsynchTaskListner {

    public ArrayList<Institute> companyList = new ArrayList<>();
    public GridView gvCompanyList;
    public InstituteListFragment instance;
    public JsonParserUniversal jParser;
    public String companyID;
    public Institute companyModel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view;
        view = inflater.inflate(R.layout.fragment_company_list, container, false);
        gvCompanyList =  view.findViewById(R.id.gv_company);
        toolbar.setTitle("Institutes");
        jParser = new JsonParserUniversal();
        instance = this;
        getCopanyList();
        return view;
    }

    private void getCopanyList() {
        new CallRequest(instance).getInstituteListFragment();
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case getInstituteListFragment:

                    Utils.hideProgressDialog();
                    companyList.clear();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {

                            //  Utils.showToast(jObj.getString("message"), getContext());
                            JSONArray ob1 = jObj.getJSONArray("data");


                            for (int i = 0; i < ob1.length(); i++) {
                                JSONObject jObject = ob1.getJSONObject(i);
                                companyModel = new Institute();
                                companyModel = (Institute) jParser.parseJson(jObject, new Institute());
                                companyList.add(companyModel);

                            }

                            if (getActivity() != null) {

                                InstituteAdapter latestJobAdapter = new InstituteAdapter(getContext(), R.layout.layout_home_listview, companyList);
                                gvCompanyList.setAdapter(latestJobAdapter);
                            }
                            gvCompanyList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                    CompanyJobListFragment companyJobListFragment = new CompanyJobListFragment();
                                    Bundle b = new Bundle();
                                    b.putString("companyID", companyID);
                                    b.putString("url", companyList.get(i).getWebsite());
                                    companyJobListFragment.setArguments(b);
                                    changeFragment(companyJobListFragment, true);
                                }
                            });



//                            Intent intent = new Intent(getContext(), MainActivity.class);
//                            startActivity(intent);
//                            finish();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;

            }
        }
    }
}

