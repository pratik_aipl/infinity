package com.nfinitydynamics.fragment.companyFragment;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nfinitydynamics.R;
import com.nfinitydynamics.utils.App;
import com.nfinitydynamics.utils.AsynchTaskListner;
import com.nfinitydynamics.utils.CallRequest;
import com.nfinitydynamics.utils.Constant;
import com.nfinitydynamics.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import static com.nfinitydynamics.activity.CompanyActivity.CompanyDashboard.changeFragment;
import static com.nfinitydynamics.activity.CompanyActivity.CompanyDashboard.toolbar;

/**
 * A simple {@link Fragment} subclass.
 */
public class CompanyHomeFragment extends Fragment implements AsynchTaskListner {

    public CompanyHomeFragment instance;

    public TextView txtCountCandidate, txtCountJob, txtCountApplied, txtCountLatetestCandidate, txt_count_shore_job;
    public SharedPreferences shared;
    public String candidateId;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view;
        view = inflater.inflate(R.layout.fragment_company_home, container, false);
        instance = this;
        toolbar.setTitle("DashBoard");
        shared = getActivity().getSharedPreferences(getContext().getPackageName(), 0);
        String totalPostedJobs = String.valueOf(shared.getInt("totalPostedJobs", 0));
        String totalCandidate = String.valueOf(shared.getInt("totalCandidate", 0));
        String totalJobApplication = String.valueOf(shared.getInt("totalJobApplication", 0));
        String totalLatestCandidate = String.valueOf(shared.getInt("totalLatestCandidate", 0));
        String total_shorejob_candidate = String.valueOf(shared.getInt("total_shorejob", 0));

        candidateId = shared.getString("userId", "");
        txtCountCandidate = view.findViewById(R.id.txt_count_candidate);
        txtCountJob = view.findViewById(R.id.txt_count_posted);
        txtCountApplied = view.findViewById(R.id.txt_count_job);
        txtCountLatetestCandidate = view.findViewById(R.id.txt_count_latestCandidate);
        txt_count_shore_job = view.findViewById(R.id.txt_count_shore_job);
        CardView cv_candidate = view.findViewById(R.id.cv_candidate);
        CardView cv_posted_job = view.findViewById(R.id.cv_posted_job);
        CardView cv_jobApplied = view.findViewById(R.id.cv_jobApplied);
        CardView cv_candidateLatest = view.findViewById(R.id.cv_latest_candidate);
        CardView cv_shorjob = view.findViewById(R.id.cv_shorjob);


        cv_candidate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                App.applicationRecevide = false;
                CandidateFragment candidateFragment = new CandidateFragment();
                Bundle bundle = new Bundle();
                bundle.putInt("value", 1);
                candidateFragment.setArguments(bundle);
                changeFragment(candidateFragment, true);
            }
        });
        cv_posted_job.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                App.applicationRecevide = false;
                ManageCompanyJobFragment manageCompanyJobFragment = new ManageCompanyJobFragment();
                Bundle bundle = new Bundle();
                bundle.putInt("value", 1);
                manageCompanyJobFragment.setArguments(bundle);
                changeFragment(manageCompanyJobFragment, true);
            }
        });
        cv_jobApplied.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                App.applicationRecevide = true;
                CandidateJobsFragment candidateJobsFragment = new CandidateJobsFragment();
                Bundle bundle = new Bundle();
                bundle.putInt("value", 1);
                candidateJobsFragment.setArguments(bundle);
                changeFragment(candidateJobsFragment, true);
            }
        });
        cv_candidateLatest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                App.applicationRecevide = false;
                CandidateFragment candidateFragment = new CandidateFragment();
                Bundle bundle = new Bundle();
                bundle.putInt("value", 2);
                candidateFragment.setArguments(bundle);
                changeFragment(candidateFragment, true);
            }
        });
        cv_shorjob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                App.applicationRecevide = false;
                ShoreJobFragment candidateFragment = new ShoreJobFragment();
                Bundle bundle = new Bundle();
                candidateFragment.setArguments(bundle);
                changeFragment(candidateFragment, true);
            }
        });

        new CallRequest(CompanyHomeFragment.this).getCounnt(candidateId);

        return view;
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case getCounnt:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            JSONObject ob1 = jObj.getJSONObject("data");
                            int totalCandidate = (ob1.getInt("total_candidate"));
                            int totalPostedJobs = (ob1.getInt("total_job"));
                            int totalJobApplication =(ob1.getInt("total_job_application"));
                            int totalLatestCandidate = (ob1.getInt("total_latest_candidate"));
                            int total_shorejob_candidate = (ob1.getInt("total_shorejob"));
                            txtCountCandidate.setText(totalCandidate+"");
                            txtCountJob.setText(totalPostedJobs+"");
                            txtCountApplied.setText(totalJobApplication+"");
                            txtCountLatetestCandidate.setText(totalLatestCandidate+"");
                            txt_count_shore_job.setText(total_shorejob_candidate+"");

                        } else {
                            Utils.showAlert(jObj.getString("message"), getActivity());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        //  Utils.showToast(jObj.getString("message"), this);
                    }
                    break;


            }
        }
    }


//    private void getAllCandidateDetail() {
//        new CallRequest(instance).getAllCandidateDetailSingle();
//    }
//    private void getJobList() {
//        new CallRequest(instance).getCompanyJobList(candidateId);
//    }
//    private void getAppliedCandidateDetail() {
//        new CallRequest(instance).getAppliedCandidateDetail(candidateId);
//    }
//    @Override
//    public void onTaskCompleted(String result, Constant.REQUESTS request) {
//        if (result != null && !result.isEmpty()) {
//            Log.i("TAG", "TAG Result : " + result);
//            switch (request) {
//                case getAllCandidateList:
//                    Utils.hideProgressDialog();
//
//                    try {
//                        JSONObject jObj = new JSONObject(result);
//                        if (jObj.getBoolean("status")) {
//                            JSONArray ob1 = jObj.getJSONArray("data");
//                              String SizeOFCandidate= String.valueOf(ob1.length());
//                                txtCountCandidate.setText(SizeOFCandidate);
//                        }
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                    break;
//
//                case getCompanyJob:
//                    Utils.hideProgressDialog();
//                    try {
//                        JSONObject jObj = new JSONObject(result);
//                        if (jObj.getBoolean("status")) {
//
//                            //  Utils.showToast(jObj.getString("message"), getContext());
//                            JSONArray ob1 = jObj.getJSONArray("data");
//                                String SizeOfJob= String.valueOf(ob1.length());
//                                txtCountJob.setText(SizeOfJob);
//                        }
//
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                    break;
//
//                case getAppliedCandidateList:
//                    Utils.hideProgressDialog();
//
//                    try {
//                        JSONObject jObj = new JSONObject(result);
//                        if (jObj.getBoolean("status")) {
//                            JSONArray ob1 = jObj.getJSONArray("data");
//                            String appliedCandidate= String.valueOf(ob1.length());
//                            txtCountApplied.setText(appliedCandidate);
//
//                        }
//
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                    break;
//            }
//        }
//    }
}