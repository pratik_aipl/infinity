package com.nfinitydynamics.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ListView;
import android.widget.TextView;

import com.nfinitydynamics.R;
import com.nfinitydynamics.utils.CallRequest;

public class InstituteDetailFragment extends Fragment {

    public InstituteDetailFragment instance;
    public String image, jobTitle, company, shipType, description, companyId, jobId, rank, isApplied, userId;
    public ListView lvJob;
    public View view;
    public TextView txtNoData;
    WebView myWebView;
    public String url;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_company_job_list, container, false);
        instance = this;
         companyId = getArguments().getString("companyID");
        txtNoData = view.findViewById(R.id.txt_nodata);
        url = getArguments().getString("url");
        myWebView = view.findViewById(R.id.webview);
        myWebView.loadUrl(url);
        return view;
    }



}
