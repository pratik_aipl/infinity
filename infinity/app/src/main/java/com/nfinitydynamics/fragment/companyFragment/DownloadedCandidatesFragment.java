package com.nfinitydynamics.fragment.companyFragment;


import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.nfinitydynamics.R;
import com.nfinitydynamics.activity.CompanyActivity.CompanyDashboard;
import com.nfinitydynamics.adapter.DownloadedCandidateAdapter;
import com.nfinitydynamics.model.DownloadedCandidateModel;
import com.nfinitydynamics.utils.AsynchTaskListner;
import com.nfinitydynamics.utils.CallRequest;
import com.nfinitydynamics.utils.Constant;
import com.nfinitydynamics.utils.JsonParserUniversal;
import com.nfinitydynamics.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 */
public class DownloadedCandidatesFragment extends Fragment implements AsynchTaskListner {

    View view;
    public ListView lvDownloadedCandidate;
    public ArrayList<DownloadedCandidateModel> list = new ArrayList<>();
    public DownloadedCandidateAdapter adapter;
    public DownloadedCandidatesFragment instance;
    public String candidateId, to_date = "", from_Date = "";
    public SharedPreferences shared;
    public JsonParserUniversal jParser;
    public LinearLayout linearFilter;
    public Dialog dialogView;
    public TextView tv_message;
    public final Calendar myCalendar = Calendar.getInstance();
    public Calendar FromDate = Calendar.getInstance();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_downloaded_candidates, container, false);
        instance = this;
        jParser = new JsonParserUniversal();
        shared = getContext().getSharedPreferences(getContext().getPackageName(), 0);
        candidateId = shared.getString("userId", "");
        CompanyDashboard.toolbar.setTitle("Downloaded Resume");
        lvDownloadedCandidate = view.findViewById(R.id.rvDownloadedCandidate);
        linearFilter = view.findViewById(R.id.linearFilter);
        linearFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showChangeLangDialog();
            }
        });
        tv_message = view.findViewById(R.id.tv_message);
        lvDownloadedCandidate.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                String name = list.get(position).getCanidateName();
                String code = list.get(position).getCandidateCode();
                String rankName = list.get(position).getRankName();
                String email = list.get(position).getEmailID();
                String maritalStatus = list.get(position).getMaritalStatus();
                String image = list.get(position).getImageURL();
                String indous = list.get(position).getIndos();
                String mobileNo = list.get(position).getMobileNo();
                String lastLogin = list.get(position).getLast_login();
                String modified = list.get(position).getModified();
                String nationality = list.get(position).getNationalName();
                String idCandidate = list.get(position).getCandidateID();
                String appliedShipType = list.get(position).getAppliedShipName();
                String resumeUrl = list.get(position).getResumeLink();
                String appliedRannk = list.get(position).getAppliedRankName();


                System.out.println("mobileNo===" + list.get(position).getMobileNo());
                NavCandidateDetailFragment candidateDetailFragment = new NavCandidateDetailFragment();
                Bundle b = new Bundle();
                b.putString("name", name);
                b.putString("code", code);
                b.putString("rankName", rankName);
                b.putString("email", email);
                b.putString("maritalStatus", maritalStatus);
                b.putString("image", image);
                b.putString("indous", indous);
                b.putString("mobileNo", mobileNo);
                b.putString("lastLogin", lastLogin);
                b.putString("modified", modified);
                b.putString("nationality", nationality);
                b.putString("idCandidate", idCandidate);
                b.putString("appliedShipType", appliedShipType);
                b.putString("appliedRank", appliedRannk);
                b.putString("resumeUrl", resumeUrl);

                b.putString("dateDownloaded", list.get(position).getDownloadedDate());
                candidateDetailFragment.setArguments(b);
                CompanyDashboard.changeFragment(candidateDetailFragment, true);
            }
        });
        new CallRequest(instance).getDownloadedResume(candidateId, "", "");
        return view;
    }

    public void showChangeLangDialog() {

        dialogView = new Dialog(getActivity());
        dialogView.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogView.setCancelable(true);
        dialogView.setContentView(R.layout.custom_filter_dialog_from_to_date);
        dialogView.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
//        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
//        LayoutInflater inflater = this.getLayoutInflater();
//        final View dialogView = inflater.inflate(R.layout.custom_filter_dialog_rank, null);
//        dialogBuilder.setView(dialogView);


        Button btnClear = dialogView.findViewById(R.id.btnClear);
        Button btnOk = dialogView.findViewById(R.id.btnOk);

        final TextView tv_from_date = dialogView.findViewById(R.id.txt_from_date);
        final TextView tv_to_date = dialogView.findViewById(R.id.txt_to_date);
        SimpleDateFormat s = new SimpleDateFormat("dd-MMM-yyyy", Locale.US);
        try {
            tv_from_date.setText(s.format(from_Date));
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            tv_to_date.setText(s.format(to_date));
        } catch (Exception e) {
            e.printStackTrace();
        }

        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv_to_date.setText("");
                tv_from_date.setText("");
                from_Date = "";
                to_date = "";
                new CallRequest(instance).getDownloadedResume(candidateId, "", "");
                dialogView.dismiss();
            }
        });
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogView.dismiss();
                new CallRequest(instance).getDownloadedResume(candidateId, from_Date, to_date);


            }
        });

        tv_from_date.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                try {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

                                @Override
                                public void onDateSet(DatePicker view, int year, int monthOfYear,
                                                      int dayOfMonth) {
                                    // TODO Auto-generated method stub
                                    myCalendar.set(Calendar.YEAR, year);
                                    myCalendar.set(Calendar.MONTH, monthOfYear);
                                    myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                                    String myFormat = "dd-MMM-yyyy"; //In which you need put here
                                    SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                                    tv_from_date.setText(sdf.format(myCalendar.getTime()));
                                    SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
                                    from_Date = s.format(myCalendar.getTime());
                                    FromDate = myCalendar;

                                }
                            };
                            DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH));
                            myCalendar.set(Calendar.HOUR_OF_DAY, myCalendar.getMinimum(Calendar.HOUR_OF_DAY));
                            myCalendar.set(Calendar.MINUTE, myCalendar.getMinimum(Calendar.MINUTE));
                            myCalendar.set(Calendar.SECOND, myCalendar.getMinimum(Calendar.SECOND));
                            myCalendar.set(Calendar.MILLISECOND, myCalendar.getMinimum(Calendar.MILLISECOND));
                            Calendar temp = Calendar.getInstance();
                            datePickerDialog.getDatePicker().setMaxDate(temp.getTimeInMillis());
                            datePickerDialog.show();

                            break;
                        case MotionEvent.ACTION_UP:
                            break;
                        default:
                            break;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


                return true;
            }
        });
        tv_to_date.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                try {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

                                @Override
                                public void onDateSet(DatePicker view, int year, int monthOfYear,
                                                      int dayOfMonth) {
                                    // TODO Auto-generated method stub
                                    myCalendar.set(Calendar.YEAR, year);
                                    myCalendar.set(Calendar.MONTH, monthOfYear);
                                    myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                                    String myFormat = "dd-MMM-yyyy"; //In which you need put here
                                    SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                                    tv_to_date.setText(sdf.format(myCalendar.getTime()));
                                    SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
                                    to_date = s.format(myCalendar.getTime());

                                }
                            };
                            DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH));
                            myCalendar.set(Calendar.HOUR_OF_DAY, myCalendar.getMinimum(Calendar.HOUR_OF_DAY));
                            myCalendar.set(Calendar.MINUTE, myCalendar.getMinimum(Calendar.MINUTE));
                            myCalendar.set(Calendar.SECOND, myCalendar.getMinimum(Calendar.SECOND));
                            myCalendar.set(Calendar.MILLISECOND, myCalendar.getMinimum(Calendar.MILLISECOND));
                            Calendar temp = Calendar.getInstance();
                            datePickerDialog.getDatePicker().setMinDate(FromDate.getTimeInMillis());
                            datePickerDialog.show();

                            break;
                        case MotionEvent.ACTION_UP:
                            break;
                        default:
                            break;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


                return true;
            }
        });
        dialogView.show();
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case getDownloadedResume:
                    Utils.hideProgressDialog();
                    list.clear();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {

                            //  Utils.showToast(jObj.getString("message"), getContext());
                            JSONArray ob1 = jObj.getJSONArray("data");

                            DownloadedCandidateModel downloadedCandidateModel;
                            if (ob1.length() > 0) {
                                lvDownloadedCandidate.setVisibility(View.VISIBLE);
                                tv_message.setVisibility(View.GONE);
                                for (int i = 0; i < ob1.length(); i++) {
                                    JSONObject jObject = ob1.getJSONObject(i);
                                    downloadedCandidateModel = new DownloadedCandidateModel();
                                    downloadedCandidateModel = (DownloadedCandidateModel) jParser.parseJson(jObject, new DownloadedCandidateModel());
                                    list.add(downloadedCandidateModel);
                                }
                                adapter = new DownloadedCandidateAdapter(getContext(), R.layout.layout_candidate, list);
                                lvDownloadedCandidate.setAdapter(adapter);
                            } else {
                                lvDownloadedCandidate.setVisibility(View.GONE);
                                tv_message.setVisibility(View.VISIBLE);
                                tv_message.setText(jObj.getString("message"));
                            }
                        } else {
                            Utils.showToast(jObj.getString("message"), getContext());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;


            }
        }
    }

}
