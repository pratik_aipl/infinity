package com.nfinitydynamics.fragment;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.nfinitydynamics.R;
import com.nfinitydynamics.activity.MainActivity;
import com.nfinitydynamics.adapter.NothingSelectedSpinnerAdapter;
import com.nfinitydynamics.model.CountryModel;
import com.nfinitydynamics.utils.AsynchTaskListner;
import com.nfinitydynamics.utils.CallRequest;
import com.nfinitydynamics.utils.Constant;
import com.nfinitydynamics.utils.JsonParserUniversal;
import com.nfinitydynamics.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import static com.nfinitydynamics.activity.MainActivity.toolbar;

public class AddDCEFragment extends Fragment implements AsynchTaskListner {


    public EditText edtNumber, edtValidity;
    public Spinner spinnerProficiency, spinnerLevel;
    public String number, placeIssue, validity, candidateId, id, uLevel,
            uNumber, uPlace, uValidity, levelValue = "", sCertificate = "";
    public AddDCEFragment instance;
    public SharedPreferences shared;
    public Button btnSave;
    public int s, s1;
    public ArrayList<String> certificateList = new ArrayList<>();
    public String[] lavelArray = {"Management", "Operational", "Support"};
    public String[] preficiencyArray = {"Oil", "Chemical", "Gas"};
    //  public  ArrayList<CertificateModel> cerList = new ArrayList<>();
    public ArrayAdapter aa, aa1, aaPreficiency;
    public ArrayList<CountryModel> countryModelArrayList = new ArrayList<CountryModel>();
    public ArrayList<String> countyStringList = new ArrayList<>();
    public int selectedPos;
    public Spinner spPlaceIssue;
    public JsonParserUniversal jParser;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view;
        view = inflater.inflate(R.layout.fragment_add_doc, container, false);
        jParser = new JsonParserUniversal();
        instance = this;
        edtNumber = view.findViewById(R.id.edt_number);
        spPlaceIssue = view.findViewById(R.id.sp_place_issue);
        edtValidity = view.findViewById(R.id.edt_validity);
        btnSave = view.findViewById(R.id.btn_save_experience);
        spinnerProficiency = view.findViewById(R.id.spinner_proficiency);
        spinnerLevel = view.findViewById(R.id.spinner_level);
        toolbar.setTitle("Add DCE Details");

        aa1 = new ArrayAdapter(getActivity(), R.layout.spinner_text, lavelArray);
        aa1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerLevel.setAdapter(new NothingSelectedSpinnerAdapter(
                aa1, R.layout.level_nothing_selected,
                getActivity()));
        aaPreficiency = new ArrayAdapter(getActivity(), R.layout.spinner_text, preficiencyArray);
        aaPreficiency.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerProficiency.setAdapter(new NothingSelectedSpinnerAdapter(
                aaPreficiency, R.layout.stc_certificate_noting_selected,
                getActivity()));
        // getCertificateType();
        getCountry();
        Bundle b = getArguments();
        s = b.getInt("value");
        Log.d("value---", String.valueOf(s));
        if (s == 1) {
            toolbar.setTitle("Edit DCE Details");
            id = b.getString("id");
            uNumber = b.getString("number");
            uValidity = b.getString("validity");
            placeIssue = b.getString("place");
            uLevel = b.getString("level");
            sCertificate = b.getString("certificateType");
            edtNumber.setText(uNumber);
            Utils.converDateToDDMMYY(edtValidity, uValidity);
            //  edtPlaceIssue.setText(uPlace);
            Log.d("sCertificate---", sCertificate);

            if (sCertificate != null) {
                int spinnerPosition = aaPreficiency.getPosition(sCertificate);
                Log.d("spinnerPositionnnnn", String.valueOf(aaPreficiency.getPosition(sCertificate)));
                spinnerProficiency.setSelection(spinnerPosition + 1);
//                if(uLevel.equalsIgnoreCase("Support")){
//                    spinnerLevel.setSelection(2);
//                }

            }
            if (uLevel != null) {
                int spinnerPosition = aa1.getPosition(uLevel);
                Log.d("spinnerPositionnnnn", String.valueOf(aa1.getPosition(uLevel)));

                spinnerLevel.setSelection(spinnerPosition + 1);

            }

        } else {
            sCertificate = "";
        }
        spPlaceIssue.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                try {
                    placeIssue = countryModelArrayList.get(i - 1).getCountryID();
                } catch (IndexOutOfBoundsException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinnerProficiency.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                try {
                    sCertificate = preficiencyArray[i - 1];

                } catch (IndexOutOfBoundsException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }

        });


        spinnerLevel.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                try {
                    levelValue = lavelArray[i - 1];
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        shared = getActivity().getSharedPreferences(getContext().getPackageName(), 0);
        candidateId = shared.getString("userId", "");
        edtValidity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                Utils.generateDatePicker(getContext(), edtValidity);
            }
        });
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                number = edtNumber.getText().toString().trim();
                //  countryId = edtPlaceIssue.getText().toString().trim();
                validity = edtValidity.getText().toString().trim();
                Calendar cal = Calendar.getInstance();
                Date sysDate = cal.getTime();
                Date intValidity = null;
                try {
                    if (!validity.equalsIgnoreCase("")) {
                        intValidity = new SimpleDateFormat("dd-MMM-yyyy").parse(validity);
                        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                        validity = formatter.format(intValidity);
                    }

                } catch (ParseException e) {
                    e.printStackTrace();
                }
                System.out.println("certificate==" + sCertificate);
                if (sCertificate.equalsIgnoreCase("")) {
                    Utils.showAlert("Please Select Certificate Type", getContext());
                } else if (TextUtils.isEmpty(levelValue)) {
                    Utils.showAlert("Please Select Level", getContext());
                } else if (number.equals("")) {
                    edtNumber.findFocus();
                    Utils.showAlert("Please Enter Certificate Number", getContext());
                } else if (TextUtils.isEmpty(placeIssue)) {
                    spPlaceIssue.findFocus();
                    Utils.showAlert("Please Select Country", getContext());
                } else if (validity.equals("")) {
                    edtValidity.findFocus();
                    Utils.showAlert("Please Enter Validity Date", getContext());
                } else if (sysDate.compareTo(intValidity) > 0) {
                    Utils.showAlert("Please Enter Valid Validity", getContext());
                } else {

                    if (s == 0) {
                        new CallRequest(instance).addDCE(candidateId, number, sCertificate, placeIssue, levelValue, validity, "0");
                    }
                    if (s == 1) {
                        new CallRequest(instance).updateDCE(candidateId, number, sCertificate, placeIssue, levelValue, validity, "1", id);
                    }

                }
            }
        });
        return view;
    }

    private void getCountry() {
        new CallRequest(instance).getCountry();
    }

    private void getCertificateType() {

        new CallRequest(instance).getCertificateType();
    }


    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case addDCE:

                    Utils.hideProgressDialog();

                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            Utils.showToast("Added Successfully", getContext());
                            MainActivity.activity.onBackPressed();
                        } else {
                            Utils.showToast("Try Again!!", getContext());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();

                        Log.d("new exception:", String.valueOf(e));
                    }
                    break;


                case updateDCE:

                    Utils.hideProgressDialog();

                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            Utils.showToast("Updated Successfully", getContext());
                            MainActivity.activity.onBackPressed();
                        } else {
                            Utils.showToast("Try Again!!", getContext());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();

                        Log.d("new exception:", String.valueOf(e));
                    }
                    break;

                case getCountryList:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            JSONArray ob1 = jObj.getJSONArray("data");
                            CountryModel countryModel;
                            for (int i = 0; i < ob1.length(); i++) {
                                countryModel = new CountryModel();
                                countryModel = (CountryModel) jParser.parseJson(ob1.getJSONObject(i), new CountryModel());
                                countryModelArrayList.add(countryModel);
                                if (placeIssue != null) {
                                    if (placeIssue.equalsIgnoreCase(countryModel.getCountryName())) {
                                        selectedPos = i + 1;
                                    }
                                }
                                countyStringList.add(countryModel.getCountryName());
                            }
                            ArrayAdapter aa = new ArrayAdapter(getActivity(), R.layout.spinner_text, countyStringList);
                            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spPlaceIssue.setAdapter(new NothingSelectedSpinnerAdapter(
                                    aa, R.layout.country_place_issue,
                                    getActivity()));
                            spPlaceIssue.setSelection(selectedPos);
                        } else {
                            Utils.showToast("Try Again!!", getContext());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d("new exception:", String.valueOf(e));
                    }
            }
        }
    }
}
