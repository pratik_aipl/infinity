package com.nfinitydynamics.fragment.companyFragment;


import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nfinitydynamics.R;
import com.nfinitydynamics.activity.CompanyActivity.CompanyDashboard;
import com.nfinitydynamics.model.UserDetailModel;
import com.nfinitydynamics.utils.AsynchTaskListner;
import com.nfinitydynamics.utils.CallRequest;
import com.nfinitydynamics.utils.Constant;
import com.nfinitydynamics.utils.FileDownloader;
import com.nfinitydynamics.utils.JsonParserUniversal;
import com.nfinitydynamics.utils.Utils;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;

import static android.net.wifi.WifiConfiguration.Status.strings;
import static com.nfinitydynamics.activity.CompanyActivity.CompanyDashboard.toolbar;
import static com.nfinitydynamics.utils.Utils.hasPermissions;

/**
 * A simple {@link Fragment} subclass.
 */
public class NavCandidateDetailFragment extends Fragment {
    View view;
    public NavCandidateDetailFragment instance;
    public SharedPreferences shared;
    public String candidateId,companyId;
    public String resumeUrl;
    public JsonParserUniversal jParser;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_candidate_detail, container, false);
        instance = this;
        shared = getActivity().getSharedPreferences(getContext().getPackageName(), 0);
          companyId = shared.getString("userId", "");
        jParser = new JsonParserUniversal();
        toolbar.setTitle("Candidate Detail");

        String name = getArguments().getString("name");
        String code=getArguments().getString("code");
        String rankName = getArguments().getString("rankName");
        String email = getArguments().getString("email");
        String maritalStatus = getArguments().getString("maritalStatus");
        String image = getArguments().getString("image");
        String indous = getArguments().getString("indous");
        String lastLogin = getArguments().getString("lastLogin");
        String modified = getArguments().getString("modified");
        String mobileNo = getArguments().getString("mobileNo");
        String nationality = getArguments().getString("nationality");
        String appliedShipType = getArguments().getString("appliedShipType");
        final String resumeLink=getArguments().getString("resumeUrl");
        candidateId = getArguments().getString("idCandidate");
        String appliedRank=getArguments().getString("appliedRank");
        String dateDownloaded=getArguments().getString("dateDownloaded");
        ImageView logo = view.findViewById(R.id.profile_image);
        TextView txtName = view.findViewById(R.id.txt_name);
        TextView txtEmailLable=view.findViewById(R.id.txt_email);
        TextView txtEmail = view.findViewById(R.id.txt_email_value);
      //  TextView txtMarital = view.findViewById(R.id.txt_marital_value);
        TextView txtMono = view.findViewById(R.id.txt_mono_value);
        TextView txt_code = view.findViewById(R.id.txt_code);
        TextView txt_seatime = view.findViewById(R.id.txt_seatime);
        TextView txtUpdated = view.findViewById(R.id.txt_update_value);
        TextView txtRank = view.findViewById(R.id.txt_rank_value);
        TextView txtShipType=view.findViewById(R.id.txt_shiptype_value);
        TextView txtShipLable=view.findViewById(R.id.txt_ship_type);
        TextView txt_mobileno=view.findViewById(R.id.txt_mobileno);
        TextView txtAppliedRank=view.findViewById(R.id.txt_appliedrank_value);
      //  TextView txtDateDownloaded=view.findViewById(R.id.txt_date_downloaded_value);
        TextView txtLastVisited=view.findViewById(R.id.txt_last_visited);
        TextView txtLastVisitedValue=view.findViewById(R.id.txt_lastvisited_value);
        RelativeLayout relativeDownlode = view.findViewById(R.id.relative_view);
        RelativeLayout relative_view_candidate = view.findViewById(R.id.relative_view_candidate);
        TextView txt_seatime_value=view.findViewById(R.id.txt_seatime_value);
        TextView txt_update=view.findViewById(R.id.txt_update);

        System.out.println("mobileNo==="+mobileNo);
        Picasso.with(getContext()).load(image).placeholder(R.drawable.profilepic).error(R.drawable.profilepic).into(logo);
        txtEmail.setText(": " + email);
        txtName.setText(name);
        txtEmail.setVisibility(View.VISIBLE);
        txtName.setVisibility(View.VISIBLE);
        txtMono.setVisibility(View.VISIBLE);
        txtEmailLable.setVisibility(View.VISIBLE);
        txt_mobileno.setVisibility(View.VISIBLE);
        txt_update.setVisibility(View.VISIBLE );
        txt_code.setText(code);
        txtLastVisited.setText("Date Downloaded");
        txt_seatime_value.setVisibility(View.GONE);
//        txtIndous.setText(": " + indous);
//        txtMarital.setText(": " + maritalStatus);
      //  txtNationality.setText(": " + nationality);
        txt_seatime.setVisibility(View.GONE);
        txtMono.setText(": " + mobileNo);
        txtRank.setText(": " + rankName);

        txtUpdated.setVisibility(View.VISIBLE);
        txtUpdated.setText(": " + modified);
        txtLastVisitedValue.setText(": "+dateDownloaded);
       // txtDateDownloaded.setText(": "+);

            txtShipLable.setVisibility(View.GONE);
            txtShipType.setVisibility(View.GONE);

        txtShipType.setText(": "+appliedShipType);
        txtAppliedRank.setText(": "+appliedRank);
        relativeDownlode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DownloadedResumeFragment bulidResumeFragment=new DownloadedResumeFragment();
                Bundle bundle=new Bundle();
                bundle.putString("url", resumeLink);
                bulidResumeFragment.setArguments(bundle);
                CompanyDashboard.changeFragment(bulidResumeFragment, true);
//                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(resumeLink));
//                startActivity(browserIntent);


              //  downlodResume();
            }
        });

        relative_view_candidate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewCandidateFragment viewCandidateFragment = new ViewCandidateFragment();
                Bundle bundle = new Bundle();
                bundle.putString("candidateId", getArguments().getString("idCandidate"));
                viewCandidateFragment.setArguments(bundle);
                CompanyDashboard.changeFragment(viewCandidateFragment, true);
            }
        });
        return view;
    }

}
