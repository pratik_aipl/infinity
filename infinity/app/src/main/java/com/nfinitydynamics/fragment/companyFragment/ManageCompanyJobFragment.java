package com.nfinitydynamics.fragment.companyFragment;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Spinner;

import com.nfinitydynamics.R;
import com.nfinitydynamics.adapter.CompanyAdapter.CompanyJobAdapter;
import com.nfinitydynamics.model.LatestJobModel;
import com.nfinitydynamics.model.RankModel;
import com.nfinitydynamics.model.ShipModel;
import com.nfinitydynamics.utils.AsynchTaskListner;
import com.nfinitydynamics.utils.CallRequest;
import com.nfinitydynamics.utils.Constant;
import com.nfinitydynamics.utils.JsonParserUniversal;
import com.nfinitydynamics.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.nfinitydynamics.activity.CompanyActivity.CompanyDashboard.changeFragment;
import static com.nfinitydynamics.activity.CompanyActivity.CompanyDashboard.toolbar;

/**
 * A simple {@link Fragment} subclass.
 */
public class ManageCompanyJobFragment extends Fragment implements AsynchTaskListner {
    public Spinner spinnerRank;
    public Spinner spinnerShip,spIssuingCountry;
    public FloatingActionButton fabAdd;
    public SharedPreferences shared;
    public ListView lvJob;
    public String candidateId, jobTitle, shipType, company, description, companyId, jobId, rank, isApplied;
    public ArrayList<LatestJobModel> companyJoList = new ArrayList<>();
    public ArrayList<RankModel> rankList = new ArrayList<>();
    public ArrayList<String> rankStringList = new ArrayList<>();
    public ArrayList<ShipModel> shipList = new ArrayList<>();
    public ArrayList<String> shipStringList = new ArrayList<>();
    public JsonParserUniversal jParser;
    public static ManageCompanyJobFragment instance;
    public String rankvalue;
    public int value;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view;
        instance=this;
        jParser=new JsonParserUniversal();
        view = inflater.inflate(R.layout.fragment_manage_company_job, container, false);
        fabAdd = view.findViewById(R.id.fab_add);
        lvJob = view.findViewById(R.id.lv_job_list);
        toolbar.setTitle("Published  Vacancies");
        value = getArguments().getInt("value");
        if (value == 1) {
            toolbar.setTitle("Published  Vacancies");
        }
        shared = getActivity().getSharedPreferences(getContext().getPackageName(), 0);
        candidateId = shared.getString("userId", "");
        fabAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AddJobsFragment addJobsFragment=new AddJobsFragment();
                Bundle b=new Bundle();
                b.putInt("value",0);
                addJobsFragment.setArguments(b);
                changeFragment(addJobsFragment, true);
            }
        });
        getJobList();
        return view;
    }
    private void getRank() {
        new CallRequest(instance).getRankFragment();
    }

    private void getShipType() {

        new CallRequest(instance).getShipFragment();
    }
    private void getJobList() {
        new CallRequest(instance).getCompanyJobList(candidateId);
    }
    public void showFilterDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.custom_filter_dialog, null);
        dialogBuilder.setView(dialogView);

        spinnerRank = dialogView.findViewById(R.id.spinner_rank);
        spinnerShip = dialogView.findViewById(R.id.spinner_ship_type);

        getShipType();
        getRank();
        spinnerRank.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

//                if (i != 0) {
//                    rankvalue = rankList.get(i).getRankId();
//
//                }
                rankvalue = rankList.get(i).getRankId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinnerShip.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i != 0) {
                    shipType = shipList.get(i).getShipID();
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        dialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //do something with edt.getText().toString();
                if (rankvalue != "") {
                    // new CallRequest(instance).getCompanyCandidateDetailRank(rankvalue);
                }
                if (shipType != "") {

                }
            }
        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //pass
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case getCompanyJob:
                    Utils.hideProgressDialog();
                    companyJoList.clear();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {

                            //  Utils.showToast(jObj.getString("message"), getContext());
                            JSONArray ob1 = jObj.getJSONArray("data");

                            LatestJobModel lJobModel;
                            for (int i = 0; i < ob1.length(); i++) {
                                JSONObject jObject = ob1.getJSONObject(i);
                                lJobModel = new LatestJobModel();
                                lJobModel = (LatestJobModel) jParser.parseJson(jObject, new LatestJobModel());
                                companyJoList.add(lJobModel);
                            }
                             CompanyJobAdapter companyJobAdapter = new CompanyJobAdapter(getContext(), R.layout.layout_company_job_list, companyJoList);
                            lvJob.setAdapter(companyJobAdapter);

                        } else {

                            lvJob.setVisibility(View.GONE);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;

                case deleteJob:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {

                            // Utils.showToast(jObj.getString("message"), getContext());
                            Utils.showToast("Deleted Successfully",getContext());

                            getJobList();

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }
}
