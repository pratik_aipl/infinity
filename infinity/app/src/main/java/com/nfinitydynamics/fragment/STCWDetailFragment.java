package com.nfinitydynamics.fragment;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.nfinitydynamics.R;
import com.nfinitydynamics.activity.MainActivity;
import com.nfinitydynamics.adapter.STCWAdapter;
import com.nfinitydynamics.model.STCWModel;
import com.nfinitydynamics.utils.AsynchTaskListner;
import com.nfinitydynamics.utils.CallRequest;
import com.nfinitydynamics.utils.Constant;
import com.nfinitydynamics.utils.JsonParserUniversal;
import com.nfinitydynamics.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.nfinitydynamics.activity.MainActivity.toolbar;

/**
 * A simple {@link Fragment} subclass.
 */
public class STCWDetailFragment extends Fragment implements AsynchTaskListner {
    public FloatingActionButton fabAdd;
    public SharedPreferences shared;
    public ListView lCOC;
    public String candidateId;
    public ArrayList<STCWModel> stcwList = new ArrayList<>();
    public JsonParserUniversal jParser;
    public static STCWDetailFragment instance;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view;
        view = inflater.inflate(R.layout.fragment_stcwdetail, container, false);
        instance = this;
        shared = getActivity().getSharedPreferences(getContext().getPackageName(), 0);
        candidateId = shared.getString("userId", "");
        jParser = new JsonParserUniversal();
        fabAdd = view.findViewById(R.id.fab_add);
        lCOC = view.findViewById(R.id.lv_stcw);
        toolbar.setTitle("STCW Certificates");
        stcwList.clear();
        getSTCWDetail();
        fabAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AddSTCWFragment fragment = new AddSTCWFragment();
                Bundle args = new Bundle();
                args.putInt("value", 0);
                fragment.setArguments(args);
                MainActivity.changeFragment(fragment, true);
            }
        });

        return view;
    }

    private void getSTCWDetail() {


        new CallRequest(instance).getSTCWDetail(candidateId);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case getSTCWDetail:
                    Utils.hideProgressDialog();
                    stcwList.clear();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            JSONArray ob1 = jObj.getJSONArray("data");
                            STCWModel stcwModel;
                            for (int i = 0; i < ob1.length(); i++) {
                                JSONObject jObject = ob1.getJSONObject(i);
                                stcwModel = new STCWModel();
                                stcwModel = (STCWModel) jParser.parseJson(jObject, new STCWModel());
                                stcwList.add(stcwModel);
                            }
                            STCWAdapter stcwAdapter = new STCWAdapter(getContext(), R.layout.layout_stcw, stcwList);
                            lCOC.setAdapter(stcwAdapter);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;

                case deleteStcw:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            Utils.showToast("Deleted Successfully", getContext());
                            getSTCWDetail();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }

}
