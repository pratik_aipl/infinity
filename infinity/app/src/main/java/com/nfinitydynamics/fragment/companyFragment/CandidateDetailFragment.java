package com.nfinitydynamics.fragment.companyFragment;


import android.app.Activity;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nfinitydynamics.R;
import com.nfinitydynamics.activity.CompanyActivity.CompanyDashboard;
import com.nfinitydynamics.model.UserDetailModel;
import com.nfinitydynamics.utils.AsynchTaskListner;
import com.nfinitydynamics.utils.CallRequest;
import com.nfinitydynamics.utils.Constant;
import com.nfinitydynamics.utils.FileDownloader;
import com.nfinitydynamics.utils.JsonParserUniversal;
import com.nfinitydynamics.utils.Utils;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;

import static android.net.wifi.WifiConfiguration.Status.strings;
import static com.nfinitydynamics.activity.CompanyActivity.CompanyDashboard.toolbar;
import static com.nfinitydynamics.utils.Utils.hasPermissions;

/**
 * A simple {@link Fragment} subclass.
 */
public class CandidateDetailFragment extends Fragment implements AsynchTaskListner {
    View view;
    public CandidateDetailFragment instance;
    public SharedPreferences shared;
    public String candidateId, companyId;
    public String resumeUrl;
    public JsonParserUniversal jParser;
    public RelativeLayout relativeViewCandidate;
    public int value;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_candidate_detail, container, false);
        instance = this;
        shared = getActivity().getSharedPreferences(getContext().getPackageName(), 0);
        companyId = shared.getString("userId", "");
        jParser = new JsonParserUniversal();
        toolbar.setTitle("Candidate Detail");

        String name = getArguments().getString("name");
        String code = getArguments().getString("code");
        String nameCandidate = getArguments().getString("name");
        String rankName = getArguments().getString("rankName");
        String email = getArguments().getString("email");
        String maritalStatus = getArguments().getString("maritalStatus");
        String image = getArguments().getString("image");
        String indous = getArguments().getString("indous");
        String lastLogin = getArguments().getString("lastLogin");
        String modified = getArguments().getString("modified");
        String mobileNo = getArguments().getString("mobileNo");
        String nationality = getArguments().getString("nationality");
        String appliedShipType = getArguments().getString("appliedShipType");
        String seatimeInLastRank = getArguments().getString("seaTime");
        candidateId = getArguments().getString("idCandidate");
        String appliedRank = getArguments().getString("appliedRank");

        System.out.println("appliedRank===" + appliedRank);
        ImageView logo = view.findViewById(R.id.profile_image);
        TextView txtName = view.findViewById(R.id.txt_name);
        TextView txtLableEmail = view.findViewById(R.id.txt_email);
        TextView txtMobileNo = view.findViewById(R.id.txt_mobileno);
        TextView txtEmailValue = view.findViewById(R.id.txt_email_value);
        TextView txtMarital = view.findViewById(R.id.txt_marital_value);
        TextView txtMonoValue = view.findViewById(R.id.txt_mono_value);
        TextView txtIndous = view.findViewById(R.id.txt_indous_value);
        TextView txtNationality = view.findViewById(R.id.txt_nationality_value);
        TextView txtUpdated = view.findViewById(R.id.txt_update_value);
        TextView txtRank = view.findViewById(R.id.txt_rank_value);
        TextView txtShipType = view.findViewById(R.id.txt_shiptype_value);
        TextView txtShipLable = view.findViewById(R.id.txt_ship_type);
        TextView txtAppliedRank = view.findViewById(R.id.txt_appliedrank_value);
        TextView txtLastVisited = view.findViewById(R.id.txt_lastvisited_value);
        TextView txtCode = view.findViewById(R.id.txt_code);
        RelativeLayout relativeDownlode = view.findViewById(R.id.relative_view);
        TextView txtSeaTime = view.findViewById(R.id.txt_seatime_value);
        relativeViewCandidate = view.findViewById(R.id.relative_view_candidate);
        Picasso.with(getContext()).load(image).placeholder(R.drawable.profilepic).error(R.drawable.profilepic).into(logo);
        relativeViewCandidate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewCandidateFragment viewCandidateFragment = new ViewCandidateFragment();
                Bundle bundle = new Bundle();
                bundle.putString("candidateId", getArguments().getString("idCandidate"));
                viewCandidateFragment.setArguments(bundle);
                CompanyDashboard.changeFragment(viewCandidateFragment, true);
            }
        });
        // txtEmail.setText(": " + email);
        txtCode.setText(code);
//        txtIndous.setText(": " + indous);
//        txtMarital.setText(": " + maritalStatus);
        txtNationality.setText(": " + nationality);
        //  txtMono.setText(": " + mobileNo);
        txtRank.setText(": " + rankName);
        txtUpdated.setText(": " + modified);
        txtAppliedRank.setText(": " + appliedRank);
        txtLastVisited.setText(": " + lastLogin);
        txtSeaTime.setText(": " + seatimeInLastRank);
        if (TextUtils.isEmpty(appliedShipType)) {
            txtShipLable.setVisibility(View.GONE);
            txtShipType.setVisibility(View.GONE);
            txtLableEmail.setVisibility(View.VISIBLE);
            txtEmailValue.setVisibility(View.VISIBLE);
            txtMobileNo.setVisibility(View.VISIBLE);
            txtMonoValue.setVisibility(View.VISIBLE);
            txtName.setVisibility(View.VISIBLE);
            txtName.setText(nameCandidate);
            txtCode.setText(code);
            txtMonoValue.setText(": " + mobileNo);
            txtEmailValue.setText(": " + email);
        }
        txtShipType.setText(": " + appliedShipType);
        relativeDownlode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                candidateId = getArguments().getString("idCandidate");
                BulidComanyResumeFragment bulidResumeFragment = new BulidComanyResumeFragment();
                Bundle bundle = new Bundle();
                bundle.putString("candidateId", getArguments().getString("idCandidate"));
                bulidResumeFragment.setArguments(bundle);
                CompanyDashboard.changeFragment(bulidResumeFragment, true);

                //  downlodResume();
            }
        });
        return view;
    }

    private void downlodResume() {
        new CallRequest(instance).bulidResumeCompamy(candidateId, companyId);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case bulidResumeCompany:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            UserDetailModel userDetailModel;
                            userDetailModel = new UserDetailModel();
                            userDetailModel = (UserDetailModel) jParser.parseJson(jObj, new UserDetailModel());
                            resumeUrl = userDetailModel.getResume_url();
                            if (Build.VERSION.SDK_INT >= 23) {
                                String[] PERMISSIONS = {android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE};
                                if (!hasPermissions(getContext(), PERMISSIONS)) {
                                    ActivityCompat.requestPermissions((Activity) getContext(), PERMISSIONS, 112);
                                } else {
                                    createPdFFile();
                                }
                            } else {
                                createPdFFile();
                            }
                            Utils.showToast("Downlod Successfully", getContext());
                            CompanyDashboard.changeFragment(new CompanyHomeFragment(), false);
                        } else {
                            Utils.showToast("Try Again!!", getContext());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d("new exception:", String.valueOf(e));
                    }
                    break;
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        //Checking the request code of our request
        if (requestCode == 112) {

            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                new DownloadFile().execute(resumeUrl);

            } else {
                Utils.showToast("Permission not granted", getContext());
            }
        }
    }

    private class DownloadFile extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... strings) {
            if (Build.VERSION.SDK_INT >= 23) {
                String[] PERMISSIONS = {android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE};
                if (!hasPermissions(getContext(), PERMISSIONS)) {
                    ActivityCompat.requestPermissions((Activity) getContext(), PERMISSIONS, 112);

                } else {

                    System.out.println("Going to Create PDF file");
                    ;
                    createPdFFile();
                }
            }

            return null;
        }
    }

    public File f;

    private void createPdFFile() {

        String fileUrl = strings[0];
        String fileName = strings[1];// -> http://maven.apache.org/maven-1.x/maven.pdf
        File dir = new File(Environment.getExternalStorageDirectory().toString() + "/infinityDynamics/");

        if (!dir.exists()) {
            System.out.println("Directory not exist");
            ;

            dir.mkdirs();
        }
        f = new File(dir.getAbsolutePath(), "infinityDynamics" + System.currentTimeMillis() + ".pdf");
        try {
            System.out.println("Creating file " + f.getAbsolutePath());
            ;
            f.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("File URL" + resumeUrl);
        ;

        Thread thread = new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    FileDownloader.downloadFile(resumeUrl, f);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        thread.start();


    }

}
