package com.nfinitydynamics.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import com.nfinitydynamics.R;
import com.nfinitydynamics.adapter.NotificationAdapter;
import com.nfinitydynamics.model.Notifications;
import com.nfinitydynamics.utils.AsynchTaskListner;
import com.nfinitydynamics.utils.CallRequest;
import com.nfinitydynamics.utils.Constant;
import com.nfinitydynamics.utils.JsonParserUniversal;
import com.nfinitydynamics.utils.Utils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import static com.nfinitydynamics.activity.MainActivity.toolbar;

public class NotificationFragment extends Fragment implements AsynchTaskListner {
   public SharedPreferences shared;
    public ListView lv_notification;
    public String candidateId;
    public ArrayList<Notifications> notificationsList = new ArrayList<>();
    public JsonParserUniversal jParser;
    public static NotificationFragment instance;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view;
        view = inflater.inflate(R.layout.fragment_notification, container, false);
        instance=this;
        shared = getActivity().getSharedPreferences(getContext().getPackageName(), 0);
        candidateId = shared.getString("user_id","");
        jParser=new JsonParserUniversal();
        lv_notification=view.findViewById(R.id.lv_notification);
        toolbar.setTitle("Notification");
        notificationsList.clear();
        getNotification();
        return view;
    }

    private void getNotification() {
        new CallRequest(instance).getNotification(candidateId);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case get_notification_dtl:
                    Utils.hideProgressDialog();
                    notificationsList.clear();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {

                            //  Utils.showToast(jObj.getString("message"), getContext());
                            JSONArray ob1 = jObj.getJSONArray("data");
                            Notifications notifications;
                            for (int i = 0; i < ob1.length(); i++) {
                                JSONObject jObject = ob1.getJSONObject(i);
                                notifications = new Notifications();
                                notifications = (Notifications) jParser.parseJson(jObject, new Notifications());
                                notificationsList.add(notifications);
                            }
                            NotificationAdapter latestJobAdapter = new NotificationAdapter(getContext(), R.layout.layout_education, notificationsList);
                            lv_notification.setAdapter(latestJobAdapter);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;

            }
        }
    }

}

