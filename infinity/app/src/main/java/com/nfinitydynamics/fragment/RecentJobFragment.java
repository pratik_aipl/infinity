package com.nfinitydynamics.fragment;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.nfinitydynamics.R;
import com.nfinitydynamics.adapter.NothingSelectedSpinnerAdapter;
import com.nfinitydynamics.adapter.RecentJobAdapter;
import com.nfinitydynamics.model.LatestJobModel;
import com.nfinitydynamics.utils.AsynchTaskListner;
import com.nfinitydynamics.utils.CallRequest;
import com.nfinitydynamics.utils.Constant;
import com.nfinitydynamics.utils.JsonParserUniversal;
import com.nfinitydynamics.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.nfinitydynamics.activity.MainActivity.changeFragment;

public class RecentJobFragment extends Fragment implements AsynchTaskListner {

    View view;
    public ListView lvRecentJob;
    public ArrayList<LatestJobModel> rJobList = new ArrayList<>();
    public JsonParserUniversal jParser;
    public RecentJobFragment instace;
    public SharedPreferences shared;
    public RecentJobAdapter adapter;
    public TextView txtNoData;
    String image, jobTitle, company, shipType, description, companyId, jobId, rank, isApplied;
    public String[] filterArray = {"Jobs by Company", "Jobs as per Rank", "Jobs by Shiptype", "Featured Jobs", " Latest Jobs", " Shore Jobs"};
    public String[] filterId = {"1", "2", "3", "4", "5", "6"};

    public Spinner sp_filter;

    public ArrayAdapter aaFilter;
    public String filter_name = "0";
    public Button btnOk;
    public EditText edtSearch;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_recent_job, container, false);
        lvRecentJob = (ListView) view.findViewById(R.id.lv_recentjob);
        sp_filter = view.findViewById(R.id.sp_filter);
        btnOk = view.findViewById(R.id.btnOk);

// txtNoData=(TextView)view.findViewById(R.id.txt_nodata);
        instace = this;
        jParser = new JsonParserUniversal();
        shared = getActivity().getSharedPreferences(getContext().getPackageName(), 0);
        if (rJobList.size() != 0) {

        }
        edtSearch = (EditText) view.findViewById(R.id.edt_search);
        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
            /*    String text = edtSearch.getText().toString().toLowerCase(Locale.getDefault());
                adapter.filters(text);
*/
            }
        });

        aaFilter = new ArrayAdapter(getContext(), R.layout.spinner_text, filterArray);
        aaFilter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_filter.setAdapter(new NothingSelectedSpinnerAdapter(
                aaFilter, R.layout.filter_spinner_row_nothing_selected,
                getContext()));


        sp_filter.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    filter_name = filterId[position - 1];
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String candidateId = shared.getString("userId", "");

                new CallRequest(instace).searchJob(candidateId, edtSearch.getText().toString(), filter_name);
            }
        });
        getRecentJob();

        return view;
    }

    private void getRecentJob() {

        String candidateId = shared.getString("userId", "");
        new CallRequest(instace).searchJob(candidateId, edtSearch.getText().toString(), filter_name);

    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case latestJob:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {

                            //   Utils.showToast(jObj.getString("message"), getContext());
                            JSONArray ob1 = jObj.getJSONArray("data");
                            rJobList.clear();
                            LatestJobModel lJobModel;
                            for (int i = 0; i < ob1.length(); i++) {
                                JSONObject jObject = ob1.getJSONObject(i);
                                lJobModel = new LatestJobModel();
                                lJobModel = (LatestJobModel) jParser.parseJson(jObject, new LatestJobModel());
                                rJobList.add(lJobModel);
                            }
                            if (getActivity() != null) {

                                adapter = new RecentJobAdapter(getContext(), R.layout.layout_recent_jobs, rJobList);
                                lvRecentJob.setAdapter(adapter);
                            }
                            lvRecentJob.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                    JobDetailFragment jobDetailFragment = new JobDetailFragment();
                                    Bundle b = new Bundle();
                                    LatestJobModel model = rJobList.get(i);
                                    image = model.getLogoImgPath();
                                    jobTitle = model.getJobTitle();
                                    shipType = model.getShipType();
                                    company = model.getCompanyName();
                                    description = model.getDescription();
                                    rank = model.getRank();
                                    isApplied = String.valueOf(model.getIs_Applied_Job());
                                    companyId = model.getCompanyID();
                                    jobId = model.getJobID();
                                    Log.d("jobTitle =====", jobTitle);
                                    Log.d("rank--", rank);
                                    Log.d("value", image + jobTitle + shipType + company + description + companyId);
                                    b.putString("image", image);
                                    b.putString("jobTitle", jobTitle);
                                    b.putString("shipType", shipType);
                                    b.putString("company", company);
                                    b.putString("description", description);
                                    b.putString("companyId", companyId);
                                    b.putString("jobId", jobId);
                                    b.putString("rank", rank);
                                    b.putString("isApplied", isApplied);
                                    b.putString("JobEndDate", model.getJobEndDate());
                                    b.putString("fromExperience", model.getFromExperience());
                                    b.putString("toExperience", model.getToExperience());
                                    jobDetailFragment.setArguments(b);
                                    changeFragment(jobDetailFragment, true);
                                }
                            });
//                            Intent intent = new Intent(getContext(), MainActivity.class);
//                            startActivity(intent);
//                            finish();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }


}
