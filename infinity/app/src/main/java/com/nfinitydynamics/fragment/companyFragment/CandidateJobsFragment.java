package com.nfinitydynamics.fragment.companyFragment;


import android.app.Dialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.nfinitydynamics.R;
import com.nfinitydynamics.activity.CompanyActivity.CompanyDashboard;
import com.nfinitydynamics.adapter.CandidateAppliedJobAdapter;
import com.nfinitydynamics.adapter.NothingSelectedSpinnerAdapter;
import com.nfinitydynamics.model.RankModel;
import com.nfinitydynamics.model.ShipModel;
import com.nfinitydynamics.model.company.CandidateModel;
import com.nfinitydynamics.utils.App;
import com.nfinitydynamics.utils.AsynchTaskListner;
import com.nfinitydynamics.utils.CallRequest;
import com.nfinitydynamics.utils.Constant;
import com.nfinitydynamics.utils.JsonParserUniversal;
import com.nfinitydynamics.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

import static com.nfinitydynamics.activity.CompanyActivity.CompanyDashboard.toolbar;

public class CandidateJobsFragment extends Fragment implements AsynchTaskListner {
    public Spinner spinnerRank;
    public Spinner spinnerShip;
    public ArrayList<RankModel> rankList = new ArrayList<>();
    public ArrayList<String> rankStringList = new ArrayList<>();
    public ArrayList<ShipModel> shipList = new ArrayList<>();
    public ArrayList<String> shipStringList = new ArrayList<>();
    public String rankValue = "", shipType = "", companyId;
    public CandidateJobsFragment instance;
    public ArrayList<CandidateModel> candidateList = new ArrayList<>();
    public ListView lvCandidateJobList;
    public JsonParserUniversal jParser;
    public TextView txtNoData;
    public LinearLayout linerFilter;
    public SharedPreferences shared;
    public CandidateModel model;
    public CandidateAppliedJobAdapter candidateAdapter;
    public ArrayAdapter aa, aaShip;
    public int rankPos = 0, shipPos = 0;
    public Dialog dialogView;
    public int value;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view;
        view = inflater.inflate(R.layout.fragment_candidate_jobs, container, false);
        instance = this;
        toolbar.setTitle("Applications Received");
        shared = getActivity().getSharedPreferences(getContext().getPackageName(), 0);
        companyId = shared.getString("userId", "");

        value = getArguments().getInt("value");
        if (value == 1) {
            toolbar.setTitle("Applications Received");
        }
        jParser = new JsonParserUniversal();
        lvCandidateJobList = view.findViewById(R.id.lv_candidate_job);
        linerFilter = view.findViewById(R.id.linearFilter);
        txtNoData = view.findViewById(R.id.txt_nodata);
        final EditText edtSearch = view.findViewById(R.id.edt_search);
        getAppliedCandidateDetail();
        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String text = edtSearch.getText().toString().toLowerCase(Locale.getDefault());
                candidateAdapter.filters(text);

            }
        });

        linerFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showChangeLangDialog();
            }
        });

        return view;
    }

    private void getAppliedCandidateDetail() {
        new CallRequest(instance).getAppliedCandidateDetail(companyId);
    }

    private void getRank() {
        new CallRequest(instance).getRankFragment();
    }

    private void getShipType() {

        new CallRequest(instance).getShipFragment();
    }

    public void showChangeLangDialog() {

        dialogView = new Dialog(getActivity());
        dialogView.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogView.setCancelable(true);
        dialogView.setContentView(R.layout.custom_filter_dialog_rank);
        dialogView.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
//        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
//        LayoutInflater inflater = this.getLayoutInflater();
//        final View dialogView = inflater.inflate(R.layout.custom_filter_dialog_rank, null);
//        dialogBuilder.setView(dialogView);

        spinnerRank = dialogView.findViewById(R.id.spinner_rank);
        spinnerShip = dialogView.findViewById(R.id.spinner_ship_type);
        Button btnClear = dialogView.findViewById(R.id.btnClear);
        Button btnOk = dialogView.findViewById(R.id.btnOk);

        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rankValue = "";
                shipType = "";
                rankPos = 0;
                shipPos = 0;
                getAppliedCandidateDetail();
//                new CallRequest(instance).getCompanyCandidateDetailRank("", "");
                dialogView.dismiss();
            }
        });
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogView.dismiss();
                if (rankValue == "" && shipType == "") {
                    getAppliedCandidateDetail();
                } else {
                    new CallRequest(instance).getAppliedWithFilterCandidateDetail(companyId,rankValue, shipType);
                }
            }
        });

        aa = new ArrayAdapter(getContext(), R.layout.spinner_text, rankStringList);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerRank.setAdapter(new NothingSelectedSpinnerAdapter(
                aa, R.layout.contact_spinner_row_nothing_selected,
                getContext()));
        // spinnerRank.setAdapter(aa);
        spinnerRank.setSelection(rankPos);

        aaShip = new ArrayAdapter(getContext(), R.layout.spinner_text, shipStringList);
        aaShip.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //  spinnerShip.setAdapter(aaShip);
        spinnerShip.setAdapter(new NothingSelectedSpinnerAdapter(
                aaShip, R.layout.contact_spinner_row_nothing_selected_ship,
                getContext()));
        spinnerShip.setSelection(shipPos);

        spinnerRank.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                try {
                    rankValue = rankList.get(i - 1).getRankId();
                 } catch (NullPointerException e) {
                    e.printStackTrace();
                } catch (ArrayIndexOutOfBoundsException ex) {
                    ex.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinnerShip.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    shipType = shipList.get(position - 1).getShipID();

                } catch (NullPointerException e) {
                    e.printStackTrace();
                } catch (ArrayIndexOutOfBoundsException ex) {
                    ex.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        dialogView.show();
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {


            switch (request) {

                case getAppliedCandidateList:
                    Utils.hideProgressDialog();
                    candidateList.clear();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            JSONArray ob1 = jObj.getJSONArray("data");

                            for (int i = 0; i < ob1.length(); i++) {
                                JSONObject jObject = ob1.getJSONObject(i);
                                model = new CandidateModel();
                                model = (CandidateModel) jParser.parseJson(jObject, new CandidateModel());
                                candidateList.add(model);
                            }

                            candidateAdapter = new CandidateAppliedJobAdapter(getContext(), R.layout.layout_candidate, candidateList);
                            lvCandidateJobList.setAdapter(candidateAdapter);
                            txtNoData.setVisibility(View.GONE);
                            lvCandidateJobList.setVisibility(View.VISIBLE);

                            lvCandidateJobList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                    model = candidateList.get(i);
                                    String name = model.getCanidateName();
                                    String rankName = model.getRankName();
                                    String CurrentRank = model.getCurrentRank();
                                    String email = model.getEmailID();
                                    String maritalStatus = model.getMaritalStatus();
                                    String image = model.getImageURL();
                                    String indous = model.getIndos();
                                    String mobileNo = model.getMobileNo();
                                    String lastLogin = model.getLast_login();
                                    String dateOfApplication = model.getDateofApplication();
                                    String availableFrom = model.getAvailableFrom();
                                    String modified = model.getModified();
                                    String nationality = model.getNationalName();
                                    String candidateId = model.getCandidateID();
                                    String appliedShipType = model.getAppliedShipName();
                                    String appliedRank = model.getAppliedRankName();
                                    String seaTime = model.getSeatimeInLastRank();
                                    String idCandidate = model.getCandidateID();
                                    if (App.applicationRecevide) {
                                        ApplicationReceivedCandidateDetailragment candidateDetailFragment = new ApplicationReceivedCandidateDetailragment();
                                        Bundle b = new Bundle();
                                        b.putString("code", model.getCandidateCode());
                                        b.putString("name", model.getCanidateName());
                                        b.putString("nameCandidate", name);
                                        b.putString("rankName", rankName);
                                        b.putString("CurrentRank", CurrentRank);
                                        b.putString("email", email);
                                        b.putString("maritalStatus", maritalStatus);
                                        b.putString("image", image);
                                        b.putString("indous", indous);
                                        b.putString("mobileNo", mobileNo);
                                        b.putString("lastLogin", lastLogin);
                                        b.putString("modified", modified);
                                        b.putString("nationality", nationality);
                                        b.putString("idCandidate", idCandidate);
                                        b.putString("appliedShipType", appliedShipType);
                                        b.putString("appliedRank", appliedRank);
                                        b.putString("lastLogin", lastLogin);
                                        b.putString("seaTime", seaTime);
                                        b.putString("availableFrom", availableFrom);
                                        b.putString("dateOfApplication", dateOfApplication);
                                        b.putString("shipType", model.getShipType());
                                        candidateDetailFragment.setArguments(b);
                                        CompanyDashboard.changeFragment(candidateDetailFragment, true);
                                    } else {
                                        CandidateDetailFragment candidateDetailFragment = new CandidateDetailFragment();
                                        Bundle b = new Bundle();
                                        b.putString("code", model.getCandidateCode());
                                        b.putString("name", model.getCanidateName());
                                        b.putString("rankName", rankName);
                                        b.putString("email", email);
                                        b.putString("maritalStatus", maritalStatus);
                                        b.putString("image", image);
                                        b.putString("indous", indous);
                                        b.putString("mobileNo", mobileNo);
                                        b.putString("lastLogin", lastLogin);
                                        b.putString("modified", modified);
                                        b.putString("nationality", nationality);
                                        b.putString("idCandidate", candidateId);
                                        b.putString("appliedShipType", appliedShipType);
                                        b.putString("appliedRank", appliedRank);
                                        b.putString("lastLogin", lastLogin);
                                        b.putString("seaTime", seaTime);

                                        candidateDetailFragment.setArguments(b);
                                        CompanyDashboard.changeFragment(candidateDetailFragment, true);
                                    }
                                }
                            });

                        } else {
                            txtNoData.setVisibility(View.VISIBLE);
                            lvCandidateJobList.setVisibility(View.GONE);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    getRank();
                    break;
                case getCandidateListRank:
                    Utils.hideProgressDialog();
                    candidateList.clear();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            JSONArray ob1 = jObj.getJSONArray("data");

                            for (int i = 0; i < ob1.length(); i++) {
                                JSONObject jObject = ob1.getJSONObject(i);
                                model = new CandidateModel();
                                model = (CandidateModel) jParser.parseJson(jObject, new CandidateModel());
                                candidateList.add(model);
                            }
                            candidateAdapter = new CandidateAppliedJobAdapter(getContext(), R.layout.layout_candidate, candidateList);
                            lvCandidateJobList.setAdapter(candidateAdapter);
                            txtNoData.setVisibility(View.GONE);
                            lvCandidateJobList.setVisibility(View.VISIBLE);
                        } else {
                            txtNoData.setVisibility(View.VISIBLE);
                            lvCandidateJobList.setVisibility(View.GONE);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    getRank();
                case getRankFragment:

                    Utils.hideProgressDialog();

                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            JSONArray ob1 = jObj.getJSONArray("data");
                            for (int i = 0; i < ob1.length(); i++) {
                                JSONObject ob = ob1.getJSONObject(i);
                                String rankId = ob.getString("RankID");
                                String rankName = ob.getString("Name");
                                rankList.add(new RankModel(rankId, rankName));
                                rankStringList.add(rankName);
                                System.out.println("rank type===" + rankValue);
                                if (rankValue != null) {
                                    if (rankValue.equalsIgnoreCase(rankList.get(i).getRankId())) {
                                        rankPos = i + 1;
                                    }
                                }
                            }


                            Utils.hideProgressDialog();
//                            if (rank != null) {
//                                int spinnerPosition = aa.getPosition(rank);
//                                Log.d("spinnerPosition", String.valueOf(aa.getPosition(rank)));
//                                spRank.setSelection(spinnerPosition);
//                            }

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();

                        Log.d("new exception:", String.valueOf(e));
                    }
                    getShipType();
                    break;
                case getShipType:

                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            JSONArray ob1 = jObj.getJSONArray("data");
                            for (int i = 0; i < ob1.length(); i++) {
                                JSONObject ob = ob1.getJSONObject(i);
                                String shipId = ob.getString("ShipID");
                                String shipName = ob.getString("ShipType");
                                shipList.add(new ShipModel(shipId, shipName));
                                shipStringList.add(shipName);

                                if (shipType != null) {
                                    if (shipType.equalsIgnoreCase(shipList.get(i).getShipID())) {
                                        shipPos = i + 1;
                                    }
                                }
                            }


                        } else {
                            Utils.showToast(jObj.getString("message"), getContext());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();

                        Log.d("new exception:", String.valueOf(e));
                    }
                    Utils.hideProgressDialog();
                    break;


            }

        }
    }
}
