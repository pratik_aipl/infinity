package com.nfinitydynamics.fragment;


import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.nfinitydynamics.R;
import com.nfinitydynamics.activity.MainActivity;
import com.nfinitydynamics.adapter.NothingSelectedSpinnerAdapter;
import com.nfinitydynamics.model.CertificateModel;
import com.nfinitydynamics.model.CountryModel;
import com.nfinitydynamics.utils.App;
import com.nfinitydynamics.utils.AsynchTaskListner;
import com.nfinitydynamics.utils.CallRequest;
import com.nfinitydynamics.utils.Constant;
import com.nfinitydynamics.utils.JsonParserUniversal;
import com.nfinitydynamics.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import static com.nfinitydynamics.activity.MainActivity.changeFragment;
import static com.nfinitydynamics.activity.MainActivity.toolbar;
import static com.nfinitydynamics.utils.App.userId;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddCOCFragment extends Fragment implements AsynchTaskListner {

    public EditText edtNumber, edtValidity;
    public Spinner spinnerProficiency;
    public String number, placeIssue, validity, candidateId, id, uNumber, uPlace, uValidity, StringIndia, ucertificateType, sCertificate;
    public AddCOCFragment instance;
    public SharedPreferences shared;
    public Button btnSave;
    public int s, s1;
    public ArrayList<String> certificateList = new ArrayList<>();
    public ArrayAdapter aaCertificate = null;
    public ArrayList<CertificateModel> cerList = new ArrayList<>();
    public ArrayList<CountryModel> countryModelArrayList = new ArrayList<CountryModel>();
    public ArrayList<String> countyStringList = new ArrayList<>();
    public int selectedPos;
    public Spinner spPlaceIssue;
    public JsonParserUniversal jParser;
    String indusno;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view;
        view = inflater.inflate(R.layout.fragment_add_coc, container, false);
        instance = this;
        jParser = new JsonParserUniversal();
        toolbar.setTitle("Add COC / COP Details");
        edtNumber = view.findViewById(R.id.edt_number);
        spPlaceIssue = view.findViewById(R.id.sp_place_issue);
        edtValidity = view.findViewById(R.id.edt_validity);
        btnSave = view.findViewById(R.id.btn_save_experience);
        spinnerProficiency = view.findViewById(R.id.spinner_proficiency);
        getCertificateType();
        shared = getActivity().getSharedPreferences(getContext().getPackageName(), 0);
        indusno = shared.getString("indusno", "");
        Bundle b = getArguments();
        s = b.getInt("value");
        if (s == 1) {
            toolbar.setTitle("Edit COC Details");
            id = b.getString("id");
            Log.d("dod detail id---", id);
            uNumber = b.getString("number");
            validity = b.getString("validity");
            placeIssue = b.getString("place");
            sCertificate = b.getString("certificateType");
            edtNumber.setText(uNumber);
            Utils.converDateToDDMMYY(edtValidity, validity);
            //  edtValidity.setText(uValidity);

            //   Log.d("certificateType---", sCertificate);


            // spinnerProficiency.setSelection(s1);

        }
        spPlaceIssue.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                try {
                    placeIssue = countryModelArrayList.get(i - 1).getCountryID();
                    StringIndia = countryModelArrayList.get(i - 1).getCountryName();

                    System.out.println("place of issue===" + StringIndia);

                    if (placeIssue.equals("97"))
                        if (TextUtils.isEmpty(indusno)) {
                            openDialog();
                        }

                } catch (IndexOutOfBoundsException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinnerProficiency.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                try {
                    ucertificateType = cerList.get(i - 1).getCertificateID();
                    Log.d("selected nation----", ucertificateType);
                } catch (IndexOutOfBoundsException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }

        });
        shared = getActivity().getSharedPreferences(getContext().getPackageName(), 0);
        candidateId = shared.getString("userId", "");
        edtValidity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                Utils.generateDatePicker(getContext(), edtValidity);
            }
        });
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                number = edtNumber.getText().toString().trim();

                validity = edtValidity.getText().toString().trim();
                //  SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                Calendar cal = Calendar.getInstance();
                Date sysDate = cal.getTime();
                Date intValidity = null;
                try {
                    intValidity = new SimpleDateFormat("dd-MMM-yyyy").parse(validity);
                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                    validity = formatter.format(intValidity);
                } catch (ParseException e) {
                    e.printStackTrace();
                }


                if (TextUtils.isEmpty(ucertificateType)) {
                    Utils.showAlert("Please Select Competency/Proficiency", getContext());
                } else if (number.equals("")) {
                    edtNumber.findFocus();
                    Utils.showAlert("Please Enter Number", getContext());
                } else if (TextUtils.isEmpty(placeIssue)) {
                    spPlaceIssue.findFocus();
                    Utils.showAlert("Please Select Country", getContext());
                } else if (validity.equals("")) {
                    edtValidity.findFocus();
                    Utils.showAlert("Please Enter Validity", getContext());
                } else if (sysDate.compareTo(intValidity) > 0) {
                    Utils.showAlert("Please Enter Valid Validity", getContext());
                } else {

                    if (s == 0) {
                        new CallRequest(instance).addCOC(candidateId, number, placeIssue, ucertificateType, validity, "0");
                    }
                    if (s == 1) {

                        new CallRequest(instance).updateCOC(candidateId, number, ucertificateType, placeIssue, validity, "1", id);
                    }

                }


            }
        });

        return view;
    }

    public void openDialog() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        //txtFileName = dialog.findViewById(R.id.txtFileName);
        Button btnOk = dialog.findViewById(R.id.btnOk);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                App.isFromCoc = true;
                changeFragment(new EditProfileFragment(), true);
            }
        });


        dialog.show();
    }

    private void getCountry() {
        new CallRequest(instance).getCountryCOC(candidateId);
    }

    private void getCertificateType() {

        new CallRequest(instance).getCertificateType();
    }

//    private void updateCOCDetail() {
//
//        number = edtNumber.getText().toString().trim();
//        countryId = edtPlaceIssue.getText().toString().trim();
//        validity = edtValidity.getText().toString().trim();
//        //  SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
//        Calendar cal = Calendar.getInstance();
//        Date sysDate = cal.getTime();
//        Date intValidity = null;
//        try {
//            intValidity = new SimpleDateFormat("dd-MMM-yyyy").parse(validity);
//            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
//            validity = formatter.format(intValidity);
//
//
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//
//        if (number.equals("")) {
//            edtNumber.findFocus();
//            Utils.showAlert("Please Enter Number", getContext());
//        } else if (countryId.equals("")) {
//            edtPlaceIssue.findFocus();
//            Utils.showAlert("Please Enter Place Of Issue", getContext());
//        } else if (validity.equals("")) {
//            edtValidity.findFocus();
//            Utils.showAlert("Please Enter Validity", getContext());
//        } else if (sysDate.compareTo(intValidity) > 0) {
//            Utils.showAlert("Please Enter Valid Validity", getContext());
//        } else {
//            new CallRequest(instance).updateCOC(candidateId, number, ucertificateType, countryId, validity, "1", id);
//        }
//    }
//
//    private void addCOCDetail() {
//        number = edtNumber.getText().toString().trim();
//        countryId = edtPlaceIssue.getText().toString().trim();
//        validity = edtValidity.getText().toString().trim();
//        Calendar cal = Calendar.getInstance();
//        Date sysDate = cal.getTime();
//        Date intValidity = null;
//        try {
//            intValidity = new SimpleDateFormat("dd-MMM-yyyy").parse(validity);
//            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
//            validity = formatter.format(intValidity);
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//        if (number.equals("")) {
//            edtNumber.findFocus();
//            Utils.showAlert("Please Enter Number", getContext());
//        } else if (countryId.equals("")) {
//            edtPlaceIssue.findFocus();
//            Utils.showAlert("Please Enter Place Of Issue", getContext());
//        } else if (validity.equals("")) {
//            edtValidity.findFocus();
//            Utils.showAlert("Please Enter Validity", getContext());
//        } else if (sysDate.compareTo(intValidity) > 0) {
//            Utils.showAlert("Please Enter Valid Validity", getContext());
//        } else {
//            new CallRequest(instance).addCOC(candidateId, number, countryId, ucertificateType, validity, "0");
//        }
//    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case addCOC:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            Utils.showToast("Added Successfully", getContext());
                            MainActivity.activity.onBackPressed();
                        } else {
                            Utils.showToast("Try Again!!", getContext());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();

                        Log.d("new exception:", String.valueOf(e));
                    }
                    break;


                case updateCOC:

                    Utils.hideProgressDialog();

                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            Utils.showToast("Updated Successfully", getContext());
                            MainActivity.activity.onBackPressed();
                        } else {
                            Utils.showToast("Try Again!!", getContext());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();

                        Log.d("new exception:", String.valueOf(e));
                    }
                    break;
                case getCertificateType:

                    //Utils.hideProgressDialog();

                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            JSONArray ob1 = jObj.getJSONArray("data");
                            for (int i = 0; i < ob1.length(); i++) {
                                JSONObject ob = ob1.getJSONObject(i);
                                String certificateId = ob.getString("CertificateID");
                                String certificateName = ob.getString("CertificateType");
                                cerList.add(new CertificateModel(certificateId, certificateName));
                                certificateList.add(certificateName);
                            }

                            Log.d("certilist-----", String.valueOf(cerList));
//                            aaCertificate = new ArrayAdapter(getContext(), R.layout.spinner_text, certificateList);
//                            aaCertificate.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//                            spinnerProficiency.setAdapter(aaCertificate);


                            aaCertificate = new ArrayAdapter(getActivity(), R.layout.spinner_text, certificateList);
                            aaCertificate.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spinnerProficiency.setAdapter(new NothingSelectedSpinnerAdapter(
                                    aaCertificate, R.layout.certificate_noting_selected,
                                    getActivity()));
                            ;
                            if (sCertificate != null) {
                                int spinnerPosition = aaCertificate.getPosition(sCertificate);
                                Log.d("spinnerPosition", String.valueOf(aaCertificate.getPosition(sCertificate)));
                                spinnerProficiency.setSelection(spinnerPosition + 1);
                            }

                        } else {
                            Utils.showToast(jObj.getString("message"), getContext());
                        }
                        getCountry();

                    } catch (JSONException e) {
                        e.printStackTrace();

                        Log.d("new exception:", String.valueOf(e));
                    }
                    break;
                case getCountryListforCOC:

                    Utils.hideProgressDialog();

                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            JSONArray ob1 = jObj.getJSONArray("data");
                            CountryModel countryModel;
                            for (int i = 0; i < ob1.length(); i++) {
                                countryModel = new CountryModel();
                                countryModel = (CountryModel) jParser.parseJson(ob1.getJSONObject(i), new CountryModel());
                                countryModelArrayList.add(countryModel);
                                if (placeIssue != null) {
                                    if (placeIssue.equalsIgnoreCase(countryModel.getCountryName())) {
                                        selectedPos = i + 1;
                                    }
                                }
                                countyStringList.add(countryModel.getCountryName());
                            }
                            ArrayAdapter aa = new ArrayAdapter(getActivity(), R.layout.spinner_text, countyStringList);
                            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spPlaceIssue.setAdapter(new NothingSelectedSpinnerAdapter(
                                    aa, R.layout.country_place_issue,
                                    getActivity()));
                            spPlaceIssue.setSelection(selectedPos);
                        } else {
                            Utils.showToast("Try Again!!", getContext());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d("new exception:", String.valueOf(e));
                    }
            }
        }
    }

}
