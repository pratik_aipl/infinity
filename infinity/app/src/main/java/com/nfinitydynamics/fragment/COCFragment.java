package com.nfinitydynamics.fragment;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.nfinitydynamics.R;
import com.nfinitydynamics.activity.MainActivity;
import com.nfinitydynamics.adapter.COCAdapter;
import com.nfinitydynamics.model.COCModel;
import com.nfinitydynamics.utils.AsynchTaskListner;
import com.nfinitydynamics.utils.CallRequest;
import com.nfinitydynamics.utils.Constant;
import com.nfinitydynamics.utils.JsonParserUniversal;
import com.nfinitydynamics.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.nfinitydynamics.activity.MainActivity.toolbar;


public class COCFragment extends Fragment implements AsynchTaskListner{
    public FloatingActionButton fabAdd ;
    public SharedPreferences shared;
    public ListView lCOC;
    public String candidateId;
    public ArrayList<COCModel> cocList = new ArrayList<>();
    public JsonParserUniversal jParser;
    public static COCFragment instance;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view;
        view= inflater.inflate(R.layout.fragment_coc, container, false);
        instance=this;
        shared = getActivity().getSharedPreferences(getContext().getPackageName(), 0);
        candidateId = shared.getString("userId","");
        jParser=new JsonParserUniversal();
        fabAdd = view. findViewById(R.id.fab_add);
        lCOC=view.findViewById(R.id.lv_coc);
        toolbar.setTitle("COC / COP Details");

        cocList.clear();

        fabAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AddCOCFragment fragment=new AddCOCFragment();
                Bundle args = new Bundle();
                args.putInt("value",0);
                fragment.setArguments(args);
                MainActivity.changeFragment(fragment,true);
            }
        });

        getCOCDEtail();

        return  view;
    }

    private void getCOCDEtail() {


        new CallRequest(instance).getCOCDetail(candidateId);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case getCOCDetail:
                    Utils.hideProgressDialog();
                    cocList.clear();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {

                            //  Utils.showToast(jObj.getString("message"), getContext());
                            JSONArray ob1 = jObj.getJSONArray("data");

                            COCModel sBookModel;
                            for (int i = 0; i < ob1.length(); i++) {
                                JSONObject jObject = ob1.getJSONObject(i);
                                sBookModel = new COCModel();
                                sBookModel = (COCModel) jParser.parseJson(jObject, new COCModel());
                                cocList.add(sBookModel);
                            }
                            COCAdapter latestJobAdapter = new COCAdapter(getContext(), R.layout.layout_coc, cocList);
                            lCOC.setAdapter(latestJobAdapter);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;

                case deleteCOC:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {

                            // Utils.showToast(jObj.getString("message"), getContext());
                            Utils.showToast("Deleted Successfully",getContext());

                            getCOCDEtail();

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }

}
