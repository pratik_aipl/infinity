package com.nfinitydynamics.fragment.companyFragment;


import android.app.Dialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;

import com.nfinitydynamics.R;
import com.nfinitydynamics.model.UserDetailModel;
import com.nfinitydynamics.utils.AsynchTaskListner;
import com.nfinitydynamics.utils.CallRequest;
import com.nfinitydynamics.utils.Constant;
import com.nfinitydynamics.utils.JsonParserUniversal;
import com.nfinitydynamics.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import static com.nfinitydynamics.activity.CompanyActivity.CompanyDashboard.activityCompany;
import static com.nfinitydynamics.activity.CompanyActivity.CompanyDashboard.toolbar;

/**
 * A simple {@link Fragment} subclass.
 */
public class ViewCandidateFragment extends Fragment implements AsynchTaskListner {


    public ViewCandidateFragment() {
        // Required empty public constructor
    }
    public JsonParserUniversal jParser;
    public ViewCandidateFragment instance;
    public String candidateId, companyId;
    public SharedPreferences shared;
    View view;
    private WebView webView;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view= inflater.inflate(R.layout.fragment_view_candidate, container, false);
        jParser = new JsonParserUniversal();
        instance = this;
        toolbar.setTitle("Preview Resume");
        webView=view.findViewById(R.id.webView);

        shared = getActivity().getSharedPreferences(getContext().getPackageName(), 0);
        companyId = shared.getString("userId", "");

        candidateId = getArguments().getString("candidateId");
        Log.i("Preview","==>"+"http://test.infinitydynamics.in/web_services/version_1/web_services/preview_build_resume?candidate_id="+candidateId);
      //  webView.loadUrl("http://test.infinitydynamics.in/web_services/version_1/web_services/preview_build_resume?candidate_id="+candidateId);
      webView.loadUrl("https://infinitydynamics.in/web_services/version_1/web_services/preview_build_resume?candidate_id="+candidateId);
        //  new CallRequest(instance).previewBuildResume(candidateId);
        return view;
    }
    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case previewBuildResume:

                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
//                            UserDetailModel userDetailModel;
//                            userDetailModel = new UserDetailModel();
//                            userDetailModel = (UserDetailModel) jParser.parseJson(jObj, new UserDetailModel());
//                            String resumeUrl = userDetailModel.getResume_url();
//                            Log.d("resume::", resumeUrl);
//                            webView.getSettings().setJavaScriptEnabled(true);
//                            // String pdf = "http://docs.google.com/gview?embedded=true&url=" + resumeUrl;
//                            String pdf = "https://drive.google.com/viewerng/viewer?embedded=true&url=" + resumeUrl;
//                            webView.loadUrl(pdf);
//                            // new DownloadFile().execute(resumeUrl, "maven.pdf");
//                            //  Utils.showToast("Downlod Successfully", getContext());
//                            // MainActivity.changeFragment(new HistoryFragment(), false);

                        } else {
                            Utils.showToast(jObj.getString("message"),getActivity());

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d("new exception:", String.valueOf(e));
                    }
                    break;

            }
        }
    }
}
