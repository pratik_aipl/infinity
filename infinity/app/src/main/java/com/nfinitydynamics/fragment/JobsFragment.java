package com.nfinitydynamics.fragment;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.nfinitydynamics.R;
import com.nfinitydynamics.adapter.TabsPagerAdapter;

import static com.nfinitydynamics.activity.MainActivity.changeFragment;
import static com.nfinitydynamics.activity.MainActivity.toolbar;

public class JobsFragment extends Fragment {

    public ViewPager viewPager;
    public TabLayout tabLayout;
    public View view;
    public TabsPagerAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_jobs, container, false);
        toolbar.setTitle("Jobs");
        viewPager =  view.findViewById(R.id.viewpager);
        //   setupViewPager(viewPager);
        tabLayout =  view.findViewById(R.id.tabs);
        tabLayout.addTab(tabLayout.newTab().setText("Available Jobs"));
        tabLayout.addTab(tabLayout.newTab().setText("MATCH JOB"));

        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        adapter = new TabsPagerAdapter
                (getChildFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }
            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        setHasOptionsMenu(true);
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.candidate, menu);

        //  MenuItem item = menu.findItem(R.id.login).setVisible(true);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.login) {
            changeFragment(new ProfileFragment(),true);
            //  Toast.makeText(getApplicationContext(), "Logout user!", Toast.LENGTH_LONG).show();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
      //  viewPager.setAdapter(adapter);

    }

//    @Override
//    public void setMenuVisibility(boolean menuVisible) {
//        super.setMenuVisibility(menuVisible);
//        Log.i("TAG", "In VISIBle viewPager");
//
//    }


}
