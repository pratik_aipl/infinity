package com.nfinitydynamics.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nfinitydynamics.activity.CompanyActivity.CompanyDashboard;
import com.nfinitydynamics.model.UserDetailModel;
import com.nfinitydynamics.model.UserModel;
import com.nfinitydynamics.utils.App;
import com.nfinitydynamics.R;
import com.nfinitydynamics.utils.AsynchTaskListner;
import com.nfinitydynamics.utils.CallRequest;
import com.nfinitydynamics.utils.Constant;
import com.nfinitydynamics.utils.JsonParserUniversal;
import com.nfinitydynamics.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import me.itangqi.waveloadingview.WaveLoadingView;


public class LoginActivity extends AppCompatActivity implements AsynchTaskListner {

    public JsonParserUniversal jParser;
    public EditText edtEmail, edtPassword;
    public Button btnLogin;
    private WaveLoadingView mWaveLoadingView;
    private int checkedItem = 0;
    public App app;
    public LinearLayout linearSignup;
    public LoginActivity instance;
    public SharedPreferences shared;
    public UserModel uModel;
    public String userType;
    public int intent;
    public TextView txtForgotPassword;
    public String tokenid = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        App.pageState = "0";
        App.childeren = "";
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, 0);
        }


        instance = this;
        tokenid = Utils.OneSignalPlearID();
        jParser = new JsonParserUniversal();
        shared = getSharedPreferences(getApplication().getPackageName(), 0);
        edtEmail = findViewById(R.id.edt_email);
        edtPassword = findViewById(R.id.edt_password);
        btnLogin = findViewById(R.id.btn_login);
        linearSignup = findViewById(R.id.linear_signup);
        txtForgotPassword = findViewById(R.id.edtForgotPassword);
        mWaveLoadingView = findViewById(R.id.waveLoadingView);
        mWaveLoadingView.setVisibility(View.GONE);
        // Sets the length of the animation, default is 1000.


        Bundle bundle = getIntent().getExtras();
        intent = bundle.getInt("value");
        if (intent == 1) {

            String email = shared.getString("email", "");
            String password = shared.getString("password", "");
            edtEmail.setText(email);
            edtPassword.setText(password);
            Validation();

        }
        shared = getSharedPreferences(getApplication().getPackageName(), 0);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Validation();
            }
        });
        linearSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(instance, FirstActivity.class);
                startActivity(intent);
            }
        });
        txtForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(instance, ForgotPassword.class);
                startActivity(i);
            }
        });
    }

    public void Validation() {
        if (edtEmail.getText().toString().equals("")) {
            edtEmail.requestFocus();
            Utils.showAlert("Please Enter Email", this);
        } else if (edtPassword.getText().toString().isEmpty()) {
            edtPassword.requestFocus();
            Utils.showToast("Please Enter Password", this);
        } else {
            if (Utils.isNetworkAvailable(instance)) {
                if (tokenid.equalsIgnoreCase("")) {
                    tokenid = Utils.OneSignalPlearID();
                    Utils.showProgressDialog(instance);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Utils.removeWorkingDialog(instance);
                        }
                    }, 6000);
                } else {
                    new CallRequest(instance).login(edtEmail.getText().toString(), edtPassword.getText().toString(), tokenid);
                }
            }
        }
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case login:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            UserDetailModel detailModel;
                            JSONArray ob1 = jObj.getJSONArray("data");
                            for (int i = 0; i < ob1.length(); i++) {
                                JSONObject jObject = ob1.getJSONObject(i);
                                uModel = new UserModel();
                                uModel = (UserModel) jParser.parseJson(jObject, new UserModel());
                                SharedPreferences.Editor et = shared.edit();
                                String id = uModel.getCompanyid();
                                String username = uModel.getUsername();
                                userType = uModel.getUsertype();
                                et.putString("email", edtEmail.getText().toString());
                                et.putString("password", edtPassword.getText().toString());
                                et.putString("userId", id);
                                et.putString("user_id", uModel.getId());
                                et.putString("username", username);
                                et.putString("mobile", uModel.getMobile());
                                et.putString("image", uModel.getImageURL());
                                et.putBoolean(app.IS_LOGGED_IN, true);
                                et.putString("userType", userType);
                                et.commit();
                                String myStrValue = shared.getString("userId", id);
                                Log.d("userType:=====", userType);
                            }

                            JSONObject jObject = jObj.getJSONObject("more_info");
                            detailModel = (UserDetailModel) jParser.parseJson(jObject, new UserDetailModel());
                            SharedPreferences.Editor et1 = shared.edit();
                            String first, last, middle, email, indous, rank, mobileno, address;
                            first = detailModel.getFirstName();
                            middle = detailModel.getMiddleName();
                            last = detailModel.getLastName();
                            indous = detailModel.getIndos();
                            mobileno = detailModel.getMobileNo();
                            email = detailModel.getEmailID();
                            rank = detailModel.getRankName();
                            address = detailModel.getAddress();
                            String logoImage = detailModel.getLogoURL();
                            int totalCandidate = detailModel.getTotal_candidate();
                            int totalPostedJobs = detailModel.getTotal_job();
                            int totalJobApplication = detailModel.getTotal_job_application();
                            int totalLatestCandidate = detailModel.getTotal_latest_candidate();
                            int total_shorejob_candidate = detailModel.getTotal_shorejob();
                            et1.putString("firstName", first);
                            et1.putString("middleName", middle);
                            et1.putString("lastName", last);
                            et1.putString("email", email);
                            et1.putString("indusno", indous);
                            et1.putString("mobileNo", mobileno);
                            et1.putString("rank", rank);
                            et1.putString("address", address);
                            et1.putString("logoImage", logoImage);
                            et1.putInt("totalPostedJobs", totalPostedJobs);
                            et1.putInt("totalCandidate", totalCandidate);
                            et1.putInt("totalJobApplication", totalJobApplication);
                            et1.putInt("totalLatestCandidate", totalLatestCandidate);
                            et1.putInt("total_shorejob", total_shorejob_candidate);
                            et1.commit();
                            App.profilePic = logoImage;
                            // App.companyName=companyNmae;
                            App.Address = address;
                            Log.d("usermodual--", uModel.getUsertype());
                            if (userType.equalsIgnoreCase("Candidate")) {
                                Intent intent = new Intent(instance, MainActivity.class);
                                startActivity(intent);
                                finish();
                            } else if (userType.equalsIgnoreCase("Company")) {
                                App.companyName = detailModel.getCompanyName();
                                Intent intent = new Intent(instance, CompanyDashboard.class);
                                startActivity(intent);
                                finish();
                            }
                        } else {
                            Utils.showToast(jObj.getString("message"), this);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }

    @Override
    public void onBackPressed() {

        finish();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                // do something here
                Intent startMain = new Intent(Intent.ACTION_MAIN);
                startMain.addCategory(Intent.CATEGORY_HOME);
                startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(startMain);
                finish();
                return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
