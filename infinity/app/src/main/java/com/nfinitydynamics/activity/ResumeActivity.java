package com.nfinitydynamics.activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.nfinitydynamics.R;

public class ResumeActivity extends AppCompatActivity {
    public ResumeActivity instance;
    WebView myWebView;
    public SharedPreferences shared;
    public String url;
    View view;
    public Toolbar toolbar;
    public TextView tv_title, tv_available;
    public ImageView img_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resume);
        instance = this;
        img_back = (ImageView) findViewById(R.id.img_back);
        tv_title = (TextView) findViewById(R.id.tv_title);
        tv_available = (TextView) findViewById(R.id.tv_available);
        tv_title.setText("Resume");
        myWebView = (WebView) findViewById(R.id.webview);
        url = getIntent().getStringExtra("resume_url");
        myWebView.getSettings().setJavaScriptEnabled(true);
        String pdf = "https://drive.google.com/viewerng/viewer?embedded=true&url=" + url;

            if (url != null && !url.equalsIgnoreCase("")) {
            tv_available.setVisibility(View.GONE);
            myWebView.setVisibility(View.VISIBLE);
            myWebView.loadUrl(url);
        } else {

            myWebView.setVisibility(View.GONE);
            tv_available.setVisibility(View.VISIBLE);
        }

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


    }
}
