package com.nfinitydynamics.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;

import com.nfinitydynamics.R;
import com.nfinitydynamics.utils.App;

import static com.nfinitydynamics.utils.App.joiningTypeValue;


public class FirstActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first);
        App.pageState = "0";
        App.childeren = "";
        App.rankvalue = "";
        App.indexListShip.clear();
        App.indexListRank.clear();
        App.availableFrom = "";

        App.maritalValue = "";
        App.children = "";
        App.joiningTypeValue = "";
        App.multiSelecteRankId = "";
        App.gender = "";
        App.multiSelecteShipTypeId = "";
        App.rankvalue = "";
        System.out.println("joining type===" + joiningTypeValue);

        RelativeLayout relativeCandidate = findViewById(R.id.relative_candidate);
        RelativeLayout relativeCompany = findViewById(R.id.relative_company);
        RelativeLayout  rel_institute = findViewById(R.id. rel_institute);
        relativeCandidate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), CandidateSignUp.class);
                startActivity(i);
            }
        });
        relativeCompany.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), CompanySignUp.class);
                startActivity(i);
            }
        });
        rel_institute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), InstituteSignUp.class);
                startActivity(i);
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
//                Intent i = new Intent(getApplicationContext(), LoginActivity.class);
//                startActivity(i);
//                this.finish();
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
