package com.nfinitydynamics.activity.CompanyActivity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.nfinitydynamics.R;
import com.nfinitydynamics.activity.LoginActivity;
import com.nfinitydynamics.adapter.DrawerAdapter;
import com.nfinitydynamics.fragment.companyFragment.CandidateFragment;
import com.nfinitydynamics.fragment.companyFragment.CandidateJobsFragment;
import com.nfinitydynamics.fragment.companyFragment.ChangePasswordFragment;
import com.nfinitydynamics.fragment.companyFragment.CompanyHomeFragment;
import com.nfinitydynamics.fragment.companyFragment.CompanyNotificationFragment;
import com.nfinitydynamics.fragment.companyFragment.DownloadedCandidatesFragment;
import com.nfinitydynamics.fragment.companyFragment.ManageCompanyJobFragment;
import com.nfinitydynamics.fragment.companyFragment.MyAccountFragment;
import com.nfinitydynamics.fragment.companyFragment.MyPlanFragment;
import com.nfinitydynamics.model.DrawerModel;
import com.nfinitydynamics.utils.App;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class CompanyDashboard extends AppCompatActivity {
    App app;
    private NavigationView navigationView;
    public static DrawerLayout drawer;
    private View navHeader;
    public static Toolbar toolbar;
    public static int navItemIndex = 0;
    private static final String TAG_HOME = "home";
    public static String CURRENT_TAG = TAG_HOME;
    public static AppCompatActivity activityCompany;
    private boolean shouldLoadHomeFragOnBackPress = true;
    private Handler mHandler;
    public ListView lvDrawer;
    public ArrayList<DrawerModel> drawerList = new ArrayList<>();
    public LinearLayout linearLogout;
    public static SharedPreferences shared;
    public static ActionBarDrawerToggle actionBarDrawerToggle;
    public static FragmentManager manager;
    public static ActionBar actionBar;
    public  static CircleImageView ivProfile;
    public static TextView txtUserName,txtAddress;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_dashboard);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        activityCompany = CompanyDashboard.this;
        manager = getSupportFragmentManager();
        actionBar = getSupportActionBar();
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        activityCompany.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayUseLogoEnabled(false);
        mHandler = new Handler();
        shared = getSharedPreferences(getApplicationContext().getPackageName(), 0);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.nvView);
        lvDrawer = (ListView) findViewById(R.id.lv_drawer);
        linearLogout = (LinearLayout) findViewById(R.id.linear_logout);
        getSupportActionBar().setTitle(R.string.app_name);
        setDrawerItem();
        linearLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doLogout();
            }
        });
        navHeader = navigationView.getHeaderView(0);

        ivProfile = (CircleImageView) navHeader.findViewById(R.id.iv_profile);
         txtUserName = (TextView) navHeader.findViewById(R.id.txt_name);
         txtAddress=(TextView)navHeader.findViewById(R.id.txt_location);
        String candidateId = shared.getString("userId", "");
        String image = shared.getString("logoImage", "");
        String candidateUserNAme = shared.getString("username", "");
        String address = shared.getString("address", "");

//        if (image != "") {
//            Log.d("shared image:--", image);
//            Picasso.with(CompanyDashboard.this).load(image).placeholder(R.drawable.user)
//                    .error(R.drawable.user).into(ivProfileComp);
//
//        }
        Picasso.with(CompanyDashboard.this).load(App.profilePic).placeholder(R.drawable.user)
                    .error(R.drawable.user).into(ivProfile);
        txtUserName.setText(App.companyName);
        txtAddress.setText(App.Address);
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.openDrawer, R.string.closeDrawer) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(drawerView.getWindowToken(), 0);
            }
        };
        drawer.setDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
        changeFragment(new CompanyHomeFragment(), false);
    }

    private void doLogout() {
        final MaterialDialog dialog = new MaterialDialog.Builder(CompanyDashboard.this)
                .title("Logout")
                .content("Are you sure you want to log out?")
                .positiveText("Ok")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                        SharedPreferences.Editor et = shared.edit();
                        et.putString("userId", "");
                        et.putBoolean(App.IS_LOGGED_IN, false);
                        et.clear();
                        et.commit();
                        // System.exit(0);
                        Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putInt("value", 0);
                        i.putExtras(bundle);
                        startActivity(i);
                        finish();

                    }
                })
                .negativeText("Cancel")
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .build();
        dialog.show();
    }

    private void setDrawerItem() {
        drawerList.clear();
        drawerList.add(new DrawerModel(R.drawable.home, "DashBoard"));
        drawerList.add(new DrawerModel(R.drawable.home, "My Plan"));

        drawerList.add(new DrawerModel(R.drawable.ship, "Publish Vacancies"));
        drawerList.add(new DrawerModel(R.drawable.user, "All Candidates"));
        drawerList.add(new DrawerModel(R.drawable.companies, "Applications Received"));
        drawerList.add(new DrawerModel(R.drawable.home, "Downloaded Resume"));
        drawerList.add(new DrawerModel(R.drawable.about_us, "Change Password"));
        drawerList.add(new DrawerModel(R.drawable.user, "My Account"));
        drawerList.add(new DrawerModel(R.drawable.user, "Notification"));
        DrawerAdapter adapter = new DrawerAdapter(getApplicationContext(), R.layout.layout_drawer, drawerList);
        lvDrawer.setAdapter(adapter);
        lvDrawer.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 0) {
                    changeFragment(new CompanyHomeFragment(), false);
                } else if (i == 1) {
                    changeFragment(new MyPlanFragment(), true);
                }else if (i == 2) {
                    ManageCompanyJobFragment manageCompanyJobFragment=new ManageCompanyJobFragment();
                    Bundle bundle=new Bundle();
                    bundle.putInt("value",0);
                    manageCompanyJobFragment.setArguments(bundle );
                    changeFragment(manageCompanyJobFragment, true);
                } else if (i == 5) {
                    changeFragment(new DownloadedCandidatesFragment(), true);
                }else if (i == 3) {

                    CandidateFragment candidateFragment=new CandidateFragment();
                    Bundle bundle=new Bundle();
                    bundle.putInt("value",1);
                    candidateFragment.setArguments(bundle );
                    changeFragment(candidateFragment, true);

                } else if (i == 4) {
                    CandidateJobsFragment candidateJobsFragment=new CandidateJobsFragment();
                    Bundle bundle=new Bundle();
                    bundle.putInt("value",0);
                    candidateJobsFragment.setArguments(bundle );
                    changeFragment(candidateJobsFragment, true);
                } else if (i == 6) {
                    changeFragment(new ChangePasswordFragment(),true);
                } else if (i == 7) {
                    changeFragment(new MyAccountFragment(), true);
                }else if (i == 8) {
                    changeFragment(new CompanyNotificationFragment(), true);
                }
            }
        });
    }


    public static void changeFragment(Fragment fragment, boolean doAddToBackStack) {

        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.frame, fragment);
        if (doAddToBackStack) {
            activityCompany.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            transaction.addToBackStack(null);
            setBackButton();

        } else {
            setDrawer();
        }
        transaction.commit();
    }

    public static void setDrawer() {


        txtUserName.setText(App.companyName);
        txtAddress.setText(App.Address);
        System.out.println("image===="+App.profilePic);
        Picasso.with(activityCompany).load(App.profilePic).placeholder(R.drawable.user)
                .error(R.drawable.user).into(CompanyDashboard.ivProfile);
        activityCompany.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        actionBarDrawerToggle.syncState();
        actionBarDrawerToggle.setDrawerIndicatorEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.openDrawer(GravityCompat.START);
            }
        });
        drawer.closeDrawers();
//        View view = getCurrentFocus();
//        if (view != null) {
//            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
//        }
    }

    public static void setBackButton() {
        activityCompany.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final Drawable upArrow = activityCompany.getResources().getDrawable(R.drawable.ic_back);
        upArrow.setColorFilter(Color.parseColor("#FFFFFF"), PorterDuff.Mode.SRC_ATOP);
        activityCompany.getSupportActionBar().setHomeAsUpIndicator(upArrow);
        actionBarDrawerToggle.setDrawerIndicatorEnabled(false);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activityCompany.onBackPressed();
            }
        });

        drawer.closeDrawers();
    }

    @Override
    public void onBackPressed() {
        FragmentManager manager = getSupportFragmentManager();

        if (manager.getBackStackEntryCount() == 1) {
            setDrawer();
            manager.popBackStack();
        } else if (manager.getBackStackEntryCount() > 1) {
            manager.popBackStack();
            setBackButton();

        } else {
            // Otherwise, ask user if he wants to leave :)
            new AlertDialog.Builder(this)
                    .setTitle("Really Exit?")
                    .setMessage("Are you sure want to exit?")
                    .setNegativeButton(android.R.string.no, null)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface arg0, int arg1) {
                            finish();
                            moveTaskToBack(true);
                        }
                    }).create().show();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            MyAccountFragment editFragment = (MyAccountFragment) getSupportFragmentManager().findFragmentById(R.id.frame);
            if (editFragment != null && editFragment.isVisible()) {
                editFragment.onActivityResult(requestCode, resultCode, data);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    protected void onResume() {
        super.onResume();
        Picasso.with(CompanyDashboard.this).load(App.profilePic).placeholder(R.drawable.user)
                .error(R.drawable.user).into(ivProfile);
    }
}
