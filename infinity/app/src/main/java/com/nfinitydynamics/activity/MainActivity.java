package com.nfinitydynamics.activity;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.nfinitydynamics.R;
import com.nfinitydynamics.adapter.DrawerAdapter;
import com.nfinitydynamics.fragment.ChangePasswordCandidateFragment;
import com.nfinitydynamics.fragment.CompanyListFragment;
import com.nfinitydynamics.fragment.EditProfileFragment;
import com.nfinitydynamics.fragment.HomeFragment;
import com.nfinitydynamics.fragment.InstituteListFragment;
import com.nfinitydynamics.fragment.JobsFragment;
import com.nfinitydynamics.fragment.NewContactUsFragment;
import com.nfinitydynamics.fragment.NotificationFragment;
import com.nfinitydynamics.fragment.ProfileFragment;
import com.nfinitydynamics.fragment.TermsAndConditionFragment;
import com.nfinitydynamics.model.DrawerModel;
import com.nfinitydynamics.utils.App;
import com.nfinitydynamics.utils.AsynchTaskListner;
import com.nfinitydynamics.utils.CallRequest;
import com.nfinitydynamics.utils.Constant;
import com.nfinitydynamics.utils.Utils;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

//
public class MainActivity extends AppCompatActivity implements AsynchTaskListner {
    App app;
    private NavigationView navigationView;
    public static DrawerLayout drawer;
    private View navHeader;
    public static Toolbar toolbar;
    public static int navItemIndex = 0;
    private static final String TAG_HOME = "home";
    public static String CURRENT_TAG = TAG_HOME;
    public static AppCompatActivity activity;
    private boolean shouldLoadHomeFragOnBackPress = true;
    private Handler mHandler;
    public ListView lvDrawer;
    public ArrayList<DrawerModel> drawerList = new ArrayList<>();
    public LinearLayout linearLogout;
    public SharedPreferences shared;
    public static ActionBarDrawerToggle actionBarDrawerToggle;
    public static FragmentManager manager;
    public static ActionBar actionBar;
    public static boolean backStatus = false;
    public String candidateId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        activity = MainActivity.this;
        manager = getSupportFragmentManager();
        actionBar = getSupportActionBar();
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayUseLogoEnabled(false);
        mHandler = new Handler();
        shared = getSharedPreferences(getApplicationContext().getPackageName(), 0);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.nvView);
        lvDrawer = (ListView) findViewById(R.id.lv_drawer);
        linearLogout = (LinearLayout) findViewById(R.id.linear_logout);
        getSupportActionBar().setTitle(R.string.app_name);
        setDrawerItem();
        linearLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doLogout();
            }
        });
        navHeader = navigationView.getHeaderView(0);

        final CircleImageView ivProfile = (CircleImageView) navHeader.findViewById(R.id.iv_profile);
        TextView txtUserName = (TextView) navHeader.findViewById(R.id.txt_name);
        TextView txtAddress = (TextView) navHeader.findViewById(R.id.txt_location);
        String candidateId = shared.getString("userId", "");
        final String image = shared.getString("image", "");
        String candidateUserNAme = shared.getString("username", "");
        String address = shared.getString("address", "");
        if (image != "") {
            Picasso.with(MainActivity.this).load(image).placeholder(R.drawable.profilepic)
                    .error(R.drawable.profilepic).into(ivProfile);

        }
        txtUserName.setText(candidateUserNAme);
        txtAddress.setText(address);
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.openDrawer, R.string.closeDrawer) {

            @Override
            public void onDrawerClosed(View drawerView) {

                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {

                super.onDrawerOpened(drawerView);
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(drawerView.getWindowToken(), 0);
                String address = shared.getString("address", "");
                String image = shared.getString("image", "");
                if (image != "") {
                    Picasso.with(MainActivity.this).load(image)
                            .error(R.drawable.profilepic).into(ivProfile);

                }
            }
        };

        drawer.setDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
        changeFragment(new HomeFragment(), false);
        candidateId = shared.getString("userId", "");

        new CallRequest(MainActivity.this).updateLastVisited(candidateId);
    }

    private void doLogout() {
        final MaterialDialog dialog = new MaterialDialog.Builder(MainActivity.this)
                .title("Logout")
                .content("Are you sure you want to log out?")
                .positiveText("Ok")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                        //    SharedPreferences preferences = getPreferences(0);
                        SharedPreferences.Editor et = shared.edit();
                        et.putString("userId", "");
                        et.putBoolean(App.IS_LOGGED_IN, false);
                        et.clear();
                        et.commit();
                        // System.exit(0);
                        Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putInt("value", 0);
                        i.putExtras(bundle);
                        startActivity(i);
                        finish();
                    }
                })
                .negativeText("Cancel")
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .build();
        dialog.show();
    }

    private void setDrawerItem() {
        drawerList.clear();
        drawerList.add(new DrawerModel(R.drawable.home, "DashBoard"));
        drawerList.add(new DrawerModel(R.drawable.user, "My Profile"));
        drawerList.add(new DrawerModel(R.drawable.ship, "Find Jobs"));
        drawerList.add(new DrawerModel(R.drawable.companies, "Companies"));
        drawerList.add(new DrawerModel(R.drawable.companies, "Institutes"));
        // drawerList.add(new DrawerModel(R.drawable.about_us, "About Us"));
        //   drawerList.add(new DrawerModel(R.drawable.news, "News"));
        drawerList.add(new DrawerModel(R.drawable.contact_us, "Contact Us"));
        drawerList.add(new DrawerModel(R.drawable.user, "Change Password"));
        drawerList.add(new DrawerModel(R.drawable.terms, "Terms And Conditions"));
        drawerList.add(new DrawerModel(R.drawable.terms, "Notification"));
        DrawerAdapter adapter = new DrawerAdapter(getApplicationContext(), R.layout.layout_drawer, drawerList);
        lvDrawer.setAdapter(adapter);
        lvDrawer.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 0) {
                    changeFragment(new HomeFragment(), false);
                } else if (i == 1) {

                    changeFragment(new ProfileFragment(), true);
                } else if (i == 2) {

                    changeFragment(new JobsFragment(), true);
                } else if (i == 3) {

                    changeFragment(new CompanyListFragment(), true);
                } else if (i == 4) {

                    changeFragment(new InstituteListFragment(), true);
                } /*else if (i == 4) {

                } else if (i == 5) {
                    changeFragment(new NewsFragment(), false);
                }*/ else if (i == 5) {
                    changeFragment(new NewContactUsFragment(), true);
                } else if (i == 6) {
                    changeFragment(new ChangePasswordCandidateFragment(), true);
                } else if (i == 7) {
                    changeFragment(new TermsAndConditionFragment(), true);
                } else if (i == 8) {
                    changeFragment(new NotificationFragment(), true);
                }
            }
        });
    }


    public static void changeFragment(Fragment fragment, boolean doAddToBackStack) {
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.frame, fragment);
        if (doAddToBackStack) {
            activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            transaction.addToBackStack(null);
            setBackButton();
        } else {
            setDrawer();
        }
        transaction.commit();
    }

    public static void setDrawer() {
        activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        actionBarDrawerToggle.syncState();
        actionBarDrawerToggle.setDrawerIndicatorEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.openDrawer(GravityCompat.START);
            }
        });
        drawer.closeDrawers();
    }

    public static void setBackButton() {
        activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        activity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
        actionBarDrawerToggle.setDrawerIndicatorEnabled(false);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.onBackPressed();
            }
        });

        drawer.closeDrawers();
    }

    @Override
    public void onBackPressed() {
        FragmentManager manager = getSupportFragmentManager();
        if (App.isApply == true) {
            setDrawer();
            manager.popBackStack();
            changeFragment(new HomeFragment(), false);
            App.isBackToHistory = true;
            App.isApply = false;
            return;
        } else if (manager.getBackStackEntryCount() == 1) {
            setDrawer();
            manager.popBackStack();
        } else if (manager.getBackStackEntryCount() > 1) {
            manager.popBackStack();
            setBackButton();
        } else {
            // Otherwise, ask user if he wants to leave :)
            new AlertDialog.Builder(this)
                    .setTitle("Really Exit?")
                    .setMessage("Are you sure you want to exit?")
                    .setNegativeButton(android.R.string.no, null)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface arg0, int arg1) {
                            finish();
                            moveTaskToBack(true);
                        }
                    }).create().show();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            EditProfileFragment editFragment = (EditProfileFragment) getSupportFragmentManager().findFragmentById(R.id.frame);
            if (editFragment != null && editFragment.isVisible()) {
                editFragment.onActivityResult(requestCode, resultCode, data);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case updateLastVisited:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {

                        } else {
                            Utils.showToast(jObj.getString("message"), this);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//
//            getMenuInflater().inflate(R.menu.candidate, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        int id = item.getItemId();
//        if (id == R.id.login) {
//          //  Toast.makeText(getApplicationContext(), "Logout user!", Toast.LENGTH_LONG).show();
//            return true;
//        }
//        return super.onOptionsItemSelected(item);
//    }
}






