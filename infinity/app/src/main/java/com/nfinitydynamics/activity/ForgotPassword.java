package com.nfinitydynamics.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;

import com.nfinitydynamics.R;
import com.nfinitydynamics.adapter.NothingSelectedSpinnerAdapter;
import com.nfinitydynamics.model.Nationality;
import com.nfinitydynamics.utils.AsynchTaskListner;
import com.nfinitydynamics.utils.CallRequest;
import com.nfinitydynamics.utils.Constant;
import com.nfinitydynamics.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ForgotPassword extends AppCompatActivity implements AsynchTaskListner{

    Button btnOk;
    EditText edtEmail;
    String email;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        btnOk=findViewById(R.id.btn_ok);
        edtEmail=findViewById(R.id.edt_email);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                email=edtEmail.getText().toString();

                if(TextUtils.isEmpty(email)){
                    Utils.showToast("Please Enter Email",getApplicationContext());
                }else {
                    new CallRequest(ForgotPassword.this).ForgotPasswprd(email);
                }
            }
        });
    }


    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case fogotPassword:
                    Utils.hideProgressDialog();

                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            Utils.showAlert(jObj.getString("message"),ForgotPassword.this);
                        }
                        else {
                            Utils.showAlert(jObj.getString("message"),ForgotPassword.this);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }
}
