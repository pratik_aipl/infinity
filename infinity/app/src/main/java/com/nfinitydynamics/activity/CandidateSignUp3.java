package com.nfinitydynamics.activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.nfinitydynamics.R;
import com.nfinitydynamics.adapter.NothingSelectedSpinnerAdapter;
import com.nfinitydynamics.model.CountryResidenceModel;
import com.nfinitydynamics.utils.App;
import com.nfinitydynamics.utils.AsynchTaskListner;
import com.nfinitydynamics.utils.CallRequest;
import com.nfinitydynamics.utils.Constant;
import com.nfinitydynamics.utils.JsonParserUniversal;
import com.nfinitydynamics.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.nfinitydynamics.activity.CandidateSignUp.contryCode;
import static com.nfinitydynamics.activity.CandidateSignUp.contryM2Code;
import static com.nfinitydynamics.activity.CandidateSignUp.contryMCode;
import static com.nfinitydynamics.activity.CandidateSignUp.dob;
import static com.nfinitydynamics.activity.CandidateSignUp.email;
import static com.nfinitydynamics.activity.CandidateSignUp.email2;
import static com.nfinitydynamics.activity.CandidateSignUp.firstName;
import static com.nfinitydynamics.activity.CandidateSignUp.indusNo;
import static com.nfinitydynamics.activity.CandidateSignUp.lastName;
import static com.nfinitydynamics.activity.CandidateSignUp.middleName;
import static com.nfinitydynamics.activity.CandidateSignUp.mobileNo1;
import static com.nfinitydynamics.activity.CandidateSignUp.mobileNo2;
import static com.nfinitydynamics.activity.CandidateSignUp.nationality;
import static com.nfinitydynamics.activity.CandidateSignUp.password;
import static com.nfinitydynamics.activity.CandidateSignUp.profImagePath;
import static com.nfinitydynamics.activity.CandidateSignUp.telCode;
import static com.nfinitydynamics.activity.CandidateSignUp.telephoneNo;
import static com.nfinitydynamics.utils.App.availableFrom;
import static com.nfinitydynamics.utils.App.city;
import static com.nfinitydynamics.utils.App.countryResidence;
import static com.nfinitydynamics.utils.App.gender;
import static com.nfinitydynamics.utils.App.joiningTypeValue;
import static com.nfinitydynamics.utils.App.maritalValue;
import static com.nfinitydynamics.utils.App.multiSelecteRankId;
import static com.nfinitydynamics.utils.App.multiSelecteShipTypeId;
import static com.nfinitydynamics.utils.App.zipCode;


public class CandidateSignUp3 extends AppCompatActivity implements AsynchTaskListner {
    public Toolbar toolbar;
    public Button btnRegister;
    public LinearLayout linearLogin;
    public CandidateSignUp3 instance;
    public EditText edtAddress, edtZipCode, edtCity;
    public Spinner spCountryResidence;
    public CheckBox agree;
    public ArrayList<CountryResidenceModel> list = new ArrayList<>();
    public ArrayList<String> listString = new ArrayList<>();
    public JsonParserUniversal jParsar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_candidate_sign_up3);
        instance = this;
        jParsar = new JsonParserUniversal();
        App.pageState = "2";
        Log.d("Applied Rank:", App.multiSelecteRankId);
        agree = findViewById(R.id.check_box);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setTitle("Candidate Sign-up");
        }
        btnRegister = findViewById(R.id.btn_register);
        linearLogin = findViewById(R.id.linear_register);
        edtAddress = findViewById(R.id.edt_address);
        edtCity = findViewById(R.id.edt_city);
        edtZipCode = findViewById(R.id.edt_zipcode);
        spCountryResidence = findViewById(R.id.spinner_country);
        App.address = edtAddress.getText().toString().trim();
        city = edtCity.getText().toString().trim();
        zipCode = edtZipCode.getText().toString().trim();
        edtAddress.setText(App.address);
        edtZipCode.setText(zipCode);
        edtCity.setText(city);
        getCountryOfResidence();
        spCountryResidence.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                try {
                    countryResidence = list.get(i - 1).getCountryID();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }

        });
        agree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                agree.setChecked(false);
                openTermsAndCondition();
            }
        });


        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                doCandidateSignUp();

            }
        });
    }

    private void getCountryOfResidence() {
        new CallRequest(instance).getCountryList();

    }

    private void openTermsAndCondition() {
        final Dialog dialog = new Dialog(instance);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_terms_condition);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        WebView webView = dialog.findViewById(R.id.webview);
        final Button btnOk = dialog.findViewById(R.id.btn_ok);
        final Button btnCancel = dialog.findViewById(R.id.btn_cancel);
        WebSettings settings = webView.getSettings();
//       settings.setMinimumFontSize(20);
        settings.setDefaultFontSize(30);
        settings.setAllowFileAccess(true);
        settings.setAllowContentAccess(true);
        //settings.setAllowFileAccessFromFileURLs(true);
        settings.setLoadWithOverviewMode(true);
        settings.setUseWideViewPort(true);
        settings.setLoadsImagesAutomatically(true);
        webView.setInitialScale(1);
        webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        webView.setScrollbarFadingEnabled(false);
        webView.loadUrl("file:///android_asset/infinitytnc.html");
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                btnOk.setVisibility(View.VISIBLE);
                btnCancel.setVisibility(View.VISIBLE);
            }
        }, 2000);

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                agree.setChecked(true);
                dialog.dismiss();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(CandidateSignUp3.this, LoginActivity.class);
                Bundle bundle = new Bundle();
                bundle.putInt("value", 0);
                i.putExtras(bundle);
                startActivity(i);
                finish();

            }
        });

        dialog.show();
    }


    private void doCandidateSignUp() {
        App.address = edtAddress.getText().toString().trim();
        city = edtCity.getText().toString().trim();
        zipCode = edtZipCode.getText().toString().trim();
        if (TextUtils.isEmpty(App.address)) {
            edtAddress.findFocus();
            Utils.showAlert("Please Enter Address", this);
        } else if (TextUtils.isEmpty(city)) {
            edtCity.findFocus();
            Utils.showAlert("Please Enter City", this);
        } else if (TextUtils.isEmpty(zipCode)) {
            edtZipCode.findFocus();
            Utils.showAlert("Please Enter ZipCode", this);
        } else if (spCountryResidence.getSelectedItemPosition() == 0/*TextUtils.isEmpty(countryResidence)*/) {
            //   spCountryResidence.findFocus();
            Utils.showAlert("Please Select Country of Residence", this);
        }/*else if (TextUtils.isEmpty(countryResidence)) {
            spCountryResidence.findFocus();
            Utils.showAlert("Please Select Country of Residence", this);
        }*/ else {
            if (agree.isChecked()) {
                new CallRequest(instance).registerCandidate(firstName, middleName, lastName, email, email2, dob, password, maritalValue,
                        App.childeren, App.address, city, zipCode, nationality, contryCode, telCode, telephoneNo, contryMCode, mobileNo1,
                        contryM2Code, mobileNo2, indusNo, App.rankvalue, App.weight.toString(), App.height.toString(), App.bmi.toString(), App.multiSelecteRankId, joiningTypeValue, availableFrom, profImagePath,
                        gender, multiSelecteShipTypeId, countryResidence);
            } else {
                //Please read terms and conditions carefully then click “Accept” to be able to register
                Utils.showAlert("Terms & Conditions must be read and accepted", this);
            }
        }
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case register:

                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            Utils.showToast("Registered Successfully", this);
                            Intent i = new Intent(CandidateSignUp3.this, LoginActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            Bundle bundle = new Bundle();
                            bundle.putInt("value", 0);
                            i.putExtras(bundle);
                            startActivity(i);
                            finish();

                        } else {
                            Utils.showToast(jObj.getString("message"), this);

                            if (jObj.getString("message").equalsIgnoreCase("Email is already used by another user. Please choose another email.")) {
                                openDialogForEnterEmail();
                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Utils.showAlert("Please Try Again", this);
                    }
                    break;
                case get_country_list:
                    Utils.hideProgressDialog();

                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {

                            JSONArray ob1 = jObj.getJSONArray("data");
                            for (int i = 0; i < ob1.length(); i++) {
                                JSONObject ob = ob1.getJSONObject(i);
                                CountryResidenceModel countryResidenceModel = new CountryResidenceModel();
                                countryResidenceModel = (CountryResidenceModel) jParsar.parseJson(ob, new CountryResidenceModel());
                                list.add(countryResidenceModel);

                                listString.add(countryResidenceModel.getCountryName());
                            }
                            ArrayAdapter aa = new ArrayAdapter(this, R.layout.spinner_text, listString);
                            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spCountryResidence.setAdapter(new NothingSelectedSpinnerAdapter(
                                    aa, R.layout.country_recidency,
                                    getApplicationContext()));
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }

    private void openDialogForEnterEmail() {
        final Dialog dialog = new Dialog(instance);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_email);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        final Button btnOk = dialog.findViewById(R.id.btnOk);
        final EditText edtEmail = dialog.findViewById(R.id.edtEmail);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String newEmail = edtEmail.getText().toString();
                if (TextUtils.isEmpty(newEmail)) {
                    edtEmail.findFocus();
                    Utils.showAlert("Please Enter Email", instance);
                } else if (email2.equalsIgnoreCase(newEmail)) {
                    Utils.showAlert("Both Email ids are same", instance);
                } else {
                    email = newEmail;
                    new CallRequest(instance).registerCandidate(firstName, middleName, lastName, email, email2, dob, password, maritalValue,
                            App.childeren, App.address, city, zipCode, nationality, contryCode, telCode, telephoneNo, contryMCode, mobileNo1,
                            contryM2Code, mobileNo2, indusNo, App.rankvalue, App.weight.toString(), App.height.toString(), App.bmi.toString(), multiSelecteRankId, joiningTypeValue, availableFrom, profImagePath,
                            gender, multiSelecteShipTypeId, countryResidence);
                    dialog.dismiss();

                }

            }
        });

        dialog.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.candidate, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                // app icon in action bar clicked; goto parent activity.
                onBackPressed();
                return true;
            case R.id.login:
                Intent i = new Intent(CandidateSignUp3.this, LoginActivity.class);
                Bundle bundle = new Bundle();
                bundle.putInt("value", 0);
                i.putExtras(bundle);
                startActivity(i);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
