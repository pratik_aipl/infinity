package com.nfinitydynamics.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import com.nfinitydynamics.R;
import com.nfinitydynamics.adapter.NothingSelectedSpinnerAdapter;
import com.nfinitydynamics.model.Nationality;
import com.nfinitydynamics.utils.App;
import com.nfinitydynamics.utils.AsynchTaskListner;
import com.nfinitydynamics.utils.CallRequest;
import com.nfinitydynamics.utils.Constant;
import com.nfinitydynamics.utils.CountryCodePicker;
import com.nfinitydynamics.utils.LatoTextView;
import com.nfinitydynamics.utils.Utils;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class CandidateSignUp extends AppCompatActivity implements AsynchTaskListner {

    public Toolbar toolbar;
    public RelativeLayout btnNext;
    public Uri selectedUri;
    public static String profImagePath;
    public LatoTextView txtEditImage, edt_telephonecode;
    public ImageView ivProfile;
    public String selectedType;
    public CandidateSignUp instance;
    public static Bitmap bm;
    public CropImage.ActivityResult result;
    public static String firstName, middleName, lastName, email, email2, password, confirmPassword, nationality = "",
            indusNo, telephoneNo, mobileNo1, mobileNo2, dob, contryCode = "", telCode, contryMCode = "", contryM2Code = "";
    public EditText edtFirstName, edtMiddleName, edtLastName, edtEmail, edtEmail2, edtPassword, edtMobileNoCode, edtMobileNoCode2,
            edtConfirmPassword, edtIndusNo, edtTelephoneNo, edtMobileNo, edtMobileNo2, edtDob, edtTelephoneNoCode;
    public CountryCodePicker ccpContryCode, ccpmContryCode, ccpm2ContryCode;
    public static Spinner spinnerNationality;
    public static ArrayList<String> nationalityStringList = new ArrayList<>();
    public static int mYear;
    public static int mMonth;
    public static int mDay;
    ArrayList<Nationality> natinalityList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_candidate_sign_up);
        instance = this;
        contryCode = "";
        contryMCode = "";
        contryM2Code = "";
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        btnNext = findViewById(R.id.relative_next);
        txtEditImage = findViewById(R.id.txt_edit_pic);
        ivProfile = findViewById(R.id.iv_candidate);
        edtFirstName = findViewById(R.id.edt_firstname);
        edtMiddleName = findViewById(R.id.edt_middlename);
        edtLastName = findViewById(R.id.edt_lastname);
        edtEmail = findViewById(R.id.edt_email);
        edtEmail2 = findViewById(R.id.edt_email2);
        edtPassword = findViewById(R.id.edt_password);
        edtConfirmPassword = findViewById(R.id.edt_confirm_password);
        edtIndusNo = findViewById(R.id.edt_indusno);
        edtTelephoneNo = findViewById(R.id.edt_telephone_no);
        edtMobileNo = findViewById(R.id.edt_mobile_no);
        edtMobileNo2 = findViewById(R.id.edt_mobile_no2);
        edtDob = findViewById(R.id.edt_dob);
        ccpContryCode = findViewById(R.id.ccpContryCode);
        ccpmContryCode = findViewById(R.id.ccpmContryCode);
        ccpm2ContryCode = findViewById(R.id.ccpm2ContryCode);
        edtTelephoneNoCode = findViewById(R.id.edt_telephone_no_code);
        spinnerNationality = findViewById(R.id.spinner_country);

        App.isback = false;
        App.weight = "";
        App.height = "";
        ccpContryCode.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected() {

                contryCode = ccpContryCode.getSelectedCountryCode();
            }
        });
        ccpmContryCode.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected() {

                contryMCode = ccpmContryCode.getSelectedCountryCode();
            }
        });

        ccpm2ContryCode.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected() {

                contryM2Code = ccpm2ContryCode.getSelectedCountryCode();
            }
        });


        edtDob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                //  Utils.generateDatePicker(instance, edtDob);
                final Calendar c = Calendar.getInstance();
                c.add(Calendar.YEAR, -18);
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog dpd;
                dpd = new DatePickerDialog(instance,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                Calendar userAge = new GregorianCalendar(year, monthOfYear, dayOfMonth);
                                Calendar minAdultAge = new GregorianCalendar();
                                minAdultAge.add(Calendar.YEAR, -18);
                                if (minAdultAge.before(userAge)) {
                                    Utils.showAlert("Select date above 18 year", CandidateSignUp.this);
                                } else {
                                    edtDob.setText(Utils.getDateFormat("d", "dd", String.valueOf(dayOfMonth)) + "-" + Utils.getDateFormat("M", "MMM", String.valueOf((monthOfYear + 1))) + "-" + year);
                                }
                            }
                        }, mYear, mMonth, mDay);
                dpd.show();


            }
        });
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setTitle("SEAFARERS REGISTRATION");
        }
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                doCandidateSignup();
            }
        });
        txtEditImage.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("NewApi")
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                if (CropImage.isExplicitCameraPermissionRequired(getApplicationContext())) {
                    requestPermissions(
                            new String[]{Manifest.permission.CAMERA},
                            CropImage.CAMERA_CAPTURE_PERMISSIONS_REQUEST_CODE);
                } else {
                    CropImage.startPickImageActivity(CandidateSignUp.this);
                }
            }
        });
        getNationality();

        spinnerNationality.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                try {
                    nationality = natinalityList.get(i - 1).getNationalityId();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }

        });
    }

    private void getNationality() {

        new CallRequest(instance).getNationality();
    }

    public void converDateToYYMMddTextView(String dateDOB) {
        try {
            DateFormat inputFormat = new SimpleDateFormat("dd-MMM-yyyy");
            DateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date date = null;
            try {
                date = inputFormat.parse(dateDOB);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            String ddMMYYDate = outputFormat.format(date);
            System.out.println("converted date====" + ddMMYYDate);
            dob = ddMMYYDate;
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    private void doCandidateSignup() {
        bm = ((BitmapDrawable) ivProfile.getDrawable()).getBitmap();
        firstName = edtFirstName.getText().toString().trim();
        lastName = edtLastName.getText().toString().trim();
        middleName = edtMiddleName.getText().toString().trim();
        email = edtEmail.getText().toString().trim();
        email2 = edtEmail2.getText().toString().trim();
        password = edtPassword.getText().toString().trim();
        confirmPassword = edtConfirmPassword.getText().toString().trim();
        indusNo = edtIndusNo.getText().toString().trim();
        telephoneNo = edtTelephoneNo.getText().toString();
        mobileNo1 = edtMobileNo.getText().toString().trim();
        mobileNo2 = edtMobileNo2.getText().toString().trim();
        dob = edtDob.getText().toString().trim();
        telCode = edtTelephoneNoCode.getText().toString().trim();
        Utils.isValidEmail(email2);
        converDateToYYMMddTextView(dob);
        if (firstName.equals("")) {
            edtFirstName.findFocus();
            Utils.showAlert("Please Enter First Name", this);
        } else if (lastName.equals("")) {
            edtLastName.findFocus();
            Utils.showAlert("Please Enter Last Name", this);
        } else if (lastName.equalsIgnoreCase(firstName)) {
            Utils.showAlert("Please Enter Different Name", this);
        } else if (TextUtils.isEmpty(email)) {
            edtEmail.findFocus();
            Utils.showAlert("Please Enter Email", this);
        } else if (Utils.emailValidation(email).equals("false")) {
            Utils.showAlert("Please Enter Valid Email", this);
        } else if (email2.equals(email)) {
            Utils.showAlert("Both Email ids are same", this);
        } else if (TextUtils.isEmpty(password)) {
            edtPassword.requestFocus();
            Utils.showAlert("Please Enter Password", this);
        } else if (edtPassword.getText().toString().length() < 6) {
            Utils.showAlert("Please enter the password of atlest 6 characters", instance);
        } else if (TextUtils.isEmpty(confirmPassword)) {
            edtConfirmPassword.findFocus();
            Utils.showAlert("Please Enter Confirm Password", this);
        } else if (!confirmPassword.equals(password)) {
            Utils.showAlert("Password Does Not Match", this);
        } else if (TextUtils.isEmpty(dob)) {
            edtDob.findFocus();
            Utils.showAlert("Please Enter Date Of Birth", this);
        } else if (spinnerNationality.getSelectedItemPosition() == 0) {
            Utils.showAlert("Please Select Nationality", this);
        } else if (mobileNo1.equalsIgnoreCase("") && !telephoneNo.equalsIgnoreCase("")) {
            if (telCode.equalsIgnoreCase("")) {
                Utils.showAlert("Please Select Telephone STD Code", this);
            } else if (contryCode.equalsIgnoreCase("")) {
                Utils.showAlert("Please Select Telephone ISD Code", this);
            } else if (mobileNo1.equalsIgnoreCase("") && telephoneNo.equalsIgnoreCase("") && mobileNo2.equalsIgnoreCase("")
                    && !contryCode.equalsIgnoreCase("") && !telCode.equalsIgnoreCase("")) {
                Utils.showAlert("Please Select Telephone No", this);
            } else if (!mobileNo1.equalsIgnoreCase("") && !telephoneNo.equalsIgnoreCase("") && contryMCode.equalsIgnoreCase("")) {
                Utils.showToast("Please Select Country Code", getApplicationContext());
            } else if (!mobileNo1.equalsIgnoreCase("") && contryMCode.equalsIgnoreCase("")) {
                Utils.showToast("Please Select Country Code", getApplicationContext());
            } else if (!mobileNo2.equalsIgnoreCase("") && contryM2Code.equalsIgnoreCase("")) {
                Utils.showToast("Please Select Country Code 2", getApplicationContext());
            } else if (!telephoneNo.equalsIgnoreCase("") && telCode.equalsIgnoreCase("")) {
                Utils.showAlert("Please Select Telephone STD Code", getApplicationContext());
            } else if (!telephoneNo.equalsIgnoreCase("") && contryCode.equalsIgnoreCase("")) {
                Utils.showAlert("Please Select Country Code", getApplicationContext());
            } else if (!contryMCode.equalsIgnoreCase("") && TextUtils.isEmpty(mobileNo1)) {
                Utils.showAlert("Please enter mobile number", this);
            } else if (contryMCode.equalsIgnoreCase("") && !TextUtils.isEmpty(mobileNo1)) {
                Utils.showToast("Please Select Country Code", getApplicationContext());
            }  else if (contryMCode.equalsIgnoreCase("91") && !mobileNo1.equalsIgnoreCase("") && mobileNo1.length() != 10) {
                Utils.showToast("Mobile No should be 10 digits for India.", getApplicationContext());
            } else if (contryM2Code.equalsIgnoreCase("91") && !mobileNo2.equalsIgnoreCase("") && mobileNo2.length() != 10) {
                Utils.showToast("Mobile No2 should be 10 digits for India.", getApplicationContext());
            }else {
                new CallRequest(CandidateSignUp.this).checkEmail(edtEmail.getText().toString());

            }
        } else if (mobileNo1.equalsIgnoreCase("") && telephoneNo.equalsIgnoreCase("")) {
            Utils.showAlert("Please Select Telephone No or Mobile No", this);
        } else if (!telephoneNo.equalsIgnoreCase("") && !contryCode.equalsIgnoreCase("") && telCode.equalsIgnoreCase("")) {
            Utils.showToast("Please Select Telephone STD Code", getApplicationContext());
        } else if (!telephoneNo.equalsIgnoreCase("") && contryCode.equalsIgnoreCase("") && !telCode.equalsIgnoreCase("")) {
            Utils.showToast("Please Select Country Code", getApplicationContext());
        } else if (telephoneNo.equalsIgnoreCase("") && !contryCode.equalsIgnoreCase("") && !telCode.equalsIgnoreCase("")) {
            Utils.showToast("Please Select Telephone No", getApplicationContext());
        } else if (!contryMCode.equalsIgnoreCase("") && TextUtils.isEmpty(mobileNo1) && !telephoneNo.equalsIgnoreCase("") && !contryCode.equalsIgnoreCase("")) {
            Utils.showAlert("Please enter mobile number", this);
        } else if (!contryMCode.equalsIgnoreCase("") && TextUtils.isEmpty(mobileNo1)) {
            Utils.showAlert("Please enter mobile number", this);
        } else if (!TextUtils.isEmpty(mobileNo1) && contryMCode.equalsIgnoreCase("")) {
            Utils.showToast("Please Select Country Code", getApplicationContext());
        } else if (contryMCode.equalsIgnoreCase("91") && !mobileNo1.equalsIgnoreCase("") && mobileNo1.length()!= 10) {
            Utils.showToast("Mobile No should be 10 digits for India.", this);
        } else if (!mobileNo1.equalsIgnoreCase("") && !contryMCode.equalsIgnoreCase("") && !telephoneNo.equalsIgnoreCase("")) {

            if (telCode.equalsIgnoreCase("")) {
                Utils.showAlert("Please Select Telephone STD Code", this);
            } else if (contryCode.equalsIgnoreCase("")) {
                Utils.showAlert("Please Select Telephone ISD Code", this);
            } else if (mobileNo1.equalsIgnoreCase("") && telephoneNo.equalsIgnoreCase("") && mobileNo2.equalsIgnoreCase("")) {
                Utils.showAlert("Please Select Telephone No or Mobile No", this);
            } else if (!contryCode.equalsIgnoreCase("") && !telCode.equalsIgnoreCase("") && telephoneNo.equalsIgnoreCase("")) {
                Utils.showAlert("Please Select Telephone No", this);
            } else if (!mobileNo1.equalsIgnoreCase("") && contryMCode.equalsIgnoreCase("")) {
                Utils.showToast("Please Select Country Code", getApplicationContext());
            } else if (!mobileNo2.equalsIgnoreCase("") && contryM2Code.equalsIgnoreCase("")) {
                Utils.showToast("Please Select Country Code 2", getApplicationContext());
            } else if (!mobileNo2.equalsIgnoreCase("") && contryM2Code.equalsIgnoreCase("")) {
                Utils.showToast("Please Select Country Code 2", getApplicationContext());
            } else if (contryM2Code.equalsIgnoreCase("91") && !mobileNo2.equalsIgnoreCase("") && mobileNo2.length() != 10) {
                Utils.showAlert("Mobile No should be 10 digits for India.", this);
            } else if (mobileNo2.equalsIgnoreCase(mobileNo1)) {
                Utils.showToast("Both Mobile Numbers are same", getApplicationContext());
            }else {
                new CallRequest(CandidateSignUp.this).checkEmail(edtEmail.getText().toString());

            }
        } else if (!mobileNo2.equalsIgnoreCase("") && contryM2Code.equalsIgnoreCase("")) {
            Utils.showToast("Please Select Country Code 2", getApplicationContext());
        } else if (contryM2Code.equalsIgnoreCase("91") && !mobileNo2.equalsIgnoreCase("") && mobileNo2.length() != 10) {
            Utils.showAlert("Mobile No2 should be 10 digits for India.", this);
        } else if (mobileNo2.equalsIgnoreCase(mobileNo1)) {
            Utils.showToast("Both Mobile Numbers are same", getApplicationContext());
        } else {
            new CallRequest(CandidateSignUp.this).checkEmail(edtEmail.getText().toString());
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.candidate, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.login:
                Intent i = new Intent(CandidateSignUp.this, LoginActivity.class);
                Bundle bundle = new Bundle();
                bundle.putInt("value", 0);
                i.putExtras(bundle);
                startActivity(i);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        App.pageState = "0";
        App.childeren = "";
        App.rankvalue = "";
        App.indexListShip.clear();
        App.indexListRank.clear();
        App.availableFrom = "";
        App.availableFromTemp = "";
        App.maritalValue = "";
        App.children = "";
        App.joiningTypeValue = "";
        App.multiSelecteRankId = "";
        App.gender = "";
        App.multiSelecteShipTypeId = "";
        App.rankvalue = "";
    }

    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setMinCropResultSize(500, 500)
                .setAspectRatio(1, 1)
                .setFixAspectRatio(true)
                .setMultiTouchEnabled(true)
                .start(instance);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(this, data);
            if (CropImage.isReadExternalStoragePermissionsRequired(this, imageUri)) {
                selectedUri = imageUri;
                profImagePath = selectedUri.getPath().toString();
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
            } else {
                startCropImageActivity(imageUri);
            }
        }

        // handle result of CropImageActivity
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                ivProfile.setImageURI(result.getUri());
                selectedUri = result.getUri();
                profImagePath = selectedUri.getPath().toString();
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {

            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        //Checking the request code of our request
        if (requestCode == 222) {

            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (selectedType.equalsIgnoreCase("Take Photo")) {

                    File f = new File(Environment.getExternalStorageDirectory() + "/InfinityDynamics/images");
                    if (!f.exists()) {
                        f.mkdirs();
                    }
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    File file = new File(Environment.getExternalStorageDirectory(), "InfinityDynamics/images/img_" + System.currentTimeMillis() + ".jpg");
                    selectedUri = Uri.fromFile(file);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, selectedUri);
                    startActivityForResult(intent, 222);
                }

            } else {
                //Utils.showToast("Permission not granted", this);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (App.pageState == "1") {
            edtFirstName.setText(firstName);
            edtMiddleName.setText(middleName);
            edtLastName.setText(lastName);
            edtEmail.setText(email);
            edtEmail2.setText(email2);
            edtDob.setText(dob);
            edtPassword.setText(password);
            edtConfirmPassword.setText(confirmPassword);
            edtIndusNo.setText(indusNo);
            edtTelephoneNo.setText(telephoneNo);
            edtMobileNo.setText(mobileNo1);
            edtMobileNo2.setText(mobileNo2);
        }
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case getNationality:
                    Utils.hideProgressDialog();

                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {

                            JSONArray ob1 = jObj.getJSONArray("data");
                            for (int i = 0; i < ob1.length(); i++) {
                                JSONObject ob = ob1.getJSONObject(i);
                                String nationalityId = ob.getString("NationalID");
                                String nationalityName = ob.getString("NationalName");
                                natinalityList.add(new Nationality(nationalityId, nationalityName));
                                nationalityStringList.add(nationalityName);
                            }
                            ArrayAdapter aa = new ArrayAdapter(this, R.layout.spinner_text, nationalityStringList);
                            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spinnerNationality.setAdapter(new NothingSelectedSpinnerAdapter(
                                    aa, R.layout.nationality_spinner_row_nothing_selected,
                                    getApplicationContext()));
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;

                case checkEmail:

                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            Intent i = new Intent(getApplicationContext(), CandidateSignUp2.class);
                            startActivity(i);

                        } else {
                            Utils.showToast(jObj.getString("message"), this);

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Utils.showAlert("Please Try Again", this);
                    }
                    break;
            }
        }
    }

}
