package com.nfinitydynamics.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.nfinitydynamics.R;
import com.nfinitydynamics.adapter.NothingSelectedSpinnerAdapter;
import com.nfinitydynamics.model.CountryResidenceModel;
import com.nfinitydynamics.model.PlanModel;
import com.nfinitydynamics.utils.App;
import com.nfinitydynamics.utils.AsynchTaskListner;
import com.nfinitydynamics.utils.CallRequest;
import com.nfinitydynamics.utils.Constant;
import com.nfinitydynamics.utils.CountryCodePicker;
import com.nfinitydynamics.utils.JsonParserUniversal;
import com.nfinitydynamics.utils.LatoTextView;
import com.nfinitydynamics.utils.Utils;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import static com.nfinitydynamics.utils.App.city;
import static com.nfinitydynamics.utils.App.countryResidence;
import static com.nfinitydynamics.utils.App.zipCode;

public class InstituteSignUp extends AppCompatActivity implements AsynchTaskListner {
    public Toolbar toolbar;
    public Button btnRegister;
    public LinearLayout linearLogin;
    public EditText edtCompanyName, edt_contact_person, edtEmail, edtPassword, edtConfirmPassword, edtWebSite,
            edtMobileNo, edtTelNo, edtAddress, edtTelNoCode, edtRpslno, edtValidUntil, edtZipCode, edtCity;
    ;
    public InstituteSignUp instance;
    public Uri selectedUri;
    public String profImagePath;
    public LatoTextView txtEditImage;
    public ImageView ivProfile;
    public CheckBox agree;
    public CropImage.ActivityResult result;
    public Spinner spinnerPlan, spCountryResidence;
    public String selectedType, compName, contact_person, compEmail, compPassword, compConfirmPassword, compWebSite, compTeleNo, compTeleNoCode, compMobileNo, compAddress, comProfile, compContryCode = "",
            compMContryCode = "", compPlane, rsplNo, validUntil;
    public CountryCodePicker ccpContryCode, ccpmContryCode;
    public JsonParserUniversal jParsar;
     public ArrayList<CountryResidenceModel> list1 = new ArrayList<>();
    public ArrayList<String> listString = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_institute_sign_up);
        instance = this;
        jParsar = new JsonParserUniversal();
        toolbar = findViewById(R.id.toolbar);
        btnRegister = findViewById(R.id.btn_register);
        linearLogin = findViewById(R.id.linear_ligin);
        txtEditImage = findViewById(R.id.txt_edit_pic);
        ivProfile = findViewById(R.id.iv_company);
        edtCompanyName = findViewById(R.id.edt_company_name);
        edt_contact_person = findViewById(R.id.edt_contact_person);
        edtEmail = findViewById(R.id.edt_email);
        edtPassword = findViewById(R.id.edt_password);
        edtConfirmPassword = findViewById(R.id.edt_confirm_password);
        edtWebSite = findViewById(R.id.edt_website);
        edtMobileNo = findViewById(R.id.edt_mobile_no);
        edtAddress = findViewById(R.id.edt_address);
        edtTelNo = findViewById(R.id.edt_tel_no);
        agree = findViewById(R.id.check_box);
        ccpContryCode = findViewById(R.id.ccpContryCode);
        ccpmContryCode = findViewById(R.id.ccpMContryCode);
        edtTelNoCode = findViewById(R.id.edt_tel_no_code);
        spinnerPlan = findViewById(R.id.spinner_plan);
        edtRpslno = findViewById(R.id.edt_rpslno);
        edtValidUntil = findViewById(R.id.edt_valid_until);
        edtZipCode = findViewById(R.id.edtZipCode);
        edtCity = findViewById(R.id.edtCity);
        spCountryResidence = findViewById(R.id.spinner_country);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setTitle("TRAINING INSTITUTE REGISTRATION");
        }
        ccpContryCode.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected() {
                compContryCode = ccpContryCode.getSelectedCountryCode();
            }
        });
        ccpmContryCode.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected() {
                compMContryCode = ccpmContryCode.getSelectedCountryCode();
                System.out.println("mobile country code-=" + compMContryCode);
                //edtMoNoCode.setText(compMContryCode);
            }
        });
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent i = new Intent(instance, MainActivity.class);
//                startActivity(i);
                doCompanySignIn();
            }
        });


        linearLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(instance, LoginActivity.class);
                startActivity(i);
            }
        });
        txtEditImage.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("NewApi")
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                if (CropImage.isExplicitCameraPermissionRequired(getApplicationContext())) {
                    requestPermissions(
                            new String[]{Manifest.permission.CAMERA},
                            CropImage.CAMERA_CAPTURE_PERMISSIONS_REQUEST_CODE);
                } else {
                    CropImage.startPickImageActivity(InstituteSignUp.this);
                }
            }
        });

        agree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              //  agree.setChecked(false);
            //  openTermsAndCondition();
            }
        });

        getCountryOfResidence();
        spCountryResidence.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                try {
                    countryResidence = list1.get(i - 1).getCountryID();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }

        });
    }



    private void getCountryOfResidence() {
        new CallRequest(instance).getCountryList();

    }

    private void doCompanySignIn() {
        Calendar cal = Calendar.getInstance();
        Date sysDate = cal.getTime();
        Date intValidity = null;

        compName = edtCompanyName.getText().toString().trim();
        contact_person = edt_contact_person.getText().toString().trim();
        compEmail = edtEmail.getText().toString().trim();
        compMobileNo = edtMobileNo.getText().toString().trim();
        compWebSite = edtWebSite.getText().toString().trim();
        compAddress = edtAddress.getText().toString().trim();
        compTeleNo = edtTelNo.getText().toString().trim();
        compTeleNoCode = edtTelNoCode.getText().toString();
         city = edtCity.getText().toString().trim();
        zipCode = edtZipCode.getText().toString().trim();

        //String profileImage= String.valueOf(ivProfileComp);
       /* if (TextUtils.isEmpty(profImagePath)) {
            ivProfile.findFocus();
            Utils.showAlert("Please Select Institute Logo", this);
        } else*/ if (TextUtils.isEmpty(compName)) {
            edtCompanyName.findFocus();
            Utils.showAlert("Please Enter Institute Name", this);
        } else if (TextUtils.isEmpty(contact_person)) {
            edt_contact_person.findFocus();
            Utils.showAlert("Please Enter Contact Person", this);
        } else if (TextUtils.isEmpty(compEmail)) {
            edtEmail.findFocus();
            Utils.showAlert("Please Enter Email", this);
        } else if (Utils.emailValidation(compEmail).equals("false")) {
            Utils.showAlert("Please enter valid email", this);
        } else if (TextUtils.isEmpty(compWebSite)) {
            edtWebSite.findFocus();
            Utils.showAlert("Please Enter Website", this);
        } else if (TextUtils.isEmpty(compTeleNo)) {
            Utils.showAlert("Please Select Telephone No", this);
        } else if (!TextUtils.isEmpty(compTeleNo) && TextUtils.isEmpty(compContryCode)) {
            Utils.showAlert("Please Select Telephone Country Code", this);
        } else if (!TextUtils.isEmpty(compTeleNo) && TextUtils.isEmpty(compTeleNoCode)) {
            Utils.showAlert("Please Select Telephone STD Code", this);
        } else if (!compMobileNo.equalsIgnoreCase("") && compMContryCode.equalsIgnoreCase("")) {
            Utils.showToast("Please Select Country Code", getApplicationContext());
        }/*else if (TextUtils.isEmpty(compMobileNo)) {
            Utils.showAlert("Please enter mobile number", this);
        }*/ else if (compMContryCode.equalsIgnoreCase("91") && !compMobileNo.equalsIgnoreCase("") && compMobileNo.length()<10) {
            Utils.showAlert("Mobile No should be 10 digits for India.", this);
        }else if (TextUtils.isEmpty(compAddress)) {
            edtAddress.findFocus();
            Utils.showAlert("Please Enter Address", this);
        } else if (TextUtils.isEmpty(city)) {
            edtCity.findFocus();
            Utils.showAlert("Please Enter City", this);
        } else if (TextUtils.isEmpty(zipCode)) {
            edtZipCode.findFocus();
            Utils.showAlert("Please Enter ZipCode", this);
        } else if (spCountryResidence.getSelectedItemPosition()==0/*TextUtils.isEmpty(countryResidence)*/) {
         //   spCountryResidence.findFocus();
            Utils.showAlert("Please Select Country ", this);
        } else {
            if (agree.isChecked()) {
                if (!TextUtils.isEmpty(compMobileNo) && TextUtils.isEmpty(compMContryCode)) {
                    compMobileNo = "";
                    compMContryCode = "";
                }
                new CallRequest(instance).insituteRegister(compName, contact_person, compEmail, compMobileNo, compMContryCode, compContryCode, compTeleNo, compTeleNoCode, compWebSite, profImagePath, compAddress, city, zipCode, countryResidence);
            } else {
                Utils.showAlert("Terms & Conditions must be read and accepted", this);
            }
        }

    }

    private void openTermsAndCondition() {
        final Dialog dialog = new Dialog(instance);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_terms_condition);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        WebView webView = dialog.findViewById(R.id.webview);
        final Button btnOk = dialog.findViewById(R.id.btn_ok);
        final Button btnCancel = dialog.findViewById(R.id.btn_cancel);
        WebSettings settings = webView.getSettings();
//       settings.setMinimumFontSize(20);
        settings.setDefaultFontSize(30);
        settings.setAllowFileAccess(true);
        settings.setAllowContentAccess(true);
        //settings.setAllowFileAccessFromFileURLs(true);
        settings.setLoadWithOverviewMode(true);
        settings.setUseWideViewPort(true);
        settings.setLoadsImagesAutomatically(true);
        webView.setInitialScale(1);
        webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        webView.setScrollbarFadingEnabled(false);
        webView.loadUrl("file:///android_asset/infinitytnc_com.html");
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                btnOk.setVisibility(View.VISIBLE);
                btnCancel.setVisibility(View.VISIBLE);
            }
        }, 2000);

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent i = new Intent(InstituteSignUp.this, LoginActivity.class);
                Bundle bundle = new Bundle();
                bundle.putInt("value", 0);
                i.putExtras(bundle);
                startActivity(i);
                finish();

            }
        });
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                agree.setChecked(true);
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case insituteRegister:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            Utils.showToast("Registered Successfully", this);
                            Intent i = new Intent(InstituteSignUp.this, LoginActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putInt("value", 0);
                            i.putExtras(bundle);
                            startActivity(i);
                            finish();
                        } else {
                            Utils.showAlert(jObj.getString("message"), this);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        //  Utils.showToast(jObj.getString("message"), this);
                    }
                    break;
                case get_country_list:
                    Utils.hideProgressDialog();

                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {

                            JSONArray ob1 = jObj.getJSONArray("data");
                            for (int i = 0; i < ob1.length(); i++) {
                                JSONObject ob = ob1.getJSONObject(i);
                                CountryResidenceModel countryResidenceModel = new CountryResidenceModel();
                                countryResidenceModel = (CountryResidenceModel) jParsar.parseJson(ob, new CountryResidenceModel());
                                list1.add(countryResidenceModel);

                                listString.add(countryResidenceModel.getCountryName());
                            }
                            ArrayAdapter aa = new ArrayAdapter(this, R.layout.spinner_text, listString);
                            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spCountryResidence.setAdapter(new NothingSelectedSpinnerAdapter(
                                    aa, R.layout.country,
                                    getApplicationContext()));
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;

            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.candidate, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent i = new Intent(getApplicationContext(), FirstActivity.class);
                startActivity(i);
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            case R.id.login:
                Intent iLogin = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(iLogin);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setMinCropResultSize(500, 500)
                .setAspectRatio(5, 2)
                .setFixAspectRatio(true)
                .setMultiTouchEnabled(false)
                .start(this);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(this, data);
            if (CropImage.isReadExternalStoragePermissionsRequired(this, imageUri)) {
                selectedUri = imageUri;
                profImagePath = selectedUri.getPath().toString();
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
            } else {
                startCropImageActivity(imageUri);
            }
        }
        // handle result of CropImageActivity
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                //iv
                ivProfile.setImageURI(result.getUri());
                selectedUri = result.getUri();
                profImagePath = selectedUri.getPath().toString();

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {

            }
        }

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        //Checking the request code of our request
        if (requestCode == 222) {

            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (selectedType.equalsIgnoreCase("Take Photo")) {

                    File f = new File(Environment.getExternalStorageDirectory() + "/InfinityDynamics/images");
                    if (!f.exists()) {
                        f.mkdirs();
                    }
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    File file = new File(Environment.getExternalStorageDirectory(), "InfinityDynamics/images/img_" + System.currentTimeMillis() + ".jpg");
                    selectedUri = Uri.fromFile(file);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, selectedUri);
                    startActivityForResult(intent, 222);
                }

            } else {
                //Utils.showToast("Permission not granted", this);
            }
        }
    }

}

