package com.nfinitydynamics.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import com.nfinitydynamics.R;
import com.nfinitydynamics.adapter.NothingSelectedSpinnerAdapter;
import com.nfinitydynamics.model.AppliedShip;
import com.nfinitydynamics.model.RankModel;
import com.nfinitydynamics.utils.App;
import com.nfinitydynamics.utils.AsynchTaskListner;
import com.nfinitydynamics.utils.CallRequest;
import com.nfinitydynamics.utils.Constant;
import com.nfinitydynamics.utils.MultiSelectionSpinnerShipType;
import com.nfinitydynamics.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.nfinitydynamics.utils.App.availableFrom;
import static com.nfinitydynamics.utils.App.availableFromTemp;
import static com.nfinitydynamics.utils.App.gender;
import static com.nfinitydynamics.utils.App.joiningTypeValue;
import static com.nfinitydynamics.utils.App.maritalValue;
import static com.nfinitydynamics.utils.App.multiSelecteRankId;
import static com.nfinitydynamics.utils.App.multiSelecteShipTypeId;
import static com.nfinitydynamics.utils.Constant.FILTER_TYPE.APPLIEDRANK;
import static com.nfinitydynamics.utils.Constant.FILTER_TYPE.SHIPTYPE;

public class CandidateSignUp2 extends AppCompatActivity implements AsynchTaskListner, MultiSelectionSpinnerShipType.OnMultipleItemsSelectedListener, AdapterView.OnItemSelectedListener {
    public CandidateSignUp2 instance;
    public Toolbar toolbar;
    public RelativeLayout btnNext;
    public EditText edtChildren, edtAvailableFrom, edt_bmi, edt_weight, edt_height;

    public ArrayList<RankModel> rankList = new ArrayList<>();
    public ArrayList<AppliedShip> shipList = new ArrayList<>();
    public ArrayList<RankModel> appliedRankList = new ArrayList<>();
    public ArrayList<AppliedShip> appliedShipList = new ArrayList<>();
    public ArrayList<String> rankStringList = new ArrayList<>();
    public ArrayList<String> shipStringList = new ArrayList<>();
    public String[] genderArray = {"Male", "Female", "Decline to declare", "Others"};
    //  public String[] maritalStatus = {"Married", "Singal", "Divorced", "Widow", "Widower"};
    public String[] maritalStatus = {"Single", "Married", "Divorced", "Widow", "Widower"};
    public Spinner spRank, spMarital, spJoinType, spGender;
    public MultiSelectionSpinnerShipType multiSelectionSpinner, multiSelectionSpinnerShipType;
    public List<String> rankMultiArray = new ArrayList<String>();
    public List<String> shipMultiArray = new ArrayList<String>();
    public String[] joiningType = {"Available After", "Urgent Joining Before"};
    public ArrayAdapter aaJoinType;
    public ArrayAdapter aaRank;
    public static String type;
    public static List<Integer> indexListFacility = new ArrayList<>();
    public int selectedPos;
    int weight;
    int height;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_candidate_sign_up2);
        instance = this;
        toolbar = findViewById(R.id.toolbar);
        App.availableFromTemp = "";
        setSupportActionBar(toolbar);
        // getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setTitle("Candidate Sign-up");
        }
        btnNext = findViewById(R.id.relative_next);
        spRank = findViewById(R.id.spinner_job_rank);
        spMarital = findViewById(R.id.spinner_marital_status);
        edtChildren = findViewById(R.id.edt_children);
        edtAvailableFrom = findViewById(R.id.edt_available_from);
        edt_bmi = findViewById(R.id.edt_bmi);
        edt_weight = findViewById(R.id.edt_weight);
        edt_height = findViewById(R.id.edt_height);
        multiSelectionSpinner = findViewById(R.id.multiselectSpinner);
        multiSelectionSpinnerShipType = findViewById(R.id.multiselectSpinnerShipType);
        spJoinType = findViewById(R.id.spinner_joing_type);
        spGender = findViewById(R.id.spinner_gender);
        Calendar cal = Calendar.getInstance();
        Date sysDate = cal.getTime();
     /*   System.out.println("rank value===" + App.rankvalue);
        System.out.println("rank value===" + App.gender);
        System.out.println("rank value===" + App.joiningTypeValue);
        System.out.println("rank value===" + App.maritalValue);
        System.out.println("rank value===" + App.rankvalue);
   */
        getRank();
        getJoingType();
        getAppliedShipType();
        getAppliedRank();
        edt_weight.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    float height = Float.parseFloat(edt_height.getText().toString());
                    float weight = Float.parseFloat(edt_weight.getText().toString());
                    float bmi = ((weight * 10000) / (height * height));
                    int x = Math.round(bmi);

                    edt_bmi.setText(String.valueOf((x)));
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if (s.length() <= 2 && s.length() > 1) {
                    //  Utils.showToast("Weight should be minimum 2 digits",CandidateSignUp2.this);
                }

            }
        });

        edt_height.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    float height = Float.parseFloat(edt_height.getText().toString());
                    float weight = Float.parseFloat(edt_weight.getText().toString());
                    float bmi = ((weight * 10000) / (height * height));
                    int x = Math.round(bmi);

                    edt_bmi.setText(String.valueOf((x)));
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if (s.length() <= 3 && s.length() > 1) {
                    //     Utils.showToast("Height should be of miminum 3 digits",CandidateSignUp2.this);
                }

            }
        });
        System.out.println("availableFromTemp====" + availableFromTemp);
        try {
            if (!TextUtils.isEmpty(App.availableFromTemp)) {
                edtAvailableFrom.setText(App.availableFromTemp);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (!TextUtils.isEmpty(App.childeren)) {
            edtChildren.setText(App.childeren);
        }
        if (!TextUtils.isEmpty(App.weight)) {
            edt_weight.setText(App.weight);
        }
        if (!TextUtils.isEmpty(App.height)) {
            edt_height.setText(App.height);
        }
        if (!TextUtils.isEmpty(App.bmi)) {
            edt_bmi.setText(App.bmi);
        }
        //  if (App.rankvalue.equalsIgnoreCase("")) {
        //   multiSelectionSpinnerShipType.setClickable(false);
        //} else {
        multiSelectionSpinnerShipType.setClickable(true);
        // }
        edtAvailableFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                Utils.generateDatePicker(instance, edtAvailableFrom);
                App.availableFromTemp = edtAvailableFrom.getText().toString();

                System.out.println("availableFromTemp===" + App.availableFromTemp);
            }
        });
        spJoinType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    joiningTypeValue = joiningType[position - 1];
                    //    System.out.println("joining type===" + joiningTypeValue);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spRank.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                try {
                    if (rankList != null) {
                        App.rankvalue = rankList.get(i - 1).getRankId();
                        getAppliedRank();

                    }
                } catch (IndexOutOfBoundsException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        ArrayAdapter aaGender = new ArrayAdapter(this, R.layout.spinner_text, genderArray);
        aaGender.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spGender.setAdapter(new NothingSelectedSpinnerAdapter(
                aaGender, R.layout.gender_spinner_row_nothing_selected,
                getApplicationContext()));

        if (gender != null) {
            int spinnerPosition = aaGender.getPosition(gender);
            spGender.setSelection(spinnerPosition + 1);
        }
        spGender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    gender = genderArray[position - 1];
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ArrayAdapter aa1 = new ArrayAdapter(this, R.layout.spinner_text, maritalStatus);
        aa1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spMarital.setAdapter(new NothingSelectedSpinnerAdapter(
                aa1, R.layout.marital_spinner_row_nothing_selected,
                getApplicationContext()));

        if (maritalValue != null) {
            int spinnerPosition = aa1.getPosition(maritalValue);
            spMarital.setSelection(spinnerPosition + 1);
        }
        spMarital.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                try {
                    maritalValue = maritalStatus[i - 1];
                    App.maritualStatus = maritalValue;
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        multiSelectionSpinnerShipType.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                App.isClickedToAppliedRank = false;
                type = "shipType";
                return false;
            }
        });

        multiSelectionSpinner.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                App.isClickedToAppliedRank = true;
                type = "appliedRank";
                return false;
            }
        });

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                App.childeren = edtChildren.getText().toString().trim();
                App.availableFrom = edtAvailableFrom.getText().toString().trim();
                App.availableFromTemp = edtAvailableFrom.getText().toString().trim();
                App.weight = edt_weight.getText().toString().trim();
                App.height = edt_height.getText().toString().trim();
                App.bmi = edt_bmi.getText().toString().trim();
                Calendar cal = Calendar.getInstance();
                Date sysDate = cal.getTime();
                Date intValidity = null;
                try {
                    String value = edt_height.getText().toString();
                    String value1 = edt_weight.getText().toString();
                    height = Integer.parseInt(value);
                    weight = Integer.parseInt(value1);
                    // height= Integer.parseInt(edt_height.getText().toString());
                    Log.i("HEight", "==>" + height + "");
                } catch (Exception e) {

                }
                if (TextUtils.isEmpty(App.rankvalue)) {
                    spRank.findFocus();
                    Utils.showAlert("Please Select Current Rank", instance);
                } else if (TextUtils.isEmpty(gender)) {
                    spGender.findFocus();
                    Utils.showAlert("Please Select Gender", instance);
                } else if (edt_height.getText().toString().isEmpty()) {
                    edt_height.findFocus();
                    Utils.showAlert("Please Enter Height in CMS.", instance);
                } else if (edt_height.getText().toString().length() < 3) {
                    Utils.showToast("Height should be of miminum 3 digits", CandidateSignUp2.this);
                    edt_height.findFocus();
                    // Utils.showAlert("Please Enter Height in CMS.", instance);
                } else if (height < 99) {
                    Utils.showToast("Height should be of miminum 100 CMS", CandidateSignUp2.this);

                } else if (edt_weight.getText().toString().isEmpty()) {
                    edt_weight.findFocus();
                    Utils.showAlert("Please Enter Weight in KGS.", instance);
                } else if (edt_weight.getText().toString().length() < 2) {
                    Utils.showToast("Weight should be minimum 2 digits", CandidateSignUp2.this);
                    edt_weight.findFocus();
                    // Utils.showAlert("Please Enter Height in CMS.", instance);
                } else if (weight < 40) {
                    Utils.showToast("Weight should be of miminum 40 KGS", CandidateSignUp2.this);

                } else if (edt_bmi.getText().toString().isEmpty()) {
                    edt_bmi.findFocus();
                    Utils.showAlert("Please Enter BMI.", instance);
                } else if (TextUtils.isEmpty(maritalValue)) {
                    spMarital.findFocus();
                    Utils.showAlert("Please Select Marital Status", instance);
                } else if (TextUtils.isEmpty(joiningTypeValue)) {
                    Utils.showAlert("Please Select Joining Type", instance);
                } else if (!joiningTypeValue.equalsIgnoreCase("Not Available") && TextUtils.isEmpty(availableFrom)) {
                    Utils.showAlert("Please select availability date", instance);
                } else if (TextUtils.isEmpty(multiSelecteRankId)) {
                    Utils.showAlert("Please Select Applied Rank", instance);
                } else if (TextUtils.isEmpty(multiSelecteShipTypeId)) {
                    Utils.showAlert("Please Select Applied Ship Type ", instance);
                } else if (!TextUtils.isEmpty(App.availableFrom)) {
                    try {
                        intValidity = new SimpleDateFormat("dd-MMM-yyyy").parse(App.availableFrom);
                        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                        App.availableFrom = formatter.format(intValidity);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    if (sysDate.compareTo(intValidity) > 0) {
                        Utils.showAlert("Enter Valid Date in Available After", instance);
                    } else {

                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                        getCandidateSignup2();
                    }
                } else {

                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    getCandidateSignup2();
                }
            }
        });
    }

    private void getAppliedShipType() {
        new CallRequest(instance).getAppliedShipType();
    }

    private void getRank() {
        new CallRequest(instance).getRank();
    }

    private void getAppliedRank() {
        new CallRequest(instance).getAppliedRankActivity(App.rankvalue);
    }

    private void getJoingType() {
//        aaJoinType = new ArrayAdapter(getApplicationContext(), android.R.layout.simple_spinner_item, joiningType);
//        aaJoinType.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        spJoinType.setAdapter(aaJoinType);
        ArrayAdapter aaJoinType = new ArrayAdapter(this, R.layout.spinner_text, joiningType);
        aaJoinType.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spJoinType.setAdapter(new NothingSelectedSpinnerAdapter(
                aaJoinType, R.layout.joiningtype_spinner_row_nothing_selected,
                getApplicationContext()));
        if (joiningTypeValue != null) {
            int spinnerPosition = aaJoinType.getPosition(joiningTypeValue);
            spJoinType.setSelection(spinnerPosition + 1);
        }

    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case getRank:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            JSONArray ob1 = jObj.getJSONArray("data");
                            for (int i = 0; i < ob1.length(); i++) {
                                JSONObject ob = ob1.getJSONObject(i);
                                String rankId = ob.getString("RankID");
                                String rankName = ob.getString("Name");
                                rankList.add(new RankModel(rankId, rankName));
                                rankStringList.add(rankName);
                                if (rankId.equalsIgnoreCase(App.rankvalue)) {
                                    selectedPos = i + 1;
                                    getAppliedRank();
                                }
                            }
                            aaRank = new ArrayAdapter(this, R.layout.spinner_text, rankStringList);
                            aaRank.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spRank.setAdapter(new NothingSelectedSpinnerAdapter(
                                    aaRank, R.layout.contact_spinner_row_nothing_selected,
                                    getApplicationContext()));
                            spRank.setSelection(selectedPos);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d("new exception:", String.valueOf(e));
                    }
                    break;
                case getAppliedRankActivity:
                    rankMultiArray.clear();
                    appliedRankList.clear();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            JSONArray ob1 = jObj.getJSONArray("data");
                            for (int i = 0; i < ob1.length(); i++) {
                                JSONObject ob = ob1.getJSONObject(i);
                                String rankId = ob.getString("RankID");
                                String rankName = ob.getString("Name");
                                appliedRankList.add(new RankModel(rankId, rankName));
                                rankMultiArray.add(rankName);
                            }
                            multiSelectionSpinner.setItems(rankMultiArray, Constant.FILTER_TYPE.APPLIEDRANK);
                            multiSelectionSpinner.setSelection(new int[]{0});
                            multiSelectionSpinner.setListener(this);
                            multiSelectionSpinner.clearSelection();

                            if (!TextUtils.isEmpty(multiSelecteRankId)) {
                                int[] array = new int[App.indexListRank.size()];
                                int counter = 0;
                                for (Integer myInt : App.indexListRank) {
                                    array[counter++] = myInt;
                                }
                                multiSelectionSpinner.setSelection(array);

                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();

                        Log.d("new exception:", String.valueOf(e));
                    }
                    Utils.hideProgressDialog();
                    break;
                case getAppliedShip:
                    shipMultiArray.clear();
                    appliedShipList.clear();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            JSONArray ob1 = jObj.getJSONArray("data");
                            for (int i = 0; i < ob1.length(); i++) {
                                JSONObject ob = ob1.getJSONObject(i);
                                String shipId = ob.getString("ShipID");
                                String shipName = ob.getString("ShipType");
                                appliedShipList.add(new AppliedShip(shipId, shipName));
                                shipMultiArray.add(shipName);
                            }
                            multiSelectionSpinnerShipType.setItems(shipMultiArray, Constant.FILTER_TYPE.SHIPTYPE);
                            multiSelectionSpinnerShipType.setSelection(new int[]{0});
                            multiSelectionSpinnerShipType.setListener(this);
                            multiSelectionSpinnerShipType.clearSelection();

                            if (!TextUtils.isEmpty(multiSelecteShipTypeId)) {
                                int[] array = new int[App.indexListShip.size()];
                                int counter = 0;
                                for (Integer myInt : App.indexListShip) {
                                    array[counter++] = myInt;
                                }
                                multiSelectionSpinnerShipType.setSelection(array);
                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d("new exception:", String.valueOf(e));
                    }
                    Utils.hideProgressDialog();
                    break;
            }
        }
    }


    private void getCandidateSignup2() {
        Intent i = new Intent(getApplicationContext(), CandidateSignUp3.class);
        startActivity(i);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.candidate, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
//                Intent i = new Intent(getApplicationContext(), CandidateSignUp.class);
//                startActivity(i);
//                // app icon in action bar clicked; goto parent activity.
//                this.finish();
                onBackPressed();
                return true;
            case R.id.login:
                Intent intent = new Intent(CandidateSignUp2.this, LoginActivity.class);
                Bundle bundle = new Bundle();
                bundle.putInt("value", 0);
                intent.putExtras(bundle);
                startActivity(intent);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        edtChildren.setText(App.childeren);
        edt_weight.setText(App.weight);
        edt_height.setText(App.height);
        edt_bmi.setText(App.bmi);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
      /*  App.weight = edt_weight.getText().toString().trim();
        App.height = edt_height.getText().toString().trim();
        App.bmi = edt_bmi.getText().toString().trim();
     */
        finish();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void selectedIndices(List<Integer> indices, Constant.FILTER_TYPE filter_type) {
        App.availableFromTemp = edtAvailableFrom.getText().toString();
        if (filter_type == APPLIEDRANK) {
            App.indexListRank = indices;
            App.multiSelecteRankId = "";
            ArrayList<String> rankIDArray = new ArrayList<>();
            for (Integer i : indices) {
                rankIDArray.add(appliedRankList.get(i).getRankId());

            }
            App.multiSelecteRankId = android.text.TextUtils.join(",", rankIDArray);
        }
        if (filter_type == SHIPTYPE) {
            App.indexListShip = indices;
            multiSelecteShipTypeId = "";
            ArrayList<String> shipIDArray = new ArrayList<>();
            for (Integer i : indices) {
                shipIDArray.add(appliedShipList.get(i).getShipID());

            }
            multiSelecteShipTypeId = android.text.TextUtils.join(",", shipIDArray);
        }
    }

    @Override
    public void selectedStrings(List<String> strings, Constant.FILTER_TYPE filter_type) {

    }


}
