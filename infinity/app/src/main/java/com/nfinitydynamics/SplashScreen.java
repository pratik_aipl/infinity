package com.nfinitydynamics;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.nfinitydynamics.activity.LoginActivity;
import com.nfinitydynamics.activity.ResumeActivity;
import com.nfinitydynamics.utils.App;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class SplashScreen extends AppCompatActivity {

    private static int SPLASH_TIME_OUT = 3000;
    public SharedPreferences shared;
    public TextView txtVirsonCode;
    public String isNoti = "0";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        shared = getSharedPreferences(getApplication().getPackageName(), 0);
        txtVirsonCode = findViewById(R.id.txtVirsonCode);

        /// difference is   7 Month and 22 days
//        String startDate = "11-Feb-2014";
//        String endDate = "02-Oct-2014";


        /// difference is    8 month 9 days
//        String startDate = "15-Aug-2012";
//        String endDate = "24-Apr-2013";


        /// difference is   4 month 19 days
//        String startDate = "13-Apr-2010";
//        String endDate = "31-Aug-2010";

        /// difference is   7 month 3 days
//        String startDate = "11-Oct-2011";
//        String endDate = "13-May-2012";

        /// difference is   7 month 13 days
//        String startDate = "11-Oct-2011";
//        String endDate = "23-May-2012";


        /// difference is   3 Month 29 days
        String startDate = "27-Jul-2016";
        String endDate = "25-Nov-2017";

        /// difference is   6 Month 15 days
//        String startDate = "15-Aug-2012";
//        String endDate = "24-Apr-2013";

        System.out.println(" Calculation start date==" + startDate + "endDate==" + endDate);
        countDifference(startDate, endDate);

        try {
            PackageInfo pInfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
            String version = pInfo.versionName;
            txtVirsonCode.setText("Version Code: " + version);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if (shared.getBoolean(App.IS_LOGGED_IN, false)) {

                    if (App.isFromNotification) {
                        App.isFromNotification = false;
                        Intent i = new Intent(getApplicationContext(), ResumeActivity.class);
                        i.putExtra("resume_url", getIntent().getStringExtra("resume_url"));
                        startActivity(i);
                        finish();
                    } else {
                        String userType = shared.getString("userType", "");
                        Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putInt("value", 1);
                        i.putExtras(bundle);
                        startActivity(i);
                        finish();
                    }


//                        Log.d("userType:::=====", userType);
//                        if(userType.equalsIgnoreCase("Candidate")) {
//                            Intent i = new Intent(SplashScreen.this, MainActivity.class);
//                            startActivity(i);
//                            finish();
//                        }else if(userType.equalsIgnoreCase("Company")) {
//                            Intent i = new Intent(SplashScreen.this, CompanyDashboard.class);
//                            startActivity(i);
//                            finish();
//                        }
//                        else {
//                            Intent i = new Intent(SplashScreen.this, LoginActivity.class);
//                            startActivity(i);
//                            finish();
//                        }


                } else {
                    Intent i = new Intent(SplashScreen.this, LoginActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putInt("value", 0);
                    i.putExtras(bundle);
                    startActivity(i);
                    finish();

                }
            }
        }, SPLASH_TIME_OUT);
    }

    public void countDifference(String start, String end) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
        Date startDate = null, endDate = null;
        try {
            startDate = dateFormat.parse(start);
            endDate = dateFormat.parse(end);
            Calendar startCalendar = new GregorianCalendar();
            startCalendar.setTime(startDate);
            Calendar endCalendar = new GregorianCalendar();
            endCalendar.setTime(endDate);
            int startYear = startCalendar.get(Calendar.YEAR);
            int startDay = startCalendar.get(Calendar.DAY_OF_MONTH);
            int startMonth = startCalendar.get(Calendar.MONTH) + 1;

            int endMonth = endCalendar.get(Calendar.MONTH) + 1;
            int endDay = endCalendar.get(Calendar.DAY_OF_MONTH);
            int endYear = endCalendar.get(Calendar.YEAR);


            int day = 0;
            int month = 0;
            int year = 0;
            if (endYear == startYear) {
                year = 0;
                System.out.println("Calculation :  in Same year");

                if (startMonth == endMonth) {
                    System.out.println("Calculation :  in Same month");

                    // in same month
                    if (endDay == startDay) {
                        System.out.println("Calculation :  in Same day");
                        day = 1;
                        month = 0;

                    } else if (endDay > startDay) {
                        System.out.println("Calculation :  in endDay > startDay");
                        day = endDay - startDay;
                        month = 0;
                    } else {
                        System.out.println("Calculation :  in startDay > endDay  is not valid");
                        // is not valid days
                    }
                } else if (endMonth > startMonth) {
                    System.out.println("Calculation :  in endMonth > startMonth");
                    // in same month
                    if (endDay == startDay) {
                        System.out.println("Calculation :  in Same day");
                        day = 1;
                        month = endMonth - startMonth;
                    } else if (endDay > startDay) {
                        System.out.println("Calculation :  in endDay > startDay");
                        day = endDay - startDay;
                        month = (endMonth - startMonth);
                        day++;
                    } else if (startDay > endDay) {
                        month = (endMonth - startMonth);
                        month--;
                        int xStartMonthDays = (getDaysOfMonth(startMonth, startCalendar.get(Calendar.YEAR)) - startDay);
                        day = xStartMonthDays + endDay;
                        day++;
                        System.out.println("Calculation :  in startDay > endDay");
                        // is not valid days
                    }
                    // valid month
                } else {
                    System.out.println("Calculation :  in endMonth > startMonth  is not valid");
                    // is not valid month in same year
                }
            } else if (endYear > startYear) {
                System.out.println("Calculation :  in endYear > startYear");

                if (startMonth == endMonth) {
                    System.out.println("Calculation :  in Same month");

                    // in same month
                    if (endDay == startDay) {
                        System.out.println("Calculation :  in Same day");
                        day = 1;
                        month = 0;
                        year = endYear - startYear;
                    } else if (endDay > startDay) {
                        System.out.println("Calculation :  in endDay > startDay");
                        day = endDay - startDay;

                        month = 0;
                        year = endYear - startYear;
                    } else if (startDay > endDay) {
                        System.out.println("Calculation :  in startDay > endDay ");
                        year = endYear - startYear;
                        year--;
                        month = 11;

                        int xStartMonthDays = (getDaysOfMonth(startMonth, startCalendar.get(Calendar.YEAR)) - startDay);
                        day = xStartMonthDays + endDay;
                        day++;
                        // is not valid days
                    }
                } else if (endMonth > startMonth) {
                    System.out.println("Calculation :  in endMonth > startMonth");
                    // in same month
                    if (endDay == startDay) {
                        System.out.println("Calculation :  in Same day");
                        year = endYear - startYear;
                        day = 1;
                        month = endMonth - startMonth;
                    } else if (endDay > startDay) {
                        System.out.println("Calculation :  in endDay > startDay");
                        year = endYear - startYear;
                        day = endDay - startDay;
                        day++;
                        month = (endMonth - startMonth);

                    } else if (startDay > endDay) {
                        System.out.println("Calculation :  in startDay > endDay ");
                        year = endYear - startYear;
                        year--;
                        month = (endMonth - startMonth);
                        month--;
                        int xStartMonthDays = (getDaysOfMonth(startMonth, startCalendar.get(Calendar.YEAR)) - startDay);
                        int xEndMonthDays = endDay;
                        day = xStartMonthDays + xEndMonthDays;
                        // is not valid days
                    }
                } else if (startMonth > endMonth) {
                    System.out.println("Calculation :  in startMonth > endMonth");
                    // in same month
                    if (endDay == startDay) {
                        System.out.println("Calculation :  in Same day");
                        year = endYear - startYear;
                        year--;
                        day = 1;
                        month = 12 - (Math.abs(endMonth - startMonth));
                    } else if (endDay > startDay) {
                        System.out.println("Calculation :  in endDay > startDay");
                        year = endYear - startYear;
                        year--;
                        month = 12 - (Math.abs(endMonth - startMonth));
                        day = endDay - startDay;
                        day++;


                    } else if (startDay > endDay) {
                        System.out.println("Calculation :  in startDay > endDay ");
                        year = endYear - startYear;
                        year--;

                        month = 12 - (Math.abs(endMonth - startMonth));
                        month--;


                        int xStartMonthDays = (getDaysOfMonth(endMonth, endCalendar.get(Calendar.YEAR)) - startDay);
                        int xEndMonthDays = endDay;
                        day = xStartMonthDays + xEndMonthDays;
                        day++;
                        // is not valid days
                    }
                }
            } else {
                /// date is not valid
            }


            if (day >= 30) {
                System.out.println("Calculation Day is Greather then 30");
            }

            int calculatedMonthsByYear = year * 12 + month;

            System.out.println("Calculation   --->  " + calculatedMonthsByYear + " Months " + day + " days");

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public int getDaysOfMonth(int month, int year) {

        System.out.println(" final Date Month :" + month);
        System.out.println(" final Date year :" + year);
        int day = 0;
        switch (month) {
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                day = 31;
                break;
            case 4:
            case 6:
            case 9:
            case 11:
                day = 30;
                break;

            case 2:
                if (year % 4 == 0) {
                    day = 29;
                } else {
                    day = 28;
                }
//// remove below lin
                day = 30;

                break;
            default:
                day = 31;

                break;

        }

        System.out.println("Calculation final Date day return :" + day);
        return day;
    }

}
