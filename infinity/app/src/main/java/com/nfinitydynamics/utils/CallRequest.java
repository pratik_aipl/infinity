package com.nfinitydynamics.utils;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.util.Log;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by admin on 10/11/2016.
 */
public class CallRequest {

    private static final String TAG = "CallRequest";
  public static String COMMON_URL = "http://test.infinitydynamics.in/web_services/version_1/web_services/";

//  public static String COMMON_URL = "https://infinitydynamics.in/web_services/version_1/web_services/";

    public App app;
    public Context ct;
    public Fragment ft;

    public CallRequest(Fragment ft) {
        app = App.getInstance();
        this.ft = ft;
    }

    public CallRequest(Context ct) {
        this.ct = ct;
        app = App.getInstance();
    }

    public void getLocation() {

        Map<String, String> map = new HashMap<String, String>();
        map.put("show", "");
        map.put("url", "http://ip-api.com/json");
        new AsynchHttpRequest(ft, Constant.REQUESTS.getLocation, Constant.POST_TYPE.GET, map);
    }

    public void getMatchJob(String candidateId) {

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_match_job");
        map.put("candidate_id", candidateId);
        new AsynchHttpRequest(ft, Constant.REQUESTS.getMatchJob, Constant.POST_TYPE.POST, map);
    }

    public void getCandidateDetail(String candidateId) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_candidate_data");
        map.put("candidate_id", candidateId);
        new AsynchHttpRequest(ft, Constant.REQUESTS.getCandidateDetail, Constant.POST_TYPE.POST, map);
    }

    public void getSTCWDetail(String candidateId) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_stcw_dtl");
        map.put("candidate_id", candidateId);
        new AsynchHttpRequest(ft, Constant.REQUESTS.getSTCWDetail, Constant.POST_TYPE.POST, map);
    }

    public void getCompanyCandidateDetail(String jobID) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_candidate_list");
        map.put("days", "1");
        new AsynchHttpRequest(ft, Constant.REQUESTS.getCandidateList, Constant.POST_TYPE.POST, map);
    }

    public void getCompanyCandidateDetailJob(String jobID) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_candidate_list");
        map.put("job_id", jobID);
        map.put("days", "1");
        new AsynchHttpRequest(ft, Constant.REQUESTS.getCandidateList, Constant.POST_TYPE.POST, map);
    }

    public void getAppliedCandidateDetail(String companyId) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_candidate_job_list");
        map.put("company_id", companyId);
        new AsynchHttpRequest(ft, Constant.REQUESTS.getAppliedCandidateList, Constant.POST_TYPE.POST, map);
    }

    public void getAppliedWithFilterCandidateDetail(String companyId, String rankID, String ship_id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_candidate_job_list");
        map.put("company_id", companyId);
        map.put("rank_id", rankID);
        map.put("ship_id", ship_id);

        new AsynchHttpRequest(ft, Constant.REQUESTS.getAppliedCandidateList, Constant.POST_TYPE.POST, map);
    }

    public void getCounnt(String companyId) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "company_count");
        map.put("companyid", companyId);
        new AsynchHttpRequest(ft, Constant.REQUESTS.getCounnt, Constant.POST_TYPE.POST, map);
    }

    public void getAllCandidateDetailSingle() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_candidate_list");
        new AsynchHttpRequest(ft, Constant.REQUESTS.getAllCandidateListSingle, Constant.POST_TYPE.POST, map);
    }

    public void getAllCompanyCandidateDetail(String rank_id, String ship_id, String job_id, String company_id,
                                             String from_experience, String to_experience, String from_age, String to_age, String days, String country_id, String latest_available) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_candidate_list");
        map.put("rank_id", rank_id);
        map.put("ship_id", ship_id);
        map.put("job_id", job_id);
        map.put("company_id", company_id);
        map.put("from_experience", from_experience);
        map.put("to_experience", to_experience);
        map.put("from_age", from_age);
        map.put("to_age", to_age);
        map.put("days", days);
        map.put("country_id", country_id);
        map.put("latest_available", "yes");
        new AsynchHttpRequest(ft, Constant.REQUESTS.getAllCandidateList, Constant.POST_TYPE.POST, map);
    }

    public void getFilterCompanyCandidateDetail(String rank_id, String ship_id,
                                                String job_id, String company_id,
                                                String from_experience,
                                                String to_experience, String from_age, String to_age, String days, String country_id, String latest_available) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_candidate_list");
        map.put("rank_id", rank_id);
        map.put("ship_id", ship_id);
        map.put("job_id", job_id);
        map.put("company_id", company_id);
        map.put("from_experience", from_experience);
        map.put("to_experience", to_experience);
        map.put("from_age", from_age);
        map.put("to_age", to_age);
        map.put("days", days);
        map.put("country_id", country_id);
        Log.d(TAG, "getFilterCompanyCandidateDetail: "+map.toString());
        new AsynchHttpRequest(ft, Constant.REQUESTS.getAllCandidateList, Constant.POST_TYPE.POST, map);
    }

    public void getFilterShoreJob(String rank_id, String ship_id, String job_id, String company_id,
                                  String from_experience, String to_experience, String from_age, String to_age, String days, String country_id, String latest_available) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_candidate_list");
        map.put("rank_id", rank_id);
        map.put("ship_id", ship_id);
        map.put("job_id", job_id);
        map.put("company_id", company_id);
        map.put("from_experience", from_experience);
        map.put("to_experience", to_experience);
        map.put("from_age", from_age);
        map.put("to_age", to_age);
        map.put("days", days);
        map.put("country_id", country_id);
        map.put("shore_job_resume", "yes");
        new AsynchHttpRequest(ft, Constant.REQUESTS.getShoreJob, Constant.POST_TYPE.POST, map);
    }

    public void getAllCompanyCandidatelatesDetail() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_candidate_list");
        map.put("days", "1");
        new AsynchHttpRequest(ft, Constant.REQUESTS.getAllCandidateList, Constant.POST_TYPE.POST, map);
    }

    public void getUrgentlyCandidateDetail() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_candidate_list");
        map.put("days", "0");
        new AsynchHttpRequest(ft, Constant.REQUESTS.getAllCandidateList, Constant.POST_TYPE.POST, map);
    }

    public void getCompanyCandidateDetailRank(String rankID, String ship_id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_candidate_job_list");
        map.put("rank_id", rankID);
        map.put("ship_id", ship_id);
        new AsynchHttpRequest(ft, Constant.REQUESTS.getCandidateListRank, Constant.POST_TYPE.POST, map);
    }

    public void getShoreJob() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_candidate_list");
        map.put("shore_job_resume", "yes");
        new AsynchHttpRequest(ft, Constant.REQUESTS.getShoreJob, Constant.POST_TYPE.POST, map);
    }

    public void bulidResume(String candidate_id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "build_resume");
        map.put("candidate_id", candidate_id);
        new AsynchHttpRequest(ft, Constant.REQUESTS.bulidResume, Constant.POST_TYPE.POST, map);
    }

    public void bulidResumeCompamy(String candidate_id, String companyId) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "build_resume");
        map.put("candidate_id", candidate_id);
        map.put("company_id", companyId);
        new AsynchHttpRequest(ft, Constant.REQUESTS.bulidResumeCompany, Constant.POST_TYPE.POST, map);
    }

    public void previewBuildResume(String candidate_id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "preview_build_resume");
        map.put("candidate_id", candidate_id);
        new AsynchHttpRequest(ft, Constant.REQUESTS.previewBuildResume, Constant.POST_TYPE.POST, map);
    }

    public void reDownlodeResumeCompamy(String candidate_id, String companyId, String redownlode) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "build_resume");
        map.put("candidate_id", candidate_id);
        map.put("company_id", companyId);
        map.put("download_again", redownlode);
        new AsynchHttpRequest(ft, Constant.REQUESTS.reDownlode, Constant.POST_TYPE.POST, map);
    }

    public void applyNow(String candidateId, String job_id, String apply_date) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "apply_job");
        map.put("candidate_id", candidateId);
        map.put("job_id", job_id);
        map.put("apply_date", apply_date);
        new AsynchHttpRequest(ft, Constant.REQUESTS.applyNow, Constant.POST_TYPE.POST, map);
    }

    public void getEducationDetail(String candidateId) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_education_dtl");
        map.put("candidate_id", candidateId);
        new AsynchHttpRequest(ft, Constant.REQUESTS.getEducationDetail, Constant.POST_TYPE.POST, map);
    }

    public void getNotification(String userId) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_notification_dtl");
        map.put("user_id", userId);
        new AsynchHttpRequest(ft, Constant.REQUESTS.get_notification_dtl, Constant.POST_TYPE.POST, map);
    }

    public void getNews() {

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_news_list");
        new AsynchHttpRequest(ft, Constant.REQUESTS.get_news, Constant.POST_TYPE.POST, map);
    }

    public void getEngineType() {

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_engine_type");
        new AsynchHttpRequest(ft, Constant.REQUESTS.getEngineType, Constant.POST_TYPE.POST, map);
    }

    public void getTonnageType() {

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_tonnage_type");
        new AsynchHttpRequest(ft, Constant.REQUESTS.getTonnageType, Constant.POST_TYPE.POST, map);
    }

    public void getRank() {

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_rank");
        new AsynchHttpRequest(ct, Constant.REQUESTS.getRank, Constant.POST_TYPE.POST, map);
    }

    public void getCertificateType() {

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_certificate");
        new AsynchHttpRequest(ft, Constant.REQUESTS.getCertificateType, Constant.POST_TYPE.POST, map);
    }

    public void getSTWCCertificateType() {

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_stcwcertificate");
        new AsynchHttpRequest(ft, Constant.REQUESTS.getSTWCCertificate, Constant.POST_TYPE.POST, map);
    }

    public void getRankFragment() {

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_rank");
        new AsynchHttpRequest(ft, Constant.REQUESTS.getRankFragment, Constant.POST_TYPE.POST, map);
    }

    public void getAppliedRankFragment(String rank_id) {

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_rank");
        map.put("rank_id", rank_id);

        new AsynchHttpRequest(ft, Constant.REQUESTS.getAppliedRankFragment, Constant.POST_TYPE.POST, map);
    }

    public void getAppliedRankActivity(String rank_id) {

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_rank");
        map.put("rank_id", rank_id);

        new AsynchHttpRequest(ct, Constant.REQUESTS.getAppliedRankActivity, Constant.POST_TYPE.POST, map);
    }

    public void getShipFragment() {

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_ship_type");
        new AsynchHttpRequest(ft, Constant.REQUESTS.getShipType, Constant.POST_TYPE.POST, map);
    }

    public void getNationality() {

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_nationality");
        new AsynchHttpRequest(ct, Constant.REQUESTS.getNationality, Constant.POST_TYPE.POST, map);
    }

    public void getNationalityFragment() {

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_nationality");
        new AsynchHttpRequest(ft, Constant.REQUESTS.getNationalityFragment, Constant.POST_TYPE.POST, map);
    }

    public void getCompanyListFragment() {

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_company_list");
        new AsynchHttpRequest(ft, Constant.REQUESTS.getCompanyListFragment, Constant.POST_TYPE.POST, map);
    }

    public void getInstituteListFragment() {

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_institute_list");
        new AsynchHttpRequest(ft, Constant.REQUESTS.getInstituteListFragment, Constant.POST_TYPE.POST, map);
    }

    public void login(String email, String password, String tokenid) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "login");
        map.put("email", email);
        map.put("password", password);
        map.put("tokenid", tokenid);

        new AsynchHttpRequest(ct, Constant.REQUESTS.login, Constant.POST_TYPE.POST, map);
    }

    public void checkEmail(String email) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "is_email_available_checked");
        map.put("email", email);
        new AsynchHttpRequest(ct, Constant.REQUESTS.checkEmail, Constant.POST_TYPE.POST, map);
    }

    public void updateLastVisited(String CandidateID) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "update_last_visited");
        map.put("CandidateID", CandidateID);
        new AsynchHttpRequest(ct, Constant.REQUESTS.updateLastVisited, Constant.POST_TYPE.POST, map);
    }

    public void getFeatured(String candidateID) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_featuredjob_list");
        map.put("candidate_id", candidateID);
        new AsynchHttpRequest(ft, Constant.REQUESTS.featureJob, Constant.POST_TYPE.POST, map);
    }

    public void getCompanyJobList(String id, String candidate) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_job_list");
        map.put("company_id", id);
        map.put("candidate_id", candidate);

        new AsynchHttpRequest(ft, Constant.REQUESTS.getCompanyJob, Constant.POST_TYPE.POST, map);
    }

    public void getCompanyJobList(String id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_job_list");
        map.put("company_id", id);
        new AsynchHttpRequest(ft, Constant.REQUESTS.getCompanyJob, Constant.POST_TYPE.POST, map);
    }

    public void getOtherJobsOfCopamny(String id, String selectedJobId) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_job_list");
        map.put("company_id", id);
        map.put("selected_job_id", selectedJobId);

        new AsynchHttpRequest(ft, Constant.REQUESTS.otherJob, Constant.POST_TYPE.POST, map);
    }

    public void getLatest(String candidateID) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_latestjob_list");
        map.put("candidate_id", candidateID);
        new AsynchHttpRequest(ft, Constant.REQUESTS.latestJob, Constant.POST_TYPE.POST, map);
    }

    public void searchJob(String candidateID, String keyword, String searchtype) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "search_job");
        map.put("candidate_id", candidateID);
        map.put("keyword", keyword);
        map.put("searchtype", searchtype);
        new AsynchHttpRequest(ft, Constant.REQUESTS.latestJob, Constant.POST_TYPE.POST, map);
    }

    public void addExperienceDetail(String candidateID, String companyName, String shipNmae, String joinDate, String leavingDate,
                                    String shipType, String engineType, String rank, String grt, String seaDuration, String update
            , String tonnage_type, String tonnage_value) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "add_update_experience_dtl");
        map.put("candidate_id", candidateID);
        map.put("company_name", companyName);
        map.put("ship_name", shipNmae);
        map.put("joining_date", joinDate);
        map.put("leaving_date", leavingDate);
        map.put("ship_type", shipType);
        map.put("engine_type", engineType);
        map.put("rank", rank);
        map.put("grt", grt);
        map.put("total_sea_duration", seaDuration);
        map.put("update", update);
        map.put("tonnage_type", tonnage_type);
        map.put("tonnage_value", tonnage_value);

        new AsynchHttpRequest(ft, Constant.REQUESTS.addExperience, Constant.POST_TYPE.POST, map);
    }

    public void updateExperienceDetail(String candidateID, String companyName, String shipNmae, String joinDate,
                                       String leavingDate, String shipType, String engineType, String rank,
                                       String grt, String seaDuration,
                                       String update, String experienceId, String tonnage_type, String tonnage_value) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "add_update_experience_dtl");
        map.put("candidate_id", candidateID);
        map.put("company_name", companyName);
        map.put("ship_name", shipNmae);
        map.put("joining_date", joinDate);
        map.put("leaving_date", leavingDate);
        map.put("ship_type", shipType);
        map.put("engine_type", engineType);
        map.put("rank", rank);
        map.put("grt", grt);
        map.put("total_sea_duration", seaDuration);
        map.put("update", update);
        map.put("experience_id", experienceId);
        map.put("tonnage_type", tonnage_type);
        map.put("tonnage_value", tonnage_value);

        new AsynchHttpRequest(ft, Constant.REQUESTS.updateExperience, Constant.POST_TYPE.POST, map);
    }

    public void updateJob(String jobID, String candidateID, String rankId, String shipId, String jobTitle, String from_experience, String to_experience, String joinDate,
                          String leavingDate, String description, String isFeatured, String isShore, String isFront, String update) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "add_update_job");
        map.put("job_id", jobID);
        map.put("company_id", candidateID);
        map.put("rank_id", rankId);
        map.put("ship_id", shipId);
        map.put("job_title", jobTitle);
        map.put("from_experience", from_experience);
        map.put("to_experience", to_experience);
        map.put("job_start_date", joinDate);
        map.put("job_end_date", leavingDate);
        map.put("description", description);
        map.put("is_featured", isFeatured);
        map.put("is_shore", isShore);
        map.put("is_front", isFront);
        map.put("update", update);

        new AsynchHttpRequest(ft, Constant.REQUESTS.updateJob, Constant.POST_TYPE.POST, map);
    }

    public void addJob(String candidateID, String rankId, String shipId, String jobTitle, String joinDate,
                       String leavingDate, String description, String isFeatured, String isShore, String isFront,
                       String from_experience, String to_experience, String update) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "add_update_job");
        map.put("company_id", candidateID);
        map.put("rank_id", rankId);
        map.put("ship_id", shipId);
        map.put("job_title", jobTitle);
        map.put("job_start_date", joinDate);
        map.put("job_end_date", leavingDate);
        map.put("description", description);
        map.put("is_featured", isFeatured);
        map.put("is_shore", isShore);
        map.put("is_front", isFront);
        map.put("from_experience", from_experience);
        map.put("to_experience", to_experience);
        map.put("update", update);

        new AsynchHttpRequest(ft, Constant.REQUESTS.addJob, Constant.POST_TYPE.POST, map);
    }


//    public void login(String email, String password) {
//        Map<String, String> map = new HashMap<String, String>();
//        map.put("url", COMMON_URL + "login");
//        map.put("email", email);
//        map.put("password", password);
//        new AsynchHttpRequest(ct, Constant.REQUESTS.login, Constant.POST_TYPE.POST, map);
//
//    }

    public void addSeamenBook(String candidate_id, String number, String place_of_issue, String validity, String update) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "add_update_seaman_dtl");
        map.put("candidate_id", candidate_id);
        map.put("number", number);
        map.put("place_of_issue", place_of_issue);
        map.put("validity", validity);
        map.put("update", update);
        new AsynchHttpRequest(ft, Constant.REQUESTS.addSemeanBook, Constant.POST_TYPE.POST, map);
    }

    public void addVisa(String candidate_id, String visa_number, String place_of_issue, String validity, String visa_type, String update) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "add_update_visa_dtl");
        map.put("candidate_id", candidate_id);
        map.put("visa_number", visa_number);
        map.put("visa_type", visa_type);
        map.put("place_of_issue", place_of_issue);
        map.put("validity", validity);
        map.put("update", update);
        new AsynchHttpRequest(ft, Constant.REQUESTS.addVisa, Constant.POST_TYPE.POST, map);
    }

    public void updateVisa(String candidate_id, String visa_number, String place_of_issue, String validity, String visa_id, String visa_type, String update) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "add_update_visa_dtl");
        map.put("candidate_id", candidate_id);
        map.put("visa_number", visa_number);
        map.put("place_of_issue", place_of_issue);
        map.put("validity", validity);
        map.put("visa_id", visa_id);
        map.put("visa_type", visa_type);
        map.put("update", update);
        new AsynchHttpRequest(ft, Constant.REQUESTS.updateVisa, Constant.POST_TYPE.POST, map);
    }

    public void addCOC(String candidate_id, String number, String placeOFIssue, String proficiency, String validity, String update) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "add_update_coc_dtl");
        map.put("candidate_id", candidate_id);
        map.put("number", number);
        map.put("place_of_issue", placeOFIssue);
        map.put("proficiency", proficiency);
        map.put("validity", validity);
        map.put("update", update);
        new AsynchHttpRequest(ft, Constant.REQUESTS.addCOC, Constant.POST_TYPE.POST, map);
    }

    public void addEducationDetail(String candidate_id, String education_type, String instituteName, String degree, String place, String passingYear, String update) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "add_update_education_dtl");
        map.put("candidate_id", candidate_id);
        map.put("education_type", education_type);
        map.put("institute_name", instituteName);
        map.put("degree", degree);
        map.put("place", place);
        map.put("passing_year", passingYear);
        map.put("update", update);
        new AsynchHttpRequest(ft, Constant.REQUESTS.addEducationDetail, Constant.POST_TYPE.POST, map);
    }

    public void updateEducationDetail(String candidate_id, String education_type, String instituteName, String degree, String place, String passingYear, String update, String educationId) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "add_update_education_dtl");
        map.put("candidate_id", candidate_id);
        map.put("education_type", education_type);
        map.put("institute_name", instituteName);
        map.put("degree", degree);
        map.put("place", place);
        map.put("passing_year", passingYear);
        map.put("update", update);
        map.put("education_id", educationId);
        new AsynchHttpRequest(ft, Constant.REQUESTS.updateEducationDetail, Constant.POST_TYPE.POST, map);
    }

    public void addDCE(String candidate_id, String number, String proficiency, String placeOFIssue, String level, String validity, String update) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "add_update_dce_dtl");
        map.put("candidate_id", candidate_id);
        map.put("number", number);
        map.put("proficiency", proficiency);
        map.put("place_of_issue", placeOFIssue);
        map.put("level", level);
        map.put("validity", validity);
        map.put("update", update);
        new AsynchHttpRequest(ft, Constant.REQUESTS.addDCE, Constant.POST_TYPE.POST, map);
    }

    public void addSTWC(String candidate_id, String number, String proficiency, String place_of_issue, String validity, String update) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "add_update_stcw_dtl");
        map.put("candidate_id", candidate_id);
        map.put("number", number);
        map.put("proficiency", proficiency);
        map.put("place_of_issue", place_of_issue);
        map.put("validity", validity);
        map.put("update", update);
        new AsynchHttpRequest(ft, Constant.REQUESTS.addSWTC, Constant.POST_TYPE.POST, map);
    }

    public void updateCompanyDetail(String companyId, String company_name, String rsplNo, String validUntil, String email, String phone, String telephone,
                                    String website, String company_image, String address, String mcountry_code, String country_code, String std_code) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "update_company_data");
        map.put("company_id", companyId);
        map.put("company_name", company_name);
        map.put("rpsl_no", rsplNo);
        map.put("validity", validUntil);
        map.put("email", email);
        map.put("phone", phone);
        map.put("telephone", telephone);
        map.put("website", website);
        map.put("company_image", company_image);
        map.put("address", address);
        map.put("mcountry_code", mcountry_code);
        map.put("country_code", country_code);
        map.put("std_code", std_code);
        new AsynchHttpRequest(ft, Constant.REQUESTS.updateCompany, Constant.POST_TYPE.POST_WITH_IMAGE, map);
    }

    public void updateSeamenBook(String candidate_id, String number, String place_of_issue, String validity, String update, String seaman_id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "add_update_seaman_dtl");
        map.put("candidate_id", candidate_id);
        map.put("number", number);
        map.put("place_of_issue", place_of_issue);
        map.put("validity", validity);
        map.put("update", update);
        map.put("seaman_id", seaman_id);
        new AsynchHttpRequest(ft, Constant.REQUESTS.updateSemeanBook, Constant.POST_TYPE.POST, map);
    }

    public void updateCOC(String candidate_id, String number, String proficiency, String place_of_issue, String validity, String update, String seaman_id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "add_update_coc_dtl");
        map.put("candidate_id", candidate_id);
        map.put("number", number);
        map.put("proficiency", proficiency);
        map.put("place_of_issue", place_of_issue);
        map.put("validity", validity);
        map.put("update", update);
        map.put("coc_id", seaman_id);
        new AsynchHttpRequest(ft, Constant.REQUESTS.updateCOC, Constant.POST_TYPE.POST, map);
    }

    public void updateSTWC(String candidate_id, String number, String proficiency, String place_of_issue, String validity, String update, String stcw_id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "add_update_stcw_dtl");
        map.put("candidate_id", candidate_id);
        map.put("number", number);
        map.put("proficiency", proficiency);
        map.put("place_of_issue", place_of_issue);
        map.put("validity", validity);
        map.put("update", update);
        map.put("stcw_id", stcw_id);
        new AsynchHttpRequest(ft, Constant.REQUESTS.updateSTWC, Constant.POST_TYPE.POST, map);
    }

    public void updateDCE(String candidate_id, String number, String proficiency, String place_of_issue, String level, String validity, String update, String seaman_id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "add_update_dce_dtl");
        map.put("candidate_id", candidate_id);
        map.put("number", number);
        map.put("proficiency", proficiency);
        map.put("place_of_issue", place_of_issue);
        map.put("level", level);
        map.put("validity", validity);
        map.put("update", update);
        map.put("dce_id", seaman_id);
        new AsynchHttpRequest(ft, Constant.REQUESTS.updateDCE, Constant.POST_TYPE.POST, map);
    }

    public void getSeamenBook(String candidate_id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_seaman_dtl");
        map.put("candidate_id", candidate_id);
        new AsynchHttpRequest(ft, Constant.REQUESTS.getSemeanBook, Constant.POST_TYPE.POST, map);
    }

    public void getVisa(String candidate_id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_visa_dtl");
        map.put("candidate_id", candidate_id);
        new AsynchHttpRequest(ft, Constant.REQUESTS.getVisa, Constant.POST_TYPE.POST, map);
    }

    public void getComapnyDetail(String companyId) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_company_data");
        map.put("company_id", companyId);
        new AsynchHttpRequest(ft, Constant.REQUESTS.getCompanyDetail, Constant.POST_TYPE.POST, map);
    }

    public void getCOCDetail(String candidate_id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_coc_dtl");
        map.put("candidate_id", candidate_id);
        new AsynchHttpRequest(ft, Constant.REQUESTS.getCOCDetail, Constant.POST_TYPE.POST, map);
    }

    public void getDCEDetail(String candidate_id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_dce_dtl");
        map.put("candidate_id", candidate_id);
        new AsynchHttpRequest(ft, Constant.REQUESTS.getDCEDetail, Constant.POST_TYPE.POST, map);
    }

    public void deleteSeaman(String semeanId) {

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "delete_seaman");
        map.put("seaman_id", semeanId);
        new AsynchHttpRequest(ft, Constant.REQUESTS.deleteSemean, Constant.POST_TYPE.POST, map);
    }

    public void deleteJob(String jobId) {

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "delete_job");
        map.put("job_id", jobId);
        new AsynchHttpRequest(ft, Constant.REQUESTS.deleteJob, Constant.POST_TYPE.POST, map);
    }

    public void deleteVisa(String visa_id) {

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "delete_visa");
        map.put("visa_id", visa_id);
        new AsynchHttpRequest(ft, Constant.REQUESTS.deleteVisa, Constant.POST_TYPE.POST, map);
    }

    public void deleteStcw(String stcw_id) {

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "delete_stcw");
        map.put("stcw_id", stcw_id);
        new AsynchHttpRequest(ft, Constant.REQUESTS.deleteStcw, Constant.POST_TYPE.POST, map);
    }

    public void deleteCOC(String cocId) {

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "delete_coc");
        map.put("coc_id", cocId);
        new AsynchHttpRequest(ft, Constant.REQUESTS.deleteCOC, Constant.POST_TYPE.POST, map);
    }

    public void deletePassport(String passport_id) {

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "delete_passport");
        map.put("passport_id", passport_id);
        new AsynchHttpRequest(ft, Constant.REQUESTS.deletePassport, Constant.POST_TYPE.POST, map);
    }

    public void deleteEducation(String cocId) {

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "delete_education");
        map.put("education_id", cocId);
        new AsynchHttpRequest(ft, Constant.REQUESTS.deleteEducation, Constant.POST_TYPE.POST, map);
    }

    public void deleteExperience(String experienceId) {

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "delete_experience");
        map.put("experience_id", experienceId);
        new AsynchHttpRequest(ft, Constant.REQUESTS.deleteExperience, Constant.POST_TYPE.POST, map);
    }

    public void deleteDCE(String dceId) {

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "delete_dce");
        map.put("dce_id", dceId);
        new AsynchHttpRequest(ft, Constant.REQUESTS.deleteDCE, Constant.POST_TYPE.POST, map);
    }

    public void getAppliedJob(String candidateId) {

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_applied_job");
        map.put("candidate_id", candidateId);
        new AsynchHttpRequest(ft, Constant.REQUESTS.getAppliedJob, Constant.POST_TYPE.POST, map);
    }

    public void getPassportDetail(String candidateId) {

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_passport_dtl");
        map.put("candidate_id", candidateId);
        new AsynchHttpRequest(ft, Constant.REQUESTS.getPassportDetail, Constant.POST_TYPE.POST, map);
    }

    public void contactUs() {

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_contact_details");

        new AsynchHttpRequest(ft, Constant.REQUESTS.contactUs, Constant.POST_TYPE.POST, map);
    }

    public void getJobList(String candidateId) {

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_job_list");
        map.put("company_id ", candidateId);
        new AsynchHttpRequest(ft, Constant.REQUESTS.getCompanyJob, Constant.POST_TYPE.POST, map);
    }

    public void updatePassportDetail(String candidateId, String passportNo, String place, String validity, String update, String passportId) {

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "add_update_passport_dtl");
        map.put("candidate_id", candidateId);
        map.put("passport_number", passportNo);
        map.put("place_of_issue", place);
        map.put("validity", validity);
        map.put("update", update);
        map.put("passport_id", passportId);

        new AsynchHttpRequest(ft, Constant.REQUESTS.updatePassportDetail, Constant.POST_TYPE.POST, map);
    }

    public void addPassportDetail(String candidateId, String passportNo, String place, String validity, String update) {

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "add_update_passport_dtl");
        map.put("candidate_id", candidateId);
        map.put("passport_number", passportNo);
        map.put("place_of_issue", place);
        map.put("validity", validity);
        map.put("update", update);
        new AsynchHttpRequest(ft, Constant.REQUESTS.addPassportDetail, Constant.POST_TYPE.POST, map);
    }

    public void register(String company_name, String contact_person, String rsplNo, String utilityDate, String email, String password, String mobile_no, String mcountry_code, String country_code, String tel_no,
                         String std_code, String website, String company_image, String address, String planeId, String city, String pincode, String country_id) {

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "company_register");
        map.put("company_name", company_name);
        map.put("contact_person", contact_person);
        map.put("rpsl_no", rsplNo);
        map.put("validity", utilityDate);
        map.put("email", email);
        map.put("password", password);
        map.put("phone", mobile_no);
        map.put("mcountry_code", mcountry_code);
        map.put("country_code", country_code);
        map.put("telephone", tel_no);
        map.put("std_code", std_code);
        map.put("website", website);
        map.put("company_image", company_image);
        map.put("address", address);
        map.put("plan_id", planeId);
        map.put("city", city);
        map.put("pincode", pincode);
        map.put("country_id", country_id);

        new AsynchHttpRequest(ct, Constant.REQUESTS.register, Constant.POST_TYPE.POST_WITH_IMAGE, map);
    }

    public void insituteRegister(String institute_name, String contact_person, String email, String mobile_no, String mcountry_code, String country_code, String tel_no,
                                 String std_code, String website, String company_image, String address, String city, String pincode, String country_id) {

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "institute_register");
        map.put("institute_name", institute_name);
        map.put("contact_person", contact_person);
        map.put("email", email);
        map.put("phone", mobile_no);
        map.put("mcountry_code", mcountry_code);
        map.put("country_code", country_code);
        map.put("telephone", tel_no);
        map.put("std_code", std_code);
        map.put("website", website);
        map.put("institute_image", company_image);
        map.put("address", address);
        map.put("city", city);
        map.put("pincode", pincode);
        map.put("country_id", country_id);

        new AsynchHttpRequest(ct, Constant.REQUESTS.insituteRegister, Constant.POST_TYPE.POST_WITH_IMAGE, map);
    }

    public void registerCandidate(String first_name, String middle_name, String last_name, String email,
                                  String email2, String dob, String password, String marital_status, String no_of_children, String address, String city_name
            , String zip_code, String nationality, String country_code, String std_code, String phone_no, String mcountry_code, String mobile_no, String mcountry_code2, String mobile_no2, String indos,
                                  String rank_id, String weight, String height, String bmi, String applied_ranks, String joining_type, String available_from, String candidate_image, String gender, String multiSelecteShipTypeId, String countryName) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "candidate_register");
        map.put("first_name", first_name);
        map.put("middle_name", middle_name);
        map.put("last_name", last_name);
        map.put("email", email);
        map.put("email2", email2);
        map.put("weight", weight);
        map.put("height", height);
        map.put("bmi", bmi);
        map.put("dob", dob);
        map.put("password", password);
        map.put("marital_status", marital_status);
        map.put("no_of_children", no_of_children);
        map.put("address", address);
        map.put("city_name", city_name);
        map.put("zip_code", zip_code);
        map.put("nationality", nationality);
        map.put("country_code", country_code);
        map.put("std_code", std_code);
        map.put("phone_no", phone_no);
        map.put("mcountry_code", mcountry_code);
        map.put("mobile_no", mobile_no);
        map.put("mcountry_code2", mcountry_code2);
        map.put("mobile_no2", mobile_no2);
        map.put("indos", indos);
        map.put("rank_id", rank_id);
        map.put("applied_ranks", applied_ranks);
        map.put("joining_type", joining_type);
        map.put("available_from", available_from);
        map.put("candidate_image", candidate_image);
        map.put("gender", gender);
        map.put("applied_ships", multiSelecteShipTypeId);
        map.put("CountryofResidenceID", countryName);
        new AsynchHttpRequest(ct, Constant.REQUESTS.register, Constant.POST_TYPE.POST_WITH_IMAGE, map);
    }

    public void UpdateCandidateProfile(String first_candidateID, String first_name, String middle_name,
                                       String last_name, String email,
                                       String email2, String dob, String marital_status, String no_of_children, String address,
                                       String city_name, String zip_code,
                                       String nationality, String country_code, String std_code, String phone_no, String mcountry_code,
                                       String mobile_no, String mcountry_code2, String mobile_no2,
                                       String indos,
                                       String rank_id, String applied_ranks, String available_from, String candidate_image, String joining_type
            , String gender, String multiSelecteShipTypeId, String countryName, String weight, String height, String bmi) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "update_candidate_data");
        map.put("candidate_id", first_candidateID);
        map.put("first_name", first_name);
        map.put("middle_name", middle_name);
        map.put("last_name", last_name);
        map.put("email", email);
        map.put("email2", email2);
        map.put("dob", dob);
        map.put("marital_status", marital_status);
        map.put("no_of_children", no_of_children);
        map.put("address", address);
        map.put("city_name", city_name);
        map.put("zip_code", zip_code);
        map.put("nationality", nationality);
        map.put("country_code", country_code);
        map.put("std_code", std_code);
        map.put("phone_no", phone_no);
        map.put("mcountry_code", mcountry_code);
        map.put("mobile_no", mobile_no);
        map.put("mcountry_code2", mcountry_code2);
        map.put("mobile_no2", mobile_no2);
        map.put("indos", indos);
        map.put("rank_id", rank_id);
        map.put("applied_ranks", applied_ranks);
        map.put("available_from", available_from);
        map.put("rank_id", rank_id);
        map.put("candidate_image", candidate_image);
        map.put("joining_type", joining_type);
        map.put("gender", gender);
        map.put("applied_ships", multiSelecteShipTypeId);
        map.put("CountryofResidenceID", countryName);
        map.put("weight", weight);
        map.put("height", height);
        map.put("bmi", bmi);
        new AsynchHttpRequest(ft, Constant.REQUESTS.updateFragment, Constant.POST_TYPE.POST_WITH_IMAGE, map);
    }

    public void getExperienceDetail(String usetId) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_experience_dtl");
        map.put("candidate_id", usetId);
        new AsynchHttpRequest(ft, Constant.REQUESTS.getExperience, Constant.POST_TYPE.POST, map);
    }

    public void change_password(String candidate, String old_password, String newPassword, String is_company) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "change_password");
        map.put("company_id", candidate);
        map.put("old_password", old_password);
        map.put("password", newPassword);
        map.put("is_company", is_company);

        new AsynchHttpRequest(ft, Constant.REQUESTS.change_password, Constant.POST_TYPE.POST_WITH_IMAGE, map);
    }


    public void getAppliedShipType() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_ship_type");
        new AsynchHttpRequest(ct, Constant.REQUESTS.getAppliedShip, Constant.POST_TYPE.POST, map);
    }

    public void getAppliedShipTypeFragment() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_ship_type");
        new AsynchHttpRequest(ft, Constant.REQUESTS.getAppliedShip, Constant.POST_TYPE.POST, map);
    }

    public void getPlan() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_plan");
        new AsynchHttpRequest(ct, Constant.REQUESTS.getCompanyPlan, Constant.POST_TYPE.POST, map);
    }

    public void getPlanDetail(String company_id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_company_plan");
        map.put("company_id", company_id);
        new AsynchHttpRequest(ft, Constant.REQUESTS.getConpanyPlanDetail, Constant.POST_TYPE.POST, map);
    }

    public void getDownloadedResume(String company_id, String from_date, String to_date) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_company_download_resume");
        map.put("company_id", company_id);
        map.put("from_date", from_date);
        map.put("to_date", to_date);

        new AsynchHttpRequest(ft, Constant.REQUESTS.getDownloadedResume, Constant.POST_TYPE.POST, map);
    }

    public void getCountry() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_country_list");
        new AsynchHttpRequest(ft, Constant.REQUESTS.getCountryList, Constant.POST_TYPE.POST, map);
    }

    public void getCountryCOC(String userId) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_country_list");
        map.put("user_id", userId);
        new AsynchHttpRequest(ft, Constant.REQUESTS.getCountryListforCOC, Constant.POST_TYPE.POST, map);
    }

    public void ForgotPasswprd(String email) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "forgot_password");
        map.put("email", email);
        new AsynchHttpRequest(ct, Constant.REQUESTS.fogotPassword, Constant.POST_TYPE.POST, map);
    }

    public void getCountryList() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_country_list");
        new AsynchHttpRequest(ct, Constant.REQUESTS.get_country_list, Constant.POST_TYPE.POST, map);
    }

    public void getCountryListFragment() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_country_list");
        new AsynchHttpRequest(ft, Constant.REQUESTS.get_country_list, Constant.POST_TYPE.POST, map);
    }

    public void sendMessage(String name, String email, String phoneNo, String subject, String message) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "contact_us");
        map.put("name", name);
        map.put("email", email);
        map.put("phone", phoneNo);
        map.put("subject", subject);
        map.put("message", message);
        new AsynchHttpRequest(ft, Constant.REQUESTS.contactUs, Constant.POST_TYPE.POST, map);
    }
}
