package com.nfinitydynamics.utils;

import java.util.Arrays;
import java.util.List;

/**
 * Created by admin on 10/12/2016.
 */
public class Constant {

    public static final int READ_WRITE_EXTERNAL_STORAGE_CAMERA = 2355;
    public static final String IMAGE_PREFIX = "";
    public static final int ACCESS_FINE_COARSE_LOCATION =820;
    public static final int ACCESS_FINE_LOCATION =821;
    public static final int NUM_OF_COLUMNS = 3;

    // Gridview image padding
    public static final int GRID_PADDING = 8; // in dp

    // SD card image directory
    public static final String PHOTO_ALBUM = "DCIM/Camera";

    // supported file formats
    public static final List<String> FILE_EXTN = Arrays.asList("jpg", "jpeg",
            "png");
    public enum POST_TYPE {
        GET, POST, POST_WITH_IMAGE,POST_WITH_JSON;
    }
    public enum FILTER_TYPE {
        APPLIEDRANK, SHIPTYPE,COUNTRY;
    }
    public enum REQUESTS {
        invite_member,join_project,add_project,denied_request,accept_request,get_request,contactUs,
        get_invited_joined_people_list,get_people_list,get_message,get_user_project_list,getAllCandidateListSingle,
        get_project_list,get_funding_list,get_indie_picks,get_learning_list,add_lerning_center,getAppliedCandidateList
        ,add_funding,change_password,change_password_candidate,get_comment_list,add_solution,get_category,news_like,news_dislike,updateEducationDetail,
        get_news,get_user_critiques,add_classified,get_classified,add_critiques,scmi_event,rsvp_event,addEducationDetail,
        getCOCDetail,getCertificateType,addCOC,updateCOC,deletePassport,deleteCOC,addDCE,getDCEDetail,updateDCE,deleteDCE,updateCompany,
        get_critiques,edit_event,add_event,get_events,get_user_events,edit_profile,user_profile,login,register,
        addJob,getCompanyJob,updateJob,deleteJob,getAllCandidateList,getCandidateList,getCandidateListRank,getCompanyDetail,
        get_plan,get_profession,get_city,get_country,getMatchJob,getSemeanBook,updateSemeanBook,getPassportDetail,getVisa,
        otherJob,getEngineType,getExperience,updateExperience,deleteExperience,getEducationDetail,deleteEducation,applyNow,getAppliedRankActivity,
        get_state,getLocation,updateFragment,getShipType,addExperience,addSemeanBook,deleteSemean,getAppliedJob,getCompanyListFragment,getConpanyPlanDetail,
        updatePassportDetail,addPassportDetail,getRank,getNationality,featureJob,latestJob,getCandidateDetail,getRankFragment,getNationalityFragment,getCompanyPlan
       , bulidResumeCompany,addVisa,deleteVisa,updateVisa,updateSTWC,getSTWCCertificate,addSWTC,getSTCWDetail,deleteStcw,bulidResume,
        getAppliedRankFragment, getAppliedShip,getDownloadedResume,getCountryList,getCountryListforCOC,reDownlode,fogotPassword,
        previewBuildResume, getShoreJob, insituteRegister, getInstituteListFragment, get_notification_dtl, getTonnageType, checkEmail, updateLastVisited, getCounnt, searchJob, get_country_list
    }
}
