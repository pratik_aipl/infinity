package com.nfinitydynamics.utils;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.StrictMode;
import android.support.multidex.MultiDexApplication;
import android.util.Log;

import com.google.gson.Gson;
import com.nfinitydynamics.SplashScreen;
import com.onesignal.OSNotification;
import com.onesignal.OSNotificationOpenResult;
import com.onesignal.OneSignal;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by admin on 10/12/2016.
 */
public class App extends MultiDexApplication {

    public static App instance;
    public static String IS_LOGGED_IN = "islogedin";
    public SharedPreferences sharedPref;
    public static String userId = "";
    public static String myPref = "InfinityDynamics";
    public static MySharedPref mySharedPref;
    public static String maritualStatus = "";
    public static String pageState = "0";
    public static String childeren = "", rankvalue = "";
    public static boolean isApply = false, isBackToHistory = false;
    public static String profilePic = "", companyName = "", Address = "";
    public static boolean isFromCoc = false;
    public static List<Integer> indexListRank = new ArrayList<>();
    public static List<Integer> indexListShip = new ArrayList<>();
    public static List<Integer> indexListCountry = new ArrayList<>();
    public static String address, city, zipCode, countryResidence;
    public static boolean isFromNotification = false, isback = false, isClickedToAppliedRank = false, isUnavailbale = false, allCandidate = false, postedJobs = false, applicationRecevide = false;
    public static String maritalValue = "", children = "", joiningTypeValue = "", multiSelecteRankId = "", weight = "", height = "", bmi = "", availableFromTemp = "", availableFrom = "", gender = "", multiSelecteShipTypeId = "";
    public String resume_url = "";

    @Override
    public void onCreate() {
        super.onCreate();

        try {
            instance = this;
            sharedPref = getSharedPreferences(getPackageName(), 0);
            mySharedPref = new MySharedPref(this);

            if (sharedPref.contains("user")) {
                Gson gson = new Gson();
                //  user = gson.fromJson(sharedPref.getString("user", ""), User.class);
            }
            Class.forName("android.os.AsyncTask");

        } catch (ClassNotFoundException e) {
        }
        if (Build.VERSION.SDK_INT >= 24) {
            try {
                Method m = StrictMode.class.getMethod("disableDeathOnFileUriExposure");
                m.invoke(null);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            builder.detectFileUriExposure();
        }

        OneSignal.startInit(this)
                .setNotificationOpenedHandler(new OneSignal.NotificationOpenedHandler() {
                    @Override
                    public void notificationOpened(OSNotificationOpenResult result) {
                        try {
                            isFromNotification = true;
                            Intent i = new Intent(getApplicationContext(), SplashScreen.class);
                            i.putExtra("resume_url", resume_url);
                            i.putExtra("isNoti", "1");

                            startActivity(i);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                })
                .setNotificationReceivedHandler(new OneSignal.NotificationReceivedHandler() {
                    @Override
                    public void notificationReceived(OSNotification notification) {

                        //DashboardActivity.redCircleNoti.setVisibility(View.VISIBLE);
                        // DashboardActivity.countTextViewNoti.setText(String.valueOf(App.notiCount));
                        JSONObject additionalData = new JSONObject();
                        try {
                            JSONObject jsonObject = new JSONObject();
                            jsonObject = notification.toJSONObject();
                            Log.i("TAG", "Objh  " + jsonObject.toString());
                            JSONObject jpayload = jsonObject.getJSONObject("payload");
                            additionalData = jpayload.getJSONObject("additionalData");

                            resume_url = additionalData.getString("resume_url");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }).inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification).init();
    }

    public App() {
        setInstance(this);
    }

    public static void setInstance(App instance) {
        App.instance = instance;
    }

    public static App getInstance() {
        return instance;
    }

}
