package com.nfinitydynamics.utils;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import com.nfinitydynamics.R;
import com.onesignal.OneSignal;

import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;


/**
 * Created by admin on 10/12/2016.
 */
public class Utils {

    private static final int READ_EXTERNAL_STORAGE = 1;
    public static Dialog dialog;

    private static ProgressDialog mProgressDialog;

    public static SimpleDateFormat InputDate = new SimpleDateFormat("yyyy-MM-dd");
    public static SimpleDateFormat output = new SimpleDateFormat("MMMM dd, yyyy");
    public static   int mYear;
    public static   int mMonth;
    public static int mDay;
    static String Pleyaer = "";
    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (inputMethodManager.isAcceptingText())
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }
    public static String emailValidation(String email){
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        if (email.matches(emailPattern))
        {
            return "true";
        }
        else
        {
            return "false";
        }
    }

    public static String OneSignalPlearID() {

        OneSignal.idsAvailable(new OneSignal.IdsAvailableHandler() {
            @Override
            public void idsAvailable(String userId, String registrationId) {
                if (userId != null)
                    Pleyaer = userId;
                Log.d("debug", "User:" + userId);

                Log.d("debug", "registrationId:" + registrationId);

            }
        });
        return Pleyaer;
    }
    public static void removeWorkingDialog(Context context) {
        if (!((Activity) context).isFinishing()) {

            if (dialog != null) {
                dialog.dismiss();
                dialog = null;
            }
        }
    }

    public static String currentAppVersionCode(Activity act) {

        PackageInfo pInfo = null;
        try {
            pInfo = act.getPackageManager().getPackageInfo(act.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return pInfo.versionName;
    }
    public static void showProgressDialog(Context context, String msg) {
        try {
            if (dialog == null) {
                dialog = new Dialog(context);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setCancelable(false);
                dialog.setContentView(R.layout.custom_progress_dialog);
            }
            final TextView txtView = (TextView) dialog.findViewById(R.id.textView);
            txtView.setText(msg);

            if (!dialog.isShowing())
                dialog.show();

        } catch (IllegalArgumentException ie) {
            ie.printStackTrace();
        } catch (RuntimeException re) {
            re.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String changeDateToMMDDYYYY(String input) {
        String outputDateStr = "";
        try {
            Date date = InputDate.parse(input);
            outputDateStr = output.format(date);
            //Log.i("output", outputDateStr);
        } catch (ParseException p) {
            p.printStackTrace();
        }
        return outputDateStr;
    }
    public static String getDateFormat(String input, String output, String mydate) {
        SimpleDateFormat srcDf = new SimpleDateFormat(input);

        try {
            Date date = srcDf.parse(mydate);
            SimpleDateFormat destDf = new SimpleDateFormat(output);
            mydate = destDf.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return mydate;
    }
    public static void generateDatePicker(final Context context, final TextView edtDate){
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog dpd;
        dpd = new DatePickerDialog(context,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                     //   edtDate.setText(Utils.getDateFormat("M", "MM", String.valueOf((monthOfYear + 1))) + "-" + Utils.getDateFormat("d", "dd", String.valueOf(dayOfMonth)) + "-" + year);
                        edtDate.setText( Utils.getDateFormat("d", "dd", String.valueOf(dayOfMonth)) + "-" + Utils.getDateFormat("M", "MMM", String.valueOf((monthOfYear + 1)))+ "-" + year);
//                        final String change_date = edtDate.getText().toString();
                    }
                }, mYear, mMonth, mDay);
        dpd.show();



    }
    public static void converDateToDDMMYY(EditText edt, String dateYYMMDD) {

        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat outputFormat = new SimpleDateFormat("dd-MMM-yyyy");
        Date date = null;
        try {
            date = inputFormat.parse(dateYYMMDD);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String ddMMYYDate = outputFormat.format(date);
        System.out.println("converted date===="+ddMMYYDate);
        edt.setText(ddMMYYDate);
    }
    public static void converDateToDDMMYYTextView(TextView edt, String dateYYMMDD) {
        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat outputFormat = new SimpleDateFormat("dd-MMM-yyyy");
        Date date = null;
        try {
            date = inputFormat.parse(dateYYMMDD);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String ddMMYYDate = outputFormat.format(date);
        System.out.println("converted date===="+ddMMYYDate);
        edt.setText(": "+ddMMYYDate);
    }
    public static void showNetworkAlert(Context ct) {
        try {
            final AlertDialog alertDialog = new AlertDialog.Builder(ct).create();

            alertDialog.setTitle("Info");
            alertDialog.setMessage("Internet not available, Cross check your internet connectivity and try again");
            alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
            alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            alertDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void hideProgressDialog() {
        try {
            if (dialog != null) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                    dialog = null;
                }
            }
        } catch (IllegalArgumentException ie) {
            ie.printStackTrace();

        } catch (RuntimeException re) {
            re.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean isNetworkAvailable(Context activity) {

        ConnectivityManager cm =
                (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null && activeNetwork.isConnectedOrConnecting()) {
            return true;
        } else {
            showToast("Please connect to Internet", activity);
            return false;
        }

    }

    public static boolean isOnline(Context Ct) {
        ConnectivityManager cm =
                (ConnectivityManager) Ct.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        } else {
            return false;
        }
    }

    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public static void showAlert(String msg, final Context ctx) {

        new AlertDialog.Builder(ctx)
                .setMessage(msg)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();
                    }
                })
                .show();

    }
    public static void showProgressDialog(Context context) {
        if (!((Activity) context).isFinishing()) {
            showProgressDialog(context, "Please Wait..");
        }
    }
//    public static void showAlertSubmit(String msg, final Context ctx) {
//
//        new AlertDialog.Builder(ctx)
//                .setMessage(msg)
//                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//
//                        ctx.startActivity(new Intent(ctx, NavDrawerActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
//                                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
//                        dialog.dismiss();
//                    }
//                })
//                .show();
//    }
//
//    public static void showAlertLogin(String msg, final Context ctx) {
//
//        new AlertDialog.Builder(ctx)
//                .setMessage(msg)
//                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//
//                        ctx.startActivity(new Intent(ctx, SignInActivity.class)
//                                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
//                                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
//                                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
//                        dialog.dismiss();
//                    }
//                })
//                .show();
//    }

    public static void showToast(String msg, final Context ctx) {
        Toast toast = Toast.makeText(ctx, msg, Toast.LENGTH_LONG);
        toast.show();
    }


    public static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public static boolean checkPhoneStatePermission(Context ct) {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(ct, Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {

                ActivityCompat.requestPermissions((Activity) ct, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        Constant.ACCESS_FINE_LOCATION);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            return true;
        }
    }

    public static boolean checkStoragePermission(Context ct) {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(ct, Manifest.permission.ACCESS_COARSE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {

                ActivityCompat.requestPermissions((Activity) ct, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                        Constant.ACCESS_FINE_COARSE_LOCATION);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            return true;
        }
    }

    public static void setupUI(View view, final Context ct) {

        //Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {

            view.setOnTouchListener(new View.OnTouchListener() {

                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard((Activity) ct);
                    return false;
                }

            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {

            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {

                View innerView = ((ViewGroup) view).getChildAt(i);

                setupUI(innerView, ct);
            }
        }
    }

    // Reading file paths from SDCard
    public static ArrayList<String> getFilePaths(final Context ctx) {
        ArrayList<String> filePaths = new ArrayList<String>();

        File directory = new File(
                android.os.Environment.getExternalStorageDirectory()
                        + File.separator + Constant.PHOTO_ALBUM);

        // check for directory
        if (directory.isDirectory()) {
            // getting list of file paths
            File[] listFiles = directory.listFiles();

            // Check for count
            if (listFiles.length > 0) {

                // loop through all files
                for (int i = 0; i < listFiles.length; i++) {

                    // get file path
                    String filePath = listFiles[i].getAbsolutePath();

                    // check for supported file extension
                    if (IsSupportedFile(filePath)) {
                        // Add image path to array list
                        filePaths.add(filePath);
                    }
                }
            } else {
                // image directory is empty
                Toast.makeText(ctx, Constant.PHOTO_ALBUM
                                + " is empty. Please load some images in it !",
                        Toast.LENGTH_LONG).show();
            }

        } else {
            AlertDialog.Builder alert = new AlertDialog.Builder(ctx);
            alert.setTitle("Error!");
            alert.setMessage(Constant.PHOTO_ALBUM
                    + " directory path is not valid! Please set the image directory name AppConstant.java class");
            alert.setPositiveButton("OK", null);
            alert.show();
        }

        return filePaths;
    }

    public static boolean checkPermission(final Context context) {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
                    alertBuilder.setCancelable(true);
                    alertBuilder.setTitle("Permission necessary");
                    alertBuilder.setMessage("External storage permission is necessary");
                    alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},READ_EXTERNAL_STORAGE);
                        }
                    });
                    AlertDialog alert = alertBuilder.create();
                    alert.show();
                } else {
                    ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},READ_EXTERNAL_STORAGE);
                }
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    // Check supported file extensions
    private static boolean IsSupportedFile(String filePath) {
        String ext = filePath.substring((filePath.lastIndexOf(".") + 1),
                filePath.length());

        if (Constant.FILE_EXTN
                .contains(ext.toLowerCase(Locale.getDefault())))
            return true;
        else
            return false;

    }


    public static String setDate(String date) {
        String CurrentString = date;
       String[] separated = CurrentString.split("-");
        String sStartMonth;
        String sShortMonthName;
        final String s = separated[0];
        final String s1 = separated[1];
        String sFinal = String.valueOf(s1.split("-"));
        System.out.println("spilits" + s1);
        if (s1.equalsIgnoreCase("01")) {
            sShortMonthName="Jan";
            sStartMonth = "January";
        } else if (s1.equalsIgnoreCase("02")) {
            sShortMonthName="Feb";
            sStartMonth = "February";
        } else if (s1.equalsIgnoreCase("03")) {
            sShortMonthName="Mar";
            sStartMonth = "March";
        } else if (s1.equalsIgnoreCase("04")) {
            sShortMonthName="Apr";
            sStartMonth = "April";
        } else if (s1.equalsIgnoreCase("05")) {
            sShortMonthName="May";
            sStartMonth = "May";
        } else if (s1.equalsIgnoreCase("06")) {
            sShortMonthName="Jun";
            sStartMonth = "June";
        } else if (s1.equalsIgnoreCase("07")) {
            sShortMonthName="Jul";
            sStartMonth = "July";
        } else if (s1.equalsIgnoreCase("08")) {
            sShortMonthName="Aug";
            sStartMonth = "August";
        } else if (s1.equalsIgnoreCase("09")) {
            sShortMonthName="Sep";
            sStartMonth = "September";
        } else if (s1.equalsIgnoreCase("10")) {
            sShortMonthName="Oct";
            sStartMonth = "October";
        } else if (s1.equalsIgnoreCase("11")) {
            sShortMonthName="Nov";
            sStartMonth = "November";
        } else if (s1.equalsIgnoreCase("12")) {
            sShortMonthName="Dec";
            sStartMonth = "December";
        }
        String asubstring = CurrentString.substring(8, 10);
        System.out.println("Date" + asubstring);
        return asubstring;


    }

    /*
     * getting screen width
     */
    public static int getScreenWidth(final Context ctx) {
        int columnWidth;
        WindowManager wm = (WindowManager) ctx
                .getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();

        final Point point = new Point();
        try {
            display.getSize(point);
        } catch (NoSuchMethodError ignore) { // Older device
            point.x = display.getWidth();
            point.y = display.getHeight();
        }
        columnWidth = point.x;
        return columnWidth;
    }
}





