package com.nfinitydynamics.adapter.CompanyAdapter;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.nfinitydynamics.R;
import com.nfinitydynamics.activity.CompanyActivity.CompanyDashboard;
import com.nfinitydynamics.fragment.COCFragment;
import com.nfinitydynamics.fragment.companyFragment.AddJobsFragment;
import com.nfinitydynamics.fragment.companyFragment.CandidateFragment;
import com.nfinitydynamics.model.LatestJobModel;
import com.nfinitydynamics.utils.CallRequest;
import com.nfinitydynamics.utils.Utils;

import java.util.ArrayList;

import static com.nfinitydynamics.fragment.companyFragment.ManageCompanyJobFragment.instance;

/**
 * Created by User on 23-02-2018.
 */

public class CompanyJobAdapter extends ArrayAdapter<LatestJobModel> {

    public ArrayList<LatestJobModel> latestJobList = new ArrayList<>();
    public Context context;
    public String id, cName, shipId, rankID, leavingDate, description, experienceRange, jobTitle, isFeatured, isShor,
            isFront,expFrom,expTo;

    public CompanyJobAdapter(@NonNull Context context, int resource, @NonNull ArrayList<LatestJobModel> latestJobList) {
        super(context, resource, latestJobList);
        this.context = context;
        this.latestJobList = latestJobList;
    }

    View view;

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        LayoutInflater infalInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = infalInflater.inflate(R.layout.layout_company_job_list, parent, false);
        // ImageView ivJobIcon = (ImageView) view.findViewById(R.id.iv_latest_job_icon);
      //  TextView txtCompany = view.findViewById(R.id.txt_comp_name_value);
        TextView txtRank = view.findViewById(R.id.txt_rank_value);
        TextView txtShip = view.findViewById(R.id.txt_ship_value);
       // TextView txtJobTitle = view.findViewById(R.id.txt_job_title_value);
        TextView txtEndDate = view.findViewById(R.id.txt_end_date_value);
        TextView txtExpFrom=view.findViewById(R.id.txt_exp_from);
        TextView txtExpTo=view.findViewById(R.id.txt_exp_to);
        RelativeLayout viewJob = view.findViewById(R.id.relative_view);
        RelativeLayout deleteJob = view.findViewById(R.id.relative_delete);
        RelativeLayout editJob = view.findViewById(R.id.relative_edit);
      //  txtJobTitle.setText(": " + latestJobList.get(position).getJobTitle());
        txtShip.setText(": " + latestJobList.get(position).getShipType());
        txtRank.setText(": " + latestJobList.get(position).getRank());
       // txtCompany.setText(": " + latestJobList.get(position).getCompanyName());
        txtExpFrom.setText(": "+ latestJobList.get(position).getFromExperience());
        txtExpTo.setText(": "+ latestJobList.get(position).getToExperience());
       // txtEndDate.setText(": " + latestJobList.get(position).getJobEndDate());

        Utils.converDateToDDMMYYTextView(txtEndDate,latestJobList.get(position).getJobEndDate());
        latestJobList.get(position).getRank();

        editJob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LatestJobModel model = latestJobList.get(position);
                id = model.getJobID();
                cName = model.getCompanyName();
                rankID = model.getRank();
                shipId = model.getShipType();
                leavingDate = model.getJobEndDate();
                description = model.getDescription();
                experienceRange = model.getExperience();
                jobTitle = model.getJobTitle();
                isFeatured=model.getIsFeatured();
                isShor=model.getIsShore();
                isFront=model.getIsFront();
                expFrom=model.getFromExperience();
                expTo=model.getToExperience();
                AddJobsFragment addJobsFragment = new AddJobsFragment();
                Bundle args = new Bundle();
                args.putInt("value", 1);
                args.putString("id", id);
                args.putString("cName", cName);
                args.putString("rank", rankID);
                args.putString("shipType", shipId);
                args.putString("leavingDate", leavingDate);
                args.putString("description", description);
                args.putString("experienceRange", experienceRange);
                args.putString("jobTitle", jobTitle);
                args.putString("isFeatured", isFeatured);
                args.putString("isShor", isShor);
                args.putString("isFront", isFront);
                args.putString("expFrom",expFrom);
                args.putString("expTo",expTo);
                addJobsFragment.setArguments(args);
                CompanyDashboard.changeFragment(addJobsFragment, true);
            }
        });

        deleteJob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                delete(position);

            }
        });
        viewJob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CandidateFragment candidateFragment = new CandidateFragment();
                Bundle b = new Bundle();
                String jobId = latestJobList.get(position).getJobID();
                b.putInt("value", 4);
                b.putString("jobId", jobId);
                candidateFragment.setArguments(b);
                CompanyDashboard.changeFragment(candidateFragment, true);
            }
        });
        return view;
    }

    private void delete(final int position) {
        final MaterialDialog dialog = new MaterialDialog.Builder(context)
                .title("Delete")
                .content("Want to Delete This Record ?")
                .positiveText("Ok")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        new CallRequest(instance).deleteJob(latestJobList.get(position).getJobID());

                        notifyDataSetChanged();

                    }
                })
                .negativeText("Cancel")
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .build();
        dialog.show();
    }
}