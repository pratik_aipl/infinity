package com.nfinitydynamics.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nfinitydynamics.R;
import com.nfinitydynamics.model.LatestJobModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by User on 01-02-2018.
 */

public class LatestJobAdapter extends ArrayAdapter<LatestJobModel> {

    public ArrayList<LatestJobModel> latestJobList = new ArrayList<>();
    public  Context context;
    public Date date1, date2;

    public LatestJobAdapter(@NonNull Context context, int resource, @NonNull ArrayList<LatestJobModel> latestJobList) {
        super(context, resource, latestJobList);
        this.context = context;
        this.latestJobList = latestJobList;
    }

    View view;

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        LayoutInflater infalInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = infalInflater.inflate(R.layout.layout_home_listview, parent, false);
        ImageView ivJobIcon =  view.findViewById(R.id.iv_latest_job_icon);
        TextView txtJobTitle = view.findViewById(R.id.txt_latest_job_title);
        TextView txtSubTitle =  view.findViewById(R.id.txt_latest_job_subtitle);
        txtJobTitle.setText(latestJobList.get(position).getJobTitle());
        txtSubTitle.setText(latestJobList.get(position).getShipType());
        latestJobList.get(position).getRank();
        if (!latestJobList.get(position).getLogoImgPath().isEmpty())
            Picasso.with(context).load(latestJobList.get(position).getLogoImgPath()).error(R.drawable.no_image).placeholder(R.drawable.no_image).into(ivJobIcon);
        return view;
    }
}
