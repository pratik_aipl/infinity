package com.nfinitydynamics.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nfinitydynamics.R;
import com.nfinitydynamics.activity.MainActivity;
import com.nfinitydynamics.model.FeaturedJobModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by User on 19-02-2018.
 */

public class SeanspanOtherJobsAdapter extends RecyclerView.Adapter<SeanspanOtherJobsAdapter.MyViewHolder> {

    private ArrayList<FeaturedJobModel> featuredJobList;
    public Date date1, date2;
    public Context context;
    @Override
    public SeanspanOtherJobsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_home_slider, parent, false);

        return new SeanspanOtherJobsAdapter.MyViewHolder(itemView);
    }
    @Override
    public void onBindViewHolder(SeanspanOtherJobsAdapter.MyViewHolder holder, int position) {

        holder.txtJobTitle.setText(featuredJobList.get(position).getJobTitle());
        holder.txtSubTitle.setText(featuredJobList.get(position).getShipType());
        String description=featuredJobList.get(position).getDescription();
        String companyId=featuredJobList.get(position).getCompanyID();
        Picasso.with(MainActivity.activity).load(featuredJobList.get(position).getLogoImgPath()).into(holder.ivJobIcon);
    }

    @Override
    public int getItemCount() {
        return featuredJobList.size();
    }

    public SeanspanOtherJobsAdapter(ArrayList<FeaturedJobModel> featuredJobList) {
        this.featuredJobList = featuredJobList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView ivJobIcon;
        TextView txtJobTitle;
        TextView txtSubTitle;
        TextView txtJobPostDays;

        public MyViewHolder(View view) {
            super(view);

            ivJobIcon =  view.findViewById(R.id.iv_logo);
            txtJobTitle =  view.findViewById(R.id.txt_job_title);
            txtSubTitle =  view.findViewById(R.id.txt_sub_title_job);
        }


    }
}