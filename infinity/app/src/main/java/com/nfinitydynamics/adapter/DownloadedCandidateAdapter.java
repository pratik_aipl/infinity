package com.nfinitydynamics.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nfinitydynamics.R;
import com.nfinitydynamics.model.DownloadedCandidateModel;
import com.nfinitydynamics.model.company.CandidateModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by User on 21-06-2018.
 */

public class DownloadedCandidateAdapter extends ArrayAdapter<DownloadedCandidateModel> {
    public ArrayList<DownloadedCandidateModel> List = new ArrayList<>();
    public Context context;
    public String id, number, validity, place, certificateType, CertificateId;
    public View view;
    public SharedPreferences shared;
    public String candidateId;

    public DownloadedCandidateAdapter(@NonNull Context context, int resource, @NonNull ArrayList<DownloadedCandidateModel> List) {
        super(context, resource, List);
        this.context = context;
        this.List = List;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        LayoutInflater infalInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = infalInflater.inflate(R.layout.layout_candidate, parent, false);
        shared = getContext().getSharedPreferences(getContext().getPackageName(), 0);
        candidateId = shared.getString("userId", "");

        TextView txtName=view.findViewById(R.id.txt_name_value);
        TextView txtRankValue=view.findViewById(R.id.txt_rank_value);
        TextView txtShipType=view.findViewById(R.id.txt_ship_value);
        ImageView ivProfile=view.findViewById(R.id.ivCandidate);
        TextView txtLastVisited=view.findViewById(R.id.txt_lastvisited_value);
        DownloadedCandidateModel model=List.get(position);
        txtName.setText(model.getCanidateName());
        txtRankValue.setText(model.getRankName());
        txtLastVisited.setText(model.getDownloadedDate());
        Picasso.with(getContext()).load(model.getImageURL()).placeholder(R.drawable.profilepic).error(R.drawable.profilepic).into(ivProfile);

        return view;
    }

}