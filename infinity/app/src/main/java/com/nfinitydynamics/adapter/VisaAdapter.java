package com.nfinitydynamics.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.nfinitydynamics.R;
import com.nfinitydynamics.activity.MainActivity;
import com.nfinitydynamics.fragment.AddVisaDetailFragment;
import com.nfinitydynamics.fragment.SeamanBookFragment;
import com.nfinitydynamics.model.VisaModel;
import com.nfinitydynamics.utils.CallRequest;
import com.nfinitydynamics.utils.Utils;

import java.util.ArrayList;

import static com.nfinitydynamics.fragment.VisaDetailFragment.instance;

/**
 * Created by User on 14-03-2018.
 */

public class VisaAdapter extends ArrayAdapter<VisaModel> {
    public ArrayList<VisaModel> listModel = new ArrayList<>();
    public Context context;
    public String id, number, validity, place, certificateType, visaType;
    public View view;
    public SharedPreferences shared;
    public String candidateId;

    public VisaAdapter(@NonNull Context context, int resource, @NonNull ArrayList<VisaModel> listModel) {
        super(context, resource, listModel);
        this.context = context;
        this.listModel = listModel;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        LayoutInflater infalInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = infalInflater.inflate(R.layout.layout_visa, parent, false);

        ImageView ivEdit =  view.findViewById(R.id.iv_edit);
        ImageView ivDelete =  view.findViewById(R.id.iv_delete);
        TextView txtValidity = view.findViewById(R.id.txt_validity_value);
        TextView txtPlaceIssue =  view.findViewById(R.id.txt_place_issue_value);
        TextView txtVisaTypeValue =  view.findViewById(R.id.txt_visa_type_value);
        shared = getContext().getSharedPreferences(getContext().getPackageName(), 0);
        candidateId = shared.getString("userId", "");
        validity = listModel.get(position).getExpiryDate();
        place = listModel.get(position).getPlaceofIssue();
        visaType = listModel.get(position).getVisaType();
        txtPlaceIssue.setText(": " + place);
        Utils.converDateToDDMMYYTextView(txtValidity, validity);
        txtVisaTypeValue.setText(": " + visaType);
        ivEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                VisaModel model = listModel.get(position);
                id = model.getVisaDetailsID();
                validity = model.getExpiryDate();
                place = model.getPlaceofIssue();
                visaType = model.getVisaType();
                AddVisaDetailFragment fragment = new AddVisaDetailFragment();
                Bundle args = new Bundle();
                args.putInt("value", 1);
                args.putString("id", id);
                args.putString("place", place);
                args.putString("validity", validity);
                args.putString("visaType", visaType);
                fragment.setArguments(args);
                MainActivity.changeFragment(fragment, true);
            }
        });

        ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              delete(position);
            }
        });
        return view;
    }

    private void delete(final int position) {
        final MaterialDialog dialog = new MaterialDialog.Builder(context)
                .title("Delete")
                .content("Want to Delete This Record ?")
                .positiveText("Ok")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        new CallRequest(instance).deleteVisa(listModel.get(position).getVisaDetailsID());
                        notifyDataSetChanged();

                    }
                })
                .negativeText("Cancel")
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .build();
        dialog.show();
    }

}