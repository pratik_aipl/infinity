package com.nfinitydynamics.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.nfinitydynamics.R;
import com.nfinitydynamics.activity.MainActivity;
import com.nfinitydynamics.fragment.AddCOCFragment;
import com.nfinitydynamics.fragment.VisaDetailFragment;
import com.nfinitydynamics.model.COCModel;
import com.nfinitydynamics.utils.CallRequest;
import com.nfinitydynamics.utils.Utils;

import java.util.ArrayList;
import java.util.Date;

import static com.nfinitydynamics.fragment.COCFragment.instance;

/**
 * Created by User on 13-02-2018.
 */

public class COCAdapter extends ArrayAdapter<COCModel> {
    public ArrayList<COCModel> cocList = new ArrayList<>();
    public Context context;
    public String id, number, validity, place, certificateType, CertificateId;
    public View view;
    public SharedPreferences shared;
    public String candidateId;

    public COCAdapter(@NonNull Context context, int resource, @NonNull ArrayList<COCModel> cocList) {
        super(context, resource, cocList);
        this.context = context;
        this.cocList = cocList;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        LayoutInflater infalInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = infalInflater.inflate(R.layout.layout_coc, parent, false);
        ImageView ivEdit =  view.findViewById(R.id.iv_edit);
        ImageView ivDelete =  view.findViewById(R.id.iv_delete);
        TextView txtNumber =  view.findViewById(R.id.txt_number_value);
        TextView txtValidity =  view.findViewById(R.id.txt_validity_value);
        TextView txtPlaceIssue =  view.findViewById(R.id.txt_place_issue_value);
        TextView txtCertificateType =  view.findViewById(R.id.txt_certificate_type_value);
        shared = getContext().getSharedPreferences(getContext().getPackageName(), 0);
        candidateId = shared.getString("userId", "");
        //id = stcwList.get(position).getCOCDetailsID();

      //  id = model.getCOCDetailsID();
        validity = cocList.get(position).getValidity();
        number = cocList.get(position).getCertificateNo();
        place = cocList.get(position).getAuthority();
        certificateType = cocList.get(position).getCertificateType();
        CertificateId = cocList.get(position).getCertificateID();
        txtNumber.setText(": " + number);
        txtPlaceIssue.setText(": " + place);
        Utils.converDateToDDMMYYTextView(txtValidity,validity);
       // txtValidity.setText(": " + validity);
        txtCertificateType.setText(": " + certificateType);
        ivEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                COCModel model=cocList.get(position);
                id = model.getCOCDetailsID();
                validity = model.getValidity();
                number = model.getCertificateNo();
                place = model.getAuthority();
                certificateType = model.getCertificateType();
                CertificateId = model.getCertificateID();
                AddCOCFragment fragment = new AddCOCFragment();
                Bundle args = new Bundle();
                args.putInt("value", 1);
                args.putString("id", id);
                args.putString("number", number);
                args.putString("place", place);
                args.putString("validity", validity);
                args.putString("certificateType", certificateType);
                fragment.setArguments(args);
                MainActivity.changeFragment(fragment, true);
            }
        });

        ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              delete(position);
            }
        });
        return view;
    }

    private void delete(final int position) {
        final MaterialDialog dialog = new MaterialDialog.Builder(context)
                .title("Delete")
                .content("Want to Delete This Record ?")
                .positiveText("Ok")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        new CallRequest(instance).deleteCOC(cocList.get(position).getCOCDetailsID());
                        notifyDataSetChanged();

                    }
                })
                .negativeText("Cancel")
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .build();
        dialog.show();
    }
}