package com.nfinitydynamics.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nfinitydynamics.R;
import com.nfinitydynamics.activity.MainActivity;
import com.nfinitydynamics.model.FeaturedJobModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by User on 05-02-2018.
 */

public class FeaturedJobAdapter extends RecyclerView.Adapter<FeaturedJobAdapter.MyViewHolder> {

    public ArrayList<FeaturedJobModel> featuredJobList;
    Context context;

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_home_slider, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        holder.txtJobTitle.setText(featuredJobList.get(position).getJobTitle());
        holder.txtSubTitle.setText(featuredJobList.get(position).getShipType());
        String companyId = featuredJobList.get(position).getCompanyID();
        String rank = featuredJobList.get(position).getRank();
        try {
            Picasso.with(MainActivity.activity).load(featuredJobList.get(position).getLogoImgPath()).error(R.drawable.no_image).placeholder(R.drawable.no_image).into(holder.ivJobIcon);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return featuredJobList.size();
    }

    public FeaturedJobAdapter(ArrayList<FeaturedJobModel> featuredJobList) {
        this.featuredJobList = featuredJobList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView ivJobIcon;
        TextView txtJobTitle;
        TextView txtSubTitle;
        TextView txtJobPostDays;

        public MyViewHolder(View view) {
            super(view);

            ivJobIcon = view.findViewById(R.id.iv_logo);
            txtJobTitle = view.findViewById(R.id.txt_job_title);
            txtSubTitle = view.findViewById(R.id.txt_sub_title_job);


        }


    }
}