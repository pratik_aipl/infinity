package com.nfinitydynamics.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.nfinitydynamics.R;
import com.nfinitydynamics.activity.MainActivity;
import com.nfinitydynamics.fragment.AddSTCWFragment;
import com.nfinitydynamics.fragment.COCFragment;
import com.nfinitydynamics.model.STCWModel;
import com.nfinitydynamics.utils.CallRequest;
import com.nfinitydynamics.utils.Utils;

import java.util.ArrayList;

import static com.nfinitydynamics.fragment.STCWDetailFragment.instance;

/**
 * Created by User on 14-03-2018.
 */

public class STCWAdapter extends ArrayAdapter<STCWModel> {
    public ArrayList<STCWModel> listModel = new ArrayList<>();
    public Context context;
    public String id, number, validity, place, certificateType;
    public View view;
    public SharedPreferences shared;
    public String candidateId;

    public STCWAdapter(@NonNull Context context, int resource, @NonNull ArrayList<STCWModel> listModel) {
        super(context, resource, listModel);
        this.context = context;
        this.listModel = listModel;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater infalInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = infalInflater.inflate(R.layout.layout_stcw, parent, false);
        ImageView ivEdit =  view.findViewById(R.id.iv_edit);
        ImageView ivDelete =  view.findViewById(R.id.iv_delete);
        TextView txtNumber =  view.findViewById(R.id.txt_number_value);
        TextView txtValidity =  view.findViewById(R.id.txt_validity_value);
        TextView txtPlaceIssue =  view.findViewById(R.id.txt_place_issue_value);
        TextView txtCertificateType =  view.findViewById(R.id.txt_certificate_type_value);
        shared = getContext().getSharedPreferences(getContext().getPackageName(), 0);
        candidateId = shared.getString("userId", "");
        validity = listModel.get(position).getValidity();
        number = listModel.get(position).getCertificateNo();
        place = listModel.get(position).getAuthority();
        certificateType=listModel.get(position).getSTCWCertificateType();
        txtNumber.setText(": " + number);
        txtPlaceIssue.setText(": " + place);
        if(!validity.equalsIgnoreCase("")){
            Utils.converDateToDDMMYYTextView(txtValidity,validity);

        }else{
            txtValidity.setText("");
        }
        txtCertificateType.setText(": "+ certificateType);

        ivEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                STCWModel model=listModel.get(position);
                AddSTCWFragment fragment = new AddSTCWFragment();
                Bundle args = new Bundle();
                args.putInt("value", 1);
                args.putSerializable("STCWModel", model);
                args.putString("certificateType", model.getSTCWCertificateType());
                Log.d("",id   +number);
                fragment.setArguments(args);
                MainActivity.changeFragment(fragment, true);
            }
        });

        ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              delete(position);
            }
        });

        return view;
    }

    private void delete(final int position) {
        final MaterialDialog dialog = new MaterialDialog.Builder(context)
                .title("Delete")
                .content("Want to Delete This Record ?")
                .positiveText("Ok")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        new CallRequest(instance).deleteStcw(listModel.get(position).getSTWCCertiDetailsID());
                        notifyDataSetChanged();
                    }
                })
                .negativeText("Cancel")
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .build();
        dialog.show();
    }

}