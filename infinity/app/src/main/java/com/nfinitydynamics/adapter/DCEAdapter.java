package com.nfinitydynamics.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.nfinitydynamics.R;
import com.nfinitydynamics.activity.MainActivity;
import com.nfinitydynamics.fragment.AddDCEFragment;
import com.nfinitydynamics.fragment.COCFragment;
import com.nfinitydynamics.model.DCEModel;
import com.nfinitydynamics.utils.CallRequest;
import com.nfinitydynamics.utils.Utils;

import java.util.ArrayList;

import static com.nfinitydynamics.fragment.DCEFragment.instance;

/**
 * Created by User on 13-02-2018.
 */

public class DCEAdapter extends ArrayAdapter<DCEModel> {
    public ArrayList<DCEModel> dceList = new ArrayList<>();
    public Context context;
    public String id, number, validity, place, certificateType, CertificateId, level;
    public View view;
    public SharedPreferences shared;
    public String candidateId;

    public DCEAdapter(@NonNull Context context, int resource, @NonNull ArrayList<DCEModel> dceList) {
        super(context, resource, dceList);
        this.context = context;
        this.dceList = dceList;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        LayoutInflater infalInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = infalInflater.inflate(R.layout.layout_dce, parent, false);
        ImageView ivEdit =  view.findViewById(R.id.iv_edit);
        ImageView ivDelete =  view.findViewById(R.id.iv_delete);
        TextView txtNumber =  view.findViewById(R.id.txt_number_value);
        TextView txtValidity =  view.findViewById(R.id.txt_validity_value);
        TextView txtPlaceIssue =  view.findViewById(R.id.txt_place_issue_value);
        TextView txtCertificateType = view.findViewById(R.id.txt_certificate_type_value);
        TextView txtLevel =  view.findViewById(R.id.txt_level_value);
        shared = getContext().getSharedPreferences(getContext().getPackageName(), 0);
        candidateId = shared.getString("userId", "");
       // id = dceList.get(position).getDCEDetailID();
        validity = dceList.get(position).getValidity();
        number = dceList.get(position).getCertificateNo();
        place = dceList.get(position).getAuthority();
        certificateType = dceList.get(position).getCertificateType();
        CertificateId = dceList.get(position).getCertificateID();
        level = dceList.get(position).getLevel();
        txtNumber.setText(": " + number);
        txtPlaceIssue.setText(": " + place);
        Utils.converDateToDDMMYYTextView(txtValidity,validity);
        txtCertificateType.setText(": " + certificateType);
        txtLevel.setText(": " + level);
        ivEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DCEModel model=dceList.get(position);
                id = model.getDCEDetailID();
                validity = model.getValidity();
                number = model.getCertificateNo();
                place = model.getAuthority();
                certificateType = model.getCertificateType();
                CertificateId = model.getCertificateID();
               // level = model.getLevel();

                AddDCEFragment fragment = new AddDCEFragment();
                Bundle args = new Bundle();
                args.putInt("value", 1);
                args.putString("id", id);
                args.putString("number", number);
                args.putString("place", place);
                args.putString("validity", validity);
                args.putString("certificateType", certificateType);
                args.putString("level", level);
                Log.d("certificateType =====", certificateType);
                fragment.setArguments(args);
                MainActivity.changeFragment(fragment, true);
            }
        });

        ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               delete(position);
            }
        });
        return view;
    }

    private void delete(final int position) {
        final MaterialDialog dialog = new MaterialDialog.Builder(context)
                .title("Delete")
                .content("Want to Delete This Record ?")
                .positiveText("Ok")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        new CallRequest(instance).deleteDCE(dceList.get(position).getDCEDetailID());
                        notifyDataSetChanged();
                    }
                })
                .negativeText("Cancel")
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .build();
        dialog.show();
    }

}