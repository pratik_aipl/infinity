package com.nfinitydynamics.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.nfinitydynamics.R;
import com.nfinitydynamics.activity.MainActivity;
import com.nfinitydynamics.fragment.AddExperienceFragment;
import com.nfinitydynamics.fragment.EducationDetailFragment;
import com.nfinitydynamics.model.ExperienceModel;
import com.nfinitydynamics.utils.CallRequest;
import com.nfinitydynamics.utils.Utils;

import java.util.ArrayList;
import java.util.Date;

import static com.nfinitydynamics.fragment.ExperienceFragment.instance;


/**
 * Created by User on 20-02-2018.
 */

public class ExperienceAdapter extends ArrayAdapter<ExperienceModel>

{
    public ArrayList<ExperienceModel> experienceList = new ArrayList<>();
    public Context context;
    public Date date1, date2;
    public String id, cName, shipType, rank, joingDate, leavingDate,shipName,grt,seaDuration,engineType,tonnageType,tonnageValue;
    public View view;
    public SharedPreferences shared;
    public String candidateId;

    public ExperienceAdapter(@NonNull Context context, int resource, @NonNull ArrayList<ExperienceModel> experienceList) {
        super(context, resource, experienceList);
        this.context = context;
        this.experienceList = experienceList;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        LayoutInflater infalInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = infalInflater.inflate(R.layout.layout_experience, parent, false);
        ImageView ivEdit =  view.findViewById(R.id.iv_edit);
        ImageView ivDelete =  view.findViewById(R.id.iv_delete);
        TextView txtCompanyName =  view.findViewById(R.id.txt_company_name_v);
        TextView txtShipType =  view.findViewById(R.id.txt_ship_type_value);
        TextView txtRankValue =  view.findViewById(R.id.txt_rank_value);
        TextView txtJDate =  view.findViewById(R.id.txt_j_date_value);
        TextView txtLDate =  view.findViewById(R.id.txt_l_date_value);
        TextView txtEngineType =  view.findViewById(R.id.txt_engine_type_v);
        TextView txtShipName =  view.findViewById(R.id.txt_ship_name_v);
        TextView txtGrt =  view.findViewById(R.id.txt_grt_v);
        TextView txt_tonnage_value =  view.findViewById(R.id.txt_tonnage_value_v);
        TextView txtSeaDuration =  view.findViewById(R.id.txt_sea_duration_v);
        shared = getContext().getSharedPreferences(getContext().getPackageName(), 0);
        candidateId = shared.getString("userId", "");
        //id = stcwList.get(position).getCOCDetailsID();

        //  id = model.getCOCDetailsID();
        cName = experienceList.get(position).getCompanyName();
        rank = experienceList.get(position).getRank();
        shipType = experienceList.get(position).getShipType();
        joingDate = experienceList.get(position).getJoiningDate();
        leavingDate = experienceList.get(position).getLeavingDate();
        engineType=experienceList.get(position).getEngineName();
        shipName=experienceList.get(position).getShipName();
        grt=experienceList.get(position).getTonnageType();
        seaDuration=experienceList.get(position).getTotalSeaDuration();
        tonnageType=experienceList.get(position).getTonnageType();
        tonnageValue=experienceList.get(position).getTonnageValue();

        txtCompanyName.setText(": " + cName);
        txtRankValue.setText(": " + rank);
        txtShipType.setText(": " + shipType);
        Utils.converDateToDDMMYYTextView(txtJDate,joingDate);
        Utils.converDateToDDMMYYTextView(txtLDate,leavingDate);
        //txtJDate.setText(": " + joingDate);
       // txtLDate.setText(": "+ leavingDate);
        txtEngineType.setText(": " + engineType);
        txtShipName.setText(": " + shipName);
        txtGrt.setText(": " + grt);
        txtSeaDuration.setText(": " + seaDuration);
        txt_tonnage_value.setText(": " + tonnageValue);
        ivEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ExperienceModel model = experienceList.get(position);
                id = model.getExperienceID();
                cName = model.getCompanyName();
                rank = model.getRank();
                shipType = model.getShipType();
                joingDate = model.getJoiningDate();
                leavingDate = model.getLeavingDate();
                grt=model.getGRT();
                shipName=model.getShipName();
                seaDuration=model.getTotalSeaDuration();
                engineType=model.getEngineName();
                tonnageType=model.getTonnageType();
                tonnageValue=model.getTonnageValue();
                AddExperienceFragment fragment = new AddExperienceFragment();
                Bundle args = new Bundle();
                args.putInt("value", 1);
                args.putString("id", id);
                args.putString("cName", cName);
                args.putString("rank", rank);
                args.putString("shipType", shipType);
                args.putString("joingDate", joingDate);
                args.putString("leavingDate", leavingDate);
                args.putString("grt", grt);
                args.putString("shipName", shipName);
                args.putString("seaDuration", seaDuration);
                args.putString("engineType", engineType);
                args.putString("tonnageType", tonnageType);
                args.putString("tonnageValue", tonnageValue);

                // Log.d("", id + number);
                fragment.setArguments(args);
                MainActivity.changeFragment(fragment, true);
            }
        });

        ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               delete(position);
            }
        });
        return view;
    }

    private void delete(final int position) {
        final MaterialDialog dialog = new MaterialDialog.Builder(context)
                .title("Delete")
                .content("Want to Delete This Record ?")
                .positiveText("Ok")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                        new CallRequest(instance).deleteExperience(experienceList.get(position).getExperienceID());
                        notifyDataSetChanged();
                    }
                })
                .negativeText("Cancel")
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .build();
        dialog.show();
    }

}
