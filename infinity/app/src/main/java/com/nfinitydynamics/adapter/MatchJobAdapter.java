package com.nfinitydynamics.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nfinitydynamics.R;
import com.nfinitydynamics.model.MatchJobModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by User on 09-02-2018.
 */

public class MatchJobAdapter  extends ArrayAdapter<MatchJobModel> {

    public ArrayList<MatchJobModel> recentJobList;
    public List<MatchJobModel> searchDoctorFilterList=new ArrayList<>();
    public  Context context;
    public MatchJobAdapter(@NonNull Fragment context, int resource, @NonNull ArrayList<MatchJobModel> recentJobList) {
        super(context.getActivity(), resource, recentJobList);
        this.recentJobList=recentJobList;
        this.context=context.getActivity();
        this.searchDoctorFilterList.addAll(recentJobList);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view;
        LayoutInflater infalInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = infalInflater.inflate(R.layout.layout_recent_jobs, parent, false);
        ImageView ivIcon=view.findViewById(R.id.iv_rjob_logo);
        TextView txtTitle=view.findViewById(R.id.txt_rjob_title);
        TextView txtDesc=view.findViewById(R.id.txt_rjob_dsc);
        TextView  txt_ship_type =view.findViewById(R.id.txt_ship_type);

        MatchJobModel model=recentJobList.get(position);
        Picasso.with(getContext()).load(model.getLogoURL()).error(R.drawable.no_image).placeholder(R.drawable.no_image).into(ivIcon);
        txtTitle.setText(model.getCompanyName());
        txtDesc.setText(model.getJobTitle());
        txt_ship_type.setText(model.getShipType());
        model.getRank();

        return view;
    }

    public void filters(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        recentJobList.clear();
        if (charText.length() == 0) {
            recentJobList.addAll(searchDoctorFilterList);
        } else {
            for (MatchJobModel bean : searchDoctorFilterList) {
                if (bean.getCompanyName().toLowerCase(Locale.getDefault())
                        .contains(charText) ||bean.getJobTitle().toLowerCase(Locale.getDefault())
                        .contains(charText))  {
                    recentJobList.add(bean);
                }
            }
        }
        notifyDataSetChanged();
    }
}
