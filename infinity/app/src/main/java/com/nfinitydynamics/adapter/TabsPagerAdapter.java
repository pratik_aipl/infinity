package com.nfinitydynamics.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.nfinitydynamics.fragment.MatchJobFragment;
import com.nfinitydynamics.fragment.RecentJobFragment;

/**
 * Created by User on 09-02-2018.
 */

public class TabsPagerAdapter extends FragmentPagerAdapter {

    int mNumOfTabs;

    public TabsPagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                RecentJobFragment tab1 = new RecentJobFragment();
                return tab1;
            case 1:
                MatchJobFragment tab2 = new MatchJobFragment();
                return tab2;
            default:
                return null;
        }
    }
    @Override
    public int getItemPosition(Object object) {
        // POSITION_NONE makes it possible to reload the PagerAdapter
        return 2;
    }
    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
