package com.nfinitydynamics.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nfinitydynamics.R;
import com.nfinitydynamics.model.JobAppliedModel;
import com.nfinitydynamics.utils.Utils;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by User on 13-02-2018.
 */

public class JobAppliedAdapter extends ArrayAdapter<JobAppliedModel> {
    public ArrayList<JobAppliedModel> appliedJobList = new ArrayList<>();
    public Context context;
    public View view;
    public SharedPreferences shared;
    public String candidateId;

    public JobAppliedAdapter(@NonNull Context context, int resource, @NonNull ArrayList<JobAppliedModel> appliedJobList) {
        super(context, resource, appliedJobList);
        this.context = context;
        this.appliedJobList = appliedJobList;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        LayoutInflater infalInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = infalInflater.inflate(R.layout.layout_job_applied, parent, false);
        TextView txtJobTitle = view.findViewById(R.id.txt_job_title);
        TextView txtShipType = view.findViewById(R.id.txt_ship_type);
        TextView txtDescription = view.findViewById(R.id.txt_job_subtitle);
        TextView txtAppliedDate = view.findViewById(R.id.txt_applied_date);
        ImageView ivIcon = (ImageView) view.findViewById(R.id.iv_job_icon);
        shared = getContext().getSharedPreferences(getContext().getPackageName(), 0);
        txtJobTitle.setText(appliedJobList.get(position).getJobTitle());
        txtShipType.setText(appliedJobList.get(position).getShipType());
        txtDescription.setText(appliedJobList.get(position).getDescription());
        try {
            makeTextViewResizable(txtDescription, 1, "View More", true);
        }catch (Exception e){
            e.printStackTrace();
        }
        converDateToDDMMYYTextView(txtAppliedDate, appliedJobList.get(position).getApplyDate());

        if (appliedJobList.get(position).getLogoURL() != "") {
            Picasso.with(context).load(appliedJobList.get(position).getLogoURL()).placeholder(R.drawable.no_image)
                    .error(R.drawable.no_image).into(ivIcon);
        }
        return view;
    }
    public static void makeTextViewResizable(final TextView tv, final int maxLine, final String expandText, final boolean viewMore) {

        if (tv.getTag() == null) {
            tv.setTag(tv.getText());
        }
        ViewTreeObserver vto = tv.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @SuppressWarnings("deprecation")
            @Override
            public void onGlobalLayout() {
                String text;
                int lineEndIndex;
                try {
                    ViewTreeObserver obs = tv.getViewTreeObserver();
                    obs.removeGlobalOnLayoutListener(this);
                    if (maxLine == 0) {
                        lineEndIndex = tv.getLayout().getLineEnd(0);
                        text = tv.getText().subSequence(0, lineEndIndex - expandText.length() + 1) + " " + expandText;
                    } else if (maxLine > 0 && tv.getLineCount() >= maxLine) {
                        lineEndIndex = tv.getLayout().getLineEnd(maxLine - 1);
                        text = tv.getText().subSequence(0, lineEndIndex - expandText.length() + 1) + " " + expandText;
                    } else {
                        lineEndIndex = tv.getLayout().getLineEnd(tv.getLayout().getLineCount() - 1);
                        text = tv.getText().subSequence(0, lineEndIndex) + " " + expandText;
                    }
                    tv.setText(text);
                    tv.setMovementMethod(LinkMovementMethod.getInstance());
                    tv.setText(
                            addClickablePartTextViewResizable(Html.fromHtml(tv.getText().toString()), tv, lineEndIndex, expandText,
                                    viewMore), TextView.BufferType.SPANNABLE);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });

    }

    private static SpannableStringBuilder addClickablePartTextViewResizable(final Spanned strSpanned, final TextView tv,
                                                                            final int maxLine, final String spanableText, final boolean viewMore) {
        String str = strSpanned.toString();
        SpannableStringBuilder ssb = new SpannableStringBuilder(strSpanned);

        if (str.contains(spanableText)) {
            ssb.setSpan(new ClickableSpan() {

                @Override
                public void onClick(View widget) {
                    tv.setLayoutParams(tv.getLayoutParams());
                    tv.setText(tv.getTag().toString(), TextView.BufferType.SPANNABLE);
                    tv.invalidate();
                    if (viewMore) {
                        makeTextViewResizable(tv, -1, "View Less", false);
                    } else {
                        makeTextViewResizable(tv, 3, "View More", true);
                    }

                }
            }, str.indexOf(spanableText), str.indexOf(spanableText) + spanableText.length(), 0);

        }
        return ssb;

    }

    public void converDateToDDMMYYTextView(TextView edt, String dateYYMMDD) {
        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat outputFormat = new SimpleDateFormat("dd-MMM-yyyy");
        Date date = null;
        try {
            date = inputFormat.parse(dateYYMMDD);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String ddMMYYDate = outputFormat.format(date);
        edt.setText("Applied on: " + ddMMYYDate);
    }
}
