package com.nfinitydynamics.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.nfinitydynamics.R;
import com.nfinitydynamics.activity.MainActivity;
import com.nfinitydynamics.fragment.AddPassportFragment;
import com.nfinitydynamics.model.PassportModel;
import com.nfinitydynamics.utils.CallRequest;
import com.nfinitydynamics.utils.Utils;

import java.util.ArrayList;
import java.util.Date;

import static com.nfinitydynamics.fragment.PassportFragment.instance;

/**
 * Created by User on 13-02-2018.
 */

public class PassportAdapter extends ArrayAdapter<PassportModel> {
    public ArrayList<PassportModel> passportList = new ArrayList<>();
    public Context context;
    public Date date1, date2;
    public String id, number, validity, place;
    public View view;
    public SharedPreferences shared;
    public String candidateId;

    public PassportAdapter(@NonNull Context context, int resource, @NonNull ArrayList<PassportModel> seamanBookList) {
        super(context, resource, seamanBookList);
        this.context = context;
        this.passportList = seamanBookList;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        LayoutInflater infalInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = infalInflater.inflate(R.layout.layout_passport, parent, false);

        TextView txtNumber = view.findViewById(R.id.txt_number_value);
        TextView txtValidity = view.findViewById(R.id.txt_validity_value);
        TextView txtPlaceIssue = view.findViewById(R.id.txt_place_issue_value);
        ImageView ivEdit = view.findViewById(R.id.iv_edit);
        ImageView ivDelete = view.findViewById(R.id.iv_delete);
        shared = getContext().getSharedPreferences(getContext().getPackageName(), 0);
        candidateId = shared.getString("userId", "");
        id = passportList.get(position).getPassportDetID();
        validity = passportList.get(position).getExpiryDate();
        number = passportList.get(position).getPassportNo();
        place = passportList.get(position).getPlaceofIssue();
        txtNumber.setText(": " + number);
        txtPlaceIssue.setText(": " + place);
        //txtValidity.setText(": "+validity);
        Utils.converDateToDDMMYYTextView(txtValidity, validity);
        ivEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                PassportModel model = passportList.get(position);
                id = model.getPassportDetID();
                validity = model.getExpiryDate();
                number = model.getPassportNo();
                place = model.getPlaceofIssue();
                AddPassportFragment fragment = new AddPassportFragment();
                Bundle args = new Bundle();
                args.putInt("value", 1);
                args.putString("id", id);
                args.putString("number", number);
                args.putString("place", place);
                args.putString("validity", validity);
                Log.d("", id + number);
                fragment.setArguments(args);
                MainActivity.changeFragment(fragment, true);
            }
        });

        ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                delete(position);
            }
        });
        return view;
    }


    private void delete(final int postion) {
        final MaterialDialog dialog = new MaterialDialog.Builder(context)
                .title("Delete")
                .content("Want to Delete This Record ?")
                .positiveText("Ok")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                        new CallRequest(instance).deletePassport(passportList.get(postion).getPassportDetID());
                        notifyDataSetChanged();


                    }
                })
                .negativeText("Cancel")
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .build();
        dialog.show();
    }

}
