package com.nfinitydynamics.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.nfinitydynamics.R;
import com.nfinitydynamics.model.LatestJobModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by User on 07-02-2018.
 */

public class RecentJobAdapter extends ArrayAdapter<LatestJobModel> {
    public   ArrayList<LatestJobModel>recentJobList;
    public List<LatestJobModel> searchDoctorFilterList=new ArrayList<>();
    public   Context context;
    public AQuery aqIMG;
    public RecentJobAdapter(@NonNull Context context, int resource, @NonNull ArrayList<LatestJobModel> recentJobList) {
        super(context, resource, recentJobList);
        this.recentJobList=recentJobList;
        this.context=context;
        aqIMG = new AQuery(context);
        this.searchDoctorFilterList.addAll(recentJobList);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view;
        LayoutInflater infalInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = infalInflater.inflate(R.layout.layout_recent_jobs, parent, false);
        ImageView ivIcon=view.findViewById(R.id.iv_rjob_logo);
        TextView txtTitle=view.findViewById(R.id.txt_rjob_title);
        TextView txtDesc=view.findViewById(R.id.txt_rjob_dsc);
        TextView txt_ship_type=view.findViewById(R.id.txt_ship_type);

//        ivIcon.setImageResource(model.getrImage());
     //   aqIMG.id(ivIcon).image(recentJobList.get(position).getLogoImgPath(), true, true, 0, R.drawable.profilepic, null, 0, 0.0f);
        Picasso.with(context).load(recentJobList.get(position).getLogoImgPath()).error(R.drawable.no_image).placeholder(R.drawable.no_image).into(ivIcon);
        txtTitle.setText(recentJobList.get(position).getCompanyName());
        txtDesc.setText(recentJobList.get(position).getJobTitle());
        txt_ship_type.setText(recentJobList.get(position).getShipType());
        String rank= recentJobList.get(position).getRank();
        return view;
    }
    public void filters(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        recentJobList.clear();
        if (charText.length() == 0) {
            recentJobList.addAll(searchDoctorFilterList);
        } else {
            for (LatestJobModel bean : searchDoctorFilterList) {
                if (bean.getCompanyName().toLowerCase(Locale.getDefault())
                        .contains(charText) ||bean.getJobTitle().toLowerCase(Locale.getDefault())
                        .contains(charText)) {
                    recentJobList.add(bean);
                }
            }
        }
        notifyDataSetChanged();
    }
}

