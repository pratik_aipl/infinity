package com.nfinitydynamics.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.nfinitydynamics.R;
import com.nfinitydynamics.activity.MainActivity;
import com.nfinitydynamics.fragment.AddEducationFragment;
import com.nfinitydynamics.fragment.PassportFragment;
import com.nfinitydynamics.model.EducationModel;
import com.nfinitydynamics.utils.CallRequest;

import java.util.ArrayList;

import static com.nfinitydynamics.fragment.EducationDetailFragment.instance;

/**
 * Created by User on 20-02-2018.
 */

public class EducationAdapter  extends ArrayAdapter<EducationModel> {
    public ArrayList<EducationModel> eduList = new ArrayList<>();
    public Context context;
    public String id, institueName, degree, place, passingYear,educationId,educationType;
    public View view;
    public SharedPreferences shared;
    public String candidateId;

    public EducationAdapter(@NonNull Context context, int resource, @NonNull ArrayList<EducationModel> eduList) {
        super(context, resource, eduList);
        this.context = context;
        this.eduList = eduList;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        LayoutInflater infalInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = infalInflater.inflate(R.layout.layout_education, parent, false);
        ImageView ivEdit =  view.findViewById(R.id.iv_edit);
        ImageView ivDelete =  view.findViewById(R.id.iv_delete);
        TextView txtInstitue =  view.findViewById(R.id.txt_inst_name_value);
        TextView txtdegree =  view.findViewById(R.id.txt_degree_value);
        TextView txtPlace =  view.findViewById(R.id.txt_place_value);
        TextView txtPassingYear =  view.findViewById(R.id.txt_passingyear_value);
        TextView txtEducationtype =  view.findViewById(R.id.txt_educationtype_value);
        shared = getContext().getSharedPreferences(getContext().getPackageName(), 0);
        candidateId = shared.getString("userId", "");
        //id = stcwList.get(position).getCOCDetailsID();

        //  id = model.getCOCDetailsID();
        institueName = eduList.get(position).getInstituteName();
        place = eduList.get(position).getPlace();
        passingYear = eduList.get(position).getPassingYear();
        degree = eduList.get(position).getDegree();
        educationId = eduList.get(position).getCanEducationID();
        educationType=eduList.get(position).getEducationType();

        Log.d("education type:",educationType);
        txtInstitue.setText(": " + institueName);
        txtdegree.setText(": " + degree);
        txtPlace.setText(": " + place);
        txtPassingYear.setText(": " + passingYear);
        txtEducationtype.setText(": "+ educationType);
        ivEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                EducationModel model=eduList.get(position);
                id = model.getCanEducationID();
                institueName = model.getInstituteName();
                degree = model.getDegree();
                place = model.getPlace();
                passingYear = model.getPassingYear();
                educationType=model.getEducationType();
                AddEducationFragment fragment = new AddEducationFragment();
                Bundle args = new Bundle();
                args.putInt("value", 1);
                args.putString("id", id);
                args.putString("institueName", institueName);
                args.putString("place", place);
                args.putString("degree", degree);
                args.putString("passingYear", passingYear);
                args.putString("educationType", educationType);

                fragment.setArguments(args);
                MainActivity.changeFragment(fragment, true);
            }
        });

        ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               delete(position);
            }
        });
        return view;
    }


    private void delete(final int position) {
        final MaterialDialog dialog = new MaterialDialog.Builder(context)
                .title("Delete")
                .content("Want to Delete This Record ?")
                .positiveText("Ok")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                        new CallRequest(instance).deleteEducation(eduList.get(position).getCanEducationID());
                        notifyDataSetChanged();

                    }
                })
                .negativeText("Cancel")
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .build();
        dialog.show();
    }

}