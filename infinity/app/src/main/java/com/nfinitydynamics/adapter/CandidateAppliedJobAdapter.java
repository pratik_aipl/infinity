package com.nfinitydynamics.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nfinitydynamics.R;
import com.nfinitydynamics.model.company.CandidateModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by User on 10-04-2018.
 */

public class CandidateAppliedJobAdapter extends ArrayAdapter<CandidateModel> {
    public ArrayList<CandidateModel> cadidateList=new ArrayList<>();
    public List<CandidateModel> searchDoctorFilterList=new ArrayList<>();
    public Context context;
    public CandidateAppliedJobAdapter(@NonNull Context context, int resource, @NonNull ArrayList<CandidateModel> cadidateList) {
        super(context, resource, cadidateList);
        this.cadidateList=cadidateList;
        this.context=context;
        this.searchDoctorFilterList.addAll(cadidateList);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view;
        LayoutInflater infalInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = infalInflater.inflate(R.layout.layout_candidate, parent, false);
        TextView txtName=view.findViewById(R.id.txt_name_value);
        TextView txtRankValue=view.findViewById(R.id.txt_rank_value);
        TextView txtShipType=view.findViewById(R.id.txt_ship_value);
        ImageView ivProfile=view.findViewById(R.id.ivCandidate);
        TextView txtLastVisited=view.findViewById(R.id.txt_lastvisited_value);
        CandidateModel model=cadidateList.get(position);
        txtName.setText(model.getCanidateName());
        txtRankValue.setText(model.getRankName());
        txtLastVisited.setText(model.getLast_login());
        Picasso.with(getContext()).load(model.getImageURL()).placeholder(R.drawable.profilepic).error(R.drawable.profilepic).into(ivProfile);

        return view;
    }
    public void filters(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        cadidateList.clear();
        if (charText.length() == 0) {
            cadidateList.addAll(searchDoctorFilterList);
        } else {
            for (CandidateModel bean : searchDoctorFilterList) {
                if (bean.getCanidateName().toLowerCase(Locale.getDefault())
                        .contains(charText)) {
                    cadidateList.add(bean);
                }
                else if (bean.getModified().toLowerCase(Locale.getDefault())
                        .contains(charText)) {
                    cadidateList.add(bean);
                }
//                else if (bean.getRankName().toLowerCase(Locale.getDefault())
//                        .contains(charText)) {
//                    cadidateList.add(bean);
//                }

            }
        }
        notifyDataSetChanged();
    }
}
