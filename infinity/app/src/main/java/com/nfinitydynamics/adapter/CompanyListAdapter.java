package com.nfinitydynamics.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nfinitydynamics.R;
import com.nfinitydynamics.model.CompanyListModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by User on 14-02-2018.
 */

public class CompanyListAdapter extends ArrayAdapter<CompanyListModel> {

    public ArrayList<CompanyListModel> companyList = new ArrayList<>();
    public Context context;

    public CompanyListAdapter(@NonNull Context context, int resource, @NonNull ArrayList<CompanyListModel> companyList) {
        super(context, resource, companyList);
        this.companyList=companyList;
        this.context=context;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view;
        LayoutInflater infalInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = infalInflater.inflate(R.layout.layout_company_list, parent, false);
        ImageView ivLogo=view.findViewById(R.id.iv_company_logo);
        TextView txtTitle=view.findViewById(R.id.txt_title);
        TextView txt_adrress=view.findViewById(R.id.txt_adrress);
        txt_adrress.setText(companyList.get(position).getAddress());
        txtTitle.setText(companyList.get(position).getCompanyName());
        Picasso.with(context).load(companyList.get(position).getLogoURL()).placeholder(R.drawable.no_image)
                .error(R.drawable.no_image).into(ivLogo);
        return  view;
    }
}
