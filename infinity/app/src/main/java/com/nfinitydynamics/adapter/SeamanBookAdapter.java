package com.nfinitydynamics.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.nfinitydynamics.R;
import com.nfinitydynamics.activity.MainActivity;
import com.nfinitydynamics.fragment.AddSemeanBookFragment;
import com.nfinitydynamics.fragment.EducationDetailFragment;
import com.nfinitydynamics.model.SeamanBookModel;
import com.nfinitydynamics.utils.CallRequest;
import com.nfinitydynamics.utils.Utils;

import java.util.ArrayList;
import java.util.Date;

import static com.nfinitydynamics.fragment.SeamanBookFragment.instance;

/**
 * Created by User on 12-02-2018.
 */

public class SeamanBookAdapter extends ArrayAdapter<SeamanBookModel> {
    public ArrayList<SeamanBookModel> seamanBookList=new ArrayList<>();
    public  Context context;
    public Date date1, date2;
    public   String id,number,validity,place;
    public View view;
    public SharedPreferences shared;
    public String candidateId;
    public SeamanBookAdapter(@NonNull Context context, int resource, @NonNull ArrayList<SeamanBookModel> seamanBookList ) {
        super(context, resource, seamanBookList);
        this.context=context;
        this.seamanBookList=seamanBookList;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        LayoutInflater infalInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = infalInflater.inflate(R.layout.layout_seaman_book_list, parent, false);
        final ImageView ivEdit=view.findViewById(R.id.iv_edit);
        ImageView ivDelete=view.findViewById(R.id.iv_delete);
        TextView txtNumber=view.findViewById(R.id.txt_number_value);
        TextView txtValidity=view.findViewById(R.id.txt_validity_value);
        TextView txtPlaceIssue=view.findViewById(R.id.txt_place_issue_value);
        shared = getContext().getSharedPreferences(getContext().getPackageName(), 0);
        candidateId = shared.getString("userId","");

        validity=seamanBookList .get(position).getValidity();
        number=seamanBookList.get(position).getNumber();
        place=seamanBookList.get(position).getAuthority();
        txtNumber.setText(": "+number);
        txtPlaceIssue.setText(": "+place);
        //txtValidity.setText(": "+validity);
        Utils.converDateToDDMMYYTextView(txtValidity,validity);
        ivEdit.setTag(position);
        ivEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                SeamanBookModel model=seamanBookList.get(position);
                //int pos= (int) ivEdit.getTag();
                id=model.getSeamanDetailID();
                validity=model.getValidity();
                number=model.getNumber();
                place=model.getAuthority();
                AddSemeanBookFragment fragment=new AddSemeanBookFragment();
                Bundle args = new Bundle();
                args.putInt("value",1);
                args.putString("id",id);
                args.putString("number",number);
                args.putString("place",place);
                args.putString("validity",validity);
                fragment.setArguments(args);
                MainActivity.changeFragment(fragment,true);
            }
        });

        ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               delete(position);
            }
        });
        return  view;
    }

    private void delete(final int position) {
        final MaterialDialog dialog = new MaterialDialog.Builder(context)
                .title("Delete")
                .content("Want to Delete This Record ?")
                .positiveText("Ok")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                        new CallRequest(instance).deleteSeaman(seamanBookList.get(position).getSeamanDetailID());
                        notifyDataSetChanged();

                    }
                })
                .negativeText("Cancel")
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .build();
        dialog.show();
    }

}
