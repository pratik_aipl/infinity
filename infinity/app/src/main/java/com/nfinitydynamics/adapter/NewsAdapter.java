package com.nfinitydynamics.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nfinitydynamics.R;
import com.nfinitydynamics.model.NewsModel;
import com.nfinitydynamics.utils.RoundedTransformation;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by User on 07-02-2018.
 */

public class NewsAdapter extends ArrayAdapter<NewsModel> {
  public   ArrayList<NewsModel> newList=new ArrayList<>();
    public Context context;
    public NewsAdapter(@NonNull Context context, int resource, @NonNull ArrayList<NewsModel> newList) {
        super(context, resource, newList);

        this.context=context;
        this.newList=newList;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view;
        LayoutInflater infalInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = infalInflater.inflate(R.layout.layout_news, parent, false);
        ImageView ivIcon=view.findViewById(R.id.iv_news);
        TextView txtTitle=view.findViewById(R.id.txt_news_title);
        TextView txtDesc=view.findViewById(R.id.txt_news_desc);
        TextView txtDate=view.findViewById(R.id.txt_news_date);
        NewsModel model=newList.get(position);
//        ivIcon.setImageResource(model.getrImage());

        Picasso.with(context)
                .load(model.getImage())
                .transform(new RoundedTransformation(15, 4))
                .resizeDimen(R.dimen.image_size_width, R.dimen.image_size_width)
                .centerCrop()
                .into(ivIcon);
       // Picasso.with(context).load(model.getImage()).into(ivIcon);
        txtTitle.setText(model.getNewTitle());
        txtDesc.setText(model.getNewDesc());
        txtDate.setText(model.getNewsDate());
        return view;
    }
}
