package com.nfinitydynamics.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nfinitydynamics.R;
import com.nfinitydynamics.model.DrawerModel;

import java.util.ArrayList;

/**
 * Created by User on 07-02-2018.
 */

public class DrawerAdapter extends ArrayAdapter<DrawerModel> {
    public ArrayList<DrawerModel>list=new ArrayList<>();
    public Context context;
    public DrawerAdapter(@NonNull Context context, int resource, @NonNull ArrayList<DrawerModel> list) {
        super(context, resource, list);
        this.list=list;
        this.context=context;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view;
        LayoutInflater infalInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = infalInflater.inflate(R.layout.layout_drawer, parent, false);
        ImageView ivIcon=view.findViewById(R.id.iv_dicon);
        TextView txtItem=view.findViewById(R.id.txt_ditem);

        DrawerModel model=list.get(position);
        ivIcon.setImageResource(model.getIcon());
        txtItem.setText(model.getName());
        return view;
    }
}
