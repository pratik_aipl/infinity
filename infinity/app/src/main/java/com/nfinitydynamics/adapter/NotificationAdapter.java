package com.nfinitydynamics.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nfinitydynamics.R;
import com.nfinitydynamics.activity.MainActivity;
import com.nfinitydynamics.fragment.AddEducationFragment;
import com.nfinitydynamics.model.EducationModel;
import com.nfinitydynamics.model.Notifications;
import com.nfinitydynamics.utils.CallRequest;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import static com.nfinitydynamics.fragment.EducationDetailFragment.instance;

public class NotificationAdapter extends ArrayAdapter<Notifications> {
    public ArrayList<Notifications> eduList = new ArrayList<>();
    public Context context;
    public String id, institueName, degree, place, passingYear,educationId,educationType;
    public View view;
    public SharedPreferences shared;
    public String candidateId;

    public NotificationAdapter(@NonNull Context context, int resource, @NonNull ArrayList<Notifications> eduList) {
        super(context, resource, eduList);
        this.context = context;
        this.eduList = eduList;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        LayoutInflater infalInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = infalInflater.inflate(R.layout.layout_notification, parent, false);
       TextView txt_noti_date=view.findViewById(R.id.txt_noti_date);
        TextView txt_noti_name=view.findViewById(R.id.txt_noti_name);
       ImageView img_icon=view.findViewById(R.id.img_icon);
        txt_noti_name.setText(eduList.get(position).getNotification());
        txt_noti_date.setText(eduList.get(position).getNotificationDate());
       // Picasso.with(context).load(eduList.get(position).getImageURL()).error(R.drawable.no_image).placeholder(R.drawable.no_image).into(img_icon);

        return view;
    }

}