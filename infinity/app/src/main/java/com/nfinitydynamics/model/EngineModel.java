package com.nfinitydynamics.model;

/**
 * Created by User on 19-02-2018.
 */

public class EngineModel {


    public String EngineTypeID="",EngineType="",Description="";

    public String getEngineTypeID() {
        return EngineTypeID;
    }

    public EngineModel(String engineTypeID, String engineType) {
        EngineTypeID = engineTypeID;
        EngineType = engineType;
    }

    public void setEngineTypeID(String engineTypeID) {
        EngineTypeID = engineTypeID;
    }

    public String getEngineType() {
        return EngineType;
    }

    public void setEngineType(String engineType) {
        EngineType = engineType;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }
}
