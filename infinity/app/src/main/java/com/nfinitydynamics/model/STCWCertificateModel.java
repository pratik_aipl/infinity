package com.nfinitydynamics.model;

/**
 * Created by User on 14-03-2018.
 */

public class STCWCertificateModel {

    public String STCWCertificateID="",STCWCertificateType="",IssuingAuthority="",Department="",ValidUntil="",
            Description="",IsActive="",Created="",Modified="";

    public STCWCertificateModel(String STCWCertificateID, String STCWCertificateType) {
        this.STCWCertificateID = STCWCertificateID;
        this.STCWCertificateType = STCWCertificateType;
    }

    public String getSTCWCertificateID() {
        return STCWCertificateID;
    }

    public void setSTCWCertificateID(String STCWCertificateID) {
        this.STCWCertificateID = STCWCertificateID;
    }

    public String getSTCWCertificateType() {
        return STCWCertificateType;
    }

    public void setSTCWCertificateType(String STCWCertificateType) {
        this.STCWCertificateType = STCWCertificateType;
    }

    public String getIssuingAuthority() {
        return IssuingAuthority;
    }

    public void setIssuingAuthority(String issuingAuthority) {
        IssuingAuthority = issuingAuthority;
    }

    public String getDepartment() {
        return Department;
    }

    public void setDepartment(String department) {
        Department = department;
    }

    public String getValidUntil() {
        return ValidUntil;
    }

    public void setValidUntil(String validUntil) {
        ValidUntil = validUntil;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getIsActive() {
        return IsActive;
    }

    public void setIsActive(String isActive) {
        IsActive = isActive;
    }

    public String getCreated() {
        return Created;
    }

    public void setCreated(String created) {
        Created = created;
    }

    public String getModified() {
        return Modified;
    }

    public void setModified(String modified) {
        Modified = modified;
    }
}
