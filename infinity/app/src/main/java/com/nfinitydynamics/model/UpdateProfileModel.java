package com.nfinitydynamics.model;

import java.io.Serializable;

/**
 * Created by User on 10-02-2018.
 */

public class UpdateProfileModel implements Serializable {


    public   String CandidateID="",FirstName="",MiddleName="",LastName="",EmailID="",EmailID2="",DOB="",MaritalStatus="",NoofChildren="",
            Address="",CityName="",Zipcode="",RankName="",Nationality="",TelephoneNo="",MobileNo="",OtherCCode="",OtherMobileNo="",Indos,RankID="",
            Created="",Modified="",NationalName="",ImageUrl="",JoiningType="",AvailableFrom="",TelCountryCode="",STDCode="",CCode="",profileimg="",
            Gender="",CountryName="",Weight="",Height="",BMI="",CountryofResidenceID="";

    public String getGender() {
        return Gender;
    }

    public String getCountryofResidenceID() {
        return CountryofResidenceID;
    }

    public void setCountryofResidenceID(String countryofResidenceID) {
        CountryofResidenceID = countryofResidenceID;
    }

    public String getWeight() {
        return Weight;
    }



    public void setWeight(String weight) {
        Weight = weight;
    }

    public String getHeight() {
        return Height;
    }

    public void setHeight(String height) {
        Height = height;
    }

    public String getBMI() {
        return BMI;
    }

    public void setBMI(String BMI) {
        this.BMI = BMI;
    }

    public void setGender(String gender) {
        Gender = gender;
    }

    public String getProfileimg() {
        return profileimg;
    }

    public void setProfileimg(String profileimg) {
        this.profileimg = profileimg;
    }

    public String getOtherCCode() {
        return OtherCCode;
    }
    public void setOtherCCode(String otherCCode) {
        OtherCCode = otherCCode;
    }

    public String getJoiningType() {
        return JoiningType;
    }

    public String getAvailableFrom() {
        return AvailableFrom;
    }

    public String getTelCountryCode() {
        return TelCountryCode;
    }

    public void setTelCountryCode(String telCountryCode) {
        TelCountryCode = telCountryCode;
    }

    public String getSTDCode() {
        return STDCode;
    }

    public void setSTDCode(String STDCode) {
        this.STDCode = STDCode;
    }

    public String getCCode() {
        return CCode;
    }

    public void setCCode(String CCode) {
        this.CCode = CCode;
    }

    public void setAvailableFrom(String availableFrom) {
        AvailableFrom = availableFrom;
    }

    public void setJoiningType(String joiningType) {
        JoiningType = joiningType;
    }

    public String getDOB() {
        return DOB;
    }

    public void setDOB(String DOB) {
        this.DOB = DOB;
    }

    public String getRankName() {
        return RankName;
    }

    public String getNationalName() {
        return NationalName;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }

    public String Country="";

    public void setNationalName(String nationalName) {
        NationalName = nationalName;
    }

    public void setRankName(String rankName) {
        RankName = rankName;
    }

    public String getCandidateID() {
        return CandidateID;
    }

    public void setCandidateID(String candidateID) {
        CandidateID = candidateID;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getMiddleName() {
        return MiddleName;
    }

    public void setMiddleName(String middleName) {
        MiddleName = middleName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getEmailID() {
        return EmailID;
    }

    public void setEmailID(String emailID) {
        EmailID = emailID;
    }

    public String getEmailID2() {
        return EmailID2;
    }

    public void setEmailID2(String emailID2) {
        EmailID2 = emailID2;
    }

    public String getMaritalStatus() {
        return MaritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        MaritalStatus = maritalStatus;
    }

    public String getNoofChildren() {
        return NoofChildren;
    }

    public void setNoofChildren(String noofChildren) {
        NoofChildren = noofChildren;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getCityName() {
        return CityName;
    }

    public void setCityName(String cityName) {
        CityName = cityName;
    }

    public String getZipcode() {
        return Zipcode;
    }

    public void setZipcode(String zipcode) {
        Zipcode = zipcode;
    }

    public String getNationality() {
        return Nationality;
    }

    public void setNationality(String nationality) {
        Nationality = nationality;
    }

    public String getTelephoneNo() {
        return TelephoneNo;
    }

    public void setTelephoneNo(String telephoneNo) {
        TelephoneNo = telephoneNo;
    }

    public String getMobileNo() {
        return MobileNo;
    }

    public void setMobileNo(String mobileNo) {
        MobileNo = mobileNo;
    }

    public String getOtherMobileNo() {
        return OtherMobileNo;
    }

    public void setOtherMobileNo(String otherMobileNo) {
        OtherMobileNo = otherMobileNo;
    }

    public String getIndos() {
        return Indos;
    }

    public void setIndos(String indos) {
        Indos = indos;
    }

    public String getRankID() {
        return RankID;
    }

    public void setRankID(String rankID) {
        RankID = rankID;
    }

    public String getCreated() {
        return Created;
    }

    public void setCreated(String created) {
        Created = created;
    }

    public String getModified() {
        return Modified;
    }

    public void setModified(String modified) {
        Modified = modified;
    }

    public String getImageUrl() {
        return ImageUrl;
    }

    public void setImageUrl(String imageUrl) {
        ImageUrl = imageUrl;
    }

    public String getCountryName() {
        return CountryName;
    }

    public void setCountryName(String countryName) {
        CountryName = countryName;
    }
}
