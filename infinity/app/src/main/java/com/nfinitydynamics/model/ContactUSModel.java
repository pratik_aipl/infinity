package com.nfinitydynamics.model;

/**
 * Created by User on 09-04-2018.
 */

public class ContactUSModel {
    public String ID,Address,Help_Line,Enquires,Fax,Email_1,Email_2,Email_3;

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getHelp_Line() {
        return Help_Line;
    }

    public void setHelp_Line(String help_Line) {
        Help_Line = help_Line;
    }

    public String getEnquires() {
        return Enquires;
    }

    public void setEnquires(String enquires) {
        Enquires = enquires;
    }

    public String getFax() {
        return Fax;
    }

    public void setFax(String fax) {
        Fax = fax;
    }

    public String getEmail_1() {
        return Email_1;
    }

    public void setEmail_1(String email_1) {
        Email_1 = email_1;
    }

    public String getEmail_2() {
        return Email_2;
    }

    public void setEmail_2(String email_2) {
        Email_2 = email_2;
    }

    public String getEmail_3() {
        return Email_3;
    }

    public void setEmail_3(String email_3) {
        Email_3 = email_3;
    }
}
