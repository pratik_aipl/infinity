package com.nfinitydynamics.model;

/**
 * Created by User on 12-02-2018.
 */

public class SeamanBookModel {

    public   String SeamanDetailID="",CandidateID="",Number="",Authority="",Validity="",IsActive="",Created="",Modified="";

    public String getSeamanDetailID() {
        return SeamanDetailID;
    }

    public void setSeamanDetailID(String seamanDetailID) {
        SeamanDetailID = seamanDetailID;
    }

    public String getCandidateID() {
        return CandidateID;
    }

    public void setCandidateID(String candidateID) {
        CandidateID = candidateID;
    }

    public String getNumber() {
        return Number;
    }

    public void setNumber(String number) {
        Number = number;
    }

    public String getAuthority() {
        return Authority;
    }

    public void setAuthority(String authority) {
        Authority = authority;
    }

    public String getValidity() {
        return Validity;
    }

    public void setValidity(String validity) {
        Validity = validity;
    }

    public String getIsActive() {
        return IsActive;
    }

    public void setIsActive(String isActive) {
        IsActive = isActive;
    }

    public String getCreated() {
        return Created;
    }

    public void setCreated(String created) {
        Created = created;
    }

    public String getModified() {
        return Modified;
    }

    public void setModified(String modified) {
        Modified = modified;
    }
}
