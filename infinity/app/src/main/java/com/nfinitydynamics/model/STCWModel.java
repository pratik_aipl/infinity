package com.nfinitydynamics.model;

import java.io.Serializable;

/**
 * Created by User on 14-03-2018.
 */

public class STCWModel implements Serializable {

    public String CandidateID="",STWCCertiDetailsID="",CertificateNo="",STCWCertificateType="",CertificateType="",Authority="",
            Validity="";

    public String getCandidateID() {
        return CandidateID;
    }

    public void setCandidateID(String candidateID) {
        CandidateID = candidateID;
    }

    public String getSTWCCertiDetailsID() {
        return STWCCertiDetailsID;
    }

    public void setSTWCCertiDetailsID(String STWCCertiDetailsID) {
        this.STWCCertiDetailsID = STWCCertiDetailsID;
    }

    public String getCertificateNo() {
        return CertificateNo;
    }

    public void setCertificateNo(String certificateNo) {
        CertificateNo = certificateNo;
    }

    public String getSTCWCertificateType() {
        return STCWCertificateType;
    }

    public void setSTCWCertificateType(String STCWCertificateType) {
        this.STCWCertificateType = STCWCertificateType;
    }

    public String getCertificateType() {
        return CertificateType;
    }

    public void setCertificateType(String certificateType) {
        CertificateType = certificateType;
    }

    public String getAuthority() {
        return Authority;
    }

    public void setAuthority(String authority) {
        Authority = authority;
    }

    public String getValidity() {
        return Validity;
    }

    public void setValidity(String validity) {
        Validity = validity;
    }
}
