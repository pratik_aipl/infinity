package com.nfinitydynamics.model;

/**
 * Created by User on 12-02-2018.
 */

public class ShipModel {

    public String ShipID="",ShipType="",Description="",IsActive="",Created="",Modified="";

    public ShipModel(String shipID, String shipType) {
        ShipID = shipID;
        ShipType = shipType;
    }

    public String getShipID() {
        return ShipID;
    }

    public void setShipID(String shipID) {
        ShipID = shipID;
    }

    public String getShipType() {
        return ShipType;
    }

    public void setShipType(String shipType) {
        ShipType = shipType;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getIsActive() {
        return IsActive;
    }

    public void setIsActive(String isActive) {
        IsActive = isActive;
    }

    public String getCreated() {
        return Created;
    }

    public void setCreated(String created) {
        Created = created;
    }

    public String getModified() {
        return Modified;
    }

    public void setModified(String modified) {
        Modified = modified;
    }
}
