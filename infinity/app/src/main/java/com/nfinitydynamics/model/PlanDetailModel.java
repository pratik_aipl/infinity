package com.nfinitydynamics.model;

/**
 * Created by User on 21-06-2018.
 */

public class PlanDetailModel {

    public String PlanName,TotalDownload,RemainDownload,Duration,Created,subscribedate,expirydate;

    public String getPlanName() {
        return PlanName;
    }

    public void setPlanName(String planName) {
        PlanName = planName;
    }

    public String getTotalDownload() {
        return TotalDownload;
    }

    public void setTotalDownload(String totalDownload) {
        TotalDownload = totalDownload;
    }

    public String getRemainDownload() {
        return RemainDownload;
    }

    public void setRemainDownload(String remainDownload) {
        RemainDownload = remainDownload;
    }

    public String getDuration() {
        return Duration;
    }

    public void setDuration(String duration) {
        Duration = duration;
    }

    public String getCreated() {
        return Created;
    }

    public void setCreated(String created) {
        Created = created;
    }

    public String getSubscribedate() {
        return subscribedate;
    }

    public void setSubscribedate(String subscribedate) {
        this.subscribedate = subscribedate;
    }

    public String getExpirydate() {
        return expirydate;
    }

    public void setExpirydate(String expirydate) {
        this.expirydate = expirydate;
    }
}
