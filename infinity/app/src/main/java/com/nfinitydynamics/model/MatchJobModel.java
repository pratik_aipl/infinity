package com.nfinitydynamics.model;

/**
 * Created by User on 07-02-2018.
 */

public class MatchJobModel {

    public String CompanyID="",CompanyName="",LogoURL="test",JobID="",JobTitle="",ShipType="",Rank="",JobEndDate="",Description="",ToExperience="",FromExperience="";
   int Is_Applied_Job;
    public String getCompanyID() {
        return CompanyID;
    }

    public int getIs_Applied_Job() {
        return Is_Applied_Job;
    }

    public void setIs_Applied_Job(int is_Applied_Job) {
        Is_Applied_Job = is_Applied_Job;
    }

    public void setCompanyID(String companyID) {
        CompanyID = companyID;
    }

    public String getCompanyName() {
        return CompanyName;
    }

    public void setCompanyName(String companyName) {
        CompanyName = companyName;
    }

    public String getLogoURL() {
        return LogoURL;
    }

    public void setLogoURL(String logoURL) {
        LogoURL = logoURL;
    }

    public String getJobID() {
        return JobID;
    }

    public void setJobID(String jobID) {
        JobID = jobID;
    }

    public String getJobTitle() {
        return JobTitle;
    }

    public void setJobTitle(String jobTitle) {
        JobTitle = jobTitle;
    }

    public String getShipType() {
        return ShipType;
    }

    public void setShipType(String shipType) {
        ShipType = shipType;
    }

    public String getRank() {
        return Rank;
    }

    public void setRank(String rank) {
        Rank = rank;
    }

    public String getJobEndDate() {
        return JobEndDate;
    }

    public void setJobEndDate(String jobEndDate) {
        JobEndDate = jobEndDate;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getToExperience() {
        return ToExperience;
    }

    public void setToExperience(String toExperience) {
        ToExperience = toExperience;
    }

    public String getFromExperience() {
        return FromExperience;
    }

    public void setFromExperience(String fromExperience) {
        FromExperience = fromExperience;
    }
}
