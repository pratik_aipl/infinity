package com.nfinitydynamics.model;

/**
 * Created by User on 18-06-2018.
 */

public class AppliedShip {

    public String ShipID;
    public String ShipType;
    public String Description;
    public String IsActive;
    public String Created;
    public String Modified;

    public String getShipTypeID() {
        return ShipTypeID;
    }

    public void setShipTypeID(String shipTypeID) {
        ShipTypeID = shipTypeID;
    }

    public String ShipTypeID="";

    public AppliedShip() {
    }

    public AppliedShip(String shipID, String shipType) {
        ShipID = shipID;
        ShipType = shipType;
    }

    public String getShipID() {
        return ShipID;
    }

    public void setShipID(String shipID) {
        ShipID = shipID;
    }

    public String getShipType() {
        return ShipType;
    }

    public void setShipType(String shipType) {
        ShipType = shipType;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getIsActive() {
        return IsActive;
    }

    public void setIsActive(String isActive) {
        IsActive = isActive;
    }

    public String getCreated() {
        return Created;
    }

    public void setCreated(String created) {
        Created = created;
    }

    public String getModified() {
        return Modified;
    }

    public void setModified(String modified) {
        Modified = modified;
    }
}
