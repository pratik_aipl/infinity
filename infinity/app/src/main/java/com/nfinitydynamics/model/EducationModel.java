package com.nfinitydynamics.model;

/**
 * Created by User on 20-02-2018.
 */

public class EducationModel {

    public String CanEducationID="",CandidateID="",InstituteName="",Degree="",Place="",PassingYear="",EducationType ="";

    public String getCanEducationID() {
        return CanEducationID;
    }

    public String getEducationType() {
        return EducationType;
    }

    public void setEducationType(String educationType) {
        EducationType = educationType;
    }

    public void setCanEducationID(String canEducationID) {
        CanEducationID = canEducationID;
    }

    public String getCandidateID() {
        return CandidateID;
    }

    public void setCandidateID(String candidateID) {
        CandidateID = candidateID;
    }

    public String getInstituteName() {
        return InstituteName;
    }

    public void setInstituteName(String instituteName) {
        InstituteName = instituteName;
    }

    public String getDegree() {
        return Degree;
    }

    public void setDegree(String degree) {
        Degree = degree;
    }

    public String getPlace() {
        return Place;
    }

    public void setPlace(String place) {
        Place = place;
    }

    public String getPassingYear() {
        return PassingYear;
    }

    public void setPassingYear(String passingYear) {
        PassingYear = passingYear;
    }
}
