package com.nfinitydynamics.model;

/**
 * Created by User on 21-06-2018.
 */

public class PlanModel {

    public  String PlanID,PlanName,NoOfDownloads,NoOfAdvertisemnet,Duration,Price,Created,Modified;

    public String getPlanID() {
        return PlanID;
    }

    public void setPlanID(String planID) {
        PlanID = planID;
    }

    public String getPlanName() {
        return PlanName;
    }

    public void setPlanName(String planName) {
        PlanName = planName;
    }

    public String getNoOfDownloads() {
        return NoOfDownloads;
    }

    public void setNoOfDownloads(String noOfDownloads) {
        NoOfDownloads = noOfDownloads;
    }

    public String getNoOfAdvertisemnet() {
        return NoOfAdvertisemnet;
    }

    public void setNoOfAdvertisemnet(String noOfAdvertisemnet) {
        NoOfAdvertisemnet = noOfAdvertisemnet;
    }

    public String getDuration() {
        return Duration;
    }

    public void setDuration(String duration) {
        Duration = duration;
    }

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        Price = price;
    }

    public String getCreated() {
        return Created;
    }

    public void setCreated(String created) {
        Created = created;
    }

    public String getModified() {
        return Modified;
    }

    public void setModified(String modified) {
        Modified = modified;
    }
}
