package com.nfinitydynamics.model;

/**
 * Created by User on 13-02-2018.
 */

public class COCModel {
    public String CandidateID="",COCDetailsID="",CertificateNo="",CertificateID="",CertificateType="",Authority="",
            Validity="";

    public String getCandidateID() {
        return CandidateID;
    }

    public String getCertificateID() {
        return CertificateID;
    }

    public void setCertificateID(String certificateID) {
        CertificateID = certificateID;
    }

    public void setCandidateID(String candidateID) {
        CandidateID = candidateID;
    }

    public String getCOCDetailsID() {
        return COCDetailsID;
    }

    public void setCOCDetailsID(String COCDetailsID) {
        this.COCDetailsID = COCDetailsID;
    }

    public String getCertificateNo() {
        return CertificateNo;
    }

    public void setCertificateNo(String certificateNo) {
        CertificateNo = certificateNo;
    }

    public String getCertificateType() {
        return CertificateType;
    }

    public void setCertificateType(String certificateType) {
        CertificateType = certificateType;
    }

    public String getAuthority() {
        return Authority;
    }

    public void setAuthority(String authority) {
        Authority = authority;
    }

    public String getValidity() {
        return Validity;
    }

    public void setValidity(String validity) {
        Validity = validity;
    }
}
