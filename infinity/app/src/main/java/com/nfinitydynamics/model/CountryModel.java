package com.nfinitydynamics.model;

public class CountryModel {

    public String CountryID,CountryName,CountryTelCode;



    public String getCountryID() {
        return CountryID;
    }

    public void setCountryID(String countryID) {
        CountryID = countryID;
    }

    public String getCountryName() {
        return CountryName;
    }

    public void setCountryName(String countryName) {
        CountryName = countryName;
    }

    public String getCountryTelCode() {
        return CountryTelCode;
    }

    public void setCountryTelCode(String countryTelCode) {
        CountryTelCode = countryTelCode;
    }
}
