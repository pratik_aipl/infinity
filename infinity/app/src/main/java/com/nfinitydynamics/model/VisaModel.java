package com.nfinitydynamics.model;

/**
 * Created by User on 14-03-2018.
 */

public class VisaModel {

    public String VisaDetailsID="",CandidateID="",VisaNo="",PlaceofIssue="",ExpiryDate="",IsActive="",Created="",Modified="",VisaType="";

    public String getVisaDetailsID() {
        return VisaDetailsID;
    }

    public void setVisaDetailsID(String visaDetailsID) {
        VisaDetailsID = visaDetailsID;
    }

    public String getCandidateID() {
        return CandidateID;
    }

    public void setCandidateID(String candidateID) {
        CandidateID = candidateID;
    }

    public String getVisaNo() {
        return VisaNo;
    }

    public void setVisaNo(String visaNo) {
        VisaNo = visaNo;
    }

    public String getPlaceofIssue() {
        return PlaceofIssue;
    }

    public void setPlaceofIssue(String placeofIssue) {
        PlaceofIssue = placeofIssue;
    }

    public String getExpiryDate() {
        return ExpiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        ExpiryDate = expiryDate;
    }

    public String getIsActive() {
        return IsActive;
    }

    public void setIsActive(String isActive) {
        IsActive = isActive;
    }

    public String getCreated() {
        return Created;
    }

    public void setCreated(String created) {
        Created = created;
    }

    public String getModified() {
        return Modified;
    }

    public void setModified(String modified) {
        Modified = modified;
    }

    public String getVisaType() {
        return VisaType;
    }

    public void setVisaType(String visaType) {
        VisaType = visaType;
    }
}
