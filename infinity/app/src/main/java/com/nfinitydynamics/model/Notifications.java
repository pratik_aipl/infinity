package com.nfinitydynamics.model;

import java.io.Serializable;

public class Notifications implements Serializable {
    public String NotificationID = "";
    public String UserID = "";
    public String Icon = "";
    public String ImageURL = "";
    public String Notification = "";
    public String NotificationDesc = "";
    public String NotificationDate = "";
    public String IsViewed = "";
    public String CreatedBy = "";

    public String getNotificationID() {
        return NotificationID;
    }

    public void setNotificationID(String notificationID) {
        NotificationID = notificationID;
    }

    public String getUserID() {
        return UserID;
    }

    public void setUserID(String userID) {
        UserID = userID;
    }

    public String getIcon() {
        return Icon;
    }

    public void setIcon(String icon) {
        Icon = icon;
    }

    public String getImageURL() {
        return ImageURL;
    }

    public void setImageURL(String imageURL) {
        ImageURL = imageURL;
    }

    public String getNotification() {
        return Notification;
    }

    public void setNotification(String notification) {
        Notification = notification;
    }

    public String getNotificationDesc() {
        return NotificationDesc;
    }

    public void setNotificationDesc(String notificationDesc) {
        NotificationDesc = notificationDesc;
    }

    public String getNotificationDate() {
        return NotificationDate;
    }

    public void setNotificationDate(String notificationDate) {
        NotificationDate = notificationDate;
    }

    public String getIsViewed() {
        return IsViewed;
    }

    public void setIsViewed(String isViewed) {
        IsViewed = isViewed;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getCreatedOn() {
        return CreatedOn;
    }

    public void setCreatedOn(String createdOn) {
        CreatedOn = createdOn;
    }

    public String getModifiedBy() {
        return ModifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        ModifiedBy = modifiedBy;
    }

    public String getModifiedOn() {
        return ModifiedOn;
    }

    public void setModifiedOn(String modifiedOn) {
        ModifiedOn = modifiedOn;
    }

    public String getUsertype() {
        return usertype;
    }

    public void setUsertype(String usertype) {
        this.usertype = usertype;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCompanyid() {
        return companyid;
    }

    public void setCompanyid(String companyid) {
        this.companyid = companyid;
    }

    public String CreatedOn = "";
    public String ModifiedBy = "";
    public String ModifiedOn = "";
    public String usertype = "";
    public String id = "";
    public String companyid = "";
}
