package com.nfinitydynamics.model.company;

/**
 * Created by User on 23-02-2018.
 */

public class CandidateModel {

    public String CandidateID;
    public String CanidateName="";
    public String RankName;
    public String EmailID;
    public String MaritalStatus;
    public String MobileNo;
    public String Indos;
    public String NationalName;
    public String DateofApplication;
    public String AvailableFrom;
    public String Modified;
    public String last_login;
    public String ImageURL;
    public String ResumeFile;
    public String RankID;
    public String Name;
    public String AppliedRankName;
    public String AppliedShipName;
    public String CandidateCode;

    public String getLastVisited() {
        return LastVisited;
    }

    public void setLastVisited(String lastVisited) {
        LastVisited = lastVisited;
    }

    public String SeatimeInLastRank;
    public String ShipType;
    public String LastVisited;

    public String getCurrentRank() {
        return CurrentRank;
    }

    public void setCurrentRank(String currentRank) {
        CurrentRank = currentRank;
    }

    public String CurrentRank="";

    public CandidateModel() {
    }


    public String getCandidateCode() {
        return CandidateCode;
    }

    public void setCandidateCode(String candidateCode) {
        CandidateCode = candidateCode;
    }

    public String getAppliedShipName() {
        return AppliedShipName;
    }

    public void setAppliedShipName(String appliedShipName) {
        AppliedShipName = appliedShipName;
    }

    public String getCandidateID() {
        return CandidateID;
    }

    public void setCandidateID(String candidateID) {
        CandidateID = candidateID;
    }

    public String getCanidateName() {
        return CanidateName;
    }

    public void setCanidateName(String canidateName) {
        CanidateName = canidateName;
    }

    public String getRankName() {
        return RankName;
    }

    public void setRankName(String rankName) {
        RankName = rankName;
    }

    public String getImageURL() {
        return ImageURL;
    }

    public void setImageURL(String imageURL) {
        ImageURL = imageURL;
    }

    public String getResumeFile() {
        return ResumeFile;
    }

    public void setResumeFile(String resumeFile) {
        ResumeFile = resumeFile;
    }

    public String getEmailID() {
        return EmailID;
    }

    public void setEmailID(String emailID) {
        EmailID = emailID;
    }

    public String getMaritalStatus() {
        return MaritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        MaritalStatus = maritalStatus;
    }

    public String getMobileNo() {
        return MobileNo;
    }

    public void setMobileNo(String mobileNo) {
        MobileNo = mobileNo;
    }

    public String getIndos() {
        return Indos;
    }

    public void setIndos(String indos) {
        Indos = indos;
    }

    public String getNationalName() {
        return NationalName;
    }

    public void setNationalName(String nationalName) {
        NationalName = nationalName;
    }

    public String getModified() {
        return Modified;
    }

    public void setModified(String modified) {
        Modified = modified;
    }

    public String getLast_login() {
        return last_login;
    }

    public void setLast_login(String last_login) {
        this.last_login = last_login;
    }

    public String getRankID() {
        return RankID;
    }

    public void setRankID(String rankID) {
        RankID = rankID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getAppliedRankName() {
        return AppliedRankName;
    }

    public String getShipType() {
        return ShipType;
    }

    public void setShipType(String shipType) {
        ShipType = shipType;
    }

    public String getSeatimeInLastRank() {
        return SeatimeInLastRank;
    }

    public String getDateofApplication() {
        return DateofApplication;
    }

    public void setDateofApplication(String dateofApplication) {
        DateofApplication = dateofApplication;
    }

    public void setSeatimeInLastRank(String seatimeInLastRank) {
        SeatimeInLastRank = seatimeInLastRank;
    }

    public void setAppliedRankName(String appliedRankName) {
        AppliedRankName = appliedRankName;
    }

    public String getAvailableFrom() {
        return AvailableFrom;
    }

    public void setAvailableFrom(String availableFrom) {
        AvailableFrom = availableFrom;
    }
}
