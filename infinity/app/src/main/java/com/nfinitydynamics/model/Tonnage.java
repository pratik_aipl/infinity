package com.nfinitydynamics.model;

import java.io.Serializable;

public class Tonnage implements Serializable {

    public String TonnageTypeID = "";
    public String TonnageType = "";

    public Tonnage(String TonnageTypeID, String TonnageType) {
        TonnageTypeID = TonnageTypeID;
        TonnageType = TonnageType;
    }

    public String getTonnageTypeID() {
        return TonnageTypeID;
    }

    public void setTonnageTypeID(String tonnageTypeID) {
        TonnageTypeID = tonnageTypeID;
    }

    public String getTonnageType() {
        return TonnageType;
    }

    public void setTonnageType(String tonnageType) {
        TonnageType = tonnageType;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String Description = "";
}
