package com.nfinitydynamics.model;

import java.io.Serializable;

/**
 * Created by empiere-vaibhav on 10/30/2018.
 */

public class Institute implements Serializable{
    public String InstituteID = "";
    public String InstituteName = "";
    public String ContactPerson = "";
    public String EmailID = "";
    public String CCode = "";
    public String Mobile = "";
    public String Website = "";
    public String TelCountryCode = "";

    public String getInstituteID() {
        return InstituteID;
    }

    public void setInstituteID(String instituteID) {
        InstituteID = instituteID;
    }

    public String getInstituteName() {
        return InstituteName;
    }

    public void setInstituteName(String instituteName) {
        InstituteName = instituteName;
    }

    public String getContactPerson() {
        return ContactPerson;
    }

    public void setContactPerson(String contactPerson) {
        ContactPerson = contactPerson;
    }

    public String getEmailID() {
        return EmailID;
    }

    public void setEmailID(String emailID) {
        EmailID = emailID;
    }

    public String getCCode() {
        return CCode;
    }

    public void setCCode(String CCode) {
        this.CCode = CCode;
    }

    public String getMobile() {
        return Mobile;
    }

    public void setMobile(String mobile) {
        Mobile = mobile;
    }

    public String getWebsite() {
        return Website;
    }

    public void setWebsite(String website) {
        Website = website;
    }

    public String getTelCountryCode() {
        return TelCountryCode;
    }

    public void setTelCountryCode(String telCountryCode) {
        TelCountryCode = telCountryCode;
    }

    public String getSTDCode() {
        return STDCode;
    }

    public void setSTDCode(String STDCode) {
        this.STDCode = STDCode;
    }

    public String getTelephone() {
        return Telephone;
    }

    public void setTelephone(String telephone) {
        Telephone = telephone;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getCityName() {
        return CityName;
    }

    public void setCityName(String cityName) {
        CityName = cityName;
    }

    public String getZipcode() {
        return Zipcode;
    }

    public void setZipcode(String zipcode) {
        Zipcode = zipcode;
    }

    public String getCountryName() {
        return CountryName;
    }

    public void setCountryName(String countryName) {
        CountryName = countryName;
    }

    public String getLogoURL() {
        return LogoURL;
    }

    public void setLogoURL(String logoURL) {
        LogoURL = logoURL;
    }

    public String getLogoImgPath() {
        return LogoImgPath;
    }

    public void setLogoImgPath(String logoImgPath) {
        LogoImgPath = logoImgPath;
    }

    public String STDCode = "";
    public String Telephone = "";
    public String Address = "";
    public String CityName = "";
    public String Zipcode = "";
    public String CountryName = "";
    public String LogoURL = "";
    public String LogoImgPath = "";
}
