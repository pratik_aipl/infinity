package com.nfinitydynamics.model;

/**
 * Created by User on 01-02-2018.
 */

public class LatestJobModel {

   public String JobID="",CompanyName="",LogoURL="test",ShipType="",Name="",JobTitle="",JobEndDate="",
           Modified="",IsApplied="",Description="",CompanyID="",Rank="",Experience="",RankID=""
           ,IsFeatured="",IsShore="",IsFront="",FromExperience="",ToExperience="";
    public String getRankID() {
        return RankID;
    }

    public String getIsFeatured() {
        return IsFeatured;
    }

    public void setIsFeatured(String isFeatured) {
        IsFeatured = isFeatured;
    }

    public String getIsShore() {
        return IsShore;
    }

    public void setIsShore(String isShore) {
        IsShore = isShore;
    }

    public String getIsFront() {
        return IsFront;
    }

    public void setIsFront(String isFront) {
        IsFront = isFront;
    }

    public void setRankID(String rankID) {
        RankID = rankID;
    }

    public  int Is_Applied_Job;
    public String getRank() {
        return Rank;
    }

    public String getExperience() {
        return Experience;
    }

    public void setExperience(String experience) {
        Experience = experience;
    }

    public void setRank(String rank) {
        Rank = rank;
    }

    public String getLogoURL() {
        return LogoURL;
    }

    public int getIs_Applied_Job() {
        return Is_Applied_Job;
    }

    public void setIs_Applied_Job(int is_Applied_Job) {
        Is_Applied_Job = is_Applied_Job;
    }

    public void setLogoURL(String logoURL) {
        LogoURL = logoURL;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getCompanyID() {
        return CompanyID;
    }

    public void setCompanyID(String companyID) {
        CompanyID = companyID;
    }

    public String getJobID() {
        return JobID;
    }

    public void setJobID(String jobID) {
        JobID = jobID;
    }

    public String getCompanyName() {
        return CompanyName;
    }

    public void setCompanyName(String companyName) {
        CompanyName = companyName;
    }

    public String getLogoImgPath() {
        return LogoURL;
    }

    public void setLogoImgPath(String logoImgPath) {
        LogoURL = logoImgPath;
    }

    public String getShipType() {
        return ShipType;
    }

    public void setShipType(String shipType) {
        ShipType = shipType;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getJobTitle() {
        return JobTitle;
    }

    public void setJobTitle(String jobTitle) {
        JobTitle = jobTitle;
    }

    public String getJobEndDate() {
        return JobEndDate;
    }

    public void setJobEndDate(String jobEndDate) {
        JobEndDate = jobEndDate;
    }

    public String getModified() {
        return Modified;
    }

    public void setModified(String modified) {
        Modified = modified;
    }

    public String getIsApplied() {
        return IsApplied;
    }

    public void setIsApplied(String isApplied) {
        IsApplied = isApplied;
    }

    public String getFromExperience() {
        return FromExperience;
    }

    public void setFromExperience(String fromExperience) {
        FromExperience = fromExperience;
    }

    public String getToExperience() {
        return ToExperience;
    }

    public void setToExperience(String toExperience) {
        ToExperience = toExperience;
    }
}
