package com.nfinitydynamics.model;

import java.io.Serializable;

/**
 * Created by User on 20-02-2018.
 */

public class ExperienceModel implements Serializable{
    public  String ExperienceID="",CandidateID="",CompanyName="",RankID="",Rank="",ShipID="",ShipType="",JoiningDate="",
            LeavingDate="",EngineType="",ShipName="",TotalSeaDuration="",GRT="",EngineName="";
    public String TonnageType="";
    public String TonnageValue="";

    public String getTonnageType() {
        return TonnageType;
    }

    public void setTonnageType(String tonnageType) {
        TonnageType = tonnageType;
    }

    public String getTonnageValue() {
        return TonnageValue;
    }

    public void setTonnageValue(String tonnageValue) {
        TonnageValue = tonnageValue;
    }

    public String getEngineName() {
        return EngineName;
    }

    public void setEngineName(String engineName) {
        EngineName = engineName;
    }

    public String getEngineType() {
        return EngineType;
    }

    public String getGRT() {
        return GRT;
    }

    public void setGRT(String GRT) {
        this.GRT = GRT;
    }

    public void setEngineType(String engineType) {
        EngineType = engineType;
    }

    public String getShipName() {
        return ShipName;
    }

    public void setShipName(String shipName) {
        ShipName = shipName;
    }

    public String getTotalSeaDuration() {
        return TotalSeaDuration;
    }

    public void setTotalSeaDuration(String totalSeaDuration) {
        TotalSeaDuration = totalSeaDuration;
    }

    public String getExperienceID() {
        return ExperienceID;
    }

    public void setExperienceID(String experienceID) {
        ExperienceID = experienceID;
    }

    public String getCandidateID() {
        return CandidateID;
    }

    public void setCandidateID(String candidateID) {
        CandidateID = candidateID;
    }

    public String getCompanyName() {
        return CompanyName;
    }

    public void setCompanyName(String companyName) {
        CompanyName = companyName;
    }

    public String getRankID() {
        return RankID;
    }

    public void setRankID(String rankID) {
        RankID = rankID;
    }

    public String getRank() {
        return Rank;
    }

    public void setRank(String rank) {
        Rank = rank;
    }

    public String getShipID() {
        return ShipID;
    }

    public void setShipID(String shipID) {
        ShipID = shipID;
    }

    public String getShipType() {
        return ShipType;
    }

    public void setShipType(String shipType) {
        ShipType = shipType;
    }

    public String getJoiningDate() {
        return JoiningDate;
    }

    public void setJoiningDate(String joiningDate) {
        JoiningDate = joiningDate;
    }

    public String getLeavingDate() {
        return LeavingDate;
    }

    public void setLeavingDate(String leavingDate) {
        LeavingDate = leavingDate;
    }


}
