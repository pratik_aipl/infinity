package com.nfinitydynamics.model;

/**
 * Created by User on 07-02-2018.
 */

public class NewsModel {

    public  String id="",image="",newTitle="",newDesc="",newsDate="";

    public NewsModel(String id, String image, String newTitle, String newDesc, String newsDate) {
        this.id = id;
        this.image = image;
        this.newTitle = newTitle;
        this.newDesc = newDesc;
        this.newsDate = newsDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getNewTitle() {
        return newTitle;
    }

    public void setNewTitle(String newTitle) {
        this.newTitle = newTitle;
    }

    public String getNewDesc() {
        return newDesc;
    }

    public void setNewDesc(String newDesc) {
        this.newDesc = newDesc;
    }

    public String getNewsDate() {
        return newsDate;
    }

    public void setNewsDate(String newsDate) {
        this.newsDate = newsDate;
    }
}
