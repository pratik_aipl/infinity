package com.nfinitydynamics.model.company;

/**
 * Created by User on 27-02-2018.
 */

public class CompanyDataModel {

  public String CompanyID="",CompanyName="",EmailID="",Mobile="",Website="",Address="",LogoURL,Telephone="",TelCountryCode="",STDCode="",CCode="",
          RPSLNo="",ValidUntil="",Zipcode="",CountryName="",CityName="";

    public String getTelCountryCode() {
        return TelCountryCode;
    }

    public void setTelCountryCode(String telCountryCode) {
        TelCountryCode = telCountryCode;
    }

    public String getZipcode() {
        return Zipcode;
    }

    public void setZipcode(String zipcode) {
        Zipcode = zipcode;
    }

    public String getCountryName() {
        return CountryName;
    }

    public void setCountryName(String countryName) {
        CountryName = countryName;
    }

    public String getCityName() {
        return CityName;
    }

    public void setCityName(String cityName) {
        CityName = cityName;
    }

    public String getSTDCode() {
        return STDCode;
    }

    public void setSTDCode(String STDCode) {
        this.STDCode = STDCode;
    }

    public String getCCode() {
        return CCode;
    }

    public void setCCode(String CCode) {
        this.CCode = CCode;
    }

    public String getCompanyID() {
        return CompanyID;
    }

    public String getTelephone() {
        return Telephone;
    }

    public void setTelephone(String telephone) {
        Telephone = telephone;
    }

    public void setCompanyID(String companyID) {
        CompanyID = companyID;
    }

    public String getCompanyName() {
        return CompanyName;
    }

    public void setCompanyName(String companyName) {
        CompanyName = companyName;
    }

    public String getEmailID() {
        return EmailID;
    }

    public void setEmailID(String emailID) {
        EmailID = emailID;
    }

    public String getMobile() {
        return Mobile;
    }

    public void setMobile(String mobile) {
        Mobile = mobile;
    }

    public String getWebsite() {
        return Website;
    }

    public void setWebsite(String website) {
        Website = website;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getLogoURL() {
        return LogoURL;
    }

    public void setLogoURL(String logoURL) {
        LogoURL = logoURL;
    }

    public String getRPSLNo() {
        return RPSLNo;
    }

    public void setRPSLNo(String RPSLNo) {
        this.RPSLNo = RPSLNo;
    }

    public String getValidUntil() {
        return ValidUntil;
    }

    public void setValidUntil(String validUntil) {
        ValidUntil = validUntil;
    }
}
