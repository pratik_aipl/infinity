package com.nfinitydynamics.model;

/**
 * Created by User on 10-02-2018.
 */

public class UserDetailModel {


    public String CandidateID="",FirstName="",MiddleName="",LastName="",EmailID="",EmailID2="",DOB="",MaritalStatus="",NoofChildren=""
            ,Address="",CityName="",Zipcode="",Nationality="",TelephoneNo="",CCode="",MobileNo="",OtherMobileNo="",
            Indos="",RankID="",IsActive="",Created="",Modified="",RankName="",ImageUrl="",LogoURL="",resume_url="",CompanyName="";
    public int total_candidate,total_job,total_job_application,total_latest_candidate,total_shorejob,latest_available;

    public int getLatest_available() {
        return latest_available;
    }

    public void setLatest_available(int latest_available) {
        this.latest_available = latest_available;
    }

    public int getTotal_shorejob() {

        return total_shorejob;
    }

    public void setTotal_shorejob(int total_shorejob) {
        this.total_shorejob = total_shorejob;
    }

    public int getTotal_latest_candidate() {
        return total_latest_candidate;
    }



    public void setTotal_latest_candidate(int total_latest_candidate) {
        this.total_latest_candidate = total_latest_candidate;
    }

    public String getCompanyName() {
        return CompanyName;
    }

    public void setCompanyName(String companyName) {
        CompanyName = companyName;
    }

    public String getResume_url() {
        return resume_url;
    }

    public void setResume_url(String resume_url) {
        this.resume_url = resume_url;
    }

    public String getCandidateID() {
        return CandidateID;
    }

    public void setCandidateID(String candidateID) {
        CandidateID = candidateID;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getMiddleName() {
        return MiddleName;
    }

    public void setMiddleName(String middleName) {
        MiddleName = middleName;
    }

    public String getLogoURL() {
        return LogoURL;
    }

    public void setLogoURL(String logoURL) {
        LogoURL = logoURL;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getEmailID() {
        return EmailID;
    }

    public void setEmailID(String emailID) {
        EmailID = emailID;
    }

    public String getEmailID2() {
        return EmailID2;
    }

    public void setEmailID2(String emailID2) {
        EmailID2 = emailID2;
    }

    public String getDOB() {
        return DOB;
    }

    public void setDOB(String DOB) {
        this.DOB = DOB;
    }

    public String getMaritalStatus() {
        return MaritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        MaritalStatus = maritalStatus;
    }

    public String getNoofChildren() {
        return NoofChildren;
    }

    public void setNoofChildren(String noofChildren) {
        NoofChildren = noofChildren;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getCityName() {
        return CityName;
    }

    public void setCityName(String cityName) {
        CityName = cityName;
    }

    public String getZipcode() {
        return Zipcode;
    }

    public void setZipcode(String zipcode) {
        Zipcode = zipcode;
    }

    public String getNationality() {
        return Nationality;
    }

    public void setNationality(String nationality) {
        Nationality = nationality;
    }

    public String getTelephoneNo() {
        return TelephoneNo;
    }

    public void setTelephoneNo(String telephoneNo) {
        TelephoneNo = telephoneNo;
    }

    public String getCCode() {
        return CCode;
    }

    public void setCCode(String CCode) {
        this.CCode = CCode;
    }

    public String getMobileNo() {
        return MobileNo;
    }

    public void setMobileNo(String mobileNo) {
        MobileNo = mobileNo;
    }

    public String getOtherMobileNo() {
        return OtherMobileNo;
    }

    public void setOtherMobileNo(String otherMobileNo) {
        OtherMobileNo = otherMobileNo;
    }

    public String getIndos() {
        return Indos;
    }

    public void setIndos(String indos) {
        Indos = indos;
    }

    public String getRankID() {
        return RankID;
    }

    public void setRankID(String rankID) {
        RankID = rankID;
    }

    public String getIsActive() {
        return IsActive;
    }

    public void setIsActive(String isActive) {
        IsActive = isActive;
    }

    public String getCreated() {
        return Created;
    }

    public void setCreated(String created) {
        Created = created;
    }

    public String getModified() {
        return Modified;
    }

    public void setModified(String modified) {
        Modified = modified;
    }

    public String getRankName() {
        return RankName;
    }

    public void setRankName(String rankName) {
        RankName = rankName;
    }

    public String getImageUrl() {
        return ImageUrl;
    }

    public void setImageUrl(String imageUrl) {
        ImageUrl = imageUrl;
    }

    public int getTotal_candidate() {
        return total_candidate;
    }

    public void setTotal_candidate(int total_candidate) {
        this.total_candidate = total_candidate;
    }

    public int getTotal_job() {
        return total_job;
    }

    public void setTotal_job(int total_job) {
        this.total_job = total_job;
    }

    public int getTotal_job_application() {
        return total_job_application;
    }

    public void setTotal_job_application(int total_job_application) {
        this.total_job_application = total_job_application;
    }
}
