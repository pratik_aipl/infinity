package com.nfinitydynamics.model;

/**
 * Created by User on 21-06-2018.
 */

public class DownloadedCandidateModel {

  public String CandidateCode,CandidateID,CanidateName,Indos,DownloadedDate,EmailID,last_login,Modified,ImageURL,
          ResumeLink,RankName,MaritalStatus,MobileNo,ResumeFile,NationalName,AppliedShipName,AppliedRankName;

    public String getAppliedShipName() {
        return AppliedShipName;
    }

    public void setAppliedShipName(String appliedShipName) {
        AppliedShipName = appliedShipName;
    }

    public String getMaritalStatus() {
        return MaritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        MaritalStatus = maritalStatus;
    }

    public String getMobileNo() {
        return MobileNo;
    }

    public void setMobileNo(String mobileNo) {
        MobileNo = mobileNo;
    }

    public String getResumeFile() {
        return ResumeFile;
    }

    public void setResumeFile(String resumeFile) {
        ResumeFile = resumeFile;
    }

    public String getNationalName() {
        return NationalName;
    }

    public void setNationalName(String nationalName) {
        NationalName = nationalName;
    }

    public String getCandidateCode() {
        return CandidateCode;
    }

    public void setCandidateCode(String candidateCode) {
        CandidateCode = candidateCode;
    }

    public String getCandidateID() {
        return CandidateID;
    }

    public void setCandidateID(String candidateID) {
        CandidateID = candidateID;
    }

    public String getCanidateName() {
        return CanidateName;
    }

    public void setCanidateName(String canidateName) {
        CanidateName = canidateName;
    }

    public String getIndos() {
        return Indos;
    }

    public String getRankName() {
        return RankName;
    }

    public void setRankName(String rankName) {
        RankName = rankName;
    }

    public void setIndos(String indos) {
        Indos = indos;
    }

    public String getDownloadedDate() {
        return DownloadedDate;
    }

    public void setDownloadedDate(String downloadedDate) {
        DownloadedDate = downloadedDate;
    }

    public String getEmailID() {
        return EmailID;
    }

    public void setEmailID(String emailID) {
        EmailID = emailID;
    }

    public String getLast_login() {
        return last_login;
    }

    public void setLast_login(String last_login) {
        this.last_login = last_login;
    }

    public String getModified() {
        return Modified;
    }

    public void setModified(String modified) {
        Modified = modified;
    }

    public String getImageURL() {
        return ImageURL;
    }

    public void setImageURL(String imageURL) {
        ImageURL = imageURL;
    }

    public String getResumeLink() {
        return ResumeLink;
    }

    public String getAppliedRankName() {
        return AppliedRankName;
    }

    public void setAppliedRankName(String appliedRankName) {
        AppliedRankName = appliedRankName;
    }

    public void setResumeLink(String resumeLink) {
        ResumeLink = resumeLink;
    }
}
