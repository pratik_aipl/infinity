package com.nfinitydynamics.model;

public class CountryResidenceModel {

    public String CountryID,CountryTelCode,ShortName,CountryName,Created,Modified;

    public String getCountryID() {
        return CountryID;
    }

    public void setCountryID(String countryID) {
        CountryID = countryID;
    }

    public String getCountryTelCode() {
        return CountryTelCode;
    }

    public void setCountryTelCode(String countryTelCode) {
        CountryTelCode = countryTelCode;
    }

    public String getShortName() {
        return ShortName;
    }

    public void setShortName(String shortName) {
        ShortName = shortName;
    }

    public String getCountryName() {
        return CountryName;
    }

    public void setCountryName(String countryName) {
        CountryName = countryName;
    }

    public String getCreated() {
        return Created;
    }

    public void setCreated(String created) {
        Created = created;
    }

    public String getModified() {
        return Modified;
    }

    public void setModified(String modified) {
        Modified = modified;
    }
}
