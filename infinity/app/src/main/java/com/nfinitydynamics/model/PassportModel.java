package com.nfinitydynamics.model;

/**
 * Created by User on 13-02-2018.
 */

public class PassportModel {

    public  String PassportDetID="",CandidateID="",PassportNo="",PlaceofIssue="",ExpiryDate="",IsActive="",Created="",Modified="";

    public String getPassportDetID() {
        return PassportDetID;
    }

    public void setPassportDetID(String passportDetID) {
        PassportDetID = passportDetID;
    }

    public String getCandidateID() {
        return CandidateID;
    }

    public void setCandidateID(String candidateID) {
        CandidateID = candidateID;
    }

    public String getPassportNo() {
        return PassportNo;
    }

    public void setPassportNo(String passportNo) {
        PassportNo = passportNo;
    }

    public String getPlaceofIssue() {
        return PlaceofIssue;
    }

    public void setPlaceofIssue(String placeofIssue) {
        PlaceofIssue = placeofIssue;
    }

    public String getExpiryDate() {
        return ExpiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        ExpiryDate = expiryDate;
    }

    public String getIsActive() {
        return IsActive;
    }

    public void setIsActive(String isActive) {
        IsActive = isActive;
    }

    public String getCreated() {
        return Created;
    }

    public void setCreated(String created) {
        Created = created;
    }

    public String getModified() {
        return Modified;
    }

    public void setModified(String modified) {
        Modified = modified;
    }
}
