package com.nfinitydynamics.model;

/**
 * Created by User on 07-02-2018.
 */

public class RankModel {

    public   String rankId="",rankName="";

    public RankModel() {
    }

    public RankModel(String rankName) {
        this.rankName = rankName;
    }

    public RankModel(String rankId, String rankName) {
        this.rankId = rankId;
        this.rankName = rankName;
    }

    public String getRankId() {
        return rankId;
    }

    public void setRankId(String rankId) {
        this.rankId = rankId;
    }

    public String getRankName() {
        return rankName;
    }

    public void setRankName(String rankName) {
        this.rankName = rankName;
    }
}
